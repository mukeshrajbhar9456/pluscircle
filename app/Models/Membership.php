<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    use HasFactory;
    protected $table = "membership";

    protected $fillable = [
        'id','userid','year','type','alphabet','countrycode','serialnumber','cardnumber','active','start_date','created_at','updated_at'
    ]; 
}

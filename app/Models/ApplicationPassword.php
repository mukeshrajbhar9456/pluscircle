<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationPassword extends Model
{
    use HasFactory;
    protected $fillable = [
        //'name', 'email', 'password',
        'id','application','password','active','expired_at'
    ]; 
}

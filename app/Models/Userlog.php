<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Userlog extends Model
{
    use HasFactory;

    protected $fillable = [
        //'name', 'email', 'password',
        'id','userid','usertype','ipaddress','session_id','login_at','last_active','logout_at','status','created_at','updated_at'
    ]; 
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $fillable = [
        //'name', 'email', 'password',
        'id','userid','name','address','country','state','city','pincode','poc','phone','email','active'
    ]; 
}

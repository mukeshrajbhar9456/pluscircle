<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $fillable = [
        'id','userid','eventtype','eventtitle','minparticipant','maxparticipant','eventothers','reservedticket','ticketnumberfrom','ticketnumberto','starttime','endtime','startdate','enddate','country','state','city','pincode','address','ticketprice','agemin','agemax','description','eventfacility','donatepcc','organisername','organiseremail','organiserphone','organisersphone','internalnote','event_profile'
    ];
}

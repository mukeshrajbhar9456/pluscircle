<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rewards extends Model
{
    use HasFactory;
        
    protected $fillable = [
        'userid','referrer_id','title','transaction_date','credit_points','used_points'
        
    ];
    
}

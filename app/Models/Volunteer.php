<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'membershipno',
        'mobile',
        'email',
        'volunteer_country',
        'volunteer_state',
        'volunteer_city',
        'fromdate',
        'todate',
        'fromtime',
        'totime',
        'message',
        'created_at',
        'updated_at'
        ];
}

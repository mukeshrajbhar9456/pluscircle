<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'title',
        'email',
        'password',
        'phonecode',
        'phone',
        'secondaryphone',
        'role',
        'dob',
        'profession',
        'gender',
        'profile',
        'kyctype',
        'kycprofile',
        'address',
        'ccode',
        'ccountry',
        'cstate',
        'ccity',
        'pincode',
        'isactive',
        'email_verified',
        'phone_verified',
        'membershiptype',
        'paymenttype',
        'tmembership',
        'pmembership',
        'kitdeliveried',
        'refferedby'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    protected $fillable = [
        'datetime','userid','usertype','payment_id','razorpay_payment_id','description','name','email','contact','entity','amount','currency','status','order_id','invoice_id','international','method','amount_refunded','refund_status','captured','card_id','bank','wallet','vpa','authcode','paydatetime' 
    ];   
}
 

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Membershipkit extends Model
{
    use HasFactory;

    protected $fillable = [
        'id','userid','addresstype','tshirtsize','price','payment_reference','payment_status'
    ]; 
}

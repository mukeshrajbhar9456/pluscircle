<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;use DB;use Auth;use Session;use Mail;use Crypt;
use App\Models\User;
use App\Models\Userlog;
use App\Models\Rewards;
use App\Models\Address;
use App\Models\Membershipkit;
use App\Models\Membership;
use App\Models\Payment;
use App\Models\Tickets;
use App\Models\Event;
use Illuminate\Support\Facades\Hash;
// require_once $_SERVER["DOCUMENT_ROOT"]."/vendor/autoload.php";
include(app_path().'/razorpay_php_250/Razorpay.php');
use Razorpay\Api\Api;

use Redirect;
use App\Mail\WelcomeMail;
use App\Mail\PaymentConfirmation1500;
use App\Mail\PaymentConfirmation785;


class PaymentController extends Controller
{
    
    public function create(){        
        //return view('payment.payWithRazorpay');
        return view('payment.razorpay');
    }

    public function viewpayment(Request $request){
        return view('payment.payWithRazorpay');
    }
    public function payment(Request $request){
        $input = $request->all();
        $api = new Api('rzp_live_MlsGLJ4URVlmoo','1lKb4LMuylKUXanejUSsU9in');

        $payment = $api->payment->fetch($input['razorpay_payment_id']);
        if(count($input)  && !empty($input['razorpay_payment_id'])) {
            $msk = new Membershipkit();
            $msk->userid = Auth::user()->id;
            $msk->addresstype = $request->address;
            $msk->tshirtsize = $request->t_shirtsize;
            $msk->price = $request->amount;
            $msk->payment_id=$request->razorpay_payment_id;
            $msk->save();

            
            
            
            if(Auth::user()->membership == ''){
                $phonecode=\DB::table('countries')->where('id',Auth::user()->ccountry)->first();
                $pcode=$phonecode->phonecode;           
                $year=date('y');
               
                $lmship = Membership::where('year',$year)->where('countrycode',$pcode)->where('type','T')->latest('id')->first();
                if($lmship){
                    $serial=((int)$lmship->serialnumber)+1;
                    $alpha=$lmship->alphabet;
                    if($serial>9999){
                        $alpha=$alpha++;
                        $alpha=strtoupper($alpha);
                        if($alpha=='A' || $alpha=='C' || $alpha=='I' || $alpha=='V'){
                            $alpha=$alpha++;  
                        }
                        $serial=str_pad(1, 4, '0', STR_PAD_LEFT);                   
                    } 
                    else{
                        $serial=str_pad($serial, 4, '0', STR_PAD_LEFT);
                    }
                    $mcard=$year.'-'.'T'.'-'.$alpha.'-'.$pcode.'-'.$serial;     
                    $memcard=new Membership();
                    $memcard->userid = Auth::user()->id;
                    $memcard->year = $year;
                    $memcard->type = 'T';
                    $memcard->alphabet = $alpha;
                    $memcard->countrycode = $pcode;
                    $memcard->serialnumber = $serial;
                    $memcard->cardnumber = $mcard;
                    $memcard->active = 1;
                    $memcard->start_date = date('Y-m-d H:i:s');
                    $memcard->created_at = date('Y-m-d H:i:s');
                    $memcard->save();
                    
                    $user = Auth::user();
                    $user->membershiptype='T';
                    $user->tmembership=$mcard;
                    $user->paymenttype=$input['razorpay_payment_id'];
                    $user->save();
                   
                    $amount=$payment->amount;
                    
                    $data['mcard']=$mcard;
                    $data['name']=Auth::user()->name;
                    $data['email']=Auth::user()->email;
                    
                    if($amount==1){
                        $mail=Mail::to(Auth::user()->email)->send(new PaymentConfirmation785($data));    
                    }
                    if($amount==2){
                        $mail=Mail::to(Auth::user()->email)->send(new PaymentConfirmation1500($data));  
                    }
                }
                else{
                    $serial=str_pad(1, 4, '0', STR_PAD_LEFT);
                    $alpha='B';  
                    $mcard=$year.'-'.'T'.'-'.$alpha.'-'.$pcode.'-'.$serial;
                    
                    $memcard=new Membership();
                    $memcard->userid = Auth::user()->id;
                    $memcard->year = $year;
                    $memcard->type = 'T';
                    $memcard->alphabet = $alpha;
                    $memcard->countrycode = $pcode;
                    $memcard->serialnumber = $serial;
                    $memcard->cardnumber = $mcard;
                    $memcard->active = 1;
                    $memcard->start_date = date('Y-m-d H:i:s');
                    $memcard->created_at = date('Y-m-d H:i:s');
                    $memcard->save();
                    
                    $user = Auth::user();
                    $user->membershiptype='T';
                    $user->tmembership=$mcard;
                    $user->paymenttype=$input['razorpay_payment_id'];
                    $user->save();
                    
                    $amount=$payment->amount;
                    
                    $data['mcard']=$mcard;
                    $data['name']=Auth::user()->name;
                    $data['email']=Auth::user()->email;
                    
                    if($amount==1){
                        $mail=Mail::to(Auth::user()->email)->send(new PaymentConfirmation785($data));    
                    }
                    if($amount==2){
                        $mail=Mail::to(Auth::user()->email)->send(new PaymentConfirmation1500($data));  
                    }
    
                } 
            }

            /** refernce rewards update */
            if(Auth::user()->refferedby !=''){
               $ref=explode('-',Auth::user()->refferedby);
               if($ref[1]=='T' || $ref[1]=='t' ){
                    $referance=User::where('tmembership',Auth::user()->refferedby)->first();
                }
               else if($ref[1]=='P' || $ref[1]=='p' ){
                    $referance=User::where('tmembership',Auth::user()->refferedby)->first();
               }
               $r = new Rewards();
               $r->userid = $referance->id;
               $r->referrer_id = Auth::user()->id;
               $r->title = 'reference';
               $r->transaction_date = date('Y-m-d H:i:s');
               $r->credit_points = 100;
               $r->used_points = 0;
               $r->save();  

            }
            

            $pay = new Payment();
            $pay->datetime=date('Y-m-d H:i:s');
            $pay->userid = Auth::user()->id;
            $pay->usertype = Auth::user()->role;
            $pay->payment_id=$payment->id;
            $pay->razorpay_payment_id=$input['razorpay_payment_id'];
            $pay->description=$payment->description;
            $pay->name = Auth::user()->name;
            $pay->email =$payment->email; 
            $pay->contact =$payment->contact;
            $pay->entity=$payment->entity;
            $pay->amount=$payment->amount;
            $pay->currency=$payment->currency;
            $pay->status=$payment->status;
            $pay->order_id=$payment->order_id;
            $pay->invoice_id=$payment->invoice_id;
            $pay->international=$payment->international;
            $pay->method=$payment->method;
            $pay->amount_refunded=$payment->amount_refunded;
            $pay->refund_status=$payment->refund_status;
            $pay->captured=$payment->captured;
            $pay->card_id=$payment->card_id;
            $pay->bank=$payment->bank;
            $pay->wallet=$payment->wallet;
            $pay->vpa=$payment->vpa;
            // $pay->authcode=$response->auth_code;
            // $pay->paydatetime=$response->created_at;
            $pay->save();
          //  print_r($payment->amount);


            
            try {
                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount'])); 
            } catch (\Exception $e) {
                // return  'error'.$e->getMessage();
                return "error";
                // \Session::put('error',$e->getMessage());
                // return redirect()->back();
            }
        }
        
        return 'success';
        
    }

    public function event_payment(Request $request){
        $input = $request->all();
        $api = new Api('rzp_live_MlsGLJ4URVlmoo','1lKb4LMuylKUXanejUSsU9in');

        $payment = $api->payment->fetch($input['razorpay_payment_id']);

        if(count($input)  && !empty($input['razorpay_payment_id'])) {
        
            $event=Event::where('id',$request->eventid)->first();

            $ticket = new Tickets();
            $ticket->userid = Auth::user()->id;
            $ticket->eventid = $request->eventid;
            $ticket->event_name = $event->eventtitle."(".$event->eventtype.")";
            $ticket->ticketprice = $event->ticketprice;
            $ticket->nooftickets = $request->nooftickets;
            $ticket->price = $request->total_price;
            $ticket->payment_ref=$input['razorpay_payment_id'];
            $ticket->created_at = $request->amount;
            $ticket->updated_at = $request->amount;
            $ticket->save();

            $pay = new Payment();
            $pay->datetime=date('Y-m-d H:i:s');
            $pay->userid = Auth::user()->id;
            $pay->usertype = Auth::user()->role;
            $pay->payment_id=$input['razorpay_payment_id'];
            $pay->razorpay_payment_id=$input['razorpay_payment_id'];
            $pay->description=$payment->description;
            $pay->name = Auth::user()->name;
            $pay->email =$payment->email; 
            $pay->contact =$payment->contact;
            $pay->entity=$payment->entity;
            $pay->amount=$payment->amount;
            $pay->currency=$payment->currency;
            $pay->status=$payment->status;
            $pay->order_id=$payment->order_id;
            $pay->invoice_id=$payment->invoice_id;
            $pay->international=$payment->international;
            $pay->method=$payment->method;
            $pay->amount_refunded=$payment->amount_refunded;
            $pay->refund_status=$payment->refund_status;
            $pay->captured=$payment->captured;
            $pay->card_id=$payment->card_id;
            $pay->bank=$payment->bank;
            $pay->wallet=$payment->wallet;
            $pay->vpa=$payment->vpa;
            // $pay->authcode=$response->auth_code;
            // $pay->paydatetime=$response->created_at;
            $pay->save();
          //  print_r($payment->amount);
        }  
            
            
        
        return 'success';
        
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;use DB;use Auth;use Session;
use App\Models\User;
use App\Models\Event;
use App\Models\Userlog;
use App\Models\Address;
use App\Models\Membershipkit;

use App\Models\Memberreference;
use App\Models\Volunteer;
use Illuminate\Support\Facades\Hash;

class GuestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function dashboard()
    {
        $memberkit=Membershipkit::where('userid',Auth::user()->id)->count();
        
        $countries = \DB::table('countries')->get();
        $countrycode = \DB::table('countries')->where('phonecode','>',0)->groupBy('phonecode')->orderby('phonecode')->get();
        $states = \DB::table('states')->where('country_id',Auth::user()->ccountry)->get();
      
        return view('user.dashboard',['country' => $countries,'memberkit' =>$memberkit,'countrycode' => $countrycode,'states' => $states]);
    }
    public function getstate(Request $request){
        $states = \DB::table('states')
        ->where('country_id',$request->country_id)
        ->where('active',1)
        ->get();
        $pkt="<option value='' class='' ></option>";
		
        foreach($states as $s){
            if($s->id == Auth::user()->cstate){
                $pkt.="<option value=".$s->id." class='".$s->country_id."' selected >".$s->name."</option>";
            }
            else{
                $pkt.="<option value=".$s->id." class='".$s->country_id."' >".$s->name."</option>";
            }		
        }
        return $pkt;
        
    }
    public function memberkit(Request $request){
        // print_r($_POST);
        // 'id','userid','addresstype','tshirtsize','price'
        // [membership] => 750 [t_shirtsize] => S [address] => default_address )
        $msk = new Membershipkit();
        $msk->userid = Auth::user()->id;
        $msk->addresstype = $request->address;
        $msk->tshirtsize = $request->t_shirtsize;
        $msk->price = $request->membership;
        $msk->save();
        if($request->membership==15000){
           $phonecode=\DB::table('countries')->where('id',Auth::user()->ccountry)->first();
           $year=date('y');
           $lmship = Membership::where('type','T')->last();
           if($lmship){
                $serial=((int)$lmship->serialnumber)+1;
                $alpha=$lmship->alphabet;
                if($serial>9999){
                    $alpha=$alpha++;
                    $alpha=strtoupper($alpha);
                    if($alpha=='A' || $alpha=='C' || $alpha=='I' || $alpha=='V'){
                        $alpha=$alpha++;  
                    }
                    $serial=str_pad(1, 4, '0', STR_PAD_LEFT);                   
                } 
                else{
                    $serial=str_pad($serial, 4, '0', STR_PAD_LEFT);
                }
                
           }
           else{
                $serial=str_pad(1, 4, '0', STR_PAD_LEFT);
                $alpha='B';  
           } 
        //    'id','userid','year','type','alphabet','countrycode','serialnumber','cardnumber','active','start_date'
    
           if(Auth::user()->role=='user'){
                $cardno= $year.'TA'.$phonecode->phonecode.'0001';
                $mship = new Membership();
                $mship->userid = Auth::user()->id;
                $mship->role = Auth::user()->role;
                $mship->year =$year ;
                $mship->type ='T' ;
                $mship->alphabet = $alpha;
                $mship->countrycode = $phonecode->phonecode ;
                $mship->serialnumber = '0001';
                // $mship->cardnumber= ;

           }
           
        }
        else if($request->membership==750){
            echo "here is 750 kit";
        
        }

        return "success";
    }
    public function saveaddress(Request $request){
       
        DB::table('addresses')->where('userid', Auth::user()->id)->update(array('active'=>0));

        $add = new Address();
        $add->userid = Auth::user()->id;
        $add->name = $request->mname;
        $add->address = $request->maddress;
        $add->country = $request->mcountry;
        $add->state = $request->mstate;
        $add->city = $request->mcity;
        $add->pincode = $request->mpincode;
        $add->poc = $request->mpoc;
        $add->phone = $request->mphone;
        $add->email = $request->memail;
        $add->active = 1;
        $add->save();
        return "sender is saved success";

    }
    public function user_profile(){
        $countries = \DB::table('countries')->get();
        $countrycode = \DB::table('countries')->where('phonecode','>',0)->groupBy('phonecode')->orderby('phonecode')->get();
        $states = \DB::table('states')->where('country_id',Auth::user()->ccountry)->get();
        return view('user.user_profile',['country' => $countries,'countrycode' => $countrycode,'states' => $states]);
    }
    public function update_user_profile(Request $request){
        $user = Auth::user();
        $user->name=$request->name;
        $user->dob=$request->bday;
        $user->secondaryphone=$request->secondaryphone;
        $user->profession=$request->profession;
        $user->ccountry=$request->profile_country;
        $user->cstate=$request->profile_state;
        $user->ccity=$request->city;
        $user->pincode=$request->pincode;
        $user->address=$request->address;
        $user->kyctype=$request->kyctype;
        $user->save();
        
        if($request->userprofilekyc!=''){
            $tmp = str_replace('data:image/png;base64,', '', $_POST['userprofilekyc']);
            $tmp2 = str_replace(' ', '+', $tmp);                    
            $image = base64_decode($tmp2); // decode the image
            $date=date('YmdHis');
            $resultset=file_put_contents('public/images/profile/user/'.Auth::user()->id.'_kyc'.$date.'.png',$image);
            
            $user = Auth::user();
            $user->kycprofile='public/images/profile/user/'.Auth::user()->id.'_kyc'.$date.'.png';
            $user->save();
            // $profile=\DB::table('sccm_application')
            // ->where('id', $application)
            // ->update(['profile' => 'public/images/profile/user/'.$application.'_profile.png']);

        }
        if($request->userprofile!=''){

            if($request->hasFile('userprofile')) {
                $file = $request->file('userprofile');
                $date=date('YmdHis');
                $name = 'public/images/profile/user/'.Auth::user()->id.'_pro'.$date.'.'.$file->getClientOriginalExtension();

                $image['filePath'] = $name;
                $file->move('public/images/profile/user/', $name);
                $user = Auth::user();
                $user->profile=$name;
                 $user->save();

            }


        }
       
        return redirect()->route('user.user_profile')
        ->with('message','Your Profile has updated');

    }
    public function uploadprofile_ajax(Request $request){
        if($request->profile_image!=''){
            $tmp = str_replace('data:image/png;base64,', '', $_POST['profile_image']);
            $tmp2 = str_replace(' ', '+', $tmp);                    
            $image = base64_decode($tmp2); // decode the image
            $date=date('YmdHis');
            $resultset=file_put_contents('public/images/profile/user/'.Auth::user()->id.'_profile'.$date.'.png',$image);
            
            $user = Auth::user();
            $user->profile='public/images/profile/user/'.Auth::user()->id.'_profile'.$date.'.png';
            $user->save();
            // $profile=\DB::table('sccm_application')
            // ->where('id', $application)
            // ->update(['profile' => 'public/images/profile/user/'.$application.'_profile.png']);

        }
        return "success";
    }



    public function create_member_reference(Request $request){
        $rules = [
            'name1' => 'required',
            'mobile1' => 'required|digits:10|unique:memberreferences,mobile',
            'name2' => 'required',
            'mobile2' => 'required|digits:10|unique:memberreferences,mobile',
            'name3' => 'required',
            'mobile3' => 'required|digits:10|unique:memberreferences,mobile',
         ];
         $messsages = array(         
            'name1.required' => 'Please Enter Name',
            'mobile1.digits' => 'Enter 10 digit mobile number' ,
            'mobile1.unique' => 'Phone number exist' ,
            'name2.required' => 'Please Enter Name',
            'mobile2.digits' => 'Enter 10 digit mobile number' ,
            'mobile2.unique' => 'Phone number exist' ,
            'name3.required' => 'Please Enter Name',
            'mobile3.digits' => 'Enter 10 digit mobile number' ,
            'mobile3.unique' => 'Phone number exist' ,
            
        );
    
         
    for($i=4;$i<15;$i++){
        if ($request->has('name'.$i )) {
            $rules['name'.$i]='required';
            $rules['mobile'.$i]='required|digits:10|unique:memberreferences,mobile';

            $messsages['name'.$i.'.required']='Please Enter Name';
            $messsages['mobile'.$i.'.digits']='Enter 10 digit mobile number';
            $messsages['mobile'.$i.'.unique']='Phone number exist';
            
        }
    }
   
         $validator = Validator::make($request->all(), $rules, $messsages);
        if($validator->fails()){ 
            $messages = $validator->messages();
            return response()->json(['status'=>'error','data'=> $validator->errors()]);
        }
        else{       
                $lotno = Memberreference::max('lot');
                if($lotno){
                    $lotno+=1;
                }
                else{
                    $lotno=1; 
                }
                for($i=1;$i<15;$i++){
                    if ($request->has('name'.$i )) {
                        $mr = new Memberreference();
                        $mr->user_id = Auth::user()->id;
                        $mr->name =$_POST['name'.$i];
                        $mr->mobile = $_POST['mobile'.$i];
                        $mr->relation = $_POST['relation'.$i];
                        $mr->lot = $lotno;
                        $mr->save();  

                    }
                }
            return response()->json(['status'=>'success']);
        }  
    }
    
    public function upcoming_events(){
        $country = \DB::table('events')
        ->join('countries', 'countries.id', '=', 'events.country')
        ->select('countries.name as country','countries.id as id')
        ->groupBy('events.country')
        ->get();
        $state = \DB::table('events')
        ->join('states', 'states.id', '=', 'events.state')
        ->select('states.name as state','states.id as id')
        ->groupBy('events.state')
        ->get();
        $city = \DB::table('events')
        ->select('events.city')
        ->groupBy('events.city')
        ->get();

        $events = \DB::table('events')
        ->orderBy('events.id','DESC')
        ->get();
        return view('user.upcoming_events',['country' => $country,'state' => $state,'city' => $city,'events' => $events]);
    }
    public function event_search_ajax(Request $request){
        $country = $request->country;
        $state = $request->state;
        $city = $request->city;
        
        $events = \DB::table('events')
        ->where('country',$country)
        ->where('state',$state)
        ->where('city',$city)
        ->orderBy('events.id','DESC')
        ->get();
        // print_r($events);exit;
        $pkt="";
        foreach($events as $e){
            $pkt.="  <div class='owl-item' style='width: 270.333px; margin-right: 15px;'>
            <div class='item'>
                <div class='slider-img'>
                    <a onclick='eventModal(".$e->id.");'>";
                    if($e->event_profile!=''){
                        $pkt.="<img class='owl-lazy' data-src='".asset($e->event_profile)."' alt='' src='".asset($e->event_profile)."' style='opacity: 1;'>";
                    }
                    else{
                        $pkt.="<img class='owl-lazy' data-src='https://localhost/pluscircle/pcc/public/images//community-img.jpg' alt='' src='https://localhost/pluscircle/pcc/public/images//community-img.jpg' style='opacity: 1;'>";
                    }
                    $pkt.="</a>
                </div>
            <div class='upcoming-slider-content'>
            <div class='events-name'>
                <strong>". $e->eventtitle." (".$e->eventtype .")</strong>
            </div>
            <p>". $e->description."</p>
            <div class='row'>
                <div class='col-md-12'>
                    <div class='text-box1'>
                        <p> <strong> From </strong> ".$e->startdate ."&nbsp; ".$e->starttime ."</p>
                        <p> <strong> To </strong> ". $e->enddate." &nbsp; ".$e->endtime." </p>
                        <p> <strong> INR </strong>". $e->ticketprice."</p>
                    </div>                        
                </div>
                <div class='col-md-12'>
                    <div class='slider-btn'>
                        <button data-toggle='modal' data-target='#myModal2' onclick='buy_ticket(3);' class='btn volunteersubmit'>Book a Ticket</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>";
        }
return $pkt;

    }
    public function ajaxgetevent(Request $request){       
        $events = \DB::table('events')->where('id',$_POST['event_id'])->first();
        $if_tickets = \DB::table('tickets')->where('eventid',$_POST['event_id'])->count();
        if($if_tickets > 0){            
            $sold_tickets = \DB::table('tickets')
            ->where('eventid','=',$events->id)
            ->sum('nooftickets');
            // echo $events->reservedticket."-".$sold_tickets;exit;
            $events->max=$events->reservedticket-$sold_tickets;    
        }
        else{
            $sold_tickets =0;
            $events->max=$events->reservedticket;
        }
        return json_encode($events);

    }
    public function i_wish_to_host_an_event(){
        $countries = \DB::table('countries')->get();
        $countrycode = \DB::table('countries')->where('phonecode','>',0)->groupBy('phonecode')->orderby('phonecode')->get();
        $states = \DB::table('states')->where('country_id',Auth::user()->ccountry)->get();
      
        return view('user.wishtohostevent',['country' => $countries,'countrycode' => $countrycode,'states' => $states]);
    }
    public function save_event(Request $request){
        
        $rules = [
            'eventtitle' => 'required',
            'minparticipant' => 'required',
            'maxparticipant' => 'required',
            'reservedticket' => 'required',
            'starttime' => 'date_format:H:i',
            'endtime' => 'date_format:H:i',
            'startdate' => 'date_format:Y-m-d|after:today',
            'enddate' => 'date_format:Y-m-d|after_or_equal:startdate',
            'city' => 'required',
            'pincode' => 'required',
            'address' => 'required',
            'ticketprice' => 'required',
            'agemin' => 'required',
            'agemax' => 'required',
            'description' => 'required',
            'eventfacility' => 'required',
            'donatepcc' => 'required',
            'organisername' => 'required',
            'organiseremail' => 'required',
            'organiserphone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'organisersphone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'internalnote' => 'required',
            'termsnconditions' => 'accepted',
         ];

         $messsages = array(
            'eventtitle.required' => 'Please Enter Title',
            'minparticipant.required' => 'Enter Minimum Participant',
            'maxparticipant.required' => 'Enter Minimum Participant',
            'reservedticket.required' => 'Please Enter Reserved Tickets',
            'starttime.date_format' => 'Enter Time',
            'endtime.date_format' => 'Enter Time',
            'startdate.date_format' => 'Enter Start Date',
            'startdate.after' => 'Date should be after Today ',
            'enddate.date_format' => 'Enter End Date',
            'enddate.after_or_equal' => 'Date should be after START date ',
            'city.required' => 'Please Enter City',
            'pincode.required' => 'Please Enter Pincode',
            'address.required' => 'Please Enter Address',
            'ticketprice.required' => 'Please Enter Ticket Price',
            'agemin.required' => 'Please Enter Minimum Age',
            'agemin.digits' => 'Only Numbers allowed',
            'agemax.required' => 'Please Enter Minimum Age',
            'agemax.digits' => 'Only Numbers allowed',
            'description.required' => 'Please Enter Description',
            'eventfacility.required' => 'Please Enter Event Facility',
            'donatepcc.required' => 'Please Enter Amount',
            'organisername.required' => 'Please Enter Organiser Name',
            'organiseremail.required' => 'Please Enter Organiser E-mail',
            'organiserphone.required' => 'Please Enter Organiser Phone',
            'organiserphone.regex' => 'Please Enter Valid Phone',
            'organiserphone.min' => 'Please Enter 10-digits Phone number',
            'organisersphone.required' => 'Please Enter Organiser Phone',
            'organisersphone.regex' => 'Please Enter Valid Phone',
            'organisersphone.min' => 'Please Enter 10-digits Phone number',
            'internalnote.required' => 'Please Enter Internal Note',
            'termsnconditions.accepted' => 'Accept the Terms & Conditions', 
        );
        $validator = Validator::make($request->all(), $rules, $messsages);
        if($validator->fails()){ 
            return redirect()->back()->withInput()->withErrors($validator);
        }
       else{
            $e = new Event();
            $e->userid = Auth::user()->id;
            $e->eventtype = $request->eventtype;
            $e->eventtitle = $request->eventtitle;
            $e->minparticipant = $request->minparticipant;
            $e->maxparticipant = $request->maxparticipant;
            $e->reservedticket = $request->reservedticket;
            $e->ticketnumberfrom = $request->ticketnumberfrom;
            $e->ticketnumberto = $request->ticketnumberto;
            $e->eventothers = $request->eventothers;
            $e->starttime = $request->starttime;
            $e->endtime = $request->endtime;
            $e->startdate = $request->startdate;                
            $e->enddate = $request->enddate;                
            $e->country = $request->country;                
            $e->state = $request->state;                
            $e->city = $request->city;                
            $e->pincode = $request->pincode;                
            $e->address = $request->address;                
            $e->ticketprice = $request->ticketprice;                
            $e->agemin = $request->agemin;                
            $e->agemax = $request->agemax;                
            $e->description = $request->description;                
            $e->eventfacility = $request->eventfacility;                
            $e->donatepcc = $request->donatepcc;                
            $e->organisername = $request->organisername;                
            $e->organiseremail = $request->organiseremail;                
            $e->organiserphone = $request->organiserphone;                
            $e->organisersphone = $request->organisersphone;                
            $e->internalnote = $request->internalnote;                
            $e->save(); 
         
            if($request->event_profile!=''){
                $tmp = str_replace('data:image/png;base64,', '', $_POST['event_profile']);
                $tmp2 = str_replace(' ', '+', $tmp);                    
                $image = base64_decode($tmp2); // decode the image
                $date=date('YmdHis');
                $resultset=file_put_contents('public/images/events/'.Auth::user()->id.'_event'.$date.'.png',$image);
                
                $event = Event::find($e->id);
                $event->event_profile = 'public/images/events/'.Auth::user()->id.'_event'.$date.'.png';
                $event->save();
            }
            return redirect()->route('user.i_wish_to_host_an_event')->with('message','Event saved successfully');

        }
    }
    
    public function update_volunteer(Request $request){      
     
        $rules = [
            'name' => 'required',
            'membershipno' => 'required',
            'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'email' => 'required|email|string|max:255',
            'membershipno' => 'required',
            // 'volunteer_country' => 'string|max:255',
            // 'volunteer_state' => 'string|max:255',
            'volunteer_city' => 'string|required',
            'fromdate' => 'date_format:Y-m-d|after:today',
            'todate' => 'date_format:Y-m-d|after:today',
            'fromtime' => 'date_format:H:i',
            'totime' => 'date_format:H:i',
            'terms' =>'accepted'
         ];

         $messsages = array(
            'name.required' => 'please Enter Name',
            'membershipno.required' => 'please Enter Membership no',
            'mobile.min' => 'Enter atleast 8 digits' ,
            'mobile.max' => 'Max limit is 13 digits' ,
            'mobile.digits' => 'Only numbers allowed' ,
            'email.required' => 'please Enter E-mail',
            'email.email' => 'Enter Valid E-mail' ,   
            'terms.accepted' => 'Accept the Terms & Conditions',  

        );

         $validator = Validator::make($request->all(), $rules, $messsages);
         if($validator->fails()){ 
            return response()->json(['status'=>'error','data'=> $validator->errors()]);
         }
        else{ 
            // print_r($_POST); print_r('else');exit;

            // $volunteers = new Volunteers();
            // $volunteers->name = $request->name;
            // $volunteers->membershipno = $request->membershipno;
            // $volunteers->mobile = $request->mobile;
            // $volunteers->email = $request->email;
            // $volunteers->volunteer_country = $request->volunteer_country;
            // $volunteers->volunteer_state = $request->volunteer_state;
            // $volunteers->volunteer_city = $request->volunteer_city;
            // $volunteers->fromdate = $request->fromdate;
            // $volunteers->todate = $request->todate;
            // $volunteers->fromtime = $request->fromtime;
            // $volunteers->totime = $request->totime;
            // $volunteers->message = $request->message;
            // $volunteers->save();
            
            $volunteer=Volunteer::create($request->all());
            return response()->json(['status'=>'success','data'=>$volunteer]);
            //  return redirect()->route('user.volunteer')->with('message','Your Volunteer has added');
       
        }  
    }
    public function my_referrences(){
        echo "1234";exit; 
    }
    public function my_contribution(){
        echo "1234";exit; 
    }
    public function circle_news(){
        echo "1234";exit; 
    }
    public function circle_heroes(){
        echo "1234";exit; 
    }



}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'string', 'min:8', 'max:255'],
            'profile' => ['required', 'string', 'max:255'],
            'ccode' => ['required', 'string', 'max:255'],
            'ccountry' => ['required', 'string', 'max:255'],
            'cstate' => ['required', 'string', 'max:255'],
            'ccity' => ['required', 'string', 'max:255'],
            'isactive' => ['required', 'string', 'max:255'],
            'membershiptype' => ['required', 'string', 'max:255'],
            'paymenttype' => ['required', 'string', 'max:255'],
            'tmembership' => ['required', 'string', 'max:255'],
            'refferedby' => ['required', 'string', 'max:255'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone' => $data['phone'],
            'profile' => $data['profile'],
            'ccode' => $data['ccode'],
            'ccountry' => $data['ccountry'],
            'cstate' => $data['cstate'],
            'ccity' => $data['ccity'],
            'isactive' => $data['isactive'],
            'membershiptype' => $data['membershiptype'],
            'paymenttype' => $data['paymenttype'],
            'tmembership' => $data['membership'],
            'refferedby' => $data['refferedby'],
        ]);
    }
}

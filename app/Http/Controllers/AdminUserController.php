<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;use DB;use Auth;use Session;use Mail;use Crypt;
use App\Models\Admin;
// use App\Models\User;
use App\Models\Userlog;
use Illuminate\Support\Facades\Hash;
use App\Mail\pluscirclemail;
use Illuminate\Support\Facades\Input;
use App\Mail\WelcomeMail;
use App\Mail\PaymentConfirmation1500;
use App\Mail\PaymentConfirmation785;

class AdminUserController extends Controller
{
    public function index(){   
        if (Auth::guard('admin')->check()) {
            return redirect()->route('admin.sa');  
        }
        return view('admin.admin.login');        
    }
    public function login(Request $request){
       
        $rules = [
            'admin_email' => 'required|email|exists:admins,email',
            'admin_password' => 'required',             
        ];
        $messsages = array(
            'admin_email.required' => 'Please enter e-mail',
            'admin_email.email' => 'Enter valid e-mail',
            'admin_email.exists' => 'E-mail does not exist',
            'admin_password.required' => 'Please enter password',          
        );
        $remember='';

        $validator = Validator::make($request->all(), $rules, $messsages);
        if($validator->fails()){             
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else{
        
           if (Auth::guard('admin')->attempt(['email' => $request->admin_email, 'password' => $request->admin_password],1)) {
           
            if(Auth::guard('admin')->user()->isactive){  
                    $uselog = new Userlog;
                    $uselog->userid=Auth::guard('admin')->user()->id;
                    $uselog->usertype='admin';
                    $uselog->ipaddress=$request->getClientIp();
                    $uselog->session_id=session()->getId();
                    $uselog->login_at=date('Y-m-d H:i:s');
                    $uselog->last_active=date('Y-m-d H:i:s');
                    $uselog->created_at=date('Y-m-d H:i:s');
                    $uselog->status=0;
                    $insert=$uselog->save();
                    
                    return redirect()->intended('admin/allmembers');
                 }
                else{
                    echo "sajgsdasdasd3";exit;
                    return redirect()->back()->withInput()->with('error','Account is not active, Please contact admin'); 
                }  
            }
            else{
                return redirect()->back()->withInput()->with('error','Wrong Password, Please enter correct password');          
            }                
        }           
    }
    public function logout(Request $request) {
        if( Auth::guard('admin')->check() ){
            Auth::guard('admin')->logout();
            Session::flush();
        }      
        return redirect()->route('admin.admin.login');
    }
    
    //  public function resendEmailVerify(){
    //     return view('admin.admin.user_resendEmailVerify');
    // }
    // public function resendemailverificationlink(Request $request){
    //     $rules = [
    //           'admin_email' => 'required|email|exists:admins,email',
    //     ];
    //     $messsages = array(
    //         'admin_email.required' => 'please Enter E-mail',
    //         'admin_email.email' => 'Enter Valid E-mail',
    //         'admin_email.exists' => 'E-mail does not exists',           
    //     );
       
    //     $validator = Validator::make($request->all(), $rules, $messsages);
    //     if($validator->fails()){ 
          
    //         return redirect()->back()->withInput()->withErrors($validator);
    //     }
    //     else{
    //         $user = Admin::where('email', request()->input('email'))->first();
    //         $user->sendEmailVerificationNotification();
    //         $message= 'We have sent an Email to verify your accout, Please verify';
    //         return redirect()->route('admin.admin.login')->with('message', $message);
    //     }
    // }
    
    
    public function showForgotPasswordForm()
    {
        return view('admin.admin.user_forgotpassword');
    }
    
     public function sendResetLink(Request $request) {
        $rules = [
            'email' => 'required|email|exists:admins,email',
        ];
        $messsages = array(
           'email.required' => 'please Enter E-mail',
            'email.email' => 'Enter Valid E-mail',
            'email.exists' => 'E-mail does not exists',           
        );
       
        $validator = Validator::make($request->all(), $rules, $messsages);
        if($validator->fails()){ 
          
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else{
            $user = Admin::where('email', request()->input('email'))->first();
            $title='Click Below link to Reset your password';
            $token = \Str::random(60);
            $user->sendPasswordResetNotification($token);
            //$link='password/reset/'.$user->id;
            //$link=url(config('app.url').route('password.reset.token', $user->id));
            // $link=route('password_reset', 'token='.\Crypt::encrypt($user->email));
          
           
            //$user->notify(new CustomPasswordReset($request->session()->token()));
            //SELECT `id`, `email`, `token`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`
            
            // $pr = new PasswordReset;
            // $pr->email=$user->email;
            // $pr->token=$user->id;
            // $pr->created_at=date('Y-m-d H:i:s');
            // $insert=$pr->save();
            DB::table('password_resets')->insert(
                array(
                    'email'     =>   $user->email, 
                    'token'   =>   \Crypt::encrypt($user->email),
                    'created_at' => date('Y-m-d H:i:s'),
                )
           );
            
            return redirect()->back()->with('message', 'Password Reset link has been sent via mail!');
        }
    }
    public function password_reset(Request $request){
        // print_r();exit;
        // $token;
        // $token=\Crypt::decrypt($request->token);
    //   print_r($request->input('email'));exit;
        $userpkt = DB::table('password_resets')
        ->where('email',$request->input('email'))
        ->first();
        // print_r($userpkt);exit;
        if($userpkt){
           
            If($userpkt){
                return view('admin.admin.user_resetpassword', compact('userpkt'));       
            }
        }
        else{
            abort(404);
        }
    }

   
    public function reset(Request $request) {
        $rules = [
            'email' => 'required|email|exists:admins,email',
            'password' => 'required|required_with:password_confirmation|same:password_confirmation',
            
            
        ];
        $messsages = array(
            'email.required' => 'please Enter E-mail',
            'email.email' => 'Enter Valid E-mail',
            'email.exists' => 'E-mail does not exists',           
            'password.required' => 'Enter Valid E-mail',
            'password.required_with' => 'Please Enter confirm Password',           
            'password.same' => 'Confirm Password Does not match' 
            

        );
        //print_r($_POST);
        //exit;
        $validator = Validator::make($request->all(), $rules, $messsages);
        if($validator->fails()){ 
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else{
            $hashPassword=Hash::make($request->password);
            $now=date('Y-m-d H:i:s');
            $ret=DB::table('admins')
            ->where("admins.email", '=',  $request->email)
            ->update(['admins.password'=> $hashPassword,'updated_at'=>$now]);
            return redirect()->route('admin.admin.login')->with("status","Password has been changed. Please Login with the New Password!");
            
        }
    }


}

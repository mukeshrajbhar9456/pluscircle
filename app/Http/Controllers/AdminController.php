<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;use DB;use Auth;use Session;use Mail;use Crypt;
use App\Models\User;
use App\Models\Userlog;
use App\Models\Address;
use App\Models\Membershipkit;
use App\Models\Membership;
use App\Models\Payment;

class AdminController extends Controller
{
    
    
    public function dashboard(){
        return view('admin.admin.dashboard');
    }
    public function sccm_applications_view(){
        $applications = DB::table('sccm_application')
        ->select('sccm_application.id','sccm_application.name','sccm_application.position','sccm_application.email','sccm_application.phone','states.name as state')
        ->join('states','states.id','=','sccm_application.appstate')
        ->where('sccm_application.active',true)
        ->get();
      
        // $applications = \DB::table('sccm_application')->selec('id','name','phone','position')->get();
        return view('admin.admin.sccm_applications_view',compact('applications'));
    }
    public function view_application(Request $request){
       $id= Crypt::decrypt($request->id);
       $applications = DB::table('sccm_application')
       ->where('id',$id)
       ->get();    
       return view('admin.admin.sccm_form_view',['application' => $applications ]);
       
    }
    public function view_user(Request $request){
        $user=User::all();
        // print_r($user);   exit;
        return view('admin.admin.user_view',['user' => $user ]);
        
     }
     public function view_payments(Request $request){
        $payments = $user=Payment::all();
        return view('admin.admin.payment_view',['payments' => $payments ]);
        
     }
     public function view_memberkits(Request $request){
        $mkit = $user=Membershipkit::all(); 
        return view('admin.admin.memberkit_view',['mkit' => $mkit ]);
        
     }
     
}

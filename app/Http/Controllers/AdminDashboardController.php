<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;use DB;use Auth;use Session;use Mail;use Crypt;
use App\Models\Admin;
use App\Models\UserType;
use App\Models\User;
use App\Models\Userlog;
use App\Models\Address;
use App\Models\Membershipkit;
use App\Models\Membership;
use App\Models\Payment;


class AdminDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('logout');
    }
    
    public function dashboard(){
       
       $user=User::all();
       
       $data = DB::table('users')
        ->join('countries', 'countries.id', '=', 'users.ccountry')
        ->leftjoin('states', 'states.id', '=', 'users.cstate')
        ->leftjoin('payments', 'payments.payment_id', '=', 'users.paymenttype')
        ->leftjoin('users as tmembership', 'tmembership.tmembership', '=', 'users.refferedby')
        ->select('users.*','countries.name as country', 'states.name as state','payments.amount','payments.datetime as dop','tmembership.name as refferedname')
        ->orderby('users.id','DESC')
        ->groupBy('users.id')
        ->get();
        $country = \DB::table('countries')->get();
        $states = \DB::table('states')->where('country_id',Auth::user()->ccountry)->get();
        $city = \DB::table('users')->whereNotNull('ccity')->get();
       //dd($data);
        return view('admin.admin.allmembers',['user' => $data,'country' => $country ,'states' => $states,'city' => $city]);

    }
    
    public function searchMembers(Request $request){
       // dd($request->all());
       $countryid = $request->input('country');
       $stateid = $request->input('state');
       $cityid = $request->input('city');
       $start = $request->input('start');
       $end  = $request->input('end');
        $data = DB::table('users')
        ->join('countries', 'countries.id', '=', 'users.ccountry')
        ->leftjoin('states', 'states.id', '=', 'users.cstate')
        ->leftjoin('payments', 'payments.payment_id', '=', 'users.paymenttype')
        ->leftjoin('users as tmembership', 'tmembership.tmembership', '=', 'users.refferedby')
        ->whereBetween('users.created_at', array($start, $end))
        ->where('users.ccity', '=', $cityid)
        ->where('users.ccountry', '=', $countryid)
        ->where('users.cstate', '=', $stateid)
        ->select('users.*','countries.name as country', 'states.name as state','payments.amount','payments.datetime as dop','tmembership.name as refferedname')
        ->orderby('users.id','DESC')
        ->get();
        $country = \DB::table('countries')->get();
        $states = \DB::table('states')->where('country_id',Auth::user()->ccountry)->get();
        $city = \DB::table('users')->whereNotNull('ccity')->get();
       //dd($data);
        return view('admin.admin.allmembers',['user' => $data,'country' => $country ,'states' => $states,'city' => $city]);

    }
    
    public function application(Request $request){
        $applications = DB::table('sccm_application')
        ->select('sccm_application.id','sccm_application.name','sccm_application.position','sccm_application.email','sccm_application.phone','states.name as state')
        ->join('states','states.id','=','sccm_application.appstate')
        ->where('sccm_application.active',true)
        ->get();      
        // $applications = \DB::table('sccm_application')->selec('id','name','phone','position')->get();
        return view('admin.admin.sccm_applications',compact('applications'));
     
    }
    public function viewuser(Request $request){
        $userid= \Crypt::decrypt($request->userview);
        
        $viewuser = DB::table('users')
        ->join('countries', 'countries.id', '=', 'users.ccountry')
        ->leftjoin('states', 'states.id', '=', 'users.cstate')
        ->select('users.*','countries.name as country', 'states.name as state')
        ->where('users.id',$userid)
        ->first();

        return view('admin.admin.viewuser',compact('viewuser')); 
    }
    
   
    
    public function createNewUser(Request $request){
        $usertype=UserType::where('active',1)->get();
        $countries = \DB::table('countries')->get();
        $countrycode = \DB::table('countries')->where('phonecode','>',0)->groupBy('phonecode')->orderby('phonecode')->get();
        $countrycode = \DB::table('countries')->where('phonecode','>',0)->groupBy('phonecode')->orderby('phonecode')->get();
        return view('admin.admin.createNewUser',['country' => $countries,'countrycode' => $countrycode,'usertype' => $usertype ]);
   
    }
    
     public function deleteuser(Request $request){
        $userid= Crypt::decrypt($request->userview);

        $user = User::find($userid);
        $user->delete();
        return redirect()->back();
    }
    public function edituser(Request $request){
        $userid= Crypt::decrypt($request->userview);

        $edituser = DB::table('users')
        ->join('countries', 'countries.id', '=', 'users.ccountry')
        ->leftjoin('states', 'states.id', '=', 'users.cstate')
        ->select('users.*','countries.name as country', 'states.name as state')
        ->where('users.id',$userid)
        ->first();

        return view('admin.admin.edituser',compact('edituser'));
    }
    public function approvalmember(Request $request){

        $userid = Crypt::decrypt($request->userview);

        User::query()->where("id","=",$userid)->update(["tmembership" => DB::raw("REPLACE(tmembership, '-T-', '-P-')"),"membershiptype" => 'P']);

        return redirect()->back();
    }

    public function updateuser(Request $request){

        $userid = $request->userid;

        User::query()->where("id","=",$userid)->update(["name" => $request->name,"email" => $request->email,"membershiptype" => $request->usertype,"refferedby" => $request->reference,"phone" => $request->phone,"secondaryphone" => $request->sphone]);

        return redirect()->route('admin.sa');
    }

    
    public function viewcomment(Request $request){

        $userid = Crypt::decrypt($request->userview);

        $user = User::find($userid);

        $comments = DB::table('comments')->where('user_id',$userid)->get();
        return view('admin.admin.commentsupdate', compact('comments','user'));
    }
    
     public function fetchState(Request $request)
    {
        $data['states'] = DB::table('states')->where("country_id",$request->country_id)->get(["name", "id"]);
        return response()->json($data);
    }

    public function fetchCity(Request $request)
    {
        $data['cities'] = DB::table('cities')->where("state_id",$request->state_id)->get(["name", "id"]);
        return response()->json($data);
    }
}

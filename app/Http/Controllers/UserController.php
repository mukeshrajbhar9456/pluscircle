<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;use DB;use Auth;use Session;use Mail;use Crypt;
use App\Models\User;
use App\Models\Userlog;
use Illuminate\Support\Facades\Hash;
use App\Mail\pluscirclemail;
use Illuminate\Support\Facades\Input;
use App\Mail\WelcomeMail;
use App\Mail\PaymentConfirmation1500;
use App\Mail\PaymentConfirmation785;



class UserController extends Controller
{

  
    public function user_login()
    {
       
        return view('users.user_login');
    }
    public function view_forgotpassword()
    {
        return view('users.user_forgotpassword');
    }
    
    public function login_user_check(Request $request){
        $email=$request->email;
        if($email==''){
            return redirect()->back()->withInput()->with('error','Please Enter E-mail');            
        }
        $usercount = \DB::table('users')->where('email',$request->email)->count();
        if($usercount > 0){
            $email= Crypt::encrypt($email);
            return redirect()->route('user_password', ['email'=>$email]);
        }
        else{
            return redirect()->back()->withInput()->with('error','Invalid E-mail, Enter valid E-mail');  
        }
    }
    public function signin(Request $request){
       
        $rules = [
            'email' => 'required|email|exists:users,email',
            'password' => 'required',             
        ];
        $messsages = array(
            'email.required' => 'Please enter e-mail',
            'email.email' => 'Enter valid e-mail',
            'email.exists' => 'E-mail does not exist',
            'password.required' => 'Please enter password',          
        );
        $remember='';

        $validator = Validator::make($request->all(), $rules, $messsages);
        if($validator->fails()){             
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else{
          if (Auth::attempt(['email' => $request->email, 'password' => $request->password],0)) {
                
            if($request->user()->hasVerifiedEmail()){               
                    if($request->user()->isactive){   
                        $uselog = new Userlog;
                        $uselog->userid=Auth::user()->id;
                        $uselog->usertype='user';
                        $uselog->ipaddress=$request->getClientIp();
                        $uselog->session_id=session()->getId();
                        $uselog->login_at=date('Y-m-d H:i:s');
                        $uselog->last_active=date('Y-m-d H:i:s');
                        $uselog->created_at=date('Y-m-d H:i:s');
                        $uselog->status=0;
                        $insert=$uselog->save();
                            return redirect()->intended('dashboard');
                    }
                    else{                  
                        return redirect()->back()->withInput()->with('error','Account is not active, Please contact admin'); 
                    }                     
                }
                else{
                    Auth::logout();
                    // print_r('Email is not verified,<br> Please verify your mail');exit;
                    return redirect()->back()->withInput()->with('error','Email is not verified, Please verify your mail');          
                }  
            }
            else{
                return redirect()->back()->withInput()->with('error','Wrong Password, Please enter correct password');          
            }                
        }           
    }
    public function user_password(Request $request){
        $email= Crypt::decrypt($request->email);
       
        return view('users.user_password',compact('email'));
    }
   
    
    public function resendEmailVerify(){
        return view('users.user_resendEmailVerify');
    }
    public function resendemailverificationlink(Request $request){
        $rules = [
            'email' => 'required|email|exists:users,email',
        ];
        $messsages = array(
            'email.required' => 'please Enter E-mail',
            'email.email' => 'Enter Valid E-mail',
            'email.exists' => 'E-mail does not exists',           
        );
       
        $validator = Validator::make($request->all(), $rules, $messsages);
        if($validator->fails()){ 
          
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else{
            $user = User::where('email', request()->input('email'))->first();
            $user->sendEmailVerificationNotification();
            $message= 'We have sent an Email to verify your accout, Please verify';
            return redirect()->route('user_login')->with('message', $message);
        }
    }

    public function user_register()
    {
        $countries = \DB::table('countries')->get();
        $countrycode = \DB::table('countries')->where('phonecode','>',0)->groupBy('phonecode')->orderby('phonecode')->get();
  
        return view('users.user_register',['country' => $countries,'countrycode' => $countrycode]);
    }
    public function register(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|email|string|max:255|unique:users,email',
            'password' => 'required|string|max:255|confirmed',
            'ccode' => 'string|max:255',
            'phone' => 'required|digits_between:8,15|unique:users,phone',
            'profile' => 'image:jpeg,jpg,png | max:10240 | nullable',
            'role' => 'string|max:255',
            'gender' => 'string|max:255',
            'ccountry' => 'string|max:255',
            'cstate' => 'string|max:255',
            'ccity' => 'string|max:255',
            'isactive' => 'string|max:255',
            'membershiptype' => 'string|max:255',
            'paymenttype' => 'string|max:255',
            'membership' => 'string|max:255',
            'terms' =>'accepted'
        ];
        $messsages = array(
            'email.unique' => 'Email exist, please enter different email' ,
            'email.email' => 'Enter valid email' ,
            'email.required' => 'Enter E-mail' ,
            'phone.min' => 'Enter atleast 8 digits' ,
            'phone.max' => 'Max limit is 13 digits' ,
            'phone.digits' => 'Only numbers allowed' ,
            'password.required' => 'Enter Password' ,
            'terms.accepted' => 'Accept the Terms & Conditions',
        );
        if($request->refferedby !=''){
            $rsp=explode('-',$request->refferedby);
            
            if(count($rsp)>1){
                if($rsp[1]=='T'){
                    $rules['refferedby'] = 'exists:users,tmembership' ;
                }
                if($rsp[1]=='P'){
                    $rules['refferedby'] = 'exists:users,pmembership' ;
                }
            }
            else{
                    $rules['refferedby'] = 'exists:users,tmembership' ;
            }
            
        }
        // print_r($_POST);exit;
        $validator = Validator::make($request->all(), $rules, $messsages);
            
            if($validator->fails()){ 
               // $user = new user;
                // print_r($validator->errors());
                // return response()->json(['status'=>'error','data'=> $validator->errors()]);
                return redirect()->back()->withErrors($validator)->withInput();
            }
            else{                         
                $email=$request->email;
                // $data['name']=$request->name;
                // $data['email']=$request->email;
                // $mail=Mail::to($email)->send(new WelcomeMail($data));
                
                $user = new User();
                $user->title = $request->title;
                $user->name = $request->name;
                $user->ccountry = $request->ccountry;
                $user->phonecode = $request->phonecode;
                $user->phone = $request->phone;
                $user->email = $request->email;
                $user->ccountry = $request->ccountry;
                $user->password = Hash::make($request->password);
                $user->refferedby = $request->refferedby;                
                $user->save();               
               
               
                $mail= $user->sendEmailVerificationNotification();
                                
                $message= 'Thank you for creating account, Please check your Email inbox and verify your E-mail to continue login';
                // return response()->json(['status'=>'success','data'=> $message]);
            
                return redirect()->route('user_login')->with('message', $message);
            }               
        
    }
    public function verify(Request $request){
        // print_r($request->id);exit;
        $user = User::where('id',$request->id)->first();
        $user->email_verified_at=date('Y-m-d H:i:s');
        $user->email_verified=true;
        $user->isactive=true;
        $user->save();

        $data['name']=$user->name;
        $data['email']=$user->email;
        $mail=Mail::to($user->email)->send(new WelcomeMail($data));

        $message= 'Your E-mail is verified, Please continue login';
                return redirect()->route('user_login')->with('message', $message);
    }
    public function user_logout(Request $request) {
       
        if( Auth::check() ){
            Auth::logout();
            Session::flush();
        }      
        return redirect()->route('user_login');
    }


    /***forgot password and reset link */
    public function showForgotPasswordForm() {
       
        return view('users.user_forgotpassword');
    }

    /**
     * Send reset password email
     * @return type
     */
    
    public function sendResetLink(Request $request) {
        $rules = [
            'email' => 'required|email|exists:users,email',
        ];
        $messsages = array(
            'email.required' => 'please Enter E-mail',
            'email.email' => 'Enter Valid E-mail',
            'email.exists' => 'E-mail does not exists',           
        );
       
        $validator = Validator::make($request->all(), $rules, $messsages);
        if($validator->fails()){ 
          
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else{
            $user = User::where('email', request()->input('email'))->first();
            $title='Click Below link to Reset your password';
            $token = \Str::random(60);
            $user->sendPasswordResetNotification($token);
            //$link='password/reset/'.$user->id;
            //$link=url(config('app.url').route('password.reset.token', $user->id));
            // $link=route('password_reset', 'token='.\Crypt::encrypt($user->email));
          
           
            //$user->notify(new CustomPasswordReset($request->session()->token()));
            //SELECT `id`, `email`, `token`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`
            
            // $pr = new PasswordReset;
            // $pr->email=$user->email;
            // $pr->token=$user->id;
            // $pr->created_at=date('Y-m-d H:i:s');
            // $insert=$pr->save();
            DB::table('password_resets')->insert(
                array(
                    'email'     =>   $user->email, 
                    'token'   =>   \Crypt::encrypt($user->email),
                    'created_at' => date('Y-m-d H:i:s'),
                )
           );
            
            return redirect()->back()->with('message', 'Password Reset link has been sent via mail!');
        }
    }
    public function password_reset(Request $request){
        // print_r();exit;
        // $token;
        // $token=\Crypt::decrypt($request->token);
    //   print_r($request->input('email'));exit;
        $userpkt = DB::table('password_resets')
        ->where('email',$request->input('email'))
        ->first();
        // print_r($userpkt);exit;
        if($userpkt){
           
            If($userpkt){
                return view('users.user_resetpassword', compact('userpkt'));       
            }
        }
        else{
            abort(404);
        }
    }
    

    public function reset(Request $request) {
        $rules = [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|required_with:password_confirmation|same:password_confirmation',
            
            
        ];
        $messsages = array(
            'email.required' => 'please Enter E-mail',
            'email.email' => 'Enter Valid E-mail',
            'email.exists' => 'E-mail does not exists',           
            'password.required' => 'Enter Valid E-mail',
            'password.required_with' => 'Please Enter confirm Password',           
            'password.same' => 'Confirm Password Does not match' 
            

        );
        //print_r($_POST);
        //exit;
        $validator = Validator::make($request->all(), $rules, $messsages);
        if($validator->fails()){ 
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else{
            $hashPassword=Hash::make($request->password);
            $now=date('Y-m-d H:i:s');
            $ret=DB::table('users')
            ->where("users.email", '=',  $request->email)
            ->update(['users.password'=> $hashPassword,'updated_at'=>$now]);
            return redirect('/')->with("status","Password has been changed. Please Login with the New Password!");
            
        }
    }


    
}

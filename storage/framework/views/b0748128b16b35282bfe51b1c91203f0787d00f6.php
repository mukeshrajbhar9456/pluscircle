	<?php $__env->startSection('content'); ?>	
		<section class="cms-page-main">
			<div class="container mb-28">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Volunteer</p><br>
					<h4 class="ybh">Volunteer</h4>
					
					<hr>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="cms-content">
							<h5 class="">Orphans</h5>
							
							
							<p>There is no greater gift than your time! Volunteering is such a rewarding venture. Lets Come together, work together and succeed together. Join with us, Become a volunteer now. Volunteering provides insight into the lives of so many, all while providing a feel-good moment for you. Volunteers reap so many benefits when volunteering with Plus Circle include developing professional connections, gaining experience, making new friends, providing relief and/or for empowering the youth, elders, and needy with community-based help.
							 </p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/volunteer_c.blade.php ENDPATH**/ ?>
	<?php $__env->startSection('content'); ?>	

		<section class="login-section">
			<div class="container">

				<div class="row">
					
					<div class="col-md-12">
						<div class="login-form">
							<div class="wizard">
				               <div class="wizard-inner">
				                  <div class="connecting-line"></div>
				                  <ul class="nav nav-tabs" role="tablist">
				                     
				                  </ul>
				               </div>
				               <form role="form" method="POST" action="<?php echo e(route('signin')); ?>" name="loginForm" class="login-form">
							  	 <?php echo e(csrf_field()); ?>

							   		<div class="tab-content" id="main_form">
				                     <div class="tab-pane active" role="tabpanel" >
				                     	<div class="form-title">
				                     		<!-- <h4>Enter your mobile number or email id to login</h4> -->
				                     		<h4>Enter your Password</h4>
										 </div>

										  <div class="col-md-12 center">
										  	<?php if(session()->has('message')): ?>
												<div class="alert1 alert-info1">&#x2714;
													<?php echo e(session('message')); ?>

												</div>
											<?php endif; ?>
											<?php if(session()->has('error')): ?>
											<div class="alert1 alert-danger1 ">&#x26A0;
												<?php echo e(session('error')); ?>

											</div>
											<?php endif; ?>
											   <!-- <div class="alert1 alert-danger1 ">&#x26A0; Your Password is incorrect or this account does not exist. <BR>Please enter valid password or<a href="#" style="text-decoration: underline;"class="alert-danger1">  Reset Password</a>
					                           </div> -->
					                     </div>


										 <input type='hidden' name='email' value="<?php echo e($email?$email:''); ?>">
												 
				                     	<div class="form-bg">
					                        <div class="floating-form">
											    <div class="floating-label">      
												  <input class="floating-input" type="password" name='password' placeholder="Enter Password ">
												  <span class="highlight"></span>
											      <!-- <label>Enter mobile number or email id</label> -->
												  <!-- <label></label> -->
												  	<?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
														<label id="password-error" class="error" for="password"><?php echo e($message); ?></label>
													<?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
													<?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
														<label id="email-error" class="error" ><?php echo e($message); ?></label>
													<?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
											    </div>
											</div>
											<div class="forgot-link mt-28">
												<a href="<?php echo e(route('showForgotPasswordForm')); ?>" class="accept1">Forgot Password? </a><br>
												<a href="<?php echo e(route('resendEmailVerify')); ?>" class="accept1">Didn't receive email to Verify? </a>
											
											</div>
					                        <div class="submit-btn">
												<input type="submit" name="next"  class="next-step" value="Login">
											</div>

											<div class="or-line">
												<span>or</span>
											</div>
											<div class="otp-btn">
												<a href="#" class="btn1">LOGIN WITH OTP</a>
											</div>



											<div class="new-user">
												<a href="<?php echo e(route('user_register')); ?>" class="accept1">Join as a new member</a>
											</div>
										</div>
									</div>
								</div>
							</form>
				            </div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php $__env->stopSection(); ?>

		
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/user_password.blade.php ENDPATH**/ ?>
	<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('user.usermodals', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>			
		<section class="dashboard-main">
			<div class="container-fluid">
				<div class="row">
				<div class="col-md-12 center">
				<!-- 	<?php if(session()->has('message')): ?>
						<div class="alert1 alert-info1">&#x2714;
							<?php echo e(session('message')); ?>

						</div>
					<?php endif; ?> -->
				</div>
					<div class="col-md-3">
						<div class="dashboard-menu">
						<?php echo $__env->make('user.dashboardleftmenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
						</div>
					</div>
					<div class="col-md-9">
						<div class="dashboard-content">
							<div class="dashboard-profile dashboard-user-profile dashboard-upcomingcss">
								<div class="floating-form">
									<div class="row mb-10">
										<div class="col-md-7"></div>
										<div class="col-md-3 center1">
										<p class="center1"><a class="right ticketcss"><i class="fa fa-ticket-alt"></i> My ticket (12)</a></p>
										</div>
										<div class="col-md-2 center1">
											<img src="<?php echo e(asset('public/images/share_btn.png')); ?>" class="right">
										</div>
									</div>
								</div>

							

								<div class="hasDatepicker-main shadowbg">

								<div class="profile-img-main volunteerhead">
										<h4 >UPCOMING ACTIVITIES</h4>
								</div>


									<div class="floating-form">
										<div class="row center1">
											<!-- <div class="col-md-3">
												<div class="floating-label">    
												<input class="floating-inputpcc" type="date" placeholder=" ">
												</div>
												<div class="floating-label floating-label-date">    
											      	<input class="floating-input" type="text" id="txtDateRange" name="txtDateRange" class="inputField shortInputField dateRangeField" placeholder="Date " data-from-field="txtDateFrom" data-to-field="txtDateTo" />
											      	<input type="hidden" id="txtDateFrom" value="" />
													<input type="hidden" id="txtDateTo" value="" />  
											      	<span class="highlight"></span>
											     
											    </div> 
											</div> -->
											<div class="col-md-3">
												<div class="floating-label">      
													<select class="floating-selectpcc1" id='search_country' value="">
														<?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value="<?php echo e($c->id); ?>"><?php echo e($c->country); ?></option>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</select>
											      	<span class="highlight"></span>											 
											    </div>
											</div>
											<div class="col-md-3">
												<div class="floating-label">      
											      	<select class="floating-selectpcc1" id='search_state' value="">
														<?php $__currentLoopData = $state; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value="<?php echo e($s->id); ?>"><?php echo e($s->state); ?></option>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>														
													</select>
											     </div>
											</div>
											<div class="col-md-3">
												<div class="floating-label">      
											      	<select class="floating-selectpcc1" id='search_city' value="">
														<?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value="<?php echo e($c->city); ?>"><?php echo e($c->city); ?></option>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>														
													</select>
											     </div>
											</div>
											<div class="col-md-2">
												<div class="submit-btn btnmargin eventbt">
													<input type="button" name="next" class="volunteersubmit" onclick='search_event()' value="Search">
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
								<div class="upcoming-events-slider">
									
									<div class="">
										<div class="upcoming-slider-main">
											<div id="upcoming-activities-2" class="owl-carousel">
												<?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $e): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<div class="item">
													<div class="slider-img">
														<!-- <a href="#myModal_upcoming" data-toggle="modal" data-target="#myModal_upcoming"> -->
														<a onclick='eventModal(<?php echo e("$e->id"); ?>);' >
														<?php if($e->event_profile != ''): ?>
															<img class="owl-lazy" data-src="<?php echo e(asset($e->event_profile)); ?>" alt="">
														<?php else: ?>
															<img class="owl-lazy" data-src="<?php echo e(asset('public/images//community-img.jpg')); ?>" alt="">
														<?php endif; ?>
														</a>
													</div>
													<div class="upcoming-slider-content">
														<div class="events-name"> 
															<strong><?php echo e($e->eventtitle); ?> ( <?php echo e($e->eventtype); ?> )</strong>
														</div>
														<p><?php echo e($e->description); ?></p>
														<div class="row">
															<div class="col-md-12">
																<div class="text-box1">
																	<p> <strong> From </strong> <?php echo e($e->startdate); ?> &nbsp; <?php echo e($e->starttime); ?></p>
																	<p> <strong> To </strong> <?php echo e($e->enddate); ?> &nbsp; <?php echo e($e->endtime); ?> </p>
																	<p> <strong> INR </strong> <?php echo e($e->ticketprice); ?></p>
																</div>
																
															</div>
															<div class="col-md-12">
																<div class="slider-btn">
																	<button data-toggle="modal" data-target="#myModal2" onclick="buy_ticket(<?php echo e($e->id); ?>);" class="btn volunteersubmit">Book a Ticket</button>
																</div>
															</div>
														</div>
													</div>
												
												</div>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												 
												<!--<div class="item">
													<div class="slider-img">
														<a href="#myModal_upcoming" data-toggle="modal" data-target="#myModal_upcoming"><img class="owl-lazy" data-src="<?php echo e(asset('public/images//community-img.jpg')); ?>" alt=""></a>
													</div>
													<div class="upcoming-slider-content">
														<div class="events-name">
															<strong>Event Name</strong>
														</div>
														<p>Lorem Ipsum is simply dummy text of the printing.</p>
														<div class="row">
															<div class="col-md-6">
																<div class="text-box1">
																	<p> 15 Nov 2020</p>
																	<p> 05:00 PM</p>
																	<p> INR 2,000.00</p>
																</div>
																
															</div>
															<div class="col-md-6">
																<div class="slider-btn">
																	<button data-toggle="modal" data-target="#myModal2" class="btn volunteersubmit">Book<br> a<br>  Ticket</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="item">
												
													<div class="slider-img">
														<a href="#myModal_upcoming" data-toggle="modal" data-target="#myModal_upcoming"><img class="owl-lazy" data-src="<?php echo e(asset('public/images//community-img.jpg')); ?>" alt=""></a>
													</div>
													<div class="upcoming-slider-content">
														<div class="events-name">
															<strong>Event Name</strong>
														</div>
														<p>Lorem Ipsum is simply dummy text of the printing.</p>
														<div class="row">
															<div class="col-md-6">
																<div class="text-box1">
																	<p> 15 Nov 2020</p>
																	<p> 05:00 PM</p>
																	<p> INR 2,000.00</p>
																</div>
																
															</div>
															<div class="col-md-6">
																<div class="slider-btn">
																	<button data-toggle="modal" data-target="#myModal2" class="btn volunteersubmit">Book<br> a<br>  Ticket</button>
																</div>
															</div>
														</div>
														
													</div>
												</div>
												<div class="item">
													<div class="slider-img">
														<a href="#myModal_upcoming" data-toggle="modal" data-target="#myModal_upcoming"><img class="owl-lazy" data-src="<?php echo e(asset('public/images//community-img.jpg')); ?>" alt=""></a>
													</div>
													<div class="upcoming-slider-content">
														<div class="events-name">
															<strong>Event Name</strong>
														</div>
														<p>Lorem Ipsum is simply dummy text of the printing.</p>
														<div class="row">
															<div class="col-md-6">
																<div class="text-box1">
																	<p> 15 Nov 2020</p>
																	<p> 05:00 PM</p>
																	<p> INR 2,000.00</p>
																</div>
																
															</div>
															<div class="col-md-6">
																<div class="slider-btn">
																	<button data-toggle="modal" data-target="#myModal2" class="btn volunteersubmit">Book<br> a<br>  Ticket</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div> -->
											<!-- <div class="view-all-btn">
												<button class="btn">View All</button>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		
		
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/user/upcoming_events.blade.php ENDPATH**/ ?>
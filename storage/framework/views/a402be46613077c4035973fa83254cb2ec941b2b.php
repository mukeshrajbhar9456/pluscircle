
	<?php $__env->startSection('content'); ?>	

		<section class="login-section">
			<div class="container">

				<div class="row">
					<div class="col-md-12 text-center">
						<?php if(session()->has('message')): ?>
							<div class="alert1 alert-info1">&#x2714;
								<?php echo e(session('message')); ?>

							</div>
						<?php endif; ?>
						<?php if(session()->has('error')): ?>
							<div class="alert1 alert-danger1">&#x26A0;
								<?php echo e(session('error')); ?>

							</div>
						<?php endif; ?>
					</div>
					<div class="col-md-12">
						<div class="login-form">
							<div class="wizard">
				               <div class="wizard-inner">
				                  <div class="connecting-line"></div>
				                  <ul class="nav nav-tabs" role="tablist">
				                     <!-- <li role="presentation" class="active">
				                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" aria-expanded="true"><span class="round-tab">1 </span> <i>Step 1</i></a>
				                     </li>
				                     <li role="presentation" class="disabled">
				                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" aria-expanded="false"><span class="round-tab">2</span> <i>Step 2</i></a>
				                     </li> -->
				                  </ul>
				               </div>
				               <form role="form" method="POST" action="<?php echo e(route('admin.reset')); ?>" name="loginForm" class="login-form">
							   		<?php echo e(csrf_field()); ?>

							   		<div class="tab-content" id="main_form">
				                     <div class="tab-pane active" role="tabpanel" >
				                     	<div class="form-title">				                     	
				                     		<h4>Reset your Password</h4>
										 </div>


										 <!--<div class="col-md-12 center">-->
					      <!--                   <p class="accept1">Reset you password here </p>-->
					      <!--               </div>-->


										 
				                     	<div class="form-bg">
					                        <div class="floating-form">
											    <div class="floating-label">      
												  <input class="floating-input" type="text" name='email' value="<?php echo e($userpkt->email); ?>" placeholder=" Enter mobile number or email id" readonly>
												  <span class="highlight"></span>
											   
												 </div>
											</div>
											<div class="floating-label">      
												<input class="floating-input <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"  name="password" type="password" id="password" placeholder="Enter password *">
												<span class="highlight"></span>									    	
												
												<?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
													<label id="password-error" class="error" for="email"><?php echo e($message); ?></label>
												<?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
											</div>
											<div class="floating-label">      
												<input class="floating-input" type="password" name="password_confirmation" id="password-confirm" placeholder="Re-enter password * ">
												<span class="highlight"></span>
											
												
											</div>
											<div class="forgot-link">
												<a href="<?php echo e(route('admin.admin.login')); ?>">Admin Login</a>
											</div>
					                        <div class="submit-btn">
												<input type="submit" name="reset"  value="Reset Password">
											</div>
											<!--<div class="new-user">-->
											<!--	<a href="<?php echo e(route('user_register')); ?>">Join as a new member</a>-->
											<!--</div>-->
										</div>
									</div>
								</div>
							</form>
				                    
				                     
				            </div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php $__env->stopSection(); ?>

		
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/admin/admin/user_resetpassword.blade.php ENDPATH**/ ?>
	<?php $__env->startSection('content'); ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
						<div class="row">
                            <div class="col-sm-12">
								<span>
									<h1>View User</h1>
								</span>
							</div>
						</div>
						
                        <div class="row">
                            <div class="col-sm-12 ">							
                                <div class="card-box table-responsive">
									<!-- <h4>Download</h4> -->
									<a href="<?php echo e(URL::previous()); ?>"><button class='btn btn-success'>Go Back</button></a>								
									<!-- <button class="btn btn-success waves-effect waves-light m-b-5"> <i class="fa fa-file-excel-o m-r-5"></i> <span>Excel</span> </button>
									<button class="btn btn-warning waves-effect waves-light m-b-5"> <i class="fa fa-file-pdf-o m-r-5"></i> <span>PDF</span> </button><BR>&nbsp; -->
									<div class='text-center'>
										<div class="user-box">
											<div class="user-profile-img ">
											<?php if($viewuser->profile!=''): ?>
												<img src="<?php echo e(asset($viewuser->profile)); ?>" alt="user-img" title="<?php echo e($viewuser->title); ?> <?php echo e($viewuser->name); ?>" >
											<?php else: ?>
												<img src="<?php echo e(asset('public/admin/assets/images/users/avatar-1.jpg')); ?>" alt="user-img" title="<?php echo e($viewuser->title); ?> <?php echo e($viewuser->name); ?>">
											<?php endif; ?>
											</div>
											<h1><?php echo e($viewuser->title); ?> <?php echo e($viewuser->name); ?> </h1>
										</div>
									</div>									
								<div>
							</div>
							<div class='row'>
								<div class='col-md-4'> 
									<label > Phone:</label>
									<div class='field-title'><?php echo e($viewuser->phonecode.' '.$viewuser->phone); ?></div>
								</div>
								<div class='col-md-4'> 
									<label > Email:</label>
									<div class='field-title'><?php echo e($viewuser->email); ?></div>
								</div>
								<div class='col-md-4'> 
									<label > Date of Birth:</label>
									<div class='field-title'><?php echo e($viewuser->dob); ?></div>
								</div>
								<div class='col-md-4'> 
									<label > Profssion:</label>
									<div class='field-title'><?php echo e($viewuser->profession); ?></div>
								</div>
								<div class='col-md-4'> 
									<label > KYC type:</label>
									<div class='field-title'><?php echo e($viewuser->kyctype); ?></div>
									<?php if($viewuser->kycprofile !=''): ?>
									<img src="<?php echo e($viewuser->kycprofile?asset($viewuser->kycprofile):asset('public/images/kyc.png')); ?>" class='kycimage' style='height:100px;' />
									<?php endif; ?>
								</div>
								<div class='col-md-4'> 
									<label > Address:</label>
									<div class='field-title'><?php echo e($viewuser->address." ".$viewuser->state." ".$viewuser->country); ?></div>
								</div>
								<div class='col-md-4'> 
									<label > T-membership:</label>
									<div class='field-title'><?php echo e($viewuser->tmembership); ?></div>
								</div>
								<div class='col-md-4'> 
									<label > P-membership:</label>
									<div class='field-title'><?php echo e($viewuser->pmembership); ?></div>
								</div>
							</div>
						</div>
						
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->


                    </div> <!-- container -->

                </div> <!-- content -->

        <?php $__env->stopSection(); ?>              
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/admin/admin/viewuser.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <span>
                                <h1>Comment Added</h1>
                            </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="card-box table-responsive">
                                <!-- <h4>Download</h4> -->
                                <a href="<?php echo e(route('admin.sa')); ?>"><button class='btn btn-success'>Go Back</button></a>
                                <!-- <button class="btn btn-success waves-effect waves-light m-b-5"> <i class="fa fa-file-excel-o m-r-5"></i> <span>Excel</span> </button>
                                <button class="btn btn-warning waves-effect waves-light m-b-5"> <i class="fa fa-file-pdf-o m-r-5"></i> <span>PDF</span> </button><BR>&nbsp; -->
                                <div class='text-center'>
                                    <h3 class="text-center text-success">PlusCircleClub</h3>
                                        <br/>
                                        <h2><?php echo e('Super Admin'); ?>          <?php echo e(Carbon\Carbon::now()); ?> </h2>

                                        <hr />
                                        <h4><span><?php echo e($user->name); ?></span>'s Comments</h4>

                                        <?php if(count($comments) > 0): ?>
                                        <?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($comment->comment); ?>             <?php echo e($comment->date); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                        <?php echo e('no comments'); ?>

                                        <?php endif; ?>
                                </div>
                            <div>
                        </div>
                        <div class='row'>
                            <hr />
            <h4>Add comment</h4>
            <form method="post" action="<?php echo e(route('comments.update')); ?>">
                <?php echo csrf_field(); ?>
               <div class="form-group">
                    <textarea class="form-control" id="comment" name="comment"></textarea>
                    <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>" />
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success comment" value="Send Comment" />
                </div>
            </form>
                        </div>
                    </div>

                            </div>
                        </div><!-- end col -->
                    </div>
                    <!-- end row -->


                </div> <!-- container -->

            </div> <!-- content -->
    <?php $__env->startPush('scripts'); ?>


    <?php $__env->stopPush(); ?>



    <?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/admin/admin/commentsupdate.blade.php ENDPATH**/ ?>
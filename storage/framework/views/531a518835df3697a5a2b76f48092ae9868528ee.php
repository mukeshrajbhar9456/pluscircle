	<?php $__env->startSection('content'); ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
						<div class="row">
                            <div class="col-sm-6">
								<span>
									<h1>Create New User</h1>
								</span>
							</div>
							<div class="col-sm-6 text-right">
								<a href="<?php echo e(URL::previous()); ?>"><button class='btn btn-success'>Go Back</button></a>	
							</div>
						</div>
						<form role="form" method="POST" action="<?php echo e(route('admin.saveNewUser')); ?>" name="loginForm" class="login-form">
							   
                  		<?php echo e(csrf_field()); ?>

                        <div class="row form">
                            <div class='col-sm-6 create-user'> 
								<div class='col-sm-4 '>
									<label> Name:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='name' />
								</div>
							</div>
							<div class='col-sm-6 create-user'> 
								<div class='col-sm-4'>
									<label> User type:</label>
								</div>
								<div class='col-sm-8'>
									<select name="usertype" >
										<?php $__currentLoopData = $usertype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ut): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($ut->usertype); ?>" ><?php echo e($ut->usertype); ?></option> 
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</select>
								</div>
							</div>
							<div class='col-sm-6 create-user'> 
								<div class='col-sm-4 '>
									<label> E-mail (Primary):</label>
								</div>
								<div class='col-sm-8 '>
									<input type='email' name='email' />
								</div>
							</div>
							<div class='col-sm-6 create-user'> 
								<div class='col-sm-4 '>
									<label> E-mail (Secondary):</label>
								</div>
								<div class='col-sm-8 '>
									<input type='email' name='email' />
								</div>
							</div>
							<div class='col-sm-6 create-user'> 
								<div class='col-sm-4 '>
									<label> Phonecode:</label>
								</div>
								<div class='col-sm-8 '>
								<select name="phonecode" id="phonecode" >														
									<?php $__currentLoopData = $countrycode; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($cc->phonecode==91): ?>
											<option value="<?php echo e($cc->phonecode); ?>" selected><?php echo e($cc->phonecode); ?></option> 
										<?php else: ?>
											<option value="<?php echo e($cc->phonecode); ?>"><?php echo e($cc->phonecode); ?></option> 
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
								</div>
							</div>
							<div class='col-sm-6 create-user'> 
								<div class='col-sm-4 '>
									<label> Phone:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='email' />
								</div>
							</div>
							<div class='col-sm-6 create-user'> 
								<div class='col-sm-4 '>
									<label> Secondary Phonecode:</label>
								</div>
								<div class='col-sm-8 '>
								<select name="sphonecode" id="sphonecode" >														
									<?php $__currentLoopData = $countrycode; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($cc->phonecode==91): ?>
											<option value="<?php echo e($cc->phonecode); ?>" selected><?php echo e($cc->phonecode); ?></option> 
										<?php else: ?>
											<option value="<?php echo e($cc->phonecode); ?>"><?php echo e($cc->phonecode); ?></option> 
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
								</div>
							</div>
							<div class='col-sm-6 create-user'> 
								<div class='col-sm-4 '>
									<label> Secondary Phone:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='sphone' />
								</div>
							</div>
							<div class='col-sm-6 create-user'> 
								<div class='col-sm-4 '>
									<label> Referred By:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='email' />
								</div>
							</div>
							<div class='col-sm-6 create-user'> 
								<div class='col-sm-4 '>
									<label> Payment:</label>
								</div>
								<div class='col-sm-8 '>
									<select name="usertype" >
										<option value="" >select</option> 
										<option value="0" >waive the fee</option> 
									</select>
								</div>
							</div>
							<div class='col-sm-6 create-user'> 
								<div class='col-sm-8 '>
									<input type="checkbox" checked data-plugin="switchery" data-color="#00b19d" data-size="small"/> Email auto-verified<BR>&nbsp;<BR>
								</div>
							</div>
							<div class='col-sm-6 create-user'> 
								<div class='col-sm-4 '>
									<label> T-membership</label>
								</div>
								<div class='col-sm-8 '>
									<select name="usertype" >
										<option value="" >select</option> 
										<option value="1" >Generate Membership</option> 
									</select>
								</div>
							</div>
							<div class='col-sm-12 create-user text-center submit-button'> 
								<button class='btn btn-success'> Create New User</button>
							</div>							
						</div>
						
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
        <?php $__env->stopSection(); ?>              
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/admin/admin/createNewUser.blade.php ENDPATH**/ ?>
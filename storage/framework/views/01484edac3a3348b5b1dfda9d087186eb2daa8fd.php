	<?php $__env->startSection('content'); ?>	
		<section class="cms-page-main">
			<div class="container mb-28">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Events</p><br>
					<h4 class="ybh">Events</h4>
					
					<hr>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="cms-content">
							<p>We at Plus Circle understand the power of networking and the need for socializing.</p><p>

							We organize various events of a small and big gathering in different venues and locations. At the same time, we also provide an equal pulpit to our respective members to grow together. </p><p>

							We conduct various events; Sports, Lunch & Dinner get-together, Dance party, Social gathering to meet and accost new people, Business meets, Cultural and other shows, family meet with fun games and many more.  </p><p>

							Live and virtually; getting societies composed to shape communities beautiful.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/events.blade.php ENDPATH**/ ?>
	<?php $__env->startSection('content'); ?>
		<section>
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Socialize just what you needed</p><br>
					<h4 class="ybh"> Socialize just what you needed</h4>
					
					<hr>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="event-activites-head">
							<p>We believes in developing the next generation of leaders. Our programs help younger leaders build leadership skills, expand education and learn the value of service.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="event-activites-section1">
			<div class="container ">
				<div class="row mb-40" id="event5">
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/socialize/Cycling.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Cycling</h4>
								<p>We propose to ride in Bangaalore City every Month 01st morning starting at 5:45 AM . The circular route will be a comfortable 20 kms and gives us a good practice opportunity.</p>
								<div class="learn-link">

									<a data-target="#myModalEventsCycling" data-toggle="modal" href="#myModalEventsCycling" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/socialize/Plus-circle-marathon.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Plus Circle Marathon</h4>
								<p>Being a virtual event, you are free to run wherever you want, whenever you want without being bound by any time restrictions. No matter whether you walk, run, skip</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsPlusCircle" data-toggle="modal" href="#myModalEventsPlusCircle" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/socialize/Foodie-fest.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Foodie Fest</h4>
								<p>Get set to experience finest food-walk dotted with rich culture and gastronomic delights.
								As the markets come alive, we surprise your taste buds </p>
								<div class="learn-link1">
									<a data-target="#myModalEventsFoodie" data-toggle="modal" href="#myModalEventsFoodie" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row mb-40" id="event6">
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/socialize/yoga.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Yoga</h4>
								<p>India is for seekers who wish to explore Yoga in its most authentic and purest form. Our course is a one of its kind, month-long, complete immersion course ideal 
								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsYoga" data-toggle="modal" href="#myModalEventsYoga" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>


					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/socialize/beach-side-camp.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Beach Side Camp</h4>
								<p>Wonderwall,  concept, with a new boutique beach festival all set to hit the stunning shores. The event will retain Goa’s natural beauty as its backdrop
								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsBeach" data-toggle="modal" href="#myModalEventsBeach" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>



					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/socialize/Adventurous.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Adventurous </h4>
								<p>Head out for this adventurous trip to Dandeli from Bangalore and indulge in an array of activities. Experience the fun and thrill as you try river 
								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsAdventurous" data-toggle="modal" href="#myModalEventsAdventurous" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="center">
						 	<div class="pagination">
							  <a href="<?php echo e(route('event_activites_three',['event',''])); ?>">&laquo;</a>
							  <a class="active" href="<?php echo e(route('event_activites_three',['event',''])); ?>">1</a>
							  <a href="<?php echo e(route('event_activites_four',['event',''])); ?>" >2</a>
							  <a href="<?php echo e(route('event_activites_four',['event',''])); ?>">&raquo;</a>
							
						  	</div>
						</div>
						
					</div>

				</div>
						
			</div>
		</section>
		
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/event_activites_three.blade.php ENDPATH**/ ?>
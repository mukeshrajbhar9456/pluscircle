	<?php $__env->startSection('content'); ?>
		<section>
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Socialize just what you needed</p><br>
					<h4 class="ybh"> Socialize just what you needed</h4>
					
					<hr>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="event-activites-head">
							<p>We believes in developing the next generation of leaders. Our programs help younger leaders build leadership skills, expand education and learn the value of service.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="event-activites-section1">
			<div class="container ">
				<div class="row mb-40" id="event7">
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/socialize/plus-circle-fashion.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Plus Circle Fashion </h4>
								<p>We organize biggest trade fair for jewelry, fashion and accessories and offers an independent appreciation of all products, from diamonds to fur coats
								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsFashion" data-toggle="modal" href="#myModalEventsFashion" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>


					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/socialize/piligrimage events.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Pilgrimage Events </h4>
								<p>It is specially designed for the one who has just stepped into meditation and spirituality.


									This Short Term Meditation is one of the best Meditation Programs in India,  
								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsPilgrimage" data-toggle="modal" href="#myModalEventsPilgrimage" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>



					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/socialize/Racing.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Racing </h4>
								<p>There’s more action happening in the Indian Motorsport scenario than ever before. Youngsters are leaping forward in making India a significant and a promising 
								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsRacing" data-toggle="modal" href="#myModalEventsRacing" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="row mb-40" id="event8">

					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/socialize/Trekking.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Trekking </h4>
								<p>It is the 10th highest peak in Karnataka, and has been declared a natural heritage sight by the government of Karnataka. The name Kodachadri is derived
								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsTrekking" data-toggle="modal" href="#myModalEventsTrekking" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>


					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/socialize/Hill_Climb.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Hill Climb </h4>
								<p>Nandi Hills is now an eco-tourism destination. Get ready for a host of activities as a build up to an eventual Nandi Hills. The aim is to spread awareness on Nandi Hills 
								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsHill" data-toggle="modal" href="#myModalEventsHill" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">

						<div class="center">
						  	<div class="pagination">
							  <a href="<?php echo e(route('event_activites_three',['event',''])); ?>">&laquo;</a>
							  <a href="<?php echo e(route('event_activites_three',['event',''])); ?>">1</a>
							  <a class="active" href="<?php echo e(route('event_activites_four',['event',''])); ?>" >2</a>
							  <a href="<?php echo e(route('event_activites_four',['event',''])); ?>">&raquo;</a>
							
						  	</div>
						</div>
						
					</div>

				</div>
						
			</div>
		</section>
		
		<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/event_activites_four.blade.php ENDPATH**/ ?>
	<?php $__env->startSection('content'); ?>	
		<section class="why-pluscircle-banner">
			<img src="<?php echo e(asset('public/images/sec-b/B-Supporting hand.jpg')); ?>" alt="">
		</section>

		<section>
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Supporting Hand</p><br>
					<h4 class="ybh1" ></h4>
					
					<hr>
				</div>

				<div class="row">
					<div class="col-md-12">
						<p> We provide resources to "orphanages" who enable children sustain their physical, emotional and spiritual needs. We also try to build awareness of the despair faced by "orphan children" around the world and the ways in which their needs can be addressed. </p>
					
					</div>
				</div>
			</div>
		</section>

		<section class="why-pluscircle-img-content">
			<div class="container">
				<div class="row">
					<!-- <div class="col-md-4">
						<div class="why-pluscircle-left-img">
							<img src="<?php echo e(asset('public/images/supporting_hands_lead_pic.jpg')); ?>" alt="">
						</div>
					</div> -->
					<div class="col-md-4">
						<div class="list-event-activites1 videocss2">
							<div class="list-event-activites-content1">
								<video controls class="videocss1">
									<source src="<?php echo e(asset('public/images/videos/Supporting_Hand.mp4')); ?>"  type="video/mp4">
								</video>
							
							</div>
						</div>
					</div>
					<div class="col-md-8 mtm-35" style="">
						<p class="mb-10">Fostering development efforts that bring about help to individual homes and the children raised in them.</p>

						<p class="mb-10"> We at Plus Circle cater elderly’s physical and emotional health. Our mission is to provide best in class senior care services for elderly to help them live happy, active and independent lives, in old age homes. Our aim is not only supporting them in their care needs, but also providing them with a valued friend and confidant.</p>

						<p class="mb-10"> We at Plus Circle cater elderly’s physical and emotional health. Our mission is to provide best in class senior care services for elderly to help them live happy, active and independent lives, in old age homes. Our aim is not only supporting them in their care needs, but also providing them with a valued friend and confidant.</p>

					</div>
				</div>
			</div>
		</section>
		
		<?php $__env->stopSection(); ?>	
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/bSupporting_Hand.blade.php ENDPATH**/ ?>
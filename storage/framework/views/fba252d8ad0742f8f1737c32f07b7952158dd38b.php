
<?php echo $__env->make('layouts.lp_head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>    
<?php echo $__env->make('layouts.lp_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>    
    <body>
            <?php echo $__env->yieldContent('content'); ?>
            <?php echo $__env->make('layouts.lp_footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
    </body>
</html>
<?php /**PATH C:\xamppnew\htdocs\plus-circle\public_html\resources\views/layouts/lp.blade.php ENDPATH**/ ?>
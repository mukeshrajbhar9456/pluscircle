	<?php $__env->startSection('content'); ?>	
	
		<section class="login-section">
			<div class="container">
				<div class="row">
					<div class="col-md-6 offset-md-3">
						<div class="login-form">
							 <form  id="signupForm" method="POST" name="signupForm" action="register "> 
							<!--<form  id="signupForm" name="signupForm" >-->
							<?php echo e(csrf_field()); ?>

							<!-- 'name','title','email','password','phone','role','gender','profile','ccode','ccountry','cstate','ccity','isactive','membershiptype','paymenttype','membership','refferedby' -->
							<!-- 	  <input type="hidden" name="_token" value="owoCYQIE17FoveSRmTFRTumCvO7Wx6hTt1E4FLMu"> -->   
								<div class="form-title">
									<h4>Sign up as new member</h4>
								</div>

								 <div class="col-md-12 center">
								 
					                 <div class="alert1 alert-danger1 ">
					                     <?php if($errors->any()): ?>
					                     <?php echo e(implode(' ', $errors->all(':message'))); ?>

					     <!--                &#x26A0; -->
									 <!--Sorry! Entered mobile or email id is already exist. -->
					     <!--            	Register with another mobile or email id. Else retrieve your password by entering same credentials.-->
					                 	<?php endif; ?>
					                 </div>
					             </div>
								<div class="form-bg">
									<div class="floating-form">

									<div class="gender-name">
                                        <div class="gender-box">
                                            <div class="floating-label">  
                                                <select class="floating-select" name="title" id="title" >
												<?php if( old("title") == 'Mr.'): ?>
                                                    <option value="Mr." selected>Mr.</option>
												<?php else: ?>
													<option value="Mr.">Mr.</option>
												<?php endif; ?>
												<?php if( old("title") == 'Mr.'): ?>
													<option value="Ms." selected>Ms.</option>
												<?php else: ?>
													<option value="Ms.">Ms.</option>
												<?php endif; ?>
												<?php if( old("title") == 'Mr.'): ?>
													<option value="Mrs." selected>Mrs.</option>
												<?php else: ?>
													<option value="Mrs.">Mrs.</option>
												<?php endif; ?>                                                   
                                                    
                                                </select>
                                                <span class="highlight"></span>
                                            <!--     <label>Gender</label> -->
                                            </div>
                                        </div>
                                        <div class="name-box">
                                            <div class="floating-label">      
                                              <input class="floating-input <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" type="text" name="name" value="<?php echo old('name'); ?>" id="name" placeholder="Enter name">
                                              <span class="highlight"></span>
                                            <!--   <label>Enter name <span class="astrick">*</span></label> -->
												<label id="name-error" class="error" for="name"></label>
											</div>
                                        </div>
									</div>
									
									<div class="floating-label"> 									
										
										<select class="floating-select" name="ccountry" id="country_id">
											<option value="">Select Country</option>
											<?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ctr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php if( old("ccountry") == $ctr->id): ?>
													<option value="<?php echo e($ctr->id); ?>" selected ><?php echo e($ctr->name); ?></option>
												<?php else: ?>
													<?php if($ctr->id == 101): ?>
														<option value="<?php echo e($ctr->id); ?>" selected><?php echo e($ctr->name); ?></option>
													<?php else: ?>
														<option value="<?php echo e($ctr->id); ?>" ><?php echo e($ctr->name); ?></option>
													<?php endif; ?>
												<?php endif; ?>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>										
										</select>
										<span class="highlight"></span>
										<!-- <label>Title</label> -->
										</div>
										
										
										<div class="gender-name">
											<div class="gender-box">
												<div class="floating-label"> 												
													<select class="floating-select" name="phonecode" id="phonecode" >														
														<?php $__currentLoopData = $countrycode; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<?php if( old("phonecode") == $cc->phonecode): ?>														
																<option value="<?php echo e($cc->phonecode); ?>" selected><?php echo e($cc->phonecode); ?></option> 
															<?php else: ?>
																<?php if($cc->phonecode==91): ?>
																	<option value="<?php echo e($cc->phonecode); ?>" selected><?php echo e($cc->phonecode); ?></option> 
																<?php else: ?>
																	<option value="<?php echo e($cc->phonecode); ?>"><?php echo e($cc->phonecode); ?></option> 
																
																<?php endif; ?>
															<?php endif; ?>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</select>
													<span class="highlight"></span>
												<!--     <label>Gender</label> -->
												</div>
											</div>
											<div class="name-box">
												<div class="floating-label">      
												<input class="floating-input <?php $__errorArgs = ['phone'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo old('phone'); ?>" type="text" name="phone" maxlength="10"placeholder="Enter mobile number * " id="phone">
												<span class="highlight"></span>
											<!-- 	<label>Enter mobile number <span class="astrick">*</span></label> -->
													
														<label id="phone-error" class="error" for="phone"></label>
													
												</div>
											</div>
										</div>

										<!-- <div class="floating-label mobile-div">      
									    	<input class="floating-input <?php $__errorArgs = ['phone'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('phone')); ?>" type="text" name="phone" maxlength="10"placeholder=" " id="phone">
											<span class="highlight"></span>
											<label>Enter mobile number <span class="astrick">*</span></label>
												<?php $__errorArgs = ['phone'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
													<label id="mobile-error" class="error" for="phone"><?php echo e($message); ?></label>
												<?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
									    </div> -->

									    <!-- <div class="floating-label email-div" >       -->
									    <div class="floating-label " >      
											<input class="floating-input <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" type="text" value="<?php echo old('email'); ?>" name="email" id="email" placeholder="Enter email address * ">
											<span class="highlight"></span>
											<!-- <label>Enter email address <span class="astrick">*</span></label> -->
											
												<label id="email-error" class="error" for="email"></label>
											
										 </div>

										 

										<div class="floating-label">      
									    	<input class="floating-input <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"  name="password" type="password" id="password"placeholder="Enter password *">
									    	<span class="highlight"></span>									    	
										<!-- 	<label>Enter password <span class="astrick">*</span></label> -->
											
												<label id="password-error" class="error" for="email"></label>
											
									    </div>
									    <div class="floating-label">      
									    	<input class="floating-input" type="password" name="password_confirmation" id="password-confirm" placeholder="Re-enter password *">
									    	<span class="highlight"></span>
									    	<!-- <label>Re-enter password <span class="astrick">*</span></label> -->
									    	
									    </div>
									    <div class="floating-label">      
									    	<input class="floating-input" type="text" name="refferedby"  placeholder="Referred By " value="<?php echo old('refferedby'); ?>" id="refferedby">
									    	<span class="highlight"></span>
									    	
									    	    <label id="refferedby-error" class="erroracceptu" for="for_refferedby"></label>
									       
									    	<!-- <label>Referred By</label> -->
									    </div>

									      <div class="floating-label">      
                                        <input type="checkbox" name="terms" id="terms" <?php echo old("terms") == 'on' ? 'checked' : ''; ?> >
                                        <span for="termcondition" style="font-size:14px;">I accept the <a href="<?php echo e(route('terms_of_services')); ?>" class="accept1">terms & conditions</a></span>
											
											
												<label id="terms-error" class="erroracceptu" for="terms"></label>
											
                                        
                                    </div> 	
									</div>
									<div class="submit-btn mt-20">
										 <input type="submit" value="Submit" id="signupFormBtn" name="submit">
									</div>
									<div id='loader' class='text-center' style="display:none;">
										<img class="gifplayer" src="<?php echo e(asset('public/images/sample.png')); ?>"
											style="width: 10%;"
											data-mode="video"
											data-mp4="<?php echo e(asset('public/images/ajax-loader.gif')); ?>" />

									  <div class="new-user">
                                        <a href="<?php echo e(route('user_login')); ?>">I'm a registered user</a>
                                    </div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/user_register.blade.php ENDPATH**/ ?>
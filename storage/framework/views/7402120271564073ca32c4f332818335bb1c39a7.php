	<?php $__env->startSection('content'); ?>	
		
		<section class="cms-page-main">
			<div class="container mb-28">

				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / About Us</p><br>
					<h4 class="ybh"> About Us</h4>
					
					<hr>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="cms-content">
							
							<p>Plus Circle is a social networking platform that helps people to build social networks or social relationships with other people who share similar personal or career interests, activities, backgrounds or real-life connections. We aspire to provide all of our members with the resources and opportunities they need to succeed because when the right people are in the right environment, great things happen.</p><p>

							People with more real life connections have more successful careers. Since our network helps them with information and support that provide career and business opportunities.</p><p>

							Our business networking platform helps members to reach out to potential customers without spending exorbitant amounts of money on advertising while staying in touch with current customers. Current customers can "check in" at your business whenever they visit, letting others know that they are there and spreading the word about your company. Having a presence on this popular social network suggests to people that your business is on the cutting edge or at least is able to keep up with the changing times. </p><p>

							Our professional networking will allow members to build their professional connections in a genuine way. We offer our members the opportunity to network, share contacts, and, most importantly, professional referrals.</p><p> 

							Our social network serves as a platform for talks, idea exchanges, and collaborations about various subjects. Its helps to get connected with local groups and events so our members can meet new people and try new things. These events are also an opportunity for fun, educational workshops, networking, sharing, skill-building, electing leadership, or for celebrating our accomplishments.</p><p>

							Beyond career, financial success and social events, our community also tries to contribute to society in every way. We work diligently to relieve suffering and better the lives of any and all people in need. Whether it raises voice against child abuse, child protection, and their education, or help the orphaned child and aged citizens or if its any kind of emergency, Plus Circle always provide support in every possible manner.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		
	<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xamppnew\htdocs\plus-circle\public_html\resources\views/users/about_us.blade.php ENDPATH**/ ?>
	<?php $__env->startSection('content'); ?>	
				<!-- main content start-->
				<div id="page-wrapper">
					<div class="main-page">
						<div class="tables">
							<h2 class="title1">Tables</h2>
							<div class="panel-body widget-shadow">
								<h4>SCCM Applications</h4>
								<table id="sccm_data" class="table table-hover table-bordere" width="100%">
								<thead>
									<tr>
										<th class="th-sm">Name</th>
										<th class="th-sm">Position</th>
										<th class="th-sm">State</th>
										<th class="th-sm">email</th>
										<th class="th-sm">Phone</th>
										<th class="th-sm">Action</th>
									</tr>
								</thead>
								<tbody>
								<?php $__currentLoopData = $applications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $app): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr>
									<td><?php echo e($app->name); ?></td>
									<td><?php echo e($app->position); ?></td>
									<td><?php echo e($app->state); ?></td>
									<td><?php echo e($app->email); ?></td>
									<td><?php echo e($app->phone); ?></td>
									<td><a href="<?php echo e(route('admin.view_application_data',['id' =>\Crypt::encrypt($app->id)])); ?>">View<a></td>
									</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
				
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/admin/admin/sccm_applications_view.blade.php ENDPATH**/ ?>
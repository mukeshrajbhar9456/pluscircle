	<?php $__env->startSection('content'); ?>	
		<section class="why-pluscircle-banner">
			<img src="<?php echo e(asset('public/images/sec-b/B-oppt-professio.jpg')); ?>" alt="">
		</section>

		<section>
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Professional Opportunity</p><br>
					<h4 class="ybh1"></h4>
					<hr>
				</div>

				<div class="row">
					<div class="col-md-12"><p>Experts agree that the most connected people are often the most successful. When you invest in your relationships - professional and personal - it can pay you back in dividends throughout the course of your career. </p>

					
					</div>
				</div>
			</div>
		</section>

		<section class="why-pluscircle-img-content">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="list-event-activites1 videocss2">
							<div class="list-event-activites-content1">
								<video controls class="videocss1">
									<source src="<?php echo e(asset('public/images/videos/Professional_Opportunity.mp4')); ?>"  type="video/mp4">
								</video>
							
							</div>
						</div>
					</div>
					<!-- <div class="col-md-4">
						<div class="why-pluscircle-left-img">
							<img src="<?php echo e(asset('public/images/sec-b/B-Professional opport.jpg')); ?>" alt="">
						</div>
					</div> -->
						<div class="col-md-8 mtm-35" style="">
							<p class="mb-10">Networking will help you develop and improve your skill set, stay on top of the latest trends in your industry, keep a pulse on the job market, meet prospective mentors, partners, and clients, and gain access to the necessary resources that will foster your career development.</p>

							<p class="mb-10">Experts agree that the most connected people are often the most successful. When you invest in your relationships - professional and personal - it can pay you back in dividends throughout the course of your career. Networking will help you develop and improve your skill set, stay on top of the latest trends in your industry, keep a pulse on the job market, meet prospective mentors, partners, and clients, and gain access to the necessary resources that will foster your career development.</p>
					</div>
				</div>
			</div>
		</section>
		
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/bOpportunities_professionals.blade.php ENDPATH**/ ?>
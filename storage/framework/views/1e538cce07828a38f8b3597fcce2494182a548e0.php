	<?php $__env->startSection('content'); ?>	
		<section class="why-pluscircle-banner">
			<img src="<?php echo e(asset('public/images/sec-b/B-health insu.jpg')); ?>" alt="">
		</section>

		<section>
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Health Insurance</p><br>
					<h4 class="ybh1" ></h4>
					
					<hr>
				</div>

				<div class="row">
					<div class="col-md-12"><p>It is rightly said that health is wealth. Changing lifestyle habits, increase in pollution levels, and many other factors have a severe impact on an individual’s health. This may cause various health conditions and medical diseases.
					</p>
					</div>
				</div>
			</div>
		</section>

		<section class="why-pluscircle-img-content">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="list-event-activites1 videocss2">
							<div class="list-event-activites-content1">
								<video controls class="videocss1">
									<source src="<?php echo e(asset('public/images/videos/Health_Insurance.mp4')); ?>"  type="video/mp4">
								</video>
							
							</div>
						</div>
					</div>
					<!-- <div class="col-md-4">
						<div class="why-pluscircle-left-img">
							
							<img src="<?php echo e(asset('public/images/sec-b/B-Health-insur.jpg')); ?>"alt="">
						</div>
					</div> -->
					<div class="col-md-8 mtm-35">
						<p class="mb-10">Medical costs associated with the treatment of such diseases are increasing rapidly. In order to safeguard your finances against high medical bills, it is necessary to invest in health insurance plans. Such plans cover all hospitalization expenses, as well as pre-hospitalization and post-hospitalization charges.</p>

						<p class="mb-10">Plus Circle takes care of our valued members health also, We provide Health cover to all our members, which have multiple benefits like </p>

						<ul><li><p>Low cost of larger risk.</p>
						 
						</li><li><p>A higher number of people increases the plan advantages.</p>
						 
						</li><li><p>	Family protection is included.</p>
						 
						</li><li><p>Easy to claim.</p></ul>

					</div>
				</div>
			</div>
		</section>
		
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/bHealth_Insurance.blade.php ENDPATH**/ ?>
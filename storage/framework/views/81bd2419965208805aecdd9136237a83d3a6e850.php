
<?php echo $__env->make('layouts.admin_head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            <?php echo $__env->make('layouts.admin_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<?php echo $__env->make('layouts.admin_sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<?php echo $__env->yieldContent('content'); ?>
			<footer class="footer">
					2020 © Plus Circle Club.
			</footer>
		</div>
        <?php echo $__env->make('layouts.admin_footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </body>
</html><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/layouts/admin.blade.php ENDPATH**/ ?>
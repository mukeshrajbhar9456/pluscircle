	<?php $__env->startSection('content'); ?>
		<section class="why-pluscircle-banner">
			<img src="<?php echo e(asset('public/images/sec-b/B-Business oppt.jpg')); ?>" alt="">
		</section>
		
		<section>
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Business Opportunity</p><br>
					<!-- <h4 class="ybh">Business Opportunity</h4> -->
					<h4 class="ybh1" ></h4>
					<hr>
				</div>
				<div class="row">
					<div class="col-md-12">
						<p>We are a member focused business referral network that works. You will develop long lasting relationships with like-minded business professionals, create opportunities through referral marketing and grow your business skills.</p>
					</div>
				</div>
			</div>
		</section>

		<section class="why-pluscircle-img-content">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="why-pluscircle-left-img">
							<!-- <video controls class="videocss1">
								<source src="http://localhost/pccmain1d/public/images/videos/Why_Video2.mp4"  type="video/mp4">
							</video> -->

							<img src="<?php echo e(asset('public/images/sec-b/B-business oppt..jpg')); ?>"
							   alt="">
						</div>
					</div>
					<div class="col-md-8 mtm-35">
						<p class="mb-10">
						 Plus, Circle is a business online community that allows many people from different overlapping industries to participate. We bring professionals together and provide a forum to communicate, learn, exchange ideas, meet challenges and share best practices in key aspects of responsible and inclusive business We are uniquely positioned to support each member of our community to develop and promote their responsible and inclusive business strategy. 
						</p>
						<p class="mb-10">
						 Plus, Circle is a business online community that allows many people from different overlapping industries to participate. We bring professionals together and provide a forum to communicate, learn, exchange ideas, meet challenges and share best practices in key aspects of responsible and inclusive business We are uniquely positioned to support each member of our community to develop and promote their responsible and inclusive business strategy. 
						</p>
						<p class="mb-10">
						<b>“The lifetime value of a referred customer is 25% higher than that of other customers”.</b></p>
					</div>
				</div>
			</div>
		</section>
		
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/bBusiness_Opportunity.blade.php ENDPATH**/ ?>
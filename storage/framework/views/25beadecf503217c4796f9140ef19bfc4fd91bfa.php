<!-- kit address -->
<div id="myModelship_add" class="modal">
		<div class="modal-content" >
			<div class="modal-header">
				<center> 
					<h5 class="modal-title kitcss" id="exampleModalLabel">My PCC Kit is to posted at</h5>
				</center>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<form id="addressform" action="" action="POST" >
						<p class="heightp"><b>Registered Address:</b></p>
						<div class="floating-form">
							<div class="row">
								<div class="col-md-12">
									<div class="floating-label1">     
										<input class="floating-input" type="text" name='mname' value="<?php echo e(Auth::user()->name?Auth::user()->name:''); ?>" placeholder=" ">
										<span class="highlight"></span>
										<label class="blue">Name:</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="floating-label1">  
										<textarea class="floating-input1"type="text" name='address' placeholder=" "><?php echo e(Auth::user()->address?Auth::user()->address:''); ?></textarea>
										<span class="highlight"></span>
										<label class="blue">Address</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="floating-label1">  
										<select class="floating-select country" name='country' onclick="this.setAttribute('value', this.value);" value="">
											<option value="101" selected >India</option>
										</select>
										<span class="highlight"></span>
										<label class="blue">Country</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="floating-label1">  
										<select class="floating-select state" id='state' name='state' onclick="this.setAttribute('value', this.value);" value="">
											<option value="12" class='101' >Gujarat</option>
											<option value="22" class='101' >Maharashtra</option>
											<option value="32" class='101' >Punjab</option>
											<option value="35" class='101' selected >Tamil Nadu</option>
										</select>
										<span class="highlight"></span>
										<label class="blue">State</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="floating-label1">
										<input class="floating-input" type="text" name='city' placeholder=" " value="<?php echo e(Auth::user()->ccity?Auth::user()->ccity:''); ?>">  
										<span class="highlight"></span>
										<label class="blue">City</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="floating-label1">       
										<input class="floating-input" type="text" name='pincode' placeholder=" " value="<?php echo e(Auth::user()->pincode?Auth::user()->pincode:''); ?>">
										<span class="highlight"></span>
										<label class="blue">Pin Code</label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="floating-label1">     
										<input class="floating-input" type="text" name='poc' placeholder=" " value="<?php echo e(Auth::user()->name?Auth::user()->name:''); ?>" >
										<span class="highlight"></span>
										<label class="blue">Contact Person</label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="floating-label1">     
										<input class="floating-input" type="text" name='phone' placeholder=" " value="<?php echo e(Auth::user()->phone?Auth::user()->phone:''); ?>">
										<span class="highlight"></span>
										<label class="blue">Mobile No </label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="floating-label1">     
										<input class="floating-input" type="email" name ='email' placeholder=" " value="<?php echo e(Auth::user()->email?Auth::user()->email:''); ?>">
										<span class="highlight"></span>
										<label class="blue">Email Id</label>
									</div>
								</div>
								<div class="col-md-12 div12"><hr></div>
								<div class="col-md-1 width-10per">	
									<div class="profile-name1" >
										<input type="radio"  name='same_as_above' id='same_as_above' checked="checked"class="floating-input inputc2">
									</div>
								</div>
								<div class="col-md-10 width-90per">	
									<div class="profile-name1" >
										<h2> 
										<b>
										Same As Above</b></h2>
									</div>
								</div>
								<div class="col-md-12">
									<div class="floating-label1">       
										<input class="floating-input" type="text" name='mname' id='mname' placeholder=" ">
										<span class="highlight"></span>
										<label class="blue">Name:</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="floating-label1">     
										<textarea class="floating-input1"type="text" name='maddress' placeholder=" "></textarea>
										<span class="highlight"></span>
										<label class="blue">Address</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="floating-label1">  
										<select class="floating-select" name='mcountry' id='mcountry' onclick="this.setAttribute('value', this.value);" value="">
											<option value="101" selected >India</option>
										</select>
										<span class="highlight"></span>
										<label class="blue">Country</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="floating-label1">  
										<select class="floating-select" name='mstate' id='mstate' onclick="this.setAttribute('value', this.value);" value="">
											<option value="12" class='101' >Gujarat</option>
											<option value="22" class='101' >Maharashtra</option>
											<option value="32" class='101' >Punjab</option>
											<option value="35" class='101' selected >Tamil Nadu</option>
										
										</select>
										<span class="highlight"></span>
										<label class="blue">State</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="floating-label1">  
										<input class="floating-input" type="text" name='mcity' placeholder=" ">
										<span class="highlight"></span>
										<label class="blue">City</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="floating-label1">  
										<input class="floating-input" type="text" name='mpincode' placeholder=" ">
										<span class="highlight"></span>
										<label class="blue">Pin Code</label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="floating-label1">      
										<input class="floating-input" type="text" name='mpoc' placeholder=" ">
										<span class="highlight"></span>
										<label class="blue">Contact Person</label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="floating-label1">      
										<input class="floating-input" type="text" name='mphone' placeholder=" ">
										<span class="highlight"></span>
										<label class="blue">Mobile No </label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="floating-label1">      
										<input class="floating-input blue" type="email" name='memail' placeholder=" ">
										<span class="highlight"></span>
										<label class="blue">Email Id</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="submit-btn">
										<input type="submit" value="Submit" id='submitAddress'>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div> <?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/user/usermodals.blade.php ENDPATH**/ ?>
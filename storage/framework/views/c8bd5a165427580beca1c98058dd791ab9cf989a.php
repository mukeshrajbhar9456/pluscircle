	<?php $__env->startSection('content'); ?>	
		<section class="cms-page-main">
			<div class="container mb-28">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Our Vision</p><br>
					<h4 class="ybh">Our Vision</h4>
					
					<hr>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="cms-content">
							
							<p>There’s a child, born to parents who have been poor for generations, not allowed to educate themselves or their children, ostracised from main society for centuries. There’s another child, born to some parents, but left with none, thrown in a dumpster, or left in a park, to die or to survive on its own, no idea where to get food from, or even how to get food, nowhere to go, and no one to love him/her.</p><p>

							We at Pluscirlce, our vision is to give these underprivileged and orphan children a Future. </p><p>

							Our goal is to achieve quality education for every underprivileged and orphaned child and to serve towards this cause, we have plan to start an educational institute where such children would be given free education along with different activities and programmes which can help in overall development of children and in making them responsible citizens.</p><p>

							Our underlying belief is that if we help one child in receiving quality education, that child would help others thus propagating our work and bringing good to the society. </p><p>


							We believe elderly people are a resource, not a burden. Older people need to be cared about, not just cared for. </p><p>

							Older people are generally dependent, constantly in need of help and support in their everyday lives. But if we give them support, they stand more chance of remaining independent. </p><p>

							Our organization  has a plan to build an Old Age Home for neglected destitute elderly persons of hundred inmates, where elderly people, Widows, Handicap and Orphans will be sheltered with sufficient space, homely atmosphere.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/our-vision.blade.php ENDPATH**/ ?>
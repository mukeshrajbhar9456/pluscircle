	<?php $__env->startSection('content'); ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
						<div class="row">
                            <div class="col-sm-6">
								<span>
									<h1>Edit User</h1>
								</span>
							</div>
							<div class="col-sm-6 text-right">
								<a href="<?php echo e(route('admin.sa')); ?>"><button class='btn btn-success'>Go Back</button></a>
							</div>
						</div>
						<form role="form" method="POST" action="<?php echo e(route('admin.UpdateUser')); ?>" name="loginForm" class="login-form">

                            <input type="hidden" name="userid" value="<?php echo e($edituser->id); ?>">

                  		<?php echo e(csrf_field()); ?>

                        <div class="row form">
                            <div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> Name:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='name' value="<?php echo e($edituser->name); ?>" />
								</div>
							</div>
							<div class='col-sm-6 create-user'>
								<div class='col-sm-4'>
									<label> User type:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='usertype' value="<?php echo e($edituser->membershiptype); ?>" />
								</div>
							</div>
							<div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> E-mail (Primary):</label>
								</div>
								<div class='col-sm-8 '>
									<input type='email' name='email' value="<?php echo e($edituser->email); ?>" />
								</div>
							</div>
							
							
							<div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> Phone:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='phone' value="<?php echo e($edituser->phone); ?>"/>
								</div>
							</div>
							<div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> Secondary Phonecode:</label>
								</div>
								<div class='col-sm-8 '>
                                    <div class='col-sm-8 '>
                                        <input type='text' name='sphone' value="<?php echo e($edituser->secondaryphone); ?>"/>
                                    </div>
								</div>
							</div>
							
							<div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> Referred By:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='referance' value="<?php echo e($edituser->refferedby); ?>"/>
								</div>
							</div>
							
							<div class='col-sm-6 create-user'>
								<div class='col-sm-8 '>
									<input type="checkbox" checked data-plugin="switchery" data-color="#00b19d" data-size="small"/> Email auto-verified<BR>&nbsp;<BR>
								</div>
							</div>
							<div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> membershipid :</label>
								</div>

									<div class='col-sm-8 '>
                                        <input type='text' name='membership' value="<?php echo e($edituser->tmembership); ?>" readonly/>
                                    </div>

							</div>
							<div class='col-sm-12 create-user text-center submit-button'>
								<button class='btn btn-success'> Edit User</button>
							</div>
						</div>

                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
        <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/admin/admin/edituser.blade.php ENDPATH**/ ?>
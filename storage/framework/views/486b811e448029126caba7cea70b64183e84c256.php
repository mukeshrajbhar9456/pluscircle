	<?php $__env->startSection('content'); ?>	
		<section class="cms-page-main">
			<div class="container mb-28">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Video</p><br>
					<h4 class="ybh"> Video</h4>
					
					<hr>
				</div>





				<div class="row">
					<div class="col-md-4">
						<div class="list-event-activites1 videocss2">
							<div class="list-event-activites-content1">
								<video controls class="videocss1">
									<source src="<?php echo e(asset('public/images/videos/Why_Video2.mp4')); ?>"  type="video/mp4">
								</video>
								<h4>WHY PCC </h4>
							</div>
						</div>
					</div>


					<div class="col-md-4">
						<div class="list-event-activites1 videocss2">
							<div class="list-event-activites-content1">
								<video controls class="videocss1">
									<source src="<?php echo e(asset('public/images/videos/How_We_Do_Video.mp4')); ?>"  type="video/mp4">
								</video>
								<h4>How We Do </h4>
							</div>
						</div>
					</div>


					<div class="col-md-4">
						<div class="list-event-activites1 videocss2">
							<div class="list-event-activites-content1">
								<video controls class="videocss1">
									<source src="<?php echo e(asset('public/images/videos/Orphans_Video2.mp4')); ?>"  type="video/mp4">
								</video>

								<h4>ORPHANED</h4>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="list-event-activites1 videocss2">
							<div class="list-event-activites-content1">
								<video controls class="videocss1" >
									<source src="<?php echo e(asset('public/images/videos/Old_Age_Video2.mp4')); ?>"  type="video/mp4">
								</video>
								<h4>OLD AGE</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/video.blade.php ENDPATH**/ ?>
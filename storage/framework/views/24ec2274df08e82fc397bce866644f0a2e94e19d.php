	<?php $__env->startSection('content'); ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
						<div class="row">
                            <div class="col-sm-6">
								<h1>All Members</h1>
							</div>
							<div class="col-sm-6 text-right">
							<a href="<?php echo e(route('admin.createNewUser')); ?>"><button class='btn btn-success'>Create New User</button></a>	
							</div>
						</div>
                        <div class="row">
                            <div class="col-sm-12">
								<div class="card-box table-responsive">
									<div class="m-b-30">
										<form action="<?php echo e(route('search_members')); ?>" method="post" data-parsley-validate novalidate>
										    <?php echo e(csrf_field()); ?>

										<div class="col-lg-2">
											<input type="text" class="form-control" name="search" placeholder="Search" required/>
										</div>
										<div class="col-lg-3">
											<div class="input-daterange input-group" id="date-range">
											<input type="text" class="form-control" name="start" value="" placeholder='From' />
											<span class="input-group-addon bg-primary b-0 text-white">to</span>
											<input type="text" class="form-control" name="end" value="" placeholder='To'  />
											</div>
										</div>
										<div class="col-lg-2">										
											<select class="form-control" name='country' id='country'>
												<?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ctr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($ctr->id); ?>">
                                                    <?php echo e($ctr->name); ?>

                                                    </option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>        										
										</div>										
										<div class="col-lg-2">
											<select class="form-control" name='state' id='state'>

											</select>
										</div>
										<div class="col-lg-2">
											<select class="form-control" name='city' id='city'>

											</select>
										</div>
										<div class="col-lg-1">
											<button class="btn btn-primary waves-effect waves-light" type="submit" value='search'><i class="zmdi zmdi-search"></i></button>
											<button class="btn btn-default waves-effect waves-light" type="reset"><i class="zmdi zmdi-refresh"></i></button>
										</div>

										<div class="col-lg-12">&nbsp;</div>
										<div class="col-lg-2">
										    <select class="form-control" name='member_type'>
												
														<option value="allmember" selected >All Members</option>
													
														<option value="regmember" >Refered Members</option>
														<option value="dirmember" >Direct Members</option>
												
											</select>
										</div>
										<div class="col-lg-2">
										    <select class="form-control" name='member_status'>
												
														<option value="permanent" selected >Permanent </option>
													
														<option value="temporary" >Temporary </option>
														<option value="underprocess" >Under Process </option>
														<option value="cancelled" >Cancelled </option>
												
											</select>
										</div>
										<!--<div class="col-lg-2">-->
										<!--    <input type="checkbox" checked data-plugin="switchery" data-color="#00b19d" data-size="small"/> Direct members<BR>&nbsp;<BR>-->
										<!--</div>-->
										<!--<div class="col-lg-2">-->
										<!--    <input type="checkbox" checked data-plugin="switchery" data-color="#00b19d" data-size="small"/> Permanent  <BR>&nbsp;<BR>-->
										<!--</div>-->
										<!--<div class="col-lg-2">-->
										<!--    <input type="checkbox" checked data-plugin="switchery" data-color="#00b19d" data-size="small"/> Temporary<BR>&nbsp;<BR>-->
										<!--</div>-->
										<!--<div class="col-lg-2">-->
										<!--    <input type="checkbox" checked data-plugin="switchery" data-color="#00b19d" data-size="small"/> Under Process<BR>&nbsp;<BR>-->
										<!--</div>-->
										<!--<div class="col-lg-2">-->
										<!--    <input type="checkbox" checked data-plugin="switchery" data-color="#00b19d" data-size="small"/> Cancelled<BR>&nbsp;<BR>-->
										<!--</div>-->
										
										<!--<div class="col-lg-3">-->
										<!--	<input type="checkbox" checked data-plugin="switchery" data-color="#00b19d" data-size="small"/> All Members<BR>&nbsp;<BR>-->
										<!--	<input type="checkbox"  data-plugin="switchery" data-color="#00b19d" data-size="small"/> Direct members<BR>&nbsp;<BR>-->
										<!--	<input type="checkbox"  data-plugin="switchery" data-color="#00b19d" data-size="small"/> Referred members-->
										<!--</div>-->

										<!--<div class="col-lg-3">-->
										<!--	<input type="checkbox" checked data-plugin="switchery" data-color="#00b19d" data-size="small"/> Permanent  <BR>&nbsp;<BR>-->
										<!--	<input type="checkbox"  data-plugin="switchery" data-color="#00b19d" data-size="small"/> Temporary<BR>&nbsp;<BR>-->
										<!--	<input type="checkbox"  data-plugin="switchery" data-color="#00b19d" data-size="small"/> Under Process<BR>&nbsp;<BR>-->
										<!--	<input type="checkbox"  data-plugin="switchery" data-color="#00b19d" data-size="small"/> Cancelled-->
										<!--</div>-->
										</form>
									</div>
								</div>
							</div>
						</div>


                        <div class="row">
                            <div class="col-sm-12">
							
                                <div class="card-box table-responsive">
									<h4>Download</h4>
									<button class="btn btn-success waves-effect waves-light m-b-5"> <i class="fa fa-file-excel-o m-r-5"></i> <span>Excel</span> </button>
									<button class="btn btn-warning waves-effect waves-light m-b-5"> <i class="fa fa-file-pdf-o m-r-5"></i> <span>PDF</span> </button><BR>&nbsp;

                                    <table id="allmembers" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>DOJ</th>
                                                <th>Name</th>												
                                                <th>Memb.#</th>
												<th>DOP</th>
												<th>Intro</th>
                                                <th>BRS</th>
                                                <th> Kit fee</th>
                                                <th>Doc type</th>											
                                                <th>Action</th>
                                                <th>Source</th>
                                                <th>Country</th>
                                                <th>State</th>
                                                <th>City</th>
                                                <th>Sp. APP</th>
												<th>PP</th>                                                
                                                <th>Abuse</th>	
												<th>IP</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php if(count($user)): ?>
											<?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $u): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<tr>
											<td><?php echo e($u->created_at); ?></td>
											<td><?php echo e($u->name); ?></td>
											<td><?php echo e($u->tmembership); ?></td>
											<td><?php echo e($u->dop); ?></td>
											<td><?php echo e(($u->refferedby==''?'---':$u->refferedname)); ?></td>
											<td>BRS</td>
											<td><?php echo e((($u->amount/100)!=15000)?((($u->amount/100)>0)?$u->amount/100:''):''); ?></td>
											<!--<td><?php echo e($u->amount); ?></td>-->
											<td><?php echo e($u->kyctype); ?></td>
											<td>
											    <?php $userid=\Crypt::encrypt($u->id); ?>
												<a href='#' title='Login to DB'><i class="ti-key"></i></a>
												<a href='#' title='Add Comment'><i class="ti-comment"></i></a>
												<a href='<?php echo e(route('admin.edituser',['userview' => $userid])); ?>' title='Edit'><i class="ti-pencil"></i></a>
												<a href='<?php echo e(route('admin.deleteuser',['userview' => $userid])); ?>' onclick="return confirm('Are you sure you want to delete <?php echo e($u->name); ?> - <?php echo e($u->id); ?> user ?');" title='Delete'><i class="ti-trash"></i></a>
												<a href="<?php echo e(route('admin.viewuser',['userview' => $userid])); ?>" title='View'><i class="ti-eye"></i></a>
												<a href='<?php echo e(route('admin.viewcomment',['userview' => $userid])); ?>' title='Internal Note'><i class="ti-write"></i></a>
											</td>
											<td> <?php echo e(($u->refferedby==''?'Direct':'Reffered')); ?></td>
											<td><?php echo e($u->country); ?></td>
											<td><?php echo e($u->state); ?></td>
											<td><?php echo e($u->ccity); ?></td>
											<td><a href='<?php echo e(route('admin.approval',['userview' => $userid])); ?>' onclick="return confirm('Confirm to approvel this member ?');" title='Special Approval'>Sp. APP</a></td>
											<td><a href='#' title='Personal Profile'>PP</a></td>                                                
											<td><a href='#' title='Abuse Report'>Abuse</a></td>	
											<td>IP</td>

											</tr>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											
											<?php else: ?>
											<tr><td colspan="16">Not Data Available</td></tr>
											<?php endif; ?>


                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->


                    </div> <!-- container -->

                </div> <!-- content -->

          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#country').on('change', function () {
                    var idCountry = this.value;
                    $("#state").html('');
                    $.ajax({
                        url: "<?php echo e(url('api/fetch-states')); ?>",
                        type: "POST",
                        data: {
                            country_id: idCountry,
                            _token: '<?php echo e(csrf_token()); ?>'
                        },
                        dataType: 'json',
                        success: function (result) {
                            $('#state').html('<option value="">Select State</option>');
                            $.each(result.states, function (key, value) {
                                $("#state").append('<option value="' + value
                                    .id + '">' + value.name + '</option>');
                            });
                            $('#city').html('<option value="">Select City</option>');
                        }
                    });
                });
                $('#state').on('change', function () {
                    var idState = this.value;
                    $("#city").html('');
                    $.ajax({
                        url: "<?php echo e(url('api/fetch-cities')); ?>",
                        type: "POST",
                        data: {
                            state_id: idState,
                            _token: '<?php echo e(csrf_token()); ?>'
                        },
                        dataType: 'json',
                        success: function (res) {
                            $('#city').html('<option value="">Select City</option>');
                            $.each(res.cities, function (key, value) {
                                $("#city").append('<option value="' + value
                                    .id + '">' + value.name + '</option>');
                            });
                        }
                    });
                });
            });

        </script>
        <script>
            $(document).ready(function() {
              $('#allmembers').DataTable();
    //             $('#allmembers').DataTable( {
			 //       "order": [["desc"]]
			 //   } );
          } );


           </script>


        <?php $__env->stopSection(); ?>              
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/admin/admin/allmembers.blade.php ENDPATH**/ ?>
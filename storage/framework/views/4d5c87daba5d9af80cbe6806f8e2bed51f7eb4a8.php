	<?php $__env->startSection('content'); ?>	
		<section class="why-pluscircle-banner">
			<img src="<?php echo e(asset('public/images/sec-b/B-ER-Support.jpg')); ?>" alt="">
		</section>

		<section>
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Emergency Support</p><br>
					<h4 class="ybh1" ></h4>
					<hr>
				</div>

				<div class="row">
					<div class="col-md-12">
						<p> Our community is committed to serving humanity by integrating resources for people in need. We strive to provide immediate response to any issues related to an emergency or disaster. In case of any disaster or emergency, our community always try to give a support to every people on that time and also take various efforts to help them in every way. 
						</p>
					</div>
				</div>
			</div>
		</section>

		<section class="why-pluscircle-img-content">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="why-pluscircle-left-img">
							<img src="<?php echo e(asset('public/images/sec-b/B-ER-Support..jpg')); ?>"  alt="">
						</div>
					</div>
					<div class="col-md-8 mtm-35">
						<p class="mb-10">We strive to provide immediate response to any issues related to an emergency or disaster. In case of any disaster or emergency, our community always try to give a support to every people on that time and also take various efforts to help them in every way.</p>

						<p class="mb-10">We strive to provide immediate response to any issues related to an emergency or disaster. In case of any disaster or emergency, our community always try to give a support to every people on that time and also take various efforts to help them in every way.</p>

						
						<p class="mb-10">We strive to provide immediate response to any issues related to an emergency or disaster. In case of any disaster or emergency, our community always try to give a support to every people on that time and also take various efforts to help them in every way.</p>

					</div>
				</div>
			</div>
		</section>
		
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/bEmergency_Support.blade.php ENDPATH**/ ?>
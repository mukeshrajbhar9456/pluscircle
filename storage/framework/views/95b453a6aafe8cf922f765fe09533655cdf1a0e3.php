
	<?php $__env->startSection('content'); ?>	

		<section class="login-section">
			<div class="container">

				<div class="row">
				
					<div class="col-md-12">
						<div class="login-form">
							<div class="wizard">
				               
				               <form role="form" method="POST" action="<?php echo e(route('admin.sendResetLink')); ?>" name="loginForm" class="login-form">
							   		<?php echo e(csrf_field()); ?>

									   
							   		<div class="tab-content" id="main_form">
				                     <div class="tab-pane active" role="tabpanel" >
				                     	<div class="form-title">
				                     		<!-- <h4>Enter your mobile number or email id to login</h4> -->
				                     		<h4>Forgot Password?<br>
				                     		<!--  Enter your E-mail ID and reset. -->
				                     		</h4>
				                     	</div>
										 <div class="col-md-12 center">
					                         <p class="accept2 center">Enter your email and we'll send you resetlink to reset your password</p>
					                     </div>
										 <div class="col-md-12 center">
											<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<div class="alert1 alert-danger1 ">&#x26A0;
												<?php echo e($message); ?>

												</div>	
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</div>
										 <div class="col-md-12 center">
											<?php if(session()->has('message')): ?>
												<div class="alert1 alert-info1">&#x2714;
													<?php echo e(session('message')); ?>

												</div>
											<?php endif; ?>
											<?php if(session()->has('error')): ?>
												<div class="alert1 alert-info1">&#x26A0;
													<?php echo e(session('error')); ?>

												</div>
											<?php endif; ?>
										</div>

				                     	<div class="form-bg">
					                        <div class="floating-form">
											    <div class="floating-label">      
												  <input class="floating-input" type="text" name='email' placeholder="Enter E-mail id to reset password ">
												  <span class="highlight"></span>
											      <!-- <label>Enter mobile number or email id</label> -->
												  <!-- <label></label> -->
												  	<?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
														<label id="email-error" class="error" for="email"><?php echo e($message); ?></label>
													<?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
											    </div>
											</div>
					                        <div class="submit-btn">
												<input type="submit" name="next"  value="Next">
											</div>
											<!--<div class="new-user">-->
											<!--	<a href="<?php echo e(route('user_register')); ?>">Join as a new member</a>-->
											<!--</div>-->
										</div>
									</div>
								</div>
							</form>
				                    
				                     
				            </div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php $__env->stopSection(); ?>

		
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/admin/admin/user_forgotpassword.blade.php ENDPATH**/ ?>
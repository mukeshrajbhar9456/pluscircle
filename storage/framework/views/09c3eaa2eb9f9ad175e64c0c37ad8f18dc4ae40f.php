	<?php $__env->startSection('content'); ?>

		<section>
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Events with Elegence</p><br>
					<h4 class="ybh"> Events with Elegence</h4>
					
					<hr>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="event-activites-head">
							<p>We believes in developing the next generation of leaders. Our programs help younger leaders build leadership skills, expand education and learn the value of service.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="event-activites-section1">
			<div class="container ">
				<div class="row mb-40" id="event1">
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/events-elegance/Seminar-conference.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Seminars & Conferences<br>&nbsp;</h4>
								<p>The aim objective of this conference is to provide a world class platform to present and discuss all the latest research and results of scientists related Industrial, Production</p>
								<div class="learn-link">

									<a data-target="#myModalEventsSeminars" data-toggle="modal" href="#myModalEventsSeminars" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/events-elegance/Trade-Shows.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Trade Shows<br>&nbsp;</h4>
								<p>The Consumer Goods Expo India - VTF Organised by Plus Circle held on 03-05 Sep 2020. What's New in This Expo 1. Separate Halls for Different Product Categories.</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsTrade" data-toggle="modal" href="#myModalEventsTrade" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/events-elegance/Board-Meetings.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Board Meetings & Shareholder Meetings</h4>
								<p>A meeting of the Board of Directors (Board) of the Plus Circle was held at  Bangalore, Karnataka, India, beginning on October 15, 2019. Picture shows the Board meeting, </p>
								<div class="learn-link1">
									<a data-target="#myModalEventsBoard1" data-toggle="modal" href="#myModalEventsBoard1" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>	
				</div>


				<div class="row" id="event2">
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/events-elegance/Product-launch-events.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Product Launch Events<br>&nbsp;</h4>
								<p>One of the greatest secrets of a winning product launch is in building excitement and creating a demand for your product or service before it is readily available </p>
								<div class="learn-link">	<a data-target="#myModalEventsProduct" data-toggle="modal" href="#myModalEventsProduct" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/events-elegance/Appreciaion-Events.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Appreciation Events<br>&nbsp;</h4>
								<p>Community helpers are all around us and they go the extra mile to keep our school clean, safe and running smoothly. To help teach our students to value them, the students of KG 1</p>
								<div class="learn-link1">	<a data-target="#myModalEventsAppreciation" data-toggle="modal" href="#myModalEventsAppreciation" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="<?php echo e(asset('public/images/events-elegance/Retreat-Incentive-Programs.jpg')); ?>">
							</div>
							<div class="list-event-activites-content1">
								<h4>Executive Retreats and Incentive Programs</h4>
								<p>Business development and organizational planning are typically the topics of the agenda for these retreats, but equal weight is given to enjoyable activities as part of the original</p>
								<div class="learn-link1">	<a data-target="#myModalEventsExecutive" data-toggle="modal" href="#myModalEventsExecutive" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>	

					<div class="col-md-12">

						<div class="center">
						 	<div class="pagination">
							  <a href="<?php echo e(route('event_activites_one',['event',''])); ?>">&laquo;</a>
							  <a class="active" href="<?php echo e(route('event_activites_one',['event',''])); ?>">1</a>
							  <a href="<?php echo e(route('event_activites_two',['event',''])); ?>" >2</a>
							  <a href="<?php echo e(route('event_activites_two',['event',''])); ?>">&raquo;</a>
							
						 	</div>
						</div>
						
					</div>

				</div>
						
			</div>
		</section>
		
		
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/event_activites_one.blade.php ENDPATH**/ ?>
	<?php $__env->startSection('content'); ?>
		<section class="why-pluscircle-banner">
			<img src="<?php echo e(asset('public/images/community-img.jpg')); ?>" alt="">
		</section>



		<section class="">
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Networking </p><br>
					<h4 class="ybh1"></h4>
					<!-- <h4 class="ybh" >Social Networking</h4> -->
					<hr>
				</div>

				<div class="row">
					<div class="col-md-12">
						<p>We believe in networking because we understand the power of people. That's why our mission is to bring together successful, motivated professionals and help them become more successful by creating connections that take their careers to the next level and beyond!</p>
						
					</div>
				</div>
			</div>
		</section>

		<section class="why-pluscircle-img-content">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="list-event-activites1 videocss2">
							<div class="list-event-activites-content1">
								<video controls class="videocss1">
									<source src="<?php echo e(asset('public/images/videos/Social_Networking.mp4')); ?>"  type="video/mp4">
								</video>
							
							</div>
						</div>
					</div>
					<!-- <div class="col-md-4">
						<div class="why-pluscircle-left-img">
							<img src="<?php echo e(asset('public/images/sec-a/A1.jpg')); ?>"    alt="">
						</div>
					</div> -->
					<div class="col-md-8 mtm-35" style="">
						<p class="mb-10">We look forward to becoming a network of all things in your social network. We share resources and organize events to share the outstanding talents and provide you with the opportunity to connect with other like-minded professionals. We are very happy to see you and hope you consider becoming a member.</p>

						<p class="mb-10">Our social networking platform will help you to personalize your brand and build a true relationship with the community. It shortens the distance to the audience, especially when you speak to them using your personal account</p><p>
						We believe in networking because we understand the power of people. That's why our mission is to bring together successful, motivated professionals and help them become more successful by creating connections that take their careers to the next level and beyond!
						</p>
						
					</div>
				</div>
				
			</div>
		</section>
		
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/bSocial_Networking.blade.php ENDPATH**/ ?>
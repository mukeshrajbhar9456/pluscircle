   <?php $__env->startSection('content'); ?>  
      <section>
         <div class="container mb-40" >
            <div class="about-us-title ">
               <h4 class="ybh">Application for District Governor & District Chairman
               </h4>
               <hr>
            </div>

               <div class="row mb-150" >
                  <?php if(session()->has('error')): ?>
                  <div class="col-md-12 center">
                     <div class="alert1 alert-danger1 ">&#x26A0; 
                        <?php echo e(session('error')); ?>

                     </div>
                  </div>
                  <?php endif; ?>
              

               <div class="col-md-2 mw-13"></div>
               <div class="col-md-9 shadowpcc">
                  <div class="cms-content">
                     <div class="dashboard-profile dashboard-user-profilepcc" >
                        <form  id="passwordForm"  method="POST" name="passwordForm" action="<?php echo e(route('sccm_password')); ?>">
                        <?php echo e(csrf_field()); ?>

                     <!--    <div class="col-md-12 text-center">
                           <?php if(session()->has('error')): ?>
                              <div class="alert alert-danger">
                                 <?php echo e(session('error')); ?>

                              </div>
                           <?php endif; ?>
                        </div> -->
                           <input type='hidden' name='application' value='sccm' />
                              <div class="row">
                                 <div class="col-md-12 sccmcountry" style="" >
                                 <div class='row appcationtitle text-center'>
                                    <div class='col-md-12 center'>
                                    This application is only against invitation
                                    </div>
                                 </div>
                                    <div class="row">
                                       <div class="col-md-3">
                                          <p><span class="sccmstate0">Enter Password <span class="alert-danger1">*</span></p>
                                       </div>
                                       <div class="col-md-7">
                                          <div class="floating-label1">
                                             <input type='password' name='password' id='password' class="floating-selectpcc1" style="font-size:47px;" required >
                                             
                                          </div>
                                       </div>
                                    
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <div class="submit-btn">
                                             <input type="submit" value="submit " name="">
                                          </div>
                                       </div>
                                    </div>


                                    <div class='row appcationtitle text-center'>
                                       <div class='col-md-12 center'>
                                       For more information please contact at <a href="mailto:info@pluscircleclub.org">info@pluscircleclub.org</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </form>
                     </div>
                  </div>
               </div>
            <div  id="dgmf6"class="col-md-1 height-105" style="display:none; " ><a  data-target="#myModalfixedgovernor" data-toggle="modal" href="#myModalfixedgovernor"class="Div1"> <p class="sscmview">View <br>Your <br>Hirearchy</p></a></div>
            <div  id="dcmf6"class="col-md-1 height-105" style="display:none; " ><a  data-target="#myModalfixedchairman" data-toggle="modal" href="#myModalfixedchairman"class="Div1"> <p class="sscmview">View<br> Your <br>Hirearchy</p></a></div>
         </div>
      </div>
   </section>
      
      <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/application_sccm_password.blade.php ENDPATH**/ ?>
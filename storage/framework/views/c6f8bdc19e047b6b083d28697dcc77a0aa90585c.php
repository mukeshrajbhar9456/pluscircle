	<?php $__env->startSection('content'); ?>
		<section class="why-pluscircle-banner">
			<img src="<?php echo e(asset('public/images/sec-b/B-Your benefits.jpg')); ?>" alt="">
		</section>



		<section>
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="<?php echo e(url('/')); ?>">Home</a> / Scholarship</p><br>
					<h4 class="ybh1" ></h4>
					
					<hr>
				</div>

				<div class="row">
					<div class="col-md-12">
						<p> Members of our community can apply for community service scholarships. The Community Scholarship Program provides capable students with scholarship opportunities to achieve their educational goals and create a better future for themselves and for the society they live in. </p>
					</div>
				</div>
			</div>
		</section>

		<section class="why-pluscircle-img-content">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="list-event-activites1 videocss2">
							<div class="list-event-activites-content1">
								<video controls class="videocss1">
									<source src="<?php echo e(asset('public/images/videos/Scholarship_Program.mp4')); ?>"  type="video/mp4">
								</video>
							
							</div>
						</div>
					</div>
					<!-- <div class="col-md-4">
						<div class="why-pluscircle-left-img">
							<img src="public/images/sec-b/scholarp.jpg" alt="">
						</div>
					</div> -->
					<div class="col-md-8 mtm-35">
						<p class="mb-10">Supporting needy Children: Poverty can inhibit children’s ability to learn and contribute to social, emotional, and behavioral enigmas.</p>

						<p class="mb-10">Our community plays a vital role in this part. Members of our Nobel community tries to provide support to increase the educational opportunities of the needy children to change the living conditions of orphaned through education and economically empowering their families.</p>
					</div>
				</div>
			</div>
		</section>
		<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.lp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/m5zovgs33b9j/public_html/resources/views/users/bScholarships_Programs.blade.php ENDPATH**/ ?>
<?php
class Form_model extends CI_Model {
  
    public function __construct()
    {
        $this->load->database();
    }
     
     public function save_imagepath($data1)
    {
        $this->db->insert('userground_details', $data1);
         return $this->db->insert_id();
    }
    
    public function create($image=null)
    {
        $this->load->helper('url');
        $ground_name = $this->input->post('groundname');
        $ground_data = json_encode($ground_name);

        $city_id = $this->input->post('city');
        $groundcity_data = json_encode($city_id);

        $city_ids = $this->input->post('city1');
        $city_data = json_encode($city_ids);

        $date = $this->input->post('date');
        $date_data = json_encode($date);

        $data = array(
            'name' => $this->input->post('first_name'),
            'designation' => $this->input->post('designation'),
            'state_id' => $this->input->post('state'),
            'ground_name' => $ground_data,
            'ground_city' => $groundcity_data,
            'date' => $date_data,
            'city' => $city_data,
            'image' => $image,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ); 

        $user = $this->db->insert('userground_details', $data);
        // $data1 = array(
        //     'ground_name' => $this->input->post('groundname'),
        //     'city_id' => $this->input->post('city'),
        //     'ug_id' => $this->db->insert_id()
        // ); 
        //  $data2 = array(
        //     'date' => $this->input->post('date'),
        //     'city_id' => $this->input->post('city1'),
        //      'ug_id' => $this->db->insert_id()
        // );
        // $ground = $this->db->insert('has_ground', $data1);
        // $date = $this->db->insert('has_calender', $data2);
        if ($user) {
           return $this->db->insert_id();
        } else {
            return false;
        }
    }
}
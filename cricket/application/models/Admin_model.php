<?php
class Admin_model extends CI_Model
   {

   public function checkLogin($email,$password){
    $this->db->select('*');
    $this->db->from('users');
    $this->db->order_by("id", "desc");
    $this->db->where('user_emailid', $email);
    $this->db->where('user_password', $password);
    $this->db->where('user_type', 1);
    $query = $this->db->get();
    if($query->num_rows() > 0 )
    {
    	$adminData = $query->row_array();
        
    	$the_session = array("logged_in"=>array("id" =>$adminData['id'], "email" =>$adminData['user_emailid'],'username'=>$adminData['user_name']));
        $this->session->set_userdata($the_session); 
        return true;
    }else{
    	return false;;
    }
  }    
    


    public function insert_data($tablename,$data)
    {
            $this->db->insert($tablename,$data);
            if($this->db->affected_rows() > 0)
                {
                    
                    return true; 
                }else{
                    return false;
                }
    }
    public function get_all_data($tablename,$cond=null)
    {
        $this->db->select("*");
        $this->db->from($tablename);
        $this->db->order_by("id", "desc");
        if(!empty($cond)) :
        $this->db->where($cond);
        endif;
        $query = $this->db->get();        
        return $query->result();
    }

    public function update_data($tablename,$cond,$data){
            $this->db->where($cond);
            $this->db->update($tablename,$data);
            if($this->db->affected_rows() > 0)
                {
                    
                    return true; 
                }else{
                    return false;
                }
    }


    public  function delete_data($tablename,$condi){
                $this->db->where($condi);
                $this->db->delete($tablename);

    }
        public function get_field($tablename,$field,$condi)
        {
           $this->db->select($field);
    $this->db->from($tablename);
    $this->db->where($condi);
    $row = $this->db->get()->row();
    if ($row) {
        return $row->$field;
    } else {
        return false;
    } 
        }
    // public function show_data($tablename,$id,$colname){
    //         $this->db->select("*");    
    //         $this->db->from($tablename);
    //         $query = $this->db->get();        
    //         return $query->result();
    // }

           public function count_data($tablename,$cond=null,$like=null)
           {
                        $this->db->select("*");
                        $this->db->from($tablename);
                        if(!empty($cond)) :
                        $this->db->where($cond);
                        endif;
                        if(!empty($like)){
                            $this->db->like($like,'both');
                        }
                        $query = $this->db->get();        
                        return $query->num_rows();
                    //echo $query->num_rows();

                } 

   
}
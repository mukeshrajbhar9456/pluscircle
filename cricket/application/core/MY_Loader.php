<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_Loader extends CI_Loader {
    public function __construct()
    {
        parent::__construct();
    }
    public function template($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('templates/header', $vars, $return);
        $content .= $this->view('templates/left_menu', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('templates/footer', $vars, $return);

        return $content;
    else:
        $this->view('templates/header', $vars);
        $this->view('templates/left_menu', $vars);
        $this->view($template_name, $vars);
        $this->view('templates/footer', $vars);
    endif;
    }
}

// class MY_Controller extends CI_Controller 
//  { 
//    var $template  = array();
//    var $data = array();  
//    public function layout($layout=false) 
//    { 
//      $this->template['header']   = $this->load->view('Admin/Elements/header', $this->data, true);
//      $this->template['left']   = $this->load->view('Admin/Elements/sidebar', $this->data, true);
//      $this->template['middle'] = $this->load->view($this->middle, $layout, true);
//      $this->template['footer'] = $this->load->view('Admin/Elements/footer', $this->data, true);
//      $this->load->view('User/index', $this->template);
//    }
//  }
<?php
if(isset($_SESSION['logged_in']['id'])):
  header("Location:user/dashboard");
 endif; 
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Forgot Your Password?</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assests/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assests/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assests/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assests/css/AdminLTE.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assests/css/blue.css">
<style type="text/css">

	.error{

		color: #FF0000;

	}
  .login-logo{
    margin-bottom: 10px;
  }
  .login-logo img{
    width: 130px;
  }
.form-control::placeholder { 
   color: #93abef !important;
 
}

.form-control:-ms-input-placeholder { 
   color: #93abef !important;
}

.form-control::-ms-input-placeholder { 
   color: #93abef !important;
}
.icheck>label {
    color: #93abef !important;
}  
  .form-control{
    background: rgb(8 32 96 / 93%);
    height: 50px;
    border-radius: 5px;
     color: #93abef !important;
  }
  .form-control-feedback {
       top: 5px;    
    width: 40px;
    height: 40px;
    line-height: 40px;   
    font-size: 16px;
    color: #93abef !important;
}

.login-page{
  background-image: url('<?php echo base_url(); ?>assests/images/admin-bg.jpg');
  background-size: cover;
  background-position: bottom center;
  background-repeat: no-repeat;

}
.login-box{   
   background: rgb(8 32 96 / 93%);
    padding: 20px;
    color: #666;
    box-shadow: 0 15px 13px 0px rgb(0 0 0 / 30%);
    width: 30%;
    border: 1px solid #6a82be;

}
.login-box-body{
    background: transparent;
   }
.login-box-body .btn-primary {
   background-color: #ff6a05;
    border-color: #ff6a05;
    padding: 12px 0px;
    font-size: 18px;
    font-weight: 700;
    border-radius: 50px;
    outline: none;
}
.login-box-body .btn-primary:hover{
   background-color: #000;
    border-color: #000;
    outline: none;
}
.login-box-msg{
      color: #fff;
    font-size: 16px;
}
.fp-link a{
  color: #FF6A05;
  font-size: 18px;
  font-weight: 700;
}
@media screen and (max-width: 768px) {
  .login-box{   
       width: 90%;   
}
}
</style>

  <!-- Google Font -->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>

<body class="hold-transition login-page">

<div class="login-box">

  <div class="login-logo">
    <h2>Reset Password?</h2>
    <!-- <a href="#"><img src="https://www.pluscircleclub.org/+91cricket/images/cricket-logo.png"></a> -->

  </div>

  <!-- /.login-logo -->

  <div class="login-box-body">

   <!-- <p class="login-box-msg">Sign in admin panel</p>-->

     <?php if(validation_errors())

     { 

     echo '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.validation_errors().' </div>';

     

     }

      if($this->session->flashdata('msg')):?>

    <p id="msg"><?php echo $this->session->flashdata('msg'); ?></p>

    <?php endif;?>

     <?php echo form_open('admin/updatepass',array('class' => 'basketForm')); ?>
    <!-- <form  action="<?php echo base_url('admin/updatepass');?>" method="post" enctype="multipart/form-data" id="admin_login"> -->
      <div class="form-group has-feedback">
        <!-- <input type="email" class="form-control" placeholder="Email" name="email"> -->
       <?php  $data = array(
         'type'         => 'password',
          'name'        => 'password',
          'value'          => set_value('password'),
          'class'       => 'form-control',
          'placeholder'       => ' Password' 
        );
        echo form_input($data);?>
        <!--<span class="glyphicon glyphicon-envelope form-control-feedback"></span>-->
      </div>
     <div class="form-group has-feedback">
        <!-- <input type="email" class="form-control" placeholder="Email" name="email"> -->
       <?php  $data = array(
            'type'         => 'password',
          'name'        => 'cpassword',
          'value'          => set_value('cpassword'),
          'class'       => 'form-control',
          'placeholder'       => 'Confirm Password' 
        );
        echo form_input($data);?>
        <!--<span class="glyphicon glyphicon-envelope form-control-feedback"></span>-->
      </div>
      <div class="row">

        
        <!-- /.col -->

        <div class="col-xs-6">

          <button type="submit" class="btn btn-primary btn-block btn-flat" >Update Password</button>

        </div>

        <!-- /.col -->

      </div>
     

    </form>



  </div>

  <!-- /.login-box-body -->

</div>

<!-- /.login-box -->



<!-- jQuery 3 -->

<script src="<?php echo base_url(); ?>assests/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?php echo base_url(); ?>assests/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assests/js/jquery.validate.js"></script>

<script src="<?php echo base_url(); ?>assests/js/validations.js"></script>

<!-- iCheck -->

<script src="<?php echo base_url(); ?>assests/js/icheck.min.js"></script>

<script>

  $(function () {

    $('input').iCheck({

      checkboxClass: 'icheckbox_square-blue',

      radioClass: 'iradio_square-blue',

      increaseArea: '20%' /* optional */

    });

  });

</script>



</body>

</html>


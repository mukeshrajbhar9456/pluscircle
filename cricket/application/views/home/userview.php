<div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">     
      <ol class="breadcrumb">
        <li><a href="https://pluscircleclub.org/cricket/user/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User</li>
      </ol>
    </section>
   
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
        <!--   <div class="box-header">
            <h3 class="box-title"><a href="<?php echo base_url('post');?>">Add New Post</a></h3>
            </div> -->
            <!-- /.box-header -->
         <!--   <div class="col-md-6"> -->
          <!-- general form elements -->
           <div class="box jb-box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6 col-xs-6">
              <h3 class="box-title">All Users</h3>
              </div>
              <div class="col-md-6 col-xs-6 text-right">
              <?php echo  form_open('user/download_excel');?>
             <button type="submit" name="export" class="btn addp_btn"><i class="fa fa-download"></i> &nbsp; Export Excel</button> 
           </form>
              </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example1_wrapper" class="lb-table">
                <div class="row">
                  <div class="col-sm-12" style="overflow: auto;">
                  <table id="postview" class="table table-bordered table-striped example">
                        <thead>
                        <tr role="row">                  
                          <th class="sorting" tabindex="0" aria-controls="example1"  aria-sort="descending" aria-label="Rendering Engine: activate to sort column descending" style="">Sr. No</th>
                  <th class="sorting" tabindex="0" aria-controls="example1"  aria-label="Browser: activate to sort column descending" style="">User Registration Id</th>
              <th class="sorting " tabindex="0" aria-controls="example1"  aria-label="Browser: activate to sort column descending" style="">User Name</th>
              <th >User Number </th>
              <th  class="sorting " tabindex="0" aria-controls="example1"  aria-label="Browser: activate to sort column descending"  style="width:10px!important;">Email Id</th>
              
<th class="sorting" tabindex="0" aria-controls="example1"  aria-label="Platform(s): activate to sort column descending" style="">User Password</th>
<th class="sorting" tabindex="0" aria-controls="example1"  aria-label="Platform(s): activate to sort column descending" style="">Age Group</th>
<th class="sorting" tabindex="0" aria-controls="example1"  aria-label="Platform(s): activate to sort column descending" style="">State</th>
 <th class="sorting" tabindex="0" aria-controls="example1"  aria-label="Platform(s): activate to sort column descending" style="">Payment Status</th>
 <th class="sorting" tabindex="0" aria-controls="example1"  aria-label="Platform(s): activate to sort column descending" style="">Payment Amount</th>
 <th class="sorting" tabindex="0" aria-controls="example1"  aria-label="Platform(s): activate to sort column descending" style="">Registration Date</th>
 <th class="sorting" tabindex="0" aria-controls="example1"  aria-label="Platform(s): activate to sort column descending" style="">Player View</th>
 <th>Action</th>
 <th>Action</th>
 <th>Action</th>
  </tr>
      </thead>
<tbody>              

                           <?php 
                                $i=1;
// echo "<pre>";                              
// print_r($users);
  foreach ($users as $row => $value) {
                              ?>
                               <tr role="row" class="<?php if($i%2==0): echo'even';else: echo'odd';endif; ?>">
                                <td><?php echo $i; ?></td>
                      <td class="sorting_1"><?php echo $value->user_reg_id;?></td>
                      <td class="sorting_1"><?php echo $value->usermame;?></td>
                      <td class="sorting_1"><?php echo $value->user_phone;?></td>
                      <td class="sorting_1 needwrap"><?php echo $value->user_email;?></td>
                      <td class="sorting_1"><?php echo $value->user_password;?></td>
                      <td class="sorting_1"><?php $agegroup= $value->age_group;
                          echo $this->Admin_model->get_field('age_group','age_title',$condi =array('id' => $agegroup));

                      ?></td>
                      <td class="sorting_1"><?php $state_id= $value->user_state;
                        echo $this->Admin_model->get_field('state','name',$condi =array('id' => $state_id));
                      ?></td>
                     <!--  <td class="sorting_1"><?php echo $value->user_createdat;?></td>
 -->
                 <!--     <td><?php  /*$posttpyid=$value->post_type;

                             $posttypename=$this->Admin_model->get_all_data('post_type',$cond = array('post_type_id' =>  $posttpyid));

                             //print_r($posttypename);

                             foreach ($posttypename as $key => $postname) {

                               echo $postname->post_type_name;

                             }

                       */ ?></td> -->
                                        <?php /*if(!empty($value->post_image)) { ?>

                  <td class="sorting_1 text-center"><img src="<?php echo base_url();?>uploads/post/<?php echo $value->post_image;?>" width="50px" height="50px"></td>
                 <?php  } else{ ?>
<td class="sorting_1 text-center"><?php echo 'no Image';  ?></td>
                 <?php } */?>
                 <td>  <?php  if($value->payment_status ==1){echo 'Paid';}else{echo 'Pending';}?></td>
                 <td><?php $user_id= $value->id;
                       $amt=$this->Admin_model->get_field('payment','amount',$condi =array('user_id' => $user_id));
                      if(!empty($amt)){
                      //setlocale(LC_MONETARY, 'en_IN');
                        echo $t=number_format($amt,2);
                      }else{echo '00.00';}
                      ?></td>
                 <td>

                 <div id="status">
                  <?php 
                  //echo $date=$value->created_at;
                  echo date('d M Y', strtotime($value->created_at)).' , '.date("h:i:s",strtotime($value->created_at));
                  /* if($value->status ==1): ?>
                  <a href="#" onclick="editstatus(<?php echo $value->id; ?>,0);" class="lb-active"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                  <?php else: ?>
                     <a href="#" onclick="editstatus(<?php echo $value->id; ?>,1);" class="lb-deactive"><i class="fa fa-ban"></i></a>
                  <?php endif; */?>
                </div>
                 </td>
                 
                  <td class="sorting_1"><?php
  ?>
 <a href="<?php echo base_url('user/player/');echo base64_encode($value->id);?>" class="btn btn-success"><center><i class="fa fa-eye"></i></center></a></td>
 <td><a href="<?php echo base_url('user/delete/');echo base64_encode($value->id);?>" onClick="return confirmDelete(this);" class="btn btn-danger"><center><i class="fa fa-trash"></i></center></a></td>
 <td>
<a href="<?php echo base_url('user/edit/');echo base64_encode($value->id);?>" onClick="return confirmEdit(this);" class="btn btn-primary"><center><i class="fa fa-pencil-square-o"></i></center></a></td>

<td><a href="<?php echo base_url('user/notepad/');echo base64_encode($value->id);?>" onClick="return confirmNotepad(this);" class="btn btn-info"><center><i class="glyphicon glyphicon-file"></i></center></a></td>


                </tr>
                        <?php    

                          $i++;

                      } ?>

                </tbody>
               
              </table>
              
            </div>
            </div>
            </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->
<!--<script type="text/javascript">-->
<!--   $(document).ready(function() {-->
<!--    $('.example').DataTable( {-->
<!--        "order": [[ "desc" ]]-->
<!--    } );-->
<!--} );-->
<!--</script>-->



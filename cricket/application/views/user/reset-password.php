<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header ds-header">
      <div class="row">
        <div class="col-md-12"><ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reset Password</li>
      </ol>
    </div>
    </div>
    </section>
    <section class="content rest-pass">
      <div class="container">
        <div class="row">
           <div class="col-md-12">
              <div class="rest-form">
                <div class="rest-form-header">
                  <img src="<?php echo base_url(); ?>assests/images/reset-password.png">
                  <h2>Reset Password</h2>
                </div>
                <div class="rest-form-body">
                 <!--  <form action="#" enctype="multipart/form-data" method="post" accept-charset="utf-8">
 -->
          <!--   <form role="form"> -->
        <?php
                  if(validation_errors())
                    { 
                         echo '<div class="alert alert-danger"><strong>Danger! </strong>'.validation_errors().'</div>';
                    }
         ?> 
        <?php if(isset($success)){
          echo '<div class="alert alert-success">
  <strong>Success! </strong>'.$success.'
</div>';
}else{
  if(isset($error)){
    echo '<div class="alert alert-danger">
  <strong>Danger! </strong>'.$error.'
</div>';
  }
}
 ?>
    <?php echo form_open("admin/setresetPassword"); ?> 
              <div class="box-body">
                <div class="form-group">
                  <input type="password" id="password-field" name="old_password" value="<?php echo set_value('old_password'); ?>" class="form-control" placeholder="Old password">
                  <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                </div>
               <div class="form-group">
                                   <input type="password" id="password-field1" name="new_password" value="" class="form-control" placeholder="New password" value="<?php echo set_value('new_password'); ?>">
                      <span toggle="#password-field1" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                  </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Reset password</button>
              </div>
            </form>
                </div>
              </div>
           </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

  
  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header ds-header">
      <div class="row">
        <div class="col-md-12"><ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Dashboard</li>

      </ol>
    </div>
    </div>

    <div class="row">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          
        </div>

    </section>

<?php   date_default_timezone_set('Asia/Kolkata');

                 $createdat=date("Y-m-d"); ?>

    <!-- Main content -->

    <section class="content">

     <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-check"></i></span>

              <div class="info-box-content">
                <span class="info-box-text"></span>
                <span class="info-box-number">
                      <?php //echo $astrologers_users= $this->Talkto_model->count_data('astrologers_users',$condi = array('verify_status' =>1  )); ?>
                  <small></small>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-clock-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text"></span>
                <span class="info-box-number"> </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>


          <!-- /.col -->

         

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-volume-control-phone"></i></span>

              <div class="info-box-content">
                <span class="info-box-text"> </span>
                <span class="info-box-number"><?php 
                 ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
               <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fa fa-headphones"></i></span>

              <div class="info-box-content">
                <span class="info-box-text"> </span>
                <span class="info-box-number"><?php  ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>

       
          <!-- /.col -->
        </div>


        <div class="row jb-product">
          <div class="col-lg-6">
            
            <!-- /.card -->

            <div class="card">
              <div class="card-header border-0">
                <h3 class="card-title">Recent Registration</h3>
               
              </div>
              <div class="card-body table-responsive p-0">
                <table class="table table-striped table-valign-middle">
                  <thead>
                  <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Price</th>
                   
                  </tr>
                  </thead>
                  <tbody>

                    <?php  $users_reg= $this->Admin_model->get_all_data("users_reg",$arrayName = array('user_reg_id!='=> ''));
                    
                
                    
                      foreach ($users_reg as $key => $value) {
                        ?>
                        <tr>
                    <td>
                    <?php echo $value->user_reg_id;?>
                    </td>
                    <td> <?php echo $value->usermame;?></td>
                    <td><?php echo $value->user_email;?></td>
                  
                    
                  </tr>
                  <tr>

                                            <?php }
                    ?>
                
                   
                 
                 
                  
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.card -->
          </div>
        
        </div>
     



    </section>

    <!-- /.content -->

  </div>
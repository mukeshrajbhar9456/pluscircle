<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">  
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel jb-left">
        <div class="image">
          <img src="<?php echo base_url(); ?>assests/images/logo.png" class="img-circle" alt="User Image">
        </div>
        <div class="info">
          <p><?php $adminDetail = $this->session->userdata("logged_in");
                echo 'Hi '.$adminDetail["username"];
              ?></p>
          <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-bar-chart"></i> <span>Home</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url('user/dashboard'); ?>"><i class="fa fa-circle-o"></i>Dashboard</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user-plus"></i>
            <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
               <li><a href="<?php echo base_url('user/alluser'); ?>"><i class="fa fa-circle-o"></i>All Users</a></li>
               <li><a href="<?php echo base_url('admin/setresetPassword'); ?>"><i class="fa fa-circle-o"></i>Reset Password</a></li>
           <!--  <li><a href="<?php //echo base_url('post'); ?>"><i class="fa fa-circle-o"></i>Add Price List</a></li>  -->    
          </ul>
          
        </li>
        <li>
          <a href="<?php echo base_url('admin/view_ground'); ?>">
            <i style="color: #ff6a05;" class="glyphicon glyphicon-th-list"></i>
            <span>View Ground</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span> -->
          </a>
        </li>
<!--           <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Profile</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-rightt pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
               <li><a href="<?php //echo base_url('profile'); ?>"><i class="fa fa-circle-o"></i>All Profile</a></li>
            <!-- <li><a href="<?php// echo base_url('post'); ?>"><i class="fa fa-circle-o"></i>Add Price List</a></li>     
          </ul>
        </li>  
 -->



  


       
      </ul>

    </section>

    <!-- /.sidebar -->

  </aside>
 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      </div>
    Copyright &copy; 2021<strong><a href="#" target="_blank">+91cricket</a> </strong> All rights
    reserved.  
  </footer>
   <div class="control-sidebar-bg"></div>
 </div>
 <!-- jQuery 3 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery.fancytable/dist/fancyTable.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assests/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assests/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assests/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url(); ?>assests/bower_components/raphael/raphael.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assests/js/adminlte.min.js"></script>

<!-- <script src="<?php echo base_url(); ?>assests/js/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assests/js/validations.js"></script>
<script src="<?php echo base_url(); ?>assests/js/demo.js"></script>
<script src="<?php echo base_url(); ?>assests/bower_components/ckeditor/ckeditor.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
 <script type="text/javascript">

  $(document).ready(function(){
  var url = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
  $('.treeview-menu li').removeClass('active');
  $('[href$="'+url+'"]').parent().addClass("active");
  $('.treeview').removeClass('menu-open active');
  $('[href$="'+url+'"]').closest('li.treeview').addClass("menu-open active");
});

 </script>
 <script type="text/javascript">
   $(document).ready(function(){
      $("#profileview").DataTable();
      $("#postview").DataTable();
      $("#productview").DataTable();
});

   $(document).ready(function() {
    $('#vedic').select2();
});
   $(document).ready(function() {
    $('#language').select2();
});
 </script>

<!--  <script>

  $.widget.bridge('uibutton', $.ui.button);

</script>

 -->

 <!--  <script>

   $(document).ready( function () {

    // Replace the <textarea id="editor1"> with a CKEditor

    // instance, using default configuration.

    CKEDITOR.replace('editor1')

    //bootstrap WYSIHTML5 - text editor

    $('.textarea').wysihtml5()

  })

</script>
 -->
<script type="text/javascript">
    function confirmDelete(){
 //confirm("Are you sure you want to delete?");
var agree=confirm("Are you sure you want to delete this file?");
if (agree == true ){ 
     return true ;
   }
  else{
     return false ;
}
}
</script>
<script>
    function remove_profile_img(val, img)
    {
         
        var dir = val;
        var img = img;
        var id = id;
        var col=col;
        var removeimg = "removeimg";
        $.ajax({
            url: "<?php echo base_url('astrologers/romove_image/') ?>", 
            type: "POST", 
            data: {removeimg:removeimg, dir:dir, img:img, id:id, col:col},
            success: function(data){
               
                    location.reload();
                    //window.location.href="profile-details.php?id="+141
            }
        });
    }

           function remove_img(val, img){
    
        var dir = val;
        var img = img;
        var id = id;
        var col=col;
        var removeimg = "removeimg";
        $.ajax({
            url: "<?php echo base_url('product/romove_image/') ?>", 
            type: "POST", 
            data: {removeimg:removeimg, dir:dir, img:img, id:id, col:col},
            success: function(data){
               
                    location.reload();
                    //window.location.href="profile-details.php?id="+141
              

            }
        });
    }

function editstatus(user_id,status)
{
    var  user_id,status;
     $.ajax({
            url: "<?php echo base_url('user/edit_status/') ?>", 
            type: "POST", 
            data: {user_id:user_id, status:status},
            success: function(data){
                    location.reload();
                    //$("#status").load(" #status");
            }
        });
}


function edit_status(user_id,status)
{
    var  user_id,status;
     $.ajax({
            url: "<?php echo base_url('astrologers/edit_status/') ?>", 
            type: "POST", 
            data: {user_id:user_id, status:status},
            success: function(data){
                    location.reload();
                    //$("#status").load(" #status");
            }
        });
}
</script>
<!-- password show -->
<script type="text/javascript">
    $(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
  </script>

  <!-- form  -->
  <script type="text/javascript">
    //DOM elements
const DOMstrings = {
  stepsBtnClass: 'multisteps-form__progress-btn',
  stepsBtns: document.querySelectorAll(`.multisteps-form__progress-btn`),
  stepsBar: document.querySelector('.multisteps-form__progress'),
  stepsForm: document.querySelector('.multisteps-form__form'),
  stepsFormTextareas: document.querySelectorAll('.multisteps-form__textarea'),
  stepFormPanelClass: 'multisteps-form__panel',
  stepFormPanels: document.querySelectorAll('.multisteps-form__panel'),
  stepPrevBtnClass: 'js-btn-prev',
  stepNextBtnClass: 'js-btn-next' };

//remove class from a set of items
const removeClasses = (elemSet, className) => {
  elemSet.forEach(elem => {
    elem.classList.remove(className);
  });
};

//return exect parent node of the element
const findParent = (elem, parentClass) => {
  let currentNode = elem;
  while (!currentNode.classList.contains(parentClass)) {
    currentNode = currentNode.parentNode;
  }
  return currentNode;
};

//get active button step number
const getActiveStep = elem => {
  return Array.from(DOMstrings.stepsBtns).indexOf(elem);
};

//set all steps before clicked (and clicked too) to active
const setActiveStep = activeStepNum => {
  //remove active state from all the state
  removeClasses(DOMstrings.stepsBtns, 'js-active');
  //set picked items to active
  DOMstrings.stepsBtns.forEach((elem, index) => {
    if (index <= activeStepNum) {
      elem.classList.add('js-active');
    }
  });
};

//get active panel
const getActivePanel = () => {
  let activePanel;
  DOMstrings.stepFormPanels.forEach(elem => {
    if (elem.classList.contains('js-active')) {
      activePanel = elem;
    }
  });
  return activePanel;
};

//open active panel (and close unactive panels)
const setActivePanel = activePanelNum => {
  //remove active class from all the panels
  removeClasses(DOMstrings.stepFormPanels, 'js-active');
  //show active panel
  DOMstrings.stepFormPanels.forEach((elem, index) => {
    if (index === activePanelNum) {
      elem.classList.add('js-active');
      setFormHeight(elem);
    }
  });
};

//set form height equal to current panel height
const formHeight = activePanel => {
  const activePanelHeight = activePanel.offsetHeight;
  DOMstrings.stepsForm.style.height = `${activePanelHeight}px`;
};

const setFormHeight = () => {
  const activePanel = getActivePanel();
  formHeight(activePanel);
};

//STEPS BAR CLICK FUNCTION
DOMstrings.stepsBar.addEventListener('click', e => {
  //check if click target is a step button
  const eventTarget = e.target;
  if (!eventTarget.classList.contains(`${DOMstrings.stepsBtnClass}`)) {
    return;
  }
  //get active button step number
  const activeStep = getActiveStep(eventTarget);
  //set all steps before clicked (and clicked too) to active
  setActiveStep(activeStep);
  //open active panel
  setActivePanel(activeStep);
});

//PREV/NEXT BTNS CLICK
DOMstrings.stepsForm.addEventListener('click', e => {
  const eventTarget = e.target;

  //check if we clicked on `PREV` or NEXT` buttons
  if (!(eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`) || eventTarget.classList.contains(`${DOMstrings.stepNextBtnClass}`)))
  {
    return;
  }

  //find active panel
  const activePanel = findParent(eventTarget, `${DOMstrings.stepFormPanelClass}`);
  let activePanelNum = Array.from(DOMstrings.stepFormPanels).indexOf(activePanel);

  //set active step and active panel onclick
  if (eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`)) {
    activePanelNum--;
  } else {
    activePanelNum++;
  }

  setActiveStep(activePanelNum);
  setActivePanel(activePanelNum);
});

//SETTING PROPER FORM HEIGHT ONLOAD
window.addEventListener('load', setFormHeight, false);

//SETTING PROPER FORM HEIGHT ONRESIZE
window.addEventListener('resize', setFormHeight, false);

//changing animation via animation select !!!YOU DON'T NEED THIS CODE (if you want to change animation type, just change form panels data-attr)

const setAnimationType = newType => {
  DOMstrings.stepFormPanels.forEach(elem => {
    elem.dataset.animation = newType;
  });
};

//selector onchange - changing animation
const animationSelect = document.querySelector('.pick-animation__select');

animationSelect.addEventListener('change', () => {
  const newAnimationType = animationSelect.value;

  setAnimationType(newAnimationType);
});
  </script>
   <script type="text/javascript">
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});
  </script>
  <script type="text/javascript">
    
    $(document).ready(function() {

         $(".close").click( function()
         {
            location.reload();
          });
    });
  </script>
</body>
</html>

				
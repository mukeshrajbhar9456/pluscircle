<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller { 
   public function __construct()
    {
        parent::__construct();
          $this->load->helper(array('form', 'url','file'));
           $this->load->model('Admin_model');
          //$this->load->model('Talkto_model');
    }
    
    public function index()
	{
		 $this->load->view('login');
	}
      public function dashboard()
      {
      	$this->load->template('user/dashboard');
      }

   public function alluser()
  {
           $data['users']= $this->Admin_model->get_all_data("users_reg");
            $this->load->template('home/userview',$data);
  } 

  
        public function player($id)
  {
            $id=base64_decode($id);
           $data['players']= $this->Admin_model->get_all_data("players",$cond=array('user_id'=>$id));
            $this->load->template('home/players',$data);
  } 

    public function edit_status()
    {
            echo $user_id=$this->input->post('user_id');
            echo $status=$this->input->post('status');
            if(!empty($user_id))
            {
               $dataa=array(
              
                         'status'=> $status
                    );
                   
                    $this->Admin_model->update_data('users',$arrayName = array('id' => $user_id),$dataa);
                    $data['success']="Successfully..";
            }else{
                 $data['error']="Something wrong";
            }
            echo  json_encode($data);
    }
       public function delete($id)
  {     
          
           if($id)
          {
                  $id= base64_decode(urldecode($id));
               $this->Admin_model->delete_data('users_reg',$condi = array('id' => $id ));
                 
          }
             redirect('user/alluser');
  } 

 public function notepad($id)
  {
         $this->load->view('home/notepad');
  }
      public function download_excel()
      {
         $output = '';
      
            $output .= '
                  <table class="table" bordered="1">  
                    <tr>  
                         <th>User Registration Id</th>  
                         <th>Use Name</th>  
                         <th>User Phone</th>  
                         <th>User Email Id</th>
                         <th>User Password</th>
                         <th>Age Group</th>
                         <th>State</th>
                         <th>Registration Date</th>
                         <th>Payment</th>
                    </tr>';
      $users= $this->Admin_model->get_all_data("users_reg");
 foreach ($users as $key => $value)
  {
    $output .= '
    <tr>  

                         <td>'.$value->user_reg_id.'</td>  
                         <td>'.$value->username.'</td>  
                         <td>'.$value->user_phone.'</td>  
                         <td>'.$value->user_email.'</td>  
                         <td>'.$value->user_password.'</td>  
                         <td>'.
                          $this->Admin_model->get_field('age_group','age_title',$condi =array('id' => $value->age_group))
.'</td>  
                         <td>'.
                        $this->Admin_model->get_field('state','name',$condi =array('id' => $value->user_state)).'</td>  
                         <td>'.date('d M Y', strtotime($value->created_at)).' , '.date("h:i:s",strtotime($value->created_at)).'</td>  
                         <td>'.$this->Admin_model->get_field('payment','amount',$condi =array('user_id' => $value->id)).'</td>  

                    </tr>

   ';

  }

  $output .= '</table>';

  header('Content-Type: application/xls');

  header('Content-Disposition: attachment; filename=All Users.xls');

  echo $output;



}
    

}
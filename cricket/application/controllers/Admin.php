<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Controller {
	public function __construct()
	{
        parent::__construct();
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->model('Form_model');
         $this->load->helper(array('form', 'url','file'));
         $this->load->library('form_validation','upload');
    }

	public function index()
	{

		 $this->load->view('login');

	}

	public function checkLoginDetail()
	{

		
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('user_emailid', 'Email', 'trim|required|valid_email');
				  if ($this->form_validation->run() == FALSE)

                {

                        $this->load->view('login');

                }else{

                		 $email = $this->input->post('user_emailid');

			 			 $password = $this->input->post('password');

			 		 

			 		 $result = $this->Admin_model->checkLogin($email,$password);

			 		if($result == false)

			 		{

				 		$this->session->set_flashdata('msg','<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Please check username and password and try  again</div>');

				 		$this->load->view('login');

					 }else{

			 			$this->load->template('user/dashboard');

			 		}

                }	

	}



  public function dashboard()
  {
  	   $this->load->template('user/dashboard'); 
  }

  public function resetpassword()
  {
     
     $this->load->template('user/reset-password'); 
  }	
   public function view_ground()
  {
           $data['state']= $this->Admin_model->get_all_data("state");
           $data['city']= $this->Admin_model->get_all_data("city");
            $this->load->template('home/view_ground',$data);
  } 
  
    public function do_upload()
    {   

  

         if(isset($_FILES["file"]["name"]))  
            {  
                echo 456; exit;
                     $config['upload_path'] = './assets/images';  
                     $config['allowed_types'] = 'jpg|jpeg|png|gif';  
                     $this->load->library('upload', $config);  
                     if(!$this->upload->do_upload('file'))  
                     {  
                     echo $this->upload->display_errors();  
                     }  
                     else  
                     {  
                     $data = array('upload_data' => $this->upload->data());
                     $image= $data['upload_data']['file_name']; 
                     
                     $insert = $this->Form_model->create($image);
                     } 
            }else{
                $insert = $this->Form_model->create();
            }

        
 
        $data = array('success' => false, 'msg'=> 'Form has been not submitted');
        if($insert){
        $data = array('success' => true, 'msg'=> 'Form has been submitted successfully');
        }
 
        echo json_encode($data);
         
    }
    
    public function setresetPassword()
  {
      
      $id=$_SESSION['logged_in']['id'];
      $data = array();
    if(isset($id))
    {
       $this->form_validation->set_rules('old_password', 'Old Password', 'required');
       $this->form_validation->set_rules('new_password', 'New Password', 'required');
       if ($this->form_validation->run() == FALSE)
                {
                       //$this->load->template('user/reset-password'); 
                }else{
                        $old_password= $this->input->post('old_password');
                      $count=$this->Admin_model->count_data('users',$cond = array('id'=>$id,'user_password' => $old_password ));
                    if($count ==1)
                    {
                           $new_password= $this->input->post('new_password');
                         if(isset($new_password))
                         {
                                 $fdata=array(
                          'user_password'=>$new_password
                        );
                             $this->Admin_model->update_data('users',$cond = array('id' => $id),$fdata);
                               $data['success']="New password  updated successfully";
                         }
                    }else{
                      $data['error']="please enter correct old password ";
                    }
                    //date_default_timezone_set('Asia/Kolkata');
                    // $createdat=date("Y-m-d H:i:s");
                }
    }
     $this->load->template('user/reset-password',$data); 
  }   


  public function adminlogout()
  {

  	  $this->session->sess_destroy();
  	  redirect(base_url()); 

  }
  
  // forgotpassword start
  public function Forgotpassword()
    {
        $this->load->view('Forgotpassword');
    }
 public function resetlink()
      {
        $email = $this->input->post('user_emailid'); 
         //print_r($email);
        $result=$this->db->query("select * from users where email='$email'");
         
         if(count($result)>0)
        
        {
             //echo "Exit";
            $tokan= rand(1000,9999);
            $this->db->query("update users set password='$tokan' where email='$email'");
            $message="Please Click on password reset link<br><a href='".base_url('admin/reset?tokan=').$tokan."'>Reset Password</a>";
           
           
            //  print_r($message);
            $this->Email($email,'Reset Password Link',$message);
        }
        else
        {
            //  echo "Email not registered";
            $this->session->set_flashdata('message',"Email not registered");
            redirect(base_url('admin/Forgotpassword'));
        }
    }
    
        public function reset()
     {
         $data['$tokan'] = $this->input->get('tokan'); 
         $_SESSION['tokan']=$data['$tokan'];
         $this->load->view('resetpass');
     }
        
          public function updatepass()
     {
          $_SESSION['tokan'];
          $data=$this->input->post();
          $result="false";
          if($data['cpassword']==$data['password'])
          {
               $result="true";
             // $this->db->query("update users set password='".$data['password']."' where password='".$_SESSION['tokan']."'");
             
               $this->db->query("update users set password='".$data['password']."' where id=1");
          }
        //   echo $_SESSION['tokan'];
        //   echo $result;
        redirect(base_url('admin/index'));
        //   exit();     
         
     }
     
     public function Email($email,$link,$message) { 
         $from_email = "rekhabgavhane@gmail.com"; 
         $to_email = $email; 
         $linkmsg=$message.$link;
//   echo $linkmsg;
//   echo $to_email;
         //Load email library 
         $this->load->library('email'); 
   
         $this->email->from($from_email, 'Plus Circle'); 
         $this->email->to($to_email);
         $this->email->subject('Reset Link'); 
         $this->email->message($linkmsg); 
   
         //Send mail 
         if($this->email->send()) {
               $result="Email sent successfully";
            $this->session->set_flashdata("email_sent","Email sent successfully."); 
         }else {
               $result="Error in sending Email.";
             $this->session->set_flashdata("email_sent","Error in sending Email.");
         }
         
    echo $result;
      } 

}


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Withdrawal extends CI_Controller { 
   public function __construct()
    {
           parent::__construct();
           $this->load->helper(array('form', 'url','file'));
           $this->load->model('Admin_model');
           $this->load->model('Talkto_model');
           $this->load->library('form_validation');
    }
    
    public function index()
  {
     $data['withdrawal']= $this->Talkto_model->get_all_data("withdraw");
    $this->load->template('home/allWithdrawal.php',$data);
  } 

   public function view($id)
  {
     $data['withdrawal']= $this->Talkto_model->get_all_data("withdraw");
    $this->load->template('home/viewWithdrawal',$data);
  } 
  
  
      /**** show add product form ***/     
  public function edit($id=null)
  { 
        $data= array();
        $id= base64_decode(urldecode($id));  
        $data['withdrawal']= $this->Talkto_model->get_all_data("withdraw",$arrayName = array('id' =>$id ));
        //$this->load->template('home/addproduct',$data);
        $this->load->template('home/addcommission',$data);

  }
    

      public function editWithdrawal($id)
      {
                $id= base64_decode(urldecode($id));
               $data=array();
      
                 date_default_timezone_set('Asia/Kolkata');
                     $created_at=date("Y-m-d H:i:s");
                     $amt=$this->input->post('amt');
                      $astro_id=$this->input->post('astro_id');
                      $trans_id=$this->input->post('trans_id');
                      $payment_status=$this->input->post('payment_status');
                      if($payment_status == 1){
                          $data_astro = array(
                            'astro_id' =>$astro_id , 
                            'debit' =>$amt,
                            'created_at'=>$created_at,
                            'status'=> 1
                          );

                        $this->Talkto_model->insert_data('astro_wallet',$data_astro);
                      }
                
                     $dataa=array(
                      // 'file_id' => NULL,
                        'transaction_id'=>$trans_id,
                        'payment_status'=>$payment_status,
                    );
                   
                    $this->Talkto_model->update_data('withdraw',$arrayName = array('id' => $id),$dataa);
                     $data['success'] = "update successfully";
                redirect('withdrawal');
                  //$data['withdrawal']= $this->Talkto_model->get_all_data("withdraw");
                  //$this->load->template('home/allWithdrawal',$data);
                  

      }



        public function delete($id)
        {
          if($id)
          {
                  $id= base64_decode(urldecode($id));
               $this->Talkto_model->delete_data('withdraw',$condi = array('id' => $id ));
                 
          }
             redirect('withdrawal');
        }


}
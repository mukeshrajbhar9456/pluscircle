<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApplicationPassword;
use Validator;use DB;use Auth;use Session;use Mail;Use Redirect;use Crypt;
use App\Models\User;
use App\Models\Userlog;
use Illuminate\Support\Facades\Hash;



class LandingPageController extends Controller
{
    public function index(){
        return view('welcome');
    }
    public function about_us_1()
    {
        return view('aabout_us_1');
    }
    public function about_us_2()
    {
        return view('aabout_us_2');
    }
    public function about_us_3()
    {
        return view('aabout_us_3');
    }    
    public function Social_Networking()
    {
        return view('bSocial_Networking');
    }
    public function Business_Opportunity()
    {
        return view('bBusiness_Opportunity');
    }
    public function Emergency_Support()
    {
        return view('bEmergency_Support');
    }
    public function Health_Insurance()
    {
        return view('bHealth_Insurance');
    }
    public function Opportunities_professionals()
    {
        return view('bOpportunities_professionals');
    }
    public function Scholarships_Programs()
    {
        return view('bScholarships_Programs');
    }
    public function Supporting_Hand()
    {
        return view('bSupporting_Hand');
    }
    public function why_pluscircle()
    {
        return view('bwhy_pluscircle');
    }
    public function privacy_policy()
    {
        return view('privacy_policy');
    }
    public function terms_of_use()
    {
        return view('terms_of_use');
    }
    
    public function terms_of_services()
    {
        return view('terms_of_services');
    }
    
    public function about_us(){
        return view('about_us');
    }
    public function our_vision(){
        return view('our-vision');
    }
    public function our_mission(){
        return view('our-mission');
    }
    public function what_we_do(){
        return view('what-we-do');
    }
    public function how_we_do(){
        return view('how-we-do');
    }
    public function events(){
        return view('events');
    }
    public function activities(){
        return view('activities');
    }
    public function volunteer_c(){
        return view('volunteer_c');
    }
    public function blog(){
        return view('blog');
    }
    public function return_refund_policy(){
        return view('return_refund_policy');
    }
    public function contact_us(){
        return view('contact_us');
    }
    public function event_activites()
    {
        return view('event_activites');
    }
    public function event_activites_one($event)
    {
        return view('event_activites_one');
    }
    public function event_activites_two($event){
        return view('event_activites_two');
    }
    public function event_activites_three($event)
    {
        return view('event_activites_three');
    }
    public function event_activites_four($event){
        return view('event_activites_four');
    }
    
    public function event_activites_five($event)
    {
        return view('event_activites_five');
    }
    
    public function event_activites_six($event)
    {
        return view('event_activites_six');
    }
    public function application_sccm(Request $request){
        if($request->session()->has('encrypted_pass')){

            $encrypted= $request->session()->get('encrypted_pass');
            $decrypted = Crypt::decrypt($encrypted);
            if($decrypted!=''){
                session()->forget('encrypted_pass');
                // session()->flush();
                return view('application_sccm');
            }
            else{
                return redirect()->route('application_sccm_password');
            }
        }
        else{
            return redirect()->route('application_sccm_password');
        }     
    }
    public function application_sccm_password(){
        return view('application_sccm_password');
    }
    public function sccm_password(Request $request){
        $today=date('Y-m-d');
        // $hashPassword=Hash::make($request->password);
        $application=ApplicationPassword::where('active','1')
                ->where('expired_at','>',$today)
                ->where('application','sccm')
                ->count();
        if($application > 0){
            $app=ApplicationPassword::where('active','1')
                ->where('expired_at','>',$today)
                ->where('application','sccm')
                ->first();
            if (Hash::check($request->password, $app->password)) {
                $encrypted = Crypt::encrypt($request->password);
                $request->session()->put('encrypted_pass', $encrypted);
                return redirect()->route('application_sccm');
            }
            else{
                return Redirect::back()->with('error', 'Password not matching');
            }      
        }
        else{
            return Redirect::back()->with('error', 'Application Expired');
        }               
    }
    public function getstate(Request $request){
        $states = \DB::table('states')->where('country_id',$request->country_id)->get();
        $pkt='';
        foreach($states as $s){
            $pkt.="<option value=".$s->id." class='".$s->country_id."' selected >".$s->name."</option>";
													
        }
        return $pkt;
        
    }
    public function sccm_application_submit(Request $request){
       
            $ref[]='';
            if( $request->referencename1 !=''){
                $ref[] = [
                    'name' => $request->referencename1,
                    'phone' => $request->referencephone1,
                    'email' => $request->referenceemail1,
                    'relationship' => $request->referencerelation1
                ];
            }
            if( $request->referencename2 !=''){
                $ref[] = [
                    'name' => $request->referencename2,
                    'phone' => $request->referencephone2,
                    'email' => $request->referenceemail2,
                    'relationship' => $request->referencerelation2
                ];
            }
            $utrref='';$utrdate='';$utramount=''; 
            if( $request->utrrefno1 !='' && $request->utrrefdate1 !=''){
                $utrref=$request->utrrefno1;
                $utrdate=$request->utrrefdate1;
                $utramount=50000;            
            }
            if( $request->utrrefno2 !='' && $request->utrrefdate2 !=''){
                $utrref=$request->utrrefno2;
                $utrdate=$request->utrrefdate2;
                $utramount=25000;            
            }
            $club[]='';
            if( $request->clubname1 !='' && $request->clubyear1 !=''){
                $club[] = [
                    'name' => $request->clubname1,
                    'year' => $request->clubyear1,
                ];
            }
            if( $request->clubname2 !='' && $request->clubyear2 !=''){
                $club[] = [
                    'name' => $request->clubname2,
                    'year' => $request->clubyear2,
                ];
            }
            $application=\DB::table('sccm_application')->insertGetId([
                'name' => $request->name,
                'appcountry' => $request->appcountry,
                'appstate' => $request->appstate,
                'position' => $request->position,
                'dob' => $request->dob,
                'spousename' =>$request->spousename ,
                'fathername' =>$request->fathername ,
                'mothername' =>$request->mothername ,
                'email' =>$request->email ,
                'phone' =>$request->phone ,
                'secondaryphone'=>$request->secondaryphone ,
                'address' => $request->address ,
                'country' =>$request->country ,
                'state' =>$request->state ,
                'city' =>$request->city ,
                'pincode' =>$request->pincode ,
                'firmaddress' =>$request->firmaddress ,
                'firmcountry' => $request->firmcountry ,
                'firmstate' =>$request->firmstate ,
                'firmcity' =>$request->firmcity,
                'firmpincode' =>$request->firmpincode ,
                'othermembership' =>serialize($club),
                'nationality' =>$request->nationality ,
                'education' =>$request->education ,
                'annualincome' =>$request->annualincome ,
                'idprooftype' =>$request->idprooftype ,
                'idproofnumber' =>$request->idproofnumber ,
                'profession' =>$request->profession ,
                'profdescription' => $request->profedescription ,
                'whypluscircle' =>$request->whypluscircle ,
                'othersocialwelfare' =>$request->socialwelfaredesc ,
                'reference' =>serialize($ref) ,
                'utrreference' =>$utrref ,
                'utrdate' =>$utrdate ,
                'utramount' =>$utramount ,
                'refundbank' =>$request->refundbank ,
                'refundname' =>$request->refundaccname ,
                'refundaccountno' =>$request->refundaccountno ,
                'refundifsccode' =>$request->refundifsccode ,
                'refundaccounttype' =>$request->accounttype ,
                //'profile' =>$request-> ,
                'active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                
            ]);
            // print_r($application);exit;
            if($request->profile!=''){
                $tmp = str_replace('data:image/png;base64,', '', $_POST['profile']);
                $tmp2 = str_replace(' ', '+', $tmp);                    
                $image = base64_decode($tmp2); // decode the image
                $resultset=file_put_contents('public/images/profile/'.$application.'_profile.png',$image);
                
                $profile=\DB::table('sccm_application')
                ->where('id', $application)
                ->update(['profile' => 'public/images/profile/'.$application.'_profile.png']);
    
            }
            if($application){
                return redirect()->route('application_sccm_success');
            } 
            else{
                return redirect()->route('application_sccm_success');
            } 
                 
    }
    public function application_sccm_success(){
        return view('application_sccm_success');
    }
    
    
}

<?php include "sidebar.php"; 
//print_r($_SESSION);

?>
<!-- END MENU SIDEBAR-->
<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- HEADER DESKTOP-->
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <!--  -->
                <div class="row">
                <div class="col-md-3">
                  <div class="logo91"><img src="images/91-logo.png"></div>
                    
                </div>
              </div>
               <!--  -->
                <div class="row">
                    <!--  -->
                    <div class="col-md-8 offset-2">
                        <div class="pc-card">
                            
                            <div class="card-body">
                                <div class="card-title">
                                            <h3>Reset Password</h3>
                                </div>
                                <!--  -->
                                <div id="reset-form" class="pc-form">
                                    <div class="reset-bg"> <img src="images/background/reset-bg.png"></div>
                                    <form method="post">
                                        <div class="row form-group">
                                           
                                            <div class="col-12 col-md-12">
                                                <input type="email" id="email_f" name="email_f" class="form-control" placeholder="Enter Your Email Id" required>
                                                <p id="email-err"></p>
                                            </div>
                                        </div>                                        
                                        <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" id="forget-form" type="button" name="forget-form">Submit</button>
                                        </div>
                                    </form>
                                </div>

                                <!--  -->
                                 <div id="reset-form-one" style="display: none;" class="pc-form pc-form1">
                                     <div class="reset-otp-bg"> <img src="images/background/reset-otp-bg.png"></div>
                                    <form method="post" id="forget-otp" name="forget-otp">
                                        <p id="reset-msg"></p>
                                        <!--<p id="resetOtp"></p>-->
                                        <div class="row form-group text-center">
                                            <div class="col col-md-12">
                                                <p class="otp-css">Enter OTP</p>
                                            </div>
                                            <div class="col-12 col-md-12">
                                                <input type="text"  maxlength="4" id="otp_number_r" name="otp_number_r" class="form-control" required> 
                                                <!-- <input type="text" id="digit-1" name="digit-1" data-next="digit-2" />
                                                <input type="text" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-1" />
                                                <input type="text" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-2" />
                                                <input type="text" id="digit-4" name="digit-4" data-next="digit-5" data-previous="digit-3" /> -->
                                                <p id="otp-error_r"></p>
                                                <input type="hidden" name="u_email" id="u_email">
                                            </div>
                                        </div>                                        
                                        <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="forget_otp">Submit OTP</button>
                                            <button class="au-btn au-btn--block au-btn--green m-b-20 btn2" type="button" id="resend" name="resend">Resend OTP</button>
                                           
                                           </div>
                                    </form>
                                </div>
                                <!--  -->
                                <!--  -->
                                <div id="reset-form-sec" style="display: none;" class="pc-form">
                                    <div class="reset-bg"> <img src="images/background/reset-bg.png"></div>
                                    <form method="post" id="forget-pass" name="forget-pass">
                                       
                                      <div class="row form-group">
                                           
                                            <div class="col-12 col-md-12">
                                                <input type="password" id="f_password" name="f_password" class="form-control" placeholder="Enter New Password" required autocomplete="off">
                                               
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                           
                                            <div class="col-12 col-md-12">
                                                <input type="password" id="f_r_password" name="f_r_password" class="form-control" placeholder="Re Enter Password" required autocomplete="off">
                                                
                                            </div>
                                        </div>                                        
                                        <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="register">Submit</button>
                                           
                                           </div>
                                    </form>
                                </div>
                                <!--  -->
                               <div id="reset-thank-you" style="display: none;" class="pc-form">
                                    <div class="rest-thank-you-bg"> 
                                        <img src="images/background/rest-thank-you.png">
                                    </div>
                                    <span class="rest-thank-you">Your password is successfully reset</span>
                                </div>
                                <!--  -->
                               
                               
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
              <!--   <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">
                            <p>Copyright © 2021 Pluse Circle. All rights reserved. Powered by <a href="https://briotechwebsolutions.com/" target="_blank">Briotech Websolutions</a>.</p>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>
<?php include ('sidebar.php'); ?>
 <div class="page-container back-blure">
  <!-- HEADER DESKTOP-->
  
  <!-- HEADER DESKTOP-->
  <!-- MAIN CONTENT-->
  <div class="main-content home-bg">
    <div class="section__content section__content--p30">
      <div class="container">
      <div class="container-fluid">
        <h1 class="text-center h1-style">Protocols and Guidelines</h1>
        <div class="proto-style">
         <ul>
           
           <li>Only Male are eligible to participate in this tournament </li>
           <li>No players shall be entitled for any allowance for participating in this tournament, if required, a candidate may have to travel and or to stay in this tournaments for which he has to make his own arrangements.</li>
           <li>On winning / qualifying to next round of match each team should pay only Rs. 100/- (Rupees One Hundred Oly)per member to continue to pay for the next round of match. The said payment need to be paid minimum one day in advance of the match date via our web portal payment gateway only </li>
           <li>Each Team should have 11 members to play the tournament. However every team will have an option to add 4 members as reserve in there respective teams.</li>
            <li>The organizers have entire rights to cancel/ postpone the event for reasons not required to be made public. No candidate shall challenge the decision in these regards.</li>
           <li>Initial round of matches will be based upon the knockout basis and Run Rate / points basis. </li>

           <li>Matches between the teams will be based on the open lottery system. </li>
           <li>The organizers will decide the venue and time of the match, which need to be honoredby the participates. </li>
           <li>The matches will be conducted in a Cricket Pitch, however few matches will be conducted in the carpet pitch also. </li>
           <li>It will be compulsory for all the team members / individual to play in the given pitch by the organizer’s.</li>
           <li>Refund Policy </li>
           <ul class="padd-left-30">
             <li>Registration will only be refunded in full, only if the organizer’s cancel the match / tournament, </li>
             <li>If any individual/group/team pays the registration fee and later on decides not to participate for any reason what so ever, under such circumstances the said registration feel will not be refunded in any manner what to ever.</li>
             <li>After paying the registration fees if management / organizers decides not to accept any individual player / team the management will refund the entire amount and the said individual / team will not be allowed to participate in the tournament. No player/s shall challenge the decision in these regards. </li>
             <li>For any refund it will only be refunded to there relevant bank account only, after verifying the KYC, and the refund process may take 2 to 3 weeks by the management </li>
             <li>Incase of any kind of refund only the registration fee will be refunded without any interest or damages in any manner what so ever. </li>
           </ul>
             <li>I</li>
             <li>Capitan of any team will have the right to change any three players by paying a nominal fee of Rs. 100/- + GST per player, whose name would not have been registered during the time of initial registration.  </li>
             <li>The organizing management committee will decide registration fee for team/group/Individual.Which may change from time to time.</li>
             <li>If any team is missing on reaching the venue at the prescribed time, the match referee will reserve the right to disqualify the said team to play the match, and the opponent team will get the advantage of the same. </li>
             <li>Any player registered with any one team will not be eligible to play for another registered team in any manner what so ever. </li>
             <li>If any player is found misbehaving or any misconduct during the match, it will be the responsibility of the Capitan and vice Capitan. However, the match referee will reserve the right to disqualify the said team. </li>
             <li>All the players should present one of their Identity card in the original during the time of the match. (Aadhar / Driving license / Passport / school ID card)</li>
             <li>All the players should carry 4 Passport sixe Photo during the time of the match. </li>
             <li>During the time of match if all the 12 players are not present the said team will not be allowed to play the match and the opposite team will get the advantage of the same. </li>
             <li>If any player is turning 12 / 15 / 18  / 35 Years in 2021. Following rules will Perivale</li>
            <ul class="padd-left-30">
              <li>If his birthday falls on or before 30th June 2021 he is will be considered as 12 / 15 / 18  / 35 Years. And he can participate in this said tournament accordingly.</li>
              <li>If his birthday falls on or after 1st July 2021 he is will be considered as 13 / 16 / 17  / 36 Years. And he can participate in this said tournament accordingly.</li>
            </ul>
            <li>All the participants should be the citizens of Indian and should carry a valid original ID proof like Aadhar card/driving license/passport/school ID card</li>
            <li>Each players/team will be responsible for there own personal belongings, management / organizer’s will not be responsible for any loss or damage in any manner what to ever. </li>
            <li>It is compulsory to wear cricket white trouser + Sports shoes (Cricket).</li>
            <li>It will be the responsibility of the team Capitan & Vice Capitan to arrange and carry relevant cricket bat for there own team. </li>
            <li>The entire team should wear the same style / design of the T-shirt during the tournament. </li>
            <li>Decision of the match umpire & Judges will be final during the time of the tournament.</li>
            <li>The prize money for the winning team will be given to the Capital / vice Capitan subject to the written concerns of all the other players. However, the said amount is to be mutually distributed among the entire team members. </li>
            <li>Your tournament date will be informed after the last date of the registration and before 15 days of the tournament is commenced.</li>
            <li>Venue of the tournament will be informed to the team capital and vice capital 15 days in advance.</li>
            <li>All the participants in this tournament should be above 12 years and below 55 years.</li>
            <li>In a situation if you register as an individual or as a group we will collectively make a group of a team within your vicinity and you could be a part of the said team, this would be subject to your acceptance. If you do not wish to be a part of the allotted team or if the said team does not accept you, we would try to allot an alternative team for you.  </li>
            <li>In a situation if you register as in individual or as a group (less then 12 members) and we are not able to comply as a 12 members team your registration will fee will be refunded in full. </li>
            <li>It will be compulsory for all the Players to display / ware the branding of one or more sponsor/s batch / logo during the time of the match. </li>
            <li>In a situation if you register as in individual or as a group, the entire team will collectively select their Capitan and Vice Capital of the said team. </li>
            <li>Participates of under 14& 18 years. It will be compulsory for their respective parents or guardian to give a written concern to our board members. </li>
            <li>Participants between the age group of 12 to 15 can play U 15 </li>
            <li>Participants between the age group of 16 to 19 can play U 19 </li>
            <li>Participants between the age group of 20 to 30 can play 19+ </li>
            <li>Participants between the age group of 31 to >can play 30+ </li>
            <li>All the participants should be physically fit to participate in this tournament. </li>
         </ul>
        </div>
      </div>
      </div>
    </div>
</div>
</div>

<?php 
include ('footer.php');
?>

  



<?php include ('sidebar.php'); ?>
 <div class="page-container back-blure">
  <!-- HEADER DESKTOP-->
  
  <!-- HEADER DESKTOP-->
  <!-- MAIN CONTENT-->
  <div class="main-content multiple-bg">
    <div class="pri-bg"><img src="images/background/bg1.png"> </div>
    <div class="pri-bg1"><img src="images/background/bg2.png"> </div>
    <div class="pri-b2"><img src="images/background/bg3.png"> </div>
    <div class="section__content section__content--p30">
      <div class="container">
              <div class="row">
                <div class="col-md-3">
                  <div class="logo91"><img src="images/91-logo.png"></div>
                    
                </div>
              </div>
              <!-- section1 -->
              <div class="row p60">
                 <div class="col-md-12">
                    <div class="prize-title">
                      <h1>Prizes</h1>
                    </div>
                 </div>
              </div>
              <!-- section1 end -->
              <!-- section2 -->
              <div class="row">
                <div class="col-md-10 offset-1">
                  <div class="prize-table-container">
                  <!-- table 1 -->
                  <table class="prize-table">
                    <tr>
                      <th class="prizes-bg3"><span class="font1">&nbsp;&nbsp;&nbsp;30+&nbsp;&nbsp;&nbsp;</span></th>
                       <th class="text-center"><span>Champion +<br> MoM <pre>(Man of the Match)</pre></span></th>
                        <th class="text-center"><span>Runner Up</span></th>
                         <th class="text-center"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th>
                          <th class="text-center"><span>Sectots</span></th>
                           <th class="text-center"><span>Total Award <br>(Amount)</span></th>
                    </tr>
                    <tr class="prizes-bg1">
                        <td><span class="font2">National</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 15,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 5,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 20,00,000</span></td>
                        <td class="text-center"><span class="font3">1</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 20,00,000</span></td>
                    </tr>
                     <tr>
                        <td><span class="font2">Zone</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 3,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 2,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 5,00,000</span></td>
                        <td class="text-center"><span class="font3">4</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 20,00,000</span></td>
                    </tr>
                     <tr class="prizes-bg1">
                        <td><span class="font2">State & UT</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 50,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 15,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 65,000</span></td>
                        <td class="text-center"><span class="font3">17</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 11,05,000</span></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-center"><span class="font2">Man of the Series</span></td>
                       
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 1,00,000</span></td>
                    </tr>
                     <tr>
                        <td colspan="5"></td>
                       
                        <td class="prizes-bg1 text-center"><span class="total-amount"><i class="fa fa-inr"></i> 52,05,000</span></td>
                    </tr>
                  </table>
                </div>
                  <!-- table1 end  -->
                   <!-- table 2 -->
                   <div class="prize-table-container">
                  <table class="prize-table">
                    <tr>
                      <th class="prizes-bg3"><span class="font1">&nbsp;&nbsp;&nbsp;19+&nbsp;&nbsp;&nbsp;</span></th>
                       <th class="text-center"><span>Champion +<br> MoM <pre>(Man of the Match)</pre></span></th>
                        <th class="text-center"><span>Runner Up</span></th>
                         <th class="text-center"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th>
                          <th class="text-center"><span>Sectots</span></th>
                           <th class="text-center"><span>Total Award <br>(Amount)</span></th>
                    </tr>
                    <tr class="prizes-bg1">
                        <td><span class="font2">National</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 15,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 5,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 20,00,000</span></td>
                        <td class="text-center"><span class="font3">1</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 20,00,000</span></td>
                    </tr>
                     <tr>
                        <td><span class="font2">Zone</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 3,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 2,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 5,00,000</span></td>
                        <td class="text-center"><span class="font3">4</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 20,00,000</span></td>
                    </tr>
                     <tr class="prizes-bg1">
                        <td><span class="font2">State & UT</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 50,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 15,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 65,000</span></td>
                        <td class="text-center"><span class="font3">17</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 11,05,000</span></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-center"><span class="font2">Man of the Series</span></td>
                       
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 1,00,000</span></td>
                    </tr>
                     <tr>
                        <td colspan="5"></td>
                       
                        <td class="prizes-bg1 text-center"><span class="total-amount"><i class="fa fa-inr"></i> 52,05,000</span></td>
                    </tr>
                  </table>
                </div>
                  <!-- table2 end  -->
                    <!-- table 3 -->
                    <div class="prize-table-container">
                  <table class="prize-table">
                    <tr>
                      <th class="prizes-bg3"><span class="font1">&nbsp;&nbsp;U&nbsp;19&nbsp;&nbsp;</span></th>
                       <th class="text-center"><span>Champion +<br> MoM <pre>(Man of the Match)</pre></span></th>
                        <th class="text-center"><span>Runner Up</span></th>
                         <th class="text-center"><span>&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;</span></th>
                          <th class="text-center"><span>Sectots</span></th>
                           <th class="text-center"><span>Total Award <br>(Amount)</span></th>
                    </tr>
                    <tr class="prizes-bg1">
                        <td><span class="font2">National</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 6,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 3,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 9,00,000</span></td>
                        <td class="text-center"><span class="font3">1</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 9,00,000</span></td>
                    </tr>
                     <tr>
                        <td><span class="font2">Zone</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 1,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 50,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 1,50,000</span></td>
                        <td class="text-center"><span class="font3">4</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 6,00,000</span></td>
                    </tr>
                     <tr class="prizes-bg1">
                        <td><span class="font2">State & UT</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 35,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 15,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 50,000</span></td>
                        <td class="text-center"><span class="font3">17</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 8,50,000</span></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-center"><span class="font2">Man of the Series</span></td>
                       
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 1,00,000</span></td>
                    </tr>
                     <tr>
                        <td colspan="5"></td>
                       
                        <td class="prizes-bg1 text-center"><span class="total-amount"><i class="fa fa-inr"></i> 24,50,000</span></td>
                    </tr>
                  </table>
                </div>
                  <!-- table3 end  -->
                   <!-- table 4 -->
                   <div class="prize-table-container">
                  <table class="prize-table">
                    <tr>
                      <th class="prizes-bg3"><span class="font1">&nbsp;&nbsp;U&nbsp;15&nbsp;&nbsp;</span></th>
                       <th class="text-center"><span>Champion +<br> MoM <pre>(Man of the Match)</pre></span></th>
                        <th class="text-center"><span>Runner Up</span></th>
                         <th class="text-center"><span>&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;</span></th>
                          <th class="text-center"><span>Sectots</span></th>
                           <th class="text-center"><span>Total Award <br>(Amount)</span></th>
                    </tr>
                    <tr class="prizes-bg1">
                        <td><span class="font2">National</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 6,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 3,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 9,00,000</span></td>
                        <td class="text-center"><span class="font3">1</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 9,00,000</span></td>
                    </tr>
                     <tr>
                        <td><span class="font2">Zone</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 1,00,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 50,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 1,50,000</span></td>
                        <td class="text-center"><span class="font3">4</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 6,00,000</span></td>
                    </tr>
                     <tr class="prizes-bg1">
                        <td><span class="font2">State & UT</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 25,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 15,000</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 40,000</span></td>
                        <td class="text-center"><span class="font3">17</span></td>
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 6,80,000</span></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-center"><span class="font2">Man of the Series</span></td>
                       
                        <td class="text-center"><span class="font3"><i class="fa fa-inr"></i> 1,00,000</span></td>
                    </tr>
                     <tr>
                        <td colspan="5"></td>
                       
                        <td class="prizes-bg1 text-center"><span class="total-amount"><i class="fa fa-inr"></i> 22,80,000</span></td>
                    </tr>
                  </table>

                  <!-- table4 end  -->
                  </div>
                </div>
              </div>
              <!-- section2 start -->
      </div>
    </div>
</div>
</div>

<?php 
include ('footer.php');
?>

  



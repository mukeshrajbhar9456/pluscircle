<?php include "sidebar.php"; 
//print_r($_SESSION);
if(!empty($_SESSION['user_id'])){
    echo "<script>window.location.href='index.php'</script>";
}
?>
<link rel="stylesheet" type="text/css" href="css/theme.css">
<!-- END MENU SIDEBAR-->
<!-- PAGE CONTAINER-->
<div class="page-container view_ground">
    <!-- HEADER DESKTOP-->
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                 <!--  -->
                <div class="row">
                    <div class="col-md-3">
                      <div class="logo91"><img src="images/91-logo.png"></div>
                    </div>
                    <div class="col-md-6 text-center">
                        <!-- <h3 for="states">Select State</h3> -->
                        <?php 
                        $select_state = "SELECT * FROM state";
                        $query_state = mysqli_query($conn, $select_state);
                       ?>
                     <?php
                           $sql = "SELECT * FROM state";
                          $result = $conn->query($sql);
                          // if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) { 
                             $id=$row["id"];
                              }
                           // }
                      ?>
                        <select name="state" id="state" class="mt-3">
                           <option value="">Select State</option>
                           <?php 
                              while ($row_s = mysqli_fetch_array($query_state)) {    
                                ?>
                              <option value="<?php echo $row_s['id']?>"><?php echo $row_s['name']?></option>  
                          <?php } ?>
                        </select>
                    </div>
                </div>
                <?php 
                    $select_image = "SELECT * FROM userground_details where state_id='$id'";
                    $query_image = mysqli_query($conn, $select_image);
                 ?>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="zonal_state_com text-center mt-3">Zonal & State Sports Committee Members</h2>
                        <div class="row mt-3" id="5" style="display: none;">
                            <div class="col-md-2 col-6">
                              <?php 
                              while ($row_s = mysqli_fetch_array($query_image)) { ?>
                                <img class="member_avatar" src="images/icon/<?php echo $row_s['image']?>">
                                <h3 class="member_name text-center"><?php echo $row_s['name']?></h3>
                                <p class="member_desgn text-center"><?php echo $row_s['designation']?></p>
                                 <?php } ?>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/avatar-01.jpg">
                                <h3 class="member_name text-center">Name</h3>
                                <p class="member_desgn text-center">Designation</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/avatar-01.jpg">
                                <h3 class="member_name text-center">Name</h3>
                                <p class="member_desgn text-center">Designation</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/avatar-01.jpg">
                                <h3 class="member_name text-center">Name</h3>
                                <p class="member_desgn text-center">Designation</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/avatar-01.jpg">
                                <h3 class="member_name text-center">Name</h3>
                                <p class="member_desgn text-center">Designation</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/avatar-01.jpg">
                                <h3 class="member_name text-center">Name</h3>
                                <p class="member_desgn text-center">Designation</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-4">
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>Ground</th>
                              <th>City</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>AA-Ground</td>
                              <td>Chennai</td>
                            </tr>
                             <tr>
                              <td>AA-Ground</td>
                              <td>Chennai</td>
                            </tr>
                             <tr>
                              <td>AA-Ground</td>
                              <td>Chennai</td>
                            </tr>
                             <tr>
                              <td>AA-Ground</td>
                              <td>Chennai</td>
                            </tr>
                             <tr>
                              <td>AA-Ground</td>
                              <td>Chennai</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col-md-8">
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>Date</th>
                              <th>City</th>
                              <th>6:30AM</th>
                              <th>9:00AM</th>
                              <th>11:30AM</th>
                              <th>2:30PM</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>13<sup>th</sup> Sep</td>
                              <td>Chennai</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td>13<sup>th</sup> Sep</td>
                              <td>Chennai</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td>13<sup>th</sup> Sep</td>
                              <td>Chennai</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td>13<sup>th</sup> Sep</td>
                              <td>Chennai</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td>13<sup>th</sup> Sep</td>
                              <td>Chennai</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                </div>
              
                
               
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  // alert('hi');
  $("#state").change(function(){
 // alert('hi');
      var state = $("#state").val();
       // alert(state);
      var images = $(".member_avatar").val();
      var memname = $(".member_name").val();
      var memdesgn = $(".member_desgn").val();
      if (state =='5' ) {
         $("#5").show();
      }
    });
  });
       
</script>

<?php include "footer.php" ?>
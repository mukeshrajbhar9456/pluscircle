<?php include "sidebar.php"; 
$user_id = $_SESSION['user_id'];
if ($user_id == '') {
    session_destroy();
    echo "<script>";
    echo "window.location.href='login.php';";
    echo "</script>";
}
?>
<!-- END MENU SIDEBAR-->
<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- HEADER DESKTOP-->
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-8 offset-2">
                        <div class="pc-card">
                            <div class="card-body">
                                <!-- <div class="card-title">
                                    <h1 class="text-center title-2">+91 OPEN CRICKET TOURNAMENT</h1>
                                    <h2>APPLICATION REGISTRATION FORM FOR THE PLAYERS</h2>
                                    <h3>REGISTER</h3>
                                </div> -->
                                <!--  -->
                                <?php 
                                    $user_details = mysqli_query($conn, "SELECT * FROM users_reg WHERE id = $user_id");
                                    $row_u = mysqli_fetch_array($user_details);
                                    /*$select_state = "SELECT * FROM state";
                                    $query_state = mysqli_query($conn, $select_state);
                                    $row_s = mysqli_fetch_array($query_state);*/
                                ?>
                                <div id="first-form" >
                                    <form method="post" id="register" name="register">
                                       <!--  <div class="row form-group">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">Age Group:</label>
                                            </div>
                                            <div class="col-12 col-md-6">
                                            <?php 
                                                $select_age = "SELECT * FROM age_group WHERE status=1";
                                                $query_age = mysqli_query($conn, $select_age);
                                            ?>
                                                <select class="form-control" required name="age_group" id="age_group">
                                                    <option value="">SELECT ANYONE</option>
                                                    <?php 
                                                        while ($row_a = mysqli_fetch_array($query_age)) { ?>
                                                        <option value="<?php echo $row_a['id']?>" <?php echo ($row_u['age_group'] == $row_a['id'] ) ? 'selected' : '' ?> ><?php echo $row_a['age_title']?></option>  
                                                    <?php } ?>
                                                    
                                                </select>
                                            </div>
                                        </div> -->
                                       <!--  <div class="row form-group">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">TEAM/GROUP/INDIVIDUAL:</label>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <select class="form-control" name="team_individual" id="team_individual" required>
                                                    <option value="">SELECT ANYONE</option>
                                                    <option value="1" <?php echo ($row_u['user_type'] == 1) ? 'selected' : '' ?> >REGISTER AS A TEAM OF 11 MEMBERS.</option>
                                                    <option value="2" <?php echo ($row_u['user_type'] == 2) ? 'selected' : '' ?> >REGISTER AS AN INDIVIDUAL OR GROUP</option>
                                                </select>
                                            </div>
                                        </div> -->
                                       <!--  <div class="row form-group" id="state_sec">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">SELECT YOUR STATE:</label>
                                            </div>
                                            <?php 

                                               
                                                
                                            ?>
                                            <div class="col-12 col-md-6" >
                                                <select class="form-control" name="state" id="state">
                                                    <option value="">SELECT STATE</option>
                                                    <?php 
                                                        while ($row_s = mysqli_fetch_array($query_state)) { ?>
                                                        <option value="<?php echo $row_s['id']?>" <?php echo ($row_u['user_state'] == $row_s['id']) ? 'selected' : '' ?> ><?php echo $row_s['name']?></option>  
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div> -->
                                        <!-- <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="edit-register">Submit</button>
                                        </div> -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    
                    <!--  -->
                <?php 
                $player_details = mysqli_query($conn, "SELECT * FROM players WHERE user_id =$user_id");
                    
                ?>
                <?php 
                    if ($row_u['user_type'] == 1) { ?>
                    <div class="col-md-12">
                        <div class="pc-card">
                            <div class="card-body">
                                <div id="sec-form">
                                    <form method="post" id="team-form" name="team-form">
                                        <p id="otp-msg"></p>
                                        <p id="otp"></p>
                                        <table class="table table-bordered" style="width: 100%;">
                                           <!-- <thead>
                                                <th>P.No</th> 
                                                <th>Name</th>
                                                <th>City</th>
                                                <th>D.O.B</th>
                                                <th>Mobile</th>
                                                <th>Email</th>
                                                <th>Action</th>
                                            </thead>--->
                                            <tbody>
                                                <?php

                                                $i=1;  
                                                    while($row = mysqli_fetch_array($player_details)) { 
                                                ?>
                                                <tr>        
                                                    <td><?php echo $i; ?></td> 
                                                    <td><input type='text' name="name" id="name<?php echo $row['id']; ?>" value="<?php echo $row['player_name'] ?>" placeholder="Name"></td>
                                                    <td>
                                                        <select  name="city" class="city" id="city<?php echo $row['id']; ?>" name="city">
                                                        <?php 
                                                            $row['player_city'];
                                                            $player_state = $row['player_state'];
                                                            $select_c = "SELECT * FROM city WHERE state_id='$player_state' ";
                                                            $query_c = mysqli_query($conn,$select_c);
                                                            if ($row_c['id'] == $row['player_city']) {
                                                                   $selected = "selected";
                                                                }
                                                            ?>
                                                            
                                                        <?php 
                                                            while ($row_c = mysqli_fetch_array($query_c) ) { ?>
                                                                
                                                                <option value="<?php echo $row_c['id']?>" <?php echo ($row_c['id'] == $row['player_city']) ? 'selected' : '' ?> ><?php echo $row_c['city_name']?></option>";
                                                        <?php    }
                                                        ?>
                                                        </select>
                                                    </td>
                                                    <td><input type='date' name="dob" id="dob<?php echo $row['id']; ?>" value="<?php echo $row['player_dob'] ?>" ></td>
                                                    <td>
                                                        <input type='text' name="mobile" id="mobile<?php echo $row['id']; ?>" value=" <?php echo $row['player_mobile'] ?>" placeholder="Mobile" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength="10" min="1">
                                                    </td>
                                                    <td><input type='text' name="email" id="email<?php echo $row['id']; ?>" value=" <?php echo $row['player_email'] ?>" placeholder="Email"></td>
                                                    <td>
                                                        <button class="btn btn-success" type="button" onclick="edit_player(<?php echo $row['id']; ?>)" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>
                                                    <input type="hidden" name="res" id="res" value="<?php echo $user_id?>" >
                                                    <input type="hidden" name="state" id="state" name="state1" value="<?php echo $row_u['user_state']; ?>">
                                                    <input type="hidden" id="team_individual" value="<?php echo $row_u['user_type'];?>">
                                                </tr>
                                                <?php $i++; } ?>  
                                            </tbody>
                                        </table>
                                       <!--  <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="edit-team-register">Submit</button>
                                        </div> -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }else{ ?>
                    <!--  -->
                    <div class="col-md-12">
                        <div class="pc-card">
                            <div class="card-body">
                                <div id="t-form" >
                                    <form method="post" id="group-form" name="group-form">
                                        <div id="tablefields">
                                            <span id="error"></span>
                                            <table class="table table-bordered" style="width: 100%;margin-bottom:10px;" >
                                                <?php

                                                $i=1;  
                                                    while($row = mysqli_fetch_array($player_details)) { 
                                                ?>
                                               <!-- <tr>
                                                    
                                                    <th>Name</th>
                                                    <th>City</th>
                                                    <th>D.O.B</th>
                                                    <th>Mobile</th>
                                                    <th>Email</th>
                                                    
                                                </tr>-->
                                                <tr>
                                                    <td><input type='text' name="g_name" id="name<?php echo $row['id'] ?>" value="<?php echo $row['player_name'] ?>" required placeholder="Name"></td>
                                                    <td>
                                                        <select class="city g_city" id="city<?php echo $row['id'] ?>" name="g_city" required>
                                                        <?php 
                                                            $row['player_city'];
                                                            $player_state = $row['player_state'];
                                                            $select_c = "SELECT * FROM city WHERE state_id='$player_state' ";
                                                            $query_c = mysqli_query($conn,$select_c);
                                                            //echo $row_c['id'];
                                                            ?>
                                                            
                                                        <?php 
                                                            while ($row_c = mysqli_fetch_array($query_c) ) { ?>
                                                                
                                                                <option value="<?php echo $row_c['id']?>" <?php echo ($row_c['id'] == $row['player_city']) ? 'selected' : '' ?> ><?php echo $row_c['city_name']?></option>";
                                                        <?php    }
                                                        ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type='date' name="g_dob" id="dob<?php echo $row['id'] ?>" value="<?php echo $row['player_dob'] ?>" required>
                                                    </td>
                                                    <td>
                                                        <input type='number' name="g_mobile" id="mobile<?php echo $row['id'] ?>" value="<?php echo $row['player_mobile'] ?>" required placeholder="Mobile" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength="10" min="1">
                                                    </td>
                                                    <td>
                                                        <input type='text' name="g_email" id="email<?php echo $row['id'] ?>" value="<?php echo $row['player_email'] ?>" required placeholder="Email">
                                                    </td>
                                                   
                                                    <input type="hidden" name="res" id="res" value="<?php echo $user_id?>">
                                                    <input type="hidden" name="state" id="state1" name="state1">
                                                </tr>    
                                                <!--<tr>
                                                    <th>Player Role</th>
                                                    <th>Batting Style</th>
                                                    <th>Bowlling Style</th>
                                                    <th>Action</th>
                                                </tr>--->
                                                <tr style="margin-bottom: 10px"><?php 
                                                        $select_role = "SELECT * FROM player_role";
                                                        $query_role = mysqli_query($conn, $select_role);
                                                    ?>
                                                    <td class="nodata">
                                                        
                                                    </td>
                                                     <td>
                                                        <select id="role<?php echo $row['id'] ?>" name="g_role" class="g_role" required>
                                                            <option value="">Select one</option>
                                                            <?php 
                                                                while ($row_role = mysqli_fetch_array($query_role)) { ?>
                                                                    <option value="<?php echo $row_role['id']?>" <?php echo ($row['player_role'] == $row_role['id']) ? 'selected' : '' ?> ><?php echo $row_role['role_title']?></option>  
                                                            <?php } ?>
                                                                
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <?php 
                                                            $select_bat = "SELECT * FROM batting";
                                                            $query_bat = mysqli_query($conn, $select_bat);
                                                        ?>
                                                        <select  class="batting" id="batting<?php echo $row['id'] ?>" name="g_batting" required>
                                                            <option value="">Select one</option>
                                                            <?php 
                                                                while ($row_bat = mysqli_fetch_array($query_bat)) { ?>  
                                                                    <option value="<?php echo $row_bat['id']?>" <?php echo ($row['player_bat'] == $row_bat['id']) ? 'selected' : '' ?> ><?php echo $row_bat['batting_type']?></option>  
                                                            <?php } ?>
                                                            
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <?php 
                                                            $select_bowl = "SELECT * FROM bowling";
                                                            $query_bowl = mysqli_query($conn, $select_bowl);
                                                        ?>
                                                        <select  class="bowling" id="bowling<?php echo $row['id'] ?>" name="g_bowling" required>
                                                            <option value="">Select one</option>
                                                            <?php 
                                                                while ($row_bowl = mysqli_fetch_array($query_bowl)) { ?>
                                                                    <option value="<?php echo $row_bowl['id']?>" <?php echo ($row['player_ball'] == $row_bowl['id']) ? 'selected' : '' ?> ><?php echo $row_bowl['bowling_type']?></option>  
                                                            <?php } ?>
                                                           
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-success" type="button" onclick="edit_player(<?php echo $row['id']; ?>)" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </table>
                                            <input type="hidden" name="res" id="res" value="<?php echo $user_id?>" >
                                            <input type="hidden" id="team_individual" value="<?php echo $row_u['user_type'];?>">
                                            <input type="hidden" name="state" id="state" name="state" value="<?php echo $row_u['user_state'];?>">
                                            <input type="hidden" id="table_id" value="1">
                                        </div>
                                        <div class="pc-btn">
                                            <a href="add-new-player.php"><button class="au-btn au-btn--block au-btn--green m-b-20" type="button" id="add_more" name="team-register">Add more</button></a>
                                        </div> 
                                        <h3 id="total_h" style="display: none">Total players <span id="total"></span></h2> 
                                        <!-- <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="group_register2">Submit</button>
                                        </div> -->    
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>    
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">
                            <p>Copyright © 2021 Pluse Circle. All rights reserved. Powered by <a href="https://briotechwebsolutions.com/" target="_blank">Briotech Websolutions</a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>



<?php 
session_start();
include "db_conn.php";
include "mail.php"; 
include "function.php";

date_default_timezone_set('Asia/Kolkata');

?>

<?php 
/* Register */

if(isset($_POST['new_register'])) {
    // print_r('hi');
	$name = $conn->real_escape_string($_POST['username']);
	$number = 	$conn->real_escape_string($_POST['number']);
	$email_id = $conn->real_escape_string($_POST['email_id']);
	$password = $conn->real_escape_string($_POST['password']);

	$date = date("Y-m-d h:i:s");
	$otp = random_int(1000, 9999);

    $insert = "INSERT INTO users_reg (user_reg_id,username, user_phone, user_email, user_password, age_group, user_type, user_status, created_at) VALUES ('','$name', '$number', '$email_id', '$password', '', '', 0, '$date')";
	if ($query = mysqli_query($conn,$insert)) {

		$last_id = mysqli_insert_id($conn);
		$insert_otp = "INSERT INTO users_otp (user_id, user_email, otp) VALUES ('$last_id', '$email_id', '$otp')";

		if (mysqli_query($conn,$insert_otp)) {
		    $update_user1 = "UPDATE users_reg SET user_status=1 WHERE id='$last_id' ";
			$query2 = mysqli_query($conn,$update_user1);
			send_otp_mail($email_id, $otp);
			
			//$data = array('email' => $email_id, 'messages' => "done", "number" => $number, 'r' => $last_id, 'otp' => $otp);
            $data = array('email' => $email_id, 'messages' => "done", "number" => $number, 'r' => $last_id);
			
			$_SESSION["user_id"] = $last_id;
			$_SESSION["user_email"] = $email_id;
		}
	}
	echo json_encode($data);

}
?>
<?php 
if (isset($_POST['login'])) {

	$email_id = $conn->real_escape_string($_POST['email_id']);
	$password = $conn->real_escape_string($_POST['password']);
	//echo "SELECT * FROM users_reg WHERE user_email = '$email_id' AND user_password='$password' AND user_status = 1";
	$check_email1 = $conn->query("SELECT * FROM users_reg WHERE user_email = '$email_id' AND user_password='$password' AND user_status = 1");
	$num_row = $check_email1->num_rows;
	if ($num_row > 0) {
		$row_u = mysqli_fetch_array($check_email1);
		$_SESSION["user_id"] = $row_u['id'];
		$_SESSION["user_email"] = $row_u['user_email'];

		$user_id=$_SESSION["user_id"];

		$check_profile = $conn->query("SELECT * FROM users_reg WHERE id = '$user_id' AND profile_status = 1");
		
		$check_profile2 = $conn->query("SELECT * FROM users_reg WHERE id = '$user_id'");
		
		$row_user = mysqli_fetch_array($check_profile2);
		$pro_row = $check_profile->num_rows;
		if ($row_user['profile_status'] ==1 && $row_user['payment_status'] == 1) {
			echo "success";

		}else{
			
			//$players_details = $conn->query("SELECT COUNT(*) AS player FROM `players` WHERE user_id=$user_id AND player_status=1");

			//$num_p_row = $players_details->num_rows;
			if ($row_user['profile_status'] ==  0) {

				echo 0;

			}else  {
				
				echo 1;
			}
		}
	}else{
		echo "error";
	}
}


?>

<?php 
/* //////////////// OTP ///////////// */

if (isset($_POST['check_email'])) {

	$email = $conn->real_escape_string($_POST['email']);

	$check_email = $conn->query("SELECT * FROM users_reg WHERE user_email = '$email' AND user_status = 1");
	$num_row = $check_email->num_rows;
	if ($num_row > 0) {
		echo "email already";
	}
}


?>

<?php 

if (isset($_POST['number_exists'])) {

	$number = $conn->real_escape_string($_POST['number']);
	//echo "SELECT * FROM users_reg WHERE user_phone = '$number' AND status = 1";
	$check_number = $conn->query("SELECT * FROM users_reg WHERE user_phone = '$number' AND user_status = 1");
	$num_row = $check_number->num_rows;
	if ($num_row > 0) {
		echo "number already";
	}
}

if (isset($_POST['check_otp'])) {
	
	$email = $conn->real_escape_string($_POST['u_email']);
	$otp = $conn->real_escape_string($_POST['otp_number']);
	$user_id = $_POST['reg_id'];

	//echo "SELECT * FROM users_otp WHERE user_email = '$email' AND otp = $otp AND status = 0 AND user_id='$user_id' ";

	$check_otp = $conn->query("SELECT * FROM users_otp WHERE user_email = '$email' AND otp = $otp AND status = 0 AND user_id='$user_id' ");
	$num_row = $check_otp->num_rows;
	if ($num_row > 0) {
		$update_otp = "UPDATE users_otp SET status = 1 WHERE user_email = '$email' AND otp = $otp AND status = 0 AND user_id='$user_id'";
		if ($query1 = mysqli_query($conn,$update_otp)) {

			$update_user1 = "UPDATE users_reg SET user_status=1 WHERE id='$user_id' ";
			$query2 = mysqli_query($conn,$update_user1);

			$select_user = "SELECT * FROM users_reg WHERE id='$user_id'";
			$query = mysqli_query($conn,$select_user);
			$numR = mysqli_num_rows($query);
			if ($numR > 0) {
				$row_u = mysqli_fetch_array($query);

				$_SESSION["user_id"] = $row_u['id'];
				$_SESSION["user_email"] = $row_u['user_email'];
			}
			echo 1;
		}
	}else{
		
		echo 0;
	}
}


/* RESEND OTP */
if (isset($_POST['resend'])) {
	# code...
	$otp = random_int(1000, 9999);
	$email = $conn->real_escape_string($_POST['u_email']);	
	//$user_id = $_POST['reg_id'];

	//if ($user_id == '') {
		$email = $conn->real_escape_string($_POST['u_email']);	
		$insert_otp = "INSERT INTO users_otp (user_email, otp) VALUES ('$email', '$otp')";
			if (mysqli_query($conn,$insert_otp)) {

				send_otp_mail($email, $otp);
				//$data = array('resend' => 1, 'otp' => $otp);
				$data = array('resend' => 1);
				echo json_encode($data);
			}
	//}else{

		//$check_otp = $conn->query("SELECT * FROM users_otp WHERE user_email = '$email' AND status = 0 AND user_id='$user_id' ");
		//$num_row = $check_otp->num_rows;
		/*if ($num_row > 0) {
			$update_otp = "UPDATE users_otp SET status = 1 WHERE user_email = '$email' AND otp = $otp AND status = 0 AND user_id='$user_id'";
			if ($query1 = mysqli_query($conn,$update_otp)) {

				$update_user1 = "UPDATE users_reg SET user_status=1 WHERE id='$user_id' ";
				if ($query2 = mysqli_query($conn,$update_user1)) {

					$insert_otp = "INSERT INTO users_otp (user_id, user_email, otp) VALUES ('$user_id', '$email', '$otp')";
					if (mysqli_query($conn,$insert_otp)) {

						send_otp_mail($email_id, $otp);
						$data = array('resend' => 1, 'otp' => $otp);
						echo json_encode($data);
					}
				}
			}
		}*/
	//}

}

/* State Wise city*/
if (isset($_POST['state_city'])) {
	$state_id = $_POST['state'];
	$select_c = "SELECT * FROM city WHERE state_id=$state_id";
	$query_c = mysqli_query($conn,$select_c);
	$output = "<option value=''>Select city</option>";
	while ($row_c = mysqli_fetch_array($query_c) ) {
		$output .= "<option value=".$row_c['id'].">".$row_c['city_name']."</option>";
	}
	echo $output;
}

if (isset($_POST['table_draw'])) { ?>

<table class="table table-bordered" style="width: 100%;margin-bottom:10px;" >
    <!-- <tr>
        <th>Name</th>
        <th>City</th>
        <th>D.O.B</th>
        <th>Mobile</th>
        <th>Email</th>
    </tr> -->
    <tr>
        <td><input type='text' name="g_name[]" id="name"  placeholder="Player Name" required></td>
        <td>
			<select  class="city g_city" id="g_city" name="g_city[]" required>
				<option value="">Select City</option>
        	<?php $state_id = $_POST['state'];
				$user_id=$_SESSION['user_id'];
				$select_c = "SELECT * FROM city WHERE state_id=$state_id";
				$query_c = mysqli_query($conn,$select_c);
				while ($row_c = mysqli_fetch_array($query_c) ) {?>
					<option value="<?php echo $row_c['id'] ?>"><?php echo $row_c['city_name']?></option>";
			<?php } ?>

            </select>
        </td>
        <td><input type='date' max="2009-12-31" name="g_dob[]" id="dob" required></td>
        <td><input type='number' name="g_mobile[]" id="mobile" required placeholder="Mobile" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength="10" min="1"></td>
        <td><input type='text' name="g_email[]" id="email" required placeholder="Email (optional)"></td>
       
        <input type="hidden" name="res[]" id="res" value="<?php echo $user_id;?>">
        <input type="hidden" name="state" id="state1" name="state1">
   <!--  <tr>
        <th>Player bat</th>
        <th>Batting Style</th>
        <th>Bowlling Style</th>
        <th></th>
    </tr> -->
    <tr><?php 
            $select_role = "SELECT * FROM player_role";
            $query_role = mysqli_query($conn, $select_role);
        ?>
         <td class="nodata"></td>
        <td>
        	<select id="g_role" class="g_role" name="g_role[]" required>
                	<option value="">Select one</option>
            <?php 
                while ($row_role = mysqli_fetch_array($query_role)) { ?>
                    <option value="<?php echo $row_role['id']?>"><?php echo $row_role['role_title']?></option>  
            <?php } ?>
                
            </select>
        </td>
        <td>
            <?php 
                $select_bat = "SELECT * FROM batting";
                $query_bat = mysqli_query($conn, $select_bat);
            ?>
            <select  class="batting" id="g_batting" name="g_batting[]" required>
                <option value="">Select one</option>
                <?php 
                    while ($row_bat = mysqli_fetch_array($query_bat)) { ?>
                        <option value="<?php echo $row_bat['id']?>"><?php echo $row_bat['batting_type']?></option>  
                <?php } ?>
                
            </select>
        </td>
        <td>
            <?php 
                $select_bowl = "SELECT * FROM bowling";
                $query_bowl = mysqli_query($conn, $select_bowl);
            ?>
            <select class="bowling" id="g_bowling" name="g_bowling[]" required>
                <option value="">Select one</option>
                <?php 
                    while ($row_bowl = mysqli_fetch_array($query_bowl)) { ?>
                        <option value="<?php echo $row_bowl['id']?>"><?php echo $row_bowl['bowling_type']?></option>  
                <?php } ?>
               
            </select>
        </td>
        <td><button type="button" class="removeclass btn btn-danger">Remove</button></td>
    </tr>    
</table>
	
<?php }

?>

<?php 
/* Reset*/
if (isset($_POST['forget'])) {

	$email_id = $conn->real_escape_string($_POST['email_id']);
	$otp = random_int(1000, 9999);
	//echo "SELECT * FROM users_reg WHERE user_email = '$email_id' AND user_status = 1";
	$check_email1 = $conn->query("SELECT * FROM users_reg WHERE user_email = '$email_id' AND user_status = 1");
	$num_row = $check_email1->num_rows;
	if ($num_row > 0) {
		
		$insert_otp = "INSERT INTO users_otp (user_email, otp) VALUES ('$email_id', '$otp')";
		
		if (mysqli_query($conn,$insert_otp)) {
			send_otp_reset($email_id, $otp);
		$data = array('email' => $email_id, 'messages' => "done");
		}
			
	}else{
		$data = array('email' => $email_id, 'messages' => "error");
	}
	echo json_encode($data);
}

?>


<?php 
/*   Forget password */



if (isset($_POST['check_otp_forget'])) {
	
	$email = $conn->real_escape_string($_POST['h_email']);
	$otp = $conn->real_escape_string($_POST['otp_number']);

	//echo "SELECT * FROM users_otp WHERE user_email = '$email' AND otp = $otp AND status = 0 AND user_id='$user_id' ";
	
	$check_otp = $conn->query("SELECT * FROM users_otp WHERE user_email = '$email' AND otp = $otp AND status = 0");
	$num_row = $check_otp->num_rows;
	if ($num_row > 0) {
		$update_otp = "UPDATE users_otp SET status = 1 WHERE user_email = '$email' AND otp = $otp AND status = 0 ";
		if ($query1 = mysqli_query($conn,$update_otp)) {

			echo 1;
		}
	}else{
		
		echo 0;
	}
}

?>

<?php 
if (isset($_POST['update_pass'])) {
	$email = $conn->real_escape_string($_POST['h_email']);
	$password = $conn->real_escape_string($_POST['password']);
	$check_otp = $conn->query("SELECT * FROM users_reg WHERE user_email = '$email' AND user_status = 1");
	$num_row = $check_otp->num_rows;
	if ($num_row > 0) {
		$update_otp = "UPDATE users_reg SET user_password = '$password' WHERE user_email = '$email' AND user_status = 1 ";
		if ($query1 = mysqli_query($conn,$update_otp)) {
			echo 1;
		}else{
			echo 0;
		}
	}
}
?>

<?php 
if (isset($_POST['edit_team'])) {
	$team_individual = $_POST["team_individual"];
	$age_group = $_POST["age_group"];
	$p_name = $_POST['p_name'];
	$p_city = $_POST['p_city'];
	$p_dob = $_POST['p_dob'];
	$p_mobile = $_POST['p_mobile'];
	$p_email = $_POST['p_email'];
	$p_res = $_POST['p_res'];
	$p_state1 = $_POST['p_state1'];
	$player = $_POST['player'];
	$role = $_POST['p_role'];
	$player_bat = $_POST['p_bat'];
	$player_bowl = $_POST['p_bowl'];


	$sql = "UPDATE players SET age_group='$age_group', player_type='$team_individual', player_name ='$p_name', player_state='$p_state1', player_city='$p_city', player_dob='$p_dob', player_mobile='$p_mobile', player_email='$p_email', player_role='$role', player_bat='$player_bat', player_ball='$player_bowl' WHERE id='$player' AND user_id='$p_res' ";
	if (mysqli_query($conn,$sql)) {
		echo 1;
	}else{
		echo 0;
	}
	
}
?>
<?php 

/*if (isset($_POST['group_team'])) {
	$team_individual = $_POST["team_individual"];
	$age_group = $_POST["age_group"];
	$p_name = $_POST['p_name'];
	$p_city = $_POST['p_city'];
	$p_dob = $_POST['p_dob'];
	$p_mobile = $_POST['p_mobile'];
	$p_email = $_POST['p_email'];
	$p_res = $_POST['p_res'];
	$p_state1 = $_POST['p_state1'];
	$player = $_POST['player'];

	$sql = "UPDATE players SET age_group='$age_group', player_type='$team_individual', player_name ='$p_name', player_state='$p_state1', player_city='$p_city', player_dob='$p_dob', player_mobile='$p_mobile', player_email='$p_email' WHERE id='$player' AND user_id='$p_res' ";
	if (mysqli_query($conn,$sql)) {
		echo 1;
	}else{
		echo 0;
	}
	
}*/

?>
<?php 
session_start();
include "db_conn.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta name="description" content="au theme template">
        <!-- <meta name="author" content="Hau Nguyen"> -->
        <meta name="keywords" content="au theme template">
        <!-- Title Page-->
        <title>+91 Cricket</title>
        <link rel="icon" href="images/fevicon.png" type="image/x-icon">
        <!-- Fontfaces CSS-->
        <link href="css/font-face.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
        <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
        <!-- Bootstrap CSS-->
        <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
        <!-- Vendor CSS-->
        
        <!--  <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all"> -->
        <!--  <link href="vendor/wow/animate.css" rel="stylesheet" media="all"> -->
        <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <!--   <link href="vendor/slick/slick.css" rel="stylesheet" media="all"> -->
        <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <!--  <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all"> -->
        <!-- Main CSS-->
        <link href="css/theme.css" rel="stylesheet" media="all">
    </head>
    <body>
        <div class="page-wrapper">
            <!-- HEADER MOBILE-->
            <header class="header-mobile d-block d-lg-none">
                <div class="header-mobile__bar">
                    <div class="container-fluid">
                        <div class="header-mobile-inner">
                            <a class="logo" href="index.php">
                                <img src="images/logo.png" alt="CoolAdmin" />
                            </a>
                            <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                            </button>
                        </div>
                    </div>
                </div>
                <nav class="navbar-mobile">
                    <div class="container-fluid">
                        <ul class="navbar-mobile__list list-unstyled">
                             <?php
        if(!empty($_SESSION['user_id'])){ 
                $user_id = $_SESSION['user_id'];
                $user_d = mysqli_query($conn, "SELECT * FROM `users_reg` WHERE id = '$user_id'");
                $row_d = mysqli_fetch_array($user_d);
                //echo "SELECT COUNT(*) AS player FROM `players` WHERE user_id=$user_id AND player_status=1";
                $players_details = $conn->query("SELECT COUNT(*) AS player FROM `players` WHERE user_id=$user_id AND player_status=1");
			    $num_p_row = $players_details->num_rows;
                
        ?>
        <div class="profile0img row">
           <!--  <div class="image img-cir img-120 col-md-4">
                <img src="images/icon/avatar-big-01.jpg" alt="John Doe" />
            </div> -->
           
            <div class="col-md-12">
                <ul>
                    <li>Hi, <?php echo $row_d['username'];?></li>
                    <!-- <li>Reg. no</li> -->
                    <?php 
                        if ($row_d['user_reg_id'] != '') { ?>
                            <li>Reg. no <?php echo $row_d['user_reg_id']?></li>
                    <?php } ?>
                    <?php if ($row_d['profile_status'] == 0 ){ ?>
                        <a href="add-player.php"><li>My profile</li></a>
                    <?php }else if($row_d['payment_status'] == 0){ ?>
                        <a href="payment.php"><li>My profile</li></a>
                    <?php } ?>
                    
                </ul>
            </div>
        </div>
        <?php } ?>
        <style>
            #viewgrd{
                background-color: #004A7F;
                  -webkit-border-radius: 10px;
                  border-radius: 10px;
                  border: none;
                  color: #FFFFFF;
                  cursor: pointer;
                  display: inline-block;
                  font-family: Arial;
                  font-size: 20px;
                  padding: 5px 10px;
                  text-align: center;
                  text-decoration: none;
                  -webkit-animation: glowing 1500ms infinite;
                  -moz-animation: glowing 1500ms infinite;
                  -o-animation: glowing 1500ms infinite;
                  animation: glowing 1500ms infinite;
            }
            @-webkit-keyframes glowing {
                  0% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
                  50% { background-color: #FF0000; -webkit-box-shadow: 0 0 40px #FF0000; }
                  100% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
                }

                @-moz-keyframes glowing {
                  0% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
                  50% { background-color: #FF0000; -moz-box-shadow: 0 0 40px #FF0000; }
                  100% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
                }

                @-o-keyframes glowing {
                  0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
                  50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
                  100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
                }

                @keyframes glowing {
                  0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
                  50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
                  100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
                }
        </style>
                            <div class="top0-border">
                              
                                      <?php if(empty($_SESSION['user_id'])){ ?>

                        <li class="has-sub">
                            <a  href="login.php">Log In </a>
                        </li>
                        <li class="has-sub">
                            <a href="register.php">Register As A New Member</a>
                        </li>
                        
                    <?php } ?>
                                    
                            <li>
                            <a id="viewgrd" href="view-ground.php">View Ground</a>
                        </li>       
                                
                            </div>
                             <?php 
                                        if (!empty($_SESSION['user_id'])) { ?> 

                                            <li class="has-sub"> 
                                            <a href="logout.php?logout=true" >Log Out</a>
                                            </li> 
                       <?php } ?>  
                            <!--  -->
                            <div class="top0-border">
                                    <li>
                                        <a href="view_ground.php">View Ground <span class="badge badge-primary blink_me">ON</span></a>
                                    </li>
                                    <?php 
                                    
                                    if (empty($_SESSION['user_id'])) { ?>
                                        <li>
                                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#access" data-backdrop="false" >My Tournament</a>
                                        </li>
                                     <?php }else { ?>
                                    <li>
                                        <a href="#" class="pop-modal" data-toggle="modal" data-target="#tournament" data-backdrop="false">My Tournament</a>
                                    </li>
                                  <?php } ?>  

                                <!--  -->
                                   <?php if(empty($_SESSION['user_id'])) { ?>
                                        <li>
                                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#access" data-backdrop="false" >My Match</a>
                                        </li>
                                     <?php }else { ?>
                                    <li>
                                        <a href="#" class="pop-modal" data-toggle="modal" data-target="#match" data-backdrop="false">My Match</a>
                                    </li>
                                  <?php } ?>
                                 <!--  -->
                                 <!--  -->
                                   <?php if(empty($_SESSION['user_id'])) { ?>
                                        <li>
                                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#access" data-backdrop="false" >My Team</a>
                                        </li>
                                     <?php }else { ?>
                                    <li>
                                        <a href="my-team.php">My Team</a>
                                    </li>
                                  <?php } ?>
                                 <!--  -->
                                  <!--  -->
                                   <?php if(empty($_SESSION['user_id'])) { ?>
                                        <li>
                                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#access" data-backdrop="false" >My Stats</a>
                                        </li>
                                     <?php }else { ?>
                                    <li>
                                        <a href="#" class="pop-modal" data-toggle="modal" data-target="#stats" data-backdrop="false">My Stats</a>
                                    </li>
                                  <?php } ?>
                                 <!--  -->
                                  <!--  -->
                                   <?php if(empty($_SESSION['user_id'])) { ?>
                                        <li>
                                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#access" data-backdrop="false" >My Performance</a>
                                        </li>
                                     <?php }else { ?>
                                    <li>
                                        <a href="#" class="pop-modal" data-toggle="modal" data-target="#performance" data-backdrop="false">My Performance</a>
                                    </li>
                                  <?php } ?>
                                 <!--  -->

                                <!--  -->
                                   <?php if(empty($_SESSION['user_id'])) { ?>
                                        <li>
                                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#access" data-backdrop="false" >Select My Team</a>
                                        </li>
                                     <?php }else { ?>
                                    <li>
                                        <a href="#" class="pop-modal" data-toggle="modal" data-target="#select-team" data-backdrop="false">Select My Team</a>
                                    </li>
                                  <?php } ?>
                                 <!--  -->
                               
                               
                               
                               
                               
                                
                            </div>
                            <!--  -->
                            <!--  -->
                            <div class="top0-border">

                                <?php if(date("Y-m-d") < "2021-04-15"){ ?>

                                <li class="has-sub" >
                                    <a href="selection.php" >Selection & Promotion</a>
                                    <span class="right0-img">
                                        <img src="images/icon1.png">
                                    </span>
                                </li>
                                <?php }else{ ?>
                                <li class="has-sub">
                                    <a href="selection.php" >Selection & Promotion</a>
                                    <span class="right0-img">
                                        <img src="images/icon1.png">
                                    </span>
                                </li>
                                

                                <?php } ?>

                                <?php if(date("Y-m-d") < "2021-04-15"){ ?>

                                <li class="has-sub">
                                    <a href="prizes.php" >Prizes </a>
                                    <span class="right0-img">
                                        <img src="images/icon2.png">
                                    </span>
                                </li>

                                <?php }else{ ?>
                                <li class="has-sub">
                                    <a href="prizes.php" >Prizes</a>
                                    <span class="right0-img">
                                        <img src="images/icon2.png">
                                    </span>
                                </li>

                                <?php } ?>
                                
                            </div>
                            <!--  -->
                            <!--  -->
                            <div class="top0-border">

                               

                                <li class="has-sub">
                                    <a href="#" class="pop-modal" data-toggle="modal" data-target="#share" class="js-arrow">Share</a> </span>  <span class="right0-img">
                                        <img src="images/icon3.png">
                                    </span>
                                </li>

                             

                            

                                <?php if(date("Y-m-d") < "2021-04-15"){ ?>

                                <li class="has-sub">
                                    
                                    <a class="js-arrow" href="#">
                                        Follow Us
                                        <span class="arrow">
                                            <i class="fas fa-angle-down"></i>
                                        </span>  <span class="right0-img">
                                        <img src="images/icon1.png">
                                        </span>
                                    </a>
                                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                         <li>
                               <a href="https://www.instagram.com/pluscircle_" target="_blank" class="w-spacing"><img src="images/icon/a.png" width="20px;" /> Instagram</a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/PLUS-Circle-Club-100431182187381" target="_blank" class="w-spacing"><img src="images/icon/b.png" width="20px;" /> Facebook</a>
                            </li>
                            <li>
                                 <a href="https://www.linkedin.com/in/plus-circle-1737b4207/" target="_blank" class=" w-spacing"><img src="images/icon/c.png" width="20px;" /> Linkedin</a>
                            </li>
                            <li>
                                 <a href="https://twitter.com/PlusCircleClub1" target="_blank" class="w-spacing"><img src="images/icon/d.png" width="20px;" /> Twitter</a>
                            </li>
                           
                                    </ul>
                                </li>

                                <?php }else{ ?>
                                <li class="has-sub">
                                    
                                    <a class="js-arrow" href="#">
                                        Follow Us
                                        <span class="arrow">
                                            <i class="fas fa-angle-down"></i>
                                        </span>  <span class="right0-img">
                                        <img src="images/icon1.png">
                                        </span>
                                    </a>
                                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                        <li>
                               <a href="https://www.instagram.com/pluscircle_" target="_blank" class="w-spacing"><img src="images/icon/a.png" width="20px;" /> Instagram</a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/PLUS-Circle-Club-100431182187381" target="_blank" class="w-spacing"><img src="images/icon/b.png" width="20px;" /> Facebook</a>
                            </li>
                            <li>
                                 <a href="https://www.linkedin.com/in/plus-circle-1737b4207/" target="_blank" class=" w-spacing"><img src="images/icon/c.png" width="20px;" /> Linkedin</a>
                            </li>
                            <li>
                                 <a href="https://twitter.com/PlusCircleClub1" target="_blank" class="w-spacing"><img src="images/icon/d.png" width="20px;" /> Twitter</a>
                            </li>
                           
                                        
                                    </ul>
                                </li>
                                <?php } ?>

                                <?php if(date("Y-m-d") < "2021-04-15"){ ?>

                                <li class="has-sub">
                                    
                                    <a  href="contact-us.php">
                                        Contact Us
                                         <span class="right0-img">
                                        <img src="images/icon1.png">
                                        </span>
                                    </a>
                               
                                </li>

                                <?php }else{ ?>
                                <li class="has-sub">
                                    
                                    <a class="js-arrow" href="#">
                                        Contact Us
                                        <span class="arrow">
                                            <i class="fas fa-angle-down"></i>
                                        </span>  <span class="right0-img">
                                        <img src="images/icon1.png">
                                        </span>
                                    </a>
                                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                        <li>
                                            <a href="#">  Contact Us 1</a>
                                        </li>
                                        <li>
                                            <a href="#"> Contact Us 2</a>
                                        </li>
                                        <li>
                                            <a href="#"> Contact Us 3</a>
                                        </li>
                                        
                                    </ul>
                                </li>
                                <?php } ?>
                    
                    
                    
                </div>
                <!--  -->
                <!--  -->
                <div class="top0-border">
                    <?php if(date("Y-m-d") < "2021-04-15"){ ?>

                    <li class="has-sub" class="pop-modal" data-toggle="modal" data-target="#exampleModalCenter" data-backdrop="false">
                        <a href="#" class="js-arrow">News</a>
                    </li>
                    <?php }else{ ?>
                     <li class="has-sub" >
                        <a href="#" class="js-arrow">News</a>
                    </li>
                    <?php } ?>
                   

                    <li class="has-sub">
                        <a href="protocols.php" class="">Protocol & Guidelines</a>
                    </li>
                    
                  
                                     
                    
                    
                </div>
                <!--  -->
                
            </ul>
        </div>
    </nav>
</header>
<!-- END HEADER MOBILE-->
<div class="page-container">
    <!-- HEADER DESKTOP-->
    <header class="header-desktop">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                
                <div class="header-wrap">
                    <div class="logo">
                        <a href="https://pluscircleclub.org/">
                            <img src="images/logo.png" alt="Plus Circle" />
                        </a>
                    </div>
                    <div class="pc-logo">
                        <a href="">
                        <img src="images/cricket-logo.png">
                        </a>
                    </div>
                    <div class="header-button">
                        <div class="noti-wrap">
                            <ul class="headerimg">
                                <li><a data-target="#myModalvideo3" data-toggle="modal" href="#myModalvideo3"><img src="https://www.pluscircleclub.org/public/images/header/why.png" class="img01"></a></li>
                                <li><a data-target="#myModalvideo1" data-toggle="modal" href="#myModalvideo1" title="How We Do"><img src="https://www.pluscircleclub.org/public/images/header/howwedo.png" class="img02"></a></li>
                                <li><a data-target="#myModalvideo55" data-toggle="modal" href="#myModalvideo55" title="Your Benefits"><img src="https://www.pluscircleclub.org/public/images/header/benefits.png" class="img03"></a></a></li>
                                <li><a data-target="#milestone" data-toggle="modal" href="#milestone" title="Road Ahead"><img src="https://www.pluscircleclub.org/public/images/header/milestone1.png" class="img04"></a></li>
                                <li><a data-target="#comment" data-toggle="modal" href="#comment" title="Complimentary Motion"><img src="https://www.pluscircleclub.org/public/images/header/complimentory.png" class="img05"></a></li>
                                <li><a href="https://www.pluscircleclub.org/+91cricket/login.php"><img src="https://www.pluscircleclub.org/public/images/header/join.png" class="img06"></a></li>
                            </ul>
                        </div>
                        <!--<div class="account-wrap">-->
                        <!--    <div class="account-item clearfix js-item-menu">-->
                                
                        <!--        <div class="content">-->
                        <!--            <a  href="#"><img src="images/link.png" class="img07"></a>-->
                        <!--        </div>-->
                        <!--        <div class="account-dropdown js-dropdown">-->
                        <!--            <div class="info clearfix">-->
                        <!--                <div class="image">-->
                        <!--                    <a href="#">-->
                        <!--                        <img src="images/icon/avatar-01.jpg" alt="John Doe" />-->
                        <!--                    </a>-->
                        <!--                </div>-->
                        <!--                <div class="content">-->
                        <!--                    <h5 class="name">-->
                        <!--                    <a href="#">john doe</a>-->
                        <!--                    </h5>-->
                        <!--                    <span class="email">johndoe@example.com</span>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--            <div class="account-dropdown__body">-->
                        <!--                <div class="account-dropdown__item">-->
                        <!--                    <a href="#">-->
                        <!--                    <i class="zmdi zmdi-account"></i>Account</a>-->
                        <!--                </div>-->
                        <!--                <div class="account-dropdown__item">-->
                        <!--                    <a href="#">-->
                        <!--                    <i class="zmdi zmdi-settings"></i>Setting</a>-->
                        <!--                </div>-->
                        <!--                <div class="account-dropdown__item">-->
                        <!--                    <a href="#">-->
                        <!--                    <i class="zmdi zmdi-money-box"></i>Billing</a>-->
                        <!--                </div>-->
                        <!--            </div>-->

                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>
</div>
<?php include "sidebar.php"; 
//print_r($_SESSION);
if(!empty($_SESSION['user_id'])){
    echo "<script>window.location.href='index.php'</script>";
}
?>
<!-- END MENU SIDEBAR-->
<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- HEADER DESKTOP-->
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                 <!--  -->
                <div class="row">
                <div class="col-md-3">
                  <div class="logo91"><img src="images/91-logo.png"></div>
                    
                </div>
              </div>
               <!--  -->
                <div class="row">
                    <!--  -->
                    <div class="col-md-8 offset-2">
                        <div class="pc-card">
                            
                            <div class="card-body">
                                <div class="card-title">
                                    
                                    <h3>Register</h3>
                                </div>
                                <!--  -->
                                <div id="first-form" class="regiteform" >
                                    <div class="register-bg"> <img src="images/background/register-bg.png"></div>
                                    <form method="post" id="register" name="register" >
                                        <div class="row form-group">
                                            <div class="col col-md-12">
                                                <label for="text-input" class=" form-control-label">Name:</label>
                                            </div>
                                            <div class="col-12 col-md-12">
                                                <input type="text" id="userName" name="userName" placeholder="Enter your full name" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-12">
                                                <label for="text-input" class=" form-control-label">Only for players in India:</label>
                                            </div>
                                            <div class="col-12 col-md-12 ">
                                                <input type="text" id="number" name="number" class="form-control" placeholder="Mobile Number" required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength="10" min="1">
                                                <p id="phone_error"></p>
                                            </div>
                                             <!-- <div class="col-12 col-md-6">
                                                <input type="number" id="re_number" name="re_number" class="form-control" placeholder="Re enter Mobile Number" required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength="10" min="1">
                                                <p id="phone_error"></p>
                                            </div> -->
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-12">
                                                <label for="text-input" class=" form-control-label">Email Id:</label>
                                            </div>
                                            <div class="col-12 col-md-12 ">
                                                <input type="email" id="email_id" name="email_id" class="form-control" placeholder="Enter your  email id" required>
                                                <p id="email_error"></p>
                                            </div>
                                           
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-12">
                                                <label for="text-input" class=" form-control-label">Enter Password:</label>
                                            </div>
                                            <div class="col-12 col-md-6 mar-mob">
                                                <input type="password" class="form-control" id="password"  name="password" placeholder="Enter your password" required autocomplete="off">
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <input type="password" class="form-control" id="c_password" name="c_password" placeholder="Re enter password" required autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="pc-btn">
                                            
                                            <p class="bottom-text">BY SIGNING IN YOU AGREE TO OUR <br><a href="https://www.pluscircleclub.org/terms_of_use">TERMS OF USE</a> & <a href="https://www.pluscircleclub.org/privacy_policy">PRIVACY POLICY</a></p>
                                        <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="register">Let's Register</button></div>
                                    </form>
                                </div>
                                <div class="con-form" style="display: none;">
                                    <div class="con-num">
                                    <h3 id="con-num"></h3>
                                    <h3 id="con-email"></h3>
                                </div>

                                    <div class="pc-btn p020">
                                        <button class="au-btn au-btn--block au-btn--green m-b-20" type="button" name="change" id="change">Edit</button> <a href="add-player.php"><button class="au-btn au-btn--block au-btn--green m-b-20" type="button" name="continue" id="continue">Continue</button></a>
                                    </div>
                                   
                                </div>
                                <div id="sec-form" style="display: none;" class="pc-form pc-form1">
                                     <div class="register-otp-bg"> <img src="images/background/register-otp-bg.png"></div>
                                    <form method="post" id="otp-form" name="otp-form">
                                        <p id="otp-msg"></p>
                                        <p id="otp"></p>
                                        <div class="row form-group">
                                            <div class="col col-md-12">
                                                <label for="text-input" class=" form-control-label">Enter OTP:</label>
                                            </div>
                                            <div class="col-12 col-md-12">
                                                <!-- <input type="text" id="otp_number" name="otp_number" maxlength="4"  class="form-control" >
                                                  <svg viewBox="0 0 240 1" xmlns="http://www.w3.org/2000/svg">
                                                    <line x1="0" y1="0" x2="240" y2="0" stroke="black" />
                                                  </svg> -->
                                                 
                                                <input type="text" id="otp_number" name="otp_number" maxlength="4"  class="form-control" required>  
                                               <!--  <input type="text" id="digit-1" name="digit-1" data-next="digit-2" />
                                                <input type="text" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-1" />
                                                <input type="text" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-2" />
                                                <input type="text" id="digit-4" name="digit-4" data-next="digit-5" data-previous="digit-3" /> -->
                                                <p id="otp-error"></p>
                                                <input type="hidden" name="u_email" id="u_email">
                                                <input type="hidden" name="r" id="r">
                                                <p id="phone_error"></p>
                                            </div>
                                        </div>
                                        
                                        <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="register">Submit OTP</button>
                                            <button class="au-btn au-btn--block au-btn--green m-b-20 btn2 " type="button" id="resend" name="resend"> Resend Otp</button>
                                           </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
               
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>
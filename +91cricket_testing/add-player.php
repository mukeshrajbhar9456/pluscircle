<?php include "sidebar.php"; 
$user_id = $_SESSION['user_id'];

if ($user_id == '') {
    session_destroy();
    echo "<script>";
    echo "window.location.href='login.php';";
    echo "</script>";
}
?>
<?php 
$players_details = "SELECT * FROM `players` WHERE user_id=$user_id AND player_status=1";
$players_q = mysqli_query($conn, $players_details);
$num_row = mysqli_num_rows($players_q);
// if ($num_row > 0) {
//     echo "<script>";
//     echo "window.location.href='index.php';";
//     echo "</script>";
// }
?>
<!-- END MENU SIDEBAR-->
<!-- PAGE CONTAINER-->

<div class="page-container">
    <!-- HEADER DESKTOP-->
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                 <!--  -->
                <div class="row">
                <div class="col-md-3">
                  <div class="logo91"><img src="images/91-logo.png"></div>
                    
                </div>
              </div>
               <!--  -->
                <div class="row">
                    <!--  -->
                    <div class="col-md-8 offset-2">
                        <div class="pc-card">
                            <div class="card-body">
                                
                                <!--  -->
                                <div id="first-form" class="add-player" >
                                    <form method="post" id="register" name="register">
                                        <div class="row form-group">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">Age Group:</label>
                                            </div>
                                            <div class="col-12 col-md-6">
                                            <?php 
                                                $select_age = "SELECT * FROM age_group WHERE status=1";
                                                $query_age = mysqli_query($conn, $select_age);
                                            ?>
                                                <select class="form-control" required name="age_group" id="age_group">
                                                    <option value="">SELECT ANYONE</option>
                                                    <?php 
                                                        while ($row_a = mysqli_fetch_array($query_age)) { ?>
                                                        <option value="<?php echo $row_a['id']?>"><?php echo $row_a['age_title']?></option>  
                                                    <?php } ?>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">Team/Group/Individual</label>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <select class="form-control" name="team_individual" id="team_individual" required>
                                                    <option value="">SELECT ANYONE</option>
                                                    <option value="1">REGISTER AS A TEAM OF 11 MEMBERS.</option>
                                                    <option value="2">REGISTER AS AN INDIVIDUAL OR GROUP</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group" id="state_sec" style="display: none">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">Select Your State:</label>
                                            </div>
                                            <?php 

                                                $select_state = "SELECT * FROM state";
                                                $query_state = mysqli_query($conn, $select_state);
                                                
                                            ?>
                                            <div class="col-12 col-md-6" >
                                                <select class="form-control" name="state" id="state">
                                                    <option value="">SELECT STATE</option>
                                                    <?php 
                                                        while ($row_s = mysqli_fetch_array($query_state)) { ?>
                                                        <option value="<?php echo $row_s['id']?>"><?php echo $row_s['name']?></option>  
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="col-md-12">
                        <div class="pc-card">
                            <div class="card-body">
                                <div id="sec-form" style="display: none;">
                                    <p class="col-p">Note: You can edit your team upto 7 days before the match date. 
                                        <b>Players from 12-15 are optional</b>. You can leave them blank or add them later.
                                    </p>
                                    <form method="post" id="team-form" name="team-form">
                                        <p id="otp-msg"></p>
                                        <p id="otp"></p>
                                        <table class="table table-bordered" style="width: 100%;">
                                           
                                            <tbody>
                                                <?php
                                                $i=1;  
                                                    for ($x = 1; $x <= 15; $x++) { ?>
                                                <tr>        
                                                    <td><?php echo $x; ?></td> 
                                                    <td><input type='text' name="name[]" id="name" <?php echo ($i ==1) ? 'required' : '' ?> placeholder="Player Name"></td>
                                                    <td><select  name="city" class="city" id="city" name="city[]" <?php echo ($i ==1) ? 'required' : '' ?>>
                                                        <option value="">Select City</option>
                                                        </select>
                                                    </td>
                                                    <td><input type='date' max="2009-12-31" name="dob[]" id="dob" <?php echo ($i ==1) ? 'required' : '' ?> ></td>
                                                    <td><input type='number' name="mobile[]" id="mobile" <?php echo ($i ==1) ? 'required' : '' ?> placeholder="Mobile" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength="10" min="1"></td>
                                                    <td><input type='email' name="email[]" id="email" placeholder="Email (optional)"></td>
                                                    <input type="hidden" name="res[]" id="res" value="<?php echo $user_id?>" >
                                                    <input type="hidden" name="state" id="state1" name="state1">
                                                </tr>
                                                <?php $i++; } ?>  
                                            </tbody>
                                        </table>
                                        
                                        
                                        <div class="pc-btn">
                                            <p class="bottom-text"> I agree to all the <a href="https://www.pluscircleclub.org/terms_of_use">terms and conditions</a></p>
                                            <button class="au-btn au-btn--block au-btn--green m-b-20 " type="submit"><a class="glyphicon glyphicon-arrow-left"style="color:#c5ab7a;" href="https://pluscircleclub.org/+91cricket/register.php">Back</a></button>
                                            &nbsp;&nbsp;&nbsp; <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="team-register">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="col-md-12">
                        <div class="pc-card">
                            <div class="card-body">
                                <div id="t-form" style="display: none;">
                                    <form method="post" id="group-form" name="group-form">
                                        <div id="tablefields">
                                            <span id="error"></span>
                                            <table class="table table-bordered" style="width: 100%;margin-bottom:10px;" >
                                               
                                                <tr>
                                                    <td><input type='text' name="g_name[]" id="name" required placeholder="Player Name"></td>
                                                    <td>
                                                        <select class="city g_city" id="g_city" name="g_city[]" required>
                                                            <option value="">Select City</option>
                                                        </select>
                                                    </td>
                                                    <td><input type='date' max="2009-12-31" name="g_dob[]" id="dob" required></td>
                                                    <td><input type='number' name="g_mobile[]" id="mobile" required placeholder="Mobile" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength="10" min="1"></td>
                                                    <td><input type='email' name="g_email[]" id="email" placeholder="Email (optional)"></td>
                                                   
                                                    <input type="hidden" name="res[]" id="res" value="<?php echo $user_id?>">
                                                    <input type="hidden" name="state" id="state1" name="state1">
                                               <!--  <tr>
                                                    <th>Player bat</th>
                                                    <th>Batting Style</th>
                                                    <th>Bowlling Style</th>
                                                </tr> -->
                                                <tr><?php 
                                                        $select_role = "SELECT * FROM player_role";
                                                        $query_role = mysqli_query($conn, $select_role);
                                                    ?>
                                                    <td class="nodata"></td>
                                                    <td class="nodata"></td>

                                                     <td>
                                                        <select id="g_role" name="g_role[]" class="g_role" required>
                                                            <option value="">Select one</option>
                                                            <?php 
                                                                while ($row_role = mysqli_fetch_array($query_role)) { ?>
                                                                    <option value="<?php echo $row_role['id']?>"><?php echo $row_role['role_title']?></option>  
                                                            <?php } ?>
                                                                
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <?php 
                                                            $select_bat = "SELECT * FROM batting";
                                                            $query_bat = mysqli_query($conn, $select_bat);
                                                        ?>
                                                        <select  class="batting" id="g_batting" name="g_batting[]" required>
                                                            <option value="">Select one</option>
                                                            <?php 
                                                                while ($row_bat = mysqli_fetch_array($query_bat)) { ?>  
                                                                    <option value="<?php echo $row_bat['id']?>"><?php echo $row_bat['batting_type']?></option>  
                                                            <?php } ?>
                                                            
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <?php 
                                                            $select_bowl = "SELECT * FROM bowling";
                                                            $query_bowl = mysqli_query($conn, $select_bowl);
                                                        ?>
                                                        <select  class="bowling" id="g_bowling" name="g_bowling[]" required>
                                                            <option value="">Select one</option>
                                                            <?php 
                                                                while ($row_bowl = mysqli_fetch_array($query_bowl)) { ?>
                                                                    <option value="<?php echo $row_bowl['id']?>"><?php echo $row_bowl['bowling_type']?></option>  
                                                            <?php } ?>
                                                           
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                            <input type="hidden" name="res[]" id="res" value="<?php echo $user_id?>" >
                                            <input type="hidden" name="state" id="state1" name="state1">
                                            <input type="hidden" id="table_id" value="1">
                                        </div>
                                        <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="button" id="add_more" name="team-register">Add more</button>
                                        </div> 
                                        <h3 id="total_h" style="display: none">Total players <span id="total"></span></h2> 
                                        <div class="pc-btn">
                                             <p class="bottom-text"> I agree to all the <a href="https://www.pluscircleclub.org/terms_of_use">terms and conditions</a></p>
                                              <button class="au-btn au-btn--block au-btn--green m-b-20 " type="submit"><a class="glyphicon glyphicon-arrow-left"style="color:#c5ab7a;" href="https://pluscircleclub.org/+91cricket/register.php">Back</a></button>
                                            &nbsp;&nbsp;&nbsp;<button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="group_register2">Submit</button>
                                        </div>    
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               <!--  <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">
                            <p>Copyright © 2021 Pluse Circle. All rights reserved. Powered by <a href="https://briotechwebsolutions.com/" target="_blank">Briotech Websolutions</a>.</p>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>



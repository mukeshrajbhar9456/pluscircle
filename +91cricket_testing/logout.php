<?php 
session_start();
if(isset($_GET['logout']))
{
	$id = $_SESSION['user_id'];
  	unset($_SESSION['user_id']);
  	session_unset();
  //header('location: index.php');
  // Note: This will destroy the session, and not just the session data!
	// if (ini_get("session.use_cookies")) {
	//     $params = session_get_cookie_params();
	//     setcookie(session_name(), '', time() - 42000,
	//         $params["path"], $params["domain"],
	//         $params["secure"], $params["httponly"]
	//     );
	// }
  session_destroy();
  echo "<script>window.location.href='index.php';</script>";

}
?>
<?php include "sidebar.php"; ?>
<!-- END MENU SIDEBAR-->
<!-- PAGE CONTAINER-->
<div class="page-container">
  <!-- HEADER DESKTOP-->
  
  <!-- HEADER DESKTOP-->
  <!-- MAIN CONTENT-->
  <div class="main-content home-bg">
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        
        <!--  section 1 -->
        
        <!-- section 1 end -->
        <!-- section 2  -->
        <div class="row">
          <div class="col-md-4">
            
            <div class="content1">
              <div class="row align0-center">
                <div class="col-md-3 col-sm-3 col-3"> <p class="float0-right"> <span class="NormalCharacterStyle40"><sup>+</sup>91</span> </p></div>
                <div class="col-md-9 col-sm-9 col-9"> <p class="NormalCharacterStyle4">National Open</p>
                <p> <span class="NormalCharacterStyle39">Cricket</span> </p>
                <p><span class="NormalCharacterStyle38">Tournament</span> <span class="NormalCharacterStyle8">2021</span></p>
              </div>
            </div>
          </div>
          <!--  -->
           <div class="col-md-4 show-mobile">
            <div class="content5">
              <img src="images/prize.png">
              <span class="NormalCharacterStyle8"><i class="fa fa-inr"></i> 1,50,00,000</span>
            </div>
          </div>    
          <!--  -->
           <div class="col-md-4 show-mobile">
            
            <div class="content2 text-center">
              <img src="images/logo.png">
            </div>
          </div>
         <!--  <div class="col-md-4 show-mobile">
            
            <div class="content2 text-center">
              <img src="images/border-img.png">
              <div class="content2-box ">
                <p class="NormalCharacterStyle44x">Registration starts<br> from <br>05<sup>th</sup> July 2021</p>

              </div>
            </div>
          </div> -->

          <!--  -->
          <!--  -->
         
          <!--  -->
          <!--  -->
         <!--  <div class="content4">
            <p class="NormalCharacterStyle42">Matches in</p>
            <p class="NormalCharacterStyle41"> 107+ Cities in India</p>
          </div> -->
          <!--  -->
          <!--  -->
          <div class="content3" style="padding-top: 118px;">
            <p class="NormalCharacterStyle42">Prizes for winners:</p>
            <p class="NormalCharacterStyle41">State, Zonal & National</p>
          </div>
          <!--  -->
          
        </div>

        <div class="col-md-4 show-desktop">

          <div class="content5">
            <img src="images/prize.png">
            <span class="NormalCharacterStyle8"><i class="fa fa-inr"></i> 1,50,00,000</span>
          </div>
        </div>
        <div class="col-md-4">
          
          <div class="content2 text-center show-desktop">
            <img src="images/logo.png">
            <!-- <div class="content2-box ">
              <p class="NormalCharacterStyle44">Registration starts<br> from <br>05<sup>th</sup> July 2021</p>
            </div> -->
          </div>
           <div class="content4">
            <p class="NormalCharacterStyle42">Matches in</p>
            <p class="NormalCharacterStyle41"> 107+ Cities in India</p>
          </div>

          <div class="content8 text-center" >
            <ul style="margin-top: 85px!important;">
              <li><span class="NormalCharacterStyle12">U 15</span>
              <!-- <img src="images/icon02.png"> -->
            </li>
            <li><span class="NormalCharacterStyle12">U 19</span></li>
          <!-- <img src="images/icon02.png"></li> -->
          <li><span class="NormalCharacterStyle12">19+</span></li>
        <!-- <img src="images/icon02.png"></li> -->
        <li><span class="NormalCharacterStyle12">30+</span></li>
      <!-- <img src="images/icon02.png"></li> -->
    </ul>
    <ul>
      <li><img src="images/icon02.png"></li>
    </ul>
  </div>
  <!--  -->
</div>
</div>
<!-- section 2 end -->
<!-- section 3 -->
<div class="row">
<!--  -->
<div class="col-md-4 show-desktop col-sm-5">
  <style>
    .content-new {
    }
  </style>
  
   <div class="content-new ">
    <!-- <img  src="images/play3.png"> -->
    <p class="NormalCharacterStyle2">Play for a</p>
    <p class="NormalCharacterStyle5">Cause!</p>
    <ul>
      <li>
        <img src="images/img.png">
        <p class="NormalCharacterStyle">Orphan</p>
      </li>
      <li>
        <img src="images/img1.png">
        <p class="NormalCharacterStyle">Old Age</p>
      </li>
    </ul>
    <p class="NormalCharacterStyle21">Your participation can support an orphan or a homeless elderly</p>
  </div> 
</div>
<!--  -->
<style>
  .liveimg{
    margin-top: -247px;
    width: 100%;
    margin-left: -124px;
    /*position: absolute;*/
  }
    /*margin-top: -74px;
    width: 100%;
    margin-left: -23px;
    position: absolute;*/
</style>
<div class="col-md-6 col-sm-7">
  <div class="content6">
    <!-- <p class="NormalCharacterStyle"><i>STREAMING</i></p> -->
    <img class="liveimg" src="images/live.png">
    <!-- <p class="NormalCharacterStyle2">Registration Fee <span  class="NormalCharacterStyle105 text-center"></span></p> -->
    <table style="margin-top:-252px;">
      <tr>
        <td><p class="NormalCharacterStyle17"><i> &check;</i> Individual</p></td>
        <td><p class="NormalCharacterStyle14 text-right"><span class="NormalCharacterStyle15"><i class="fa fa-inr"></i></span> 600/- &nbsp;&nbsp; + GST</p></td>
      </tr>
      <tr>
        <td><p class="NormalCharacterStyle17"><i> &check;</i> Group <span class="NormalCharacterStyle15">(Per Head)</span></p></td>
        <td><p class="NormalCharacterStyle14 text-right"><span class="NormalCharacterStyle15"><i class="fa fa-inr"></i></span> 600/- &nbsp;&nbsp; + GST</p></td>
      </tr>
      <tr>
        <td><p class="NormalCharacterStyle17"><i> &check;</i> Team  <span class="NormalCharacterStyle15"></span></p></td>
        <td><p class="NormalCharacterStyle14 text-right"><span class="NormalCharacterStyle15"><i class="fa fa-inr"></i></span> 6,325/- &nbsp; + GST</p></td>
      </tr>
    </table>
    
  </div>
  <!--  -->
  <div class="content7">
    
    
    <div class="content7-img">
      <!-- <p class="NormalCharacterStyle6">Hurry!</p> -->
      <!--   <img src="images/btn.png"> -->
      <!-- <img src="images/start-icon.png" class="start-icon"> -->
      <!--  <span class="">Limited Registration</span> -->
      <span class="pc0-btn NormalCharacterStyle9"><b>Valid upto 7<sup>th</sup>Aug.2021</b></span>
      <a href="register.php" class="NormalCharacterStyle38">
        Click Here to Register
      </a>
    </div>
    
  </div>
  <!--  -->
</div>
<!--  -->
<div class="col-md-4 show-mobile">
  <!-- <img style="width: 100% !important;height: 100% !important;" src="images/play3.png"> -->
  <div class="content-new ">
    <p class="NormalCharacterStyle2">Play for a</p>
    <p class="NormalCharacterStyle5">Cause!</p>
    <ul>
      <li>
        <img src="images/img.png">
        <p class="NormalCharacterStyle">Orphan</p>
      </li>
      <li>
        <img src="images/img1.png">
        <p class="NormalCharacterStyle">Old Age</p>
      </li>
    </ul>
    <p class="NormalCharacterStyle21">Your participation can support an orphan or a homeless elderly</p>
  </div>
</div>
<!--  -->
<div class="content9" style="position: absolute;
    right: 75px;
    z-index: -1;
    bottom: 69px;">
  <img src="images/bg-img.png">
</div>
</div>
<!-- section 3 end -->
<!-- section 4  -->

<!-- section 4 end -->

</div>
</div>
</div></div>
<!-- END MAIN CONTENT-->
<!-- END PAGE CONTAINER-->
</div>
<!-- <br>
<style>
  .myGallery {
    padding-top: 12px;
}
.myGallery img {
  /*display: grid;*/
  width: 110px;
  height: 110px;
  /*margin-top: 100px;*/
  margin-left:30px;
  border-radius: 50px;
}
.zonedata{
  /*margin-left: 79px;*/
  text-align: center;
}
.maindata{
  margin-left: 241px;
}
.imtext{
  margin-left: 40px!important;
}
.imstext{
  margin-left: 80px !important;
}
.vl {
  border-left: 6px solid gray;
  height: 200px;
  position: absolute;
  left: 50px;
  /*margin-left: -3px;
  top: 0;*/
}
@media only screen and (max-width: 600px){
  .maindata{
  margin-left: 1px !important;
}

.zonedata{
  /*margin-left: 79px !important;*/
  text-align: center;
}

.myGallery img {
 margin-left:100px !important;

}
}
</style>

<style>
  li img{
    float: left;
    border-radius: 50px;
  }
  .content81{
    float: left;
  }
 .content23 li{
   margin-top: 40px!important;
 }
</style>
  <div class="content8 text-center content23" style="background-color: rgb(247,221,170);">
    <ul style="padding-bottom: 15px;padding-top: 15px;">
      <p class="NormalCharacterStyle" style="float: left;margin-left:20%;"><u>North Zone</u></p>
      <li style="float: left;margin-left: -157px;">
        <img src="images/icon/avatar-01.jpg">
        <p><b>Pawan Singh</b><br>Chairman</p>
      </li>
      <li style="float: left;margin-left: ;">
        <img src="images/icon/avatar-01.jpg">
        <p><b>Jitendra Kaushik</b><br>C.E.O</p>
      </li>
        <p class="NormalCharacterStyle" style="float: left;margin-left: "><u>East Zone</u></p>
      <li style="margin-left:-118px;float: left;">
        <img src="images/icon/avatar-01.jpg">
        <p><b>Dr.Bidyut Mukherjee</b><br>Chairman</p>
      </li>
      <li style="margin-left:;float: left;">
        <img src="images/icon/avatar-01.jpg">
        <p><b>Shurashankha Karmakar</b><br>C.E.O</p>
      </li>
      <p class="NormalCharacterStyle"style="float: left;"><u>South Zone</u></p>
      <li style="margin-left:-147px;float: left;">
        <img src="images/icon/avatar-01.jpg">
         <p><b>Raghav Mathood T</b><br>Chairman</p>
      </li>
      <li style="margin-left:;float: left;">
        <img src="images/icon/avatar-01.jpg">
        <p><b>G.Sreedhar Kumark</b><br>Treasurer</p>
      </li>
      <p class="NormalCharacterStyle"style="float: left;"><u>West Zone</u></p>
      <li style="margin-left:-126px;float: left;">
        <img src="images/icon/avatar-01.jpg">
        <p><b>Dr.Lakshank Mehta</b><br>Chairman</p>
      </li>
      <li style="">
        <img src="images/icon/avatar-01.jpg">
        <p><b>Shelly Tibak</b><br>C.E.O</p>
      </li>
      
    </ul>
  </div>  
<div class="row maindata text-center"  style="background-color: rgb(253,233,196);">
   <div class="col-md-3 "><p class="NormalCharacterStyle">Zonal Sports Committee Members</p>
    
   </div>
   <div class="col-md-3"><p class="NormalCharacterStyle"><img style="width: 40px;" src="images/what.png">9513072088</p>
   </div>
   <div class="col-md-3"><p class="NormalCharacterStyle">bliss@plus<span style="color: rgb(216,174,64);">circle</span>club.org</p>
   </div>
   <div class="col-md-3"><p class="NormalCharacterStyle">www.plus<span style="color: rgb(216,174,64);">circle</span>circle</span>club.org</p>
   </div>
</div>
<div class="row maindata"  style="width:auto;background-color: rgb(253,233,196);">
  <div class="col-md-6">
    <h4 style="float: left;color: gray;">*https://www.pluscircleclub.org/+91cricket/protocols.php</h4>
   </div>
   <div class="col-md-6"><p style="float: right;font-size:20px;">© 2021<b> Plus Circle</b>.</p>
   </div>
  </div> -->
<?php include "footer.php"; ?>
<?php require 'header.php'; ?>
<aside class="menu-sidebar2">
    <div class="menu-sidebar2__content js-scrollbar1">
        <?php
        if(!empty($_SESSION['user_id'])){ 
                $user_id = $_SESSION['user_id'];
                $user_d = mysqli_query($conn, "SELECT * FROM `users_reg` WHERE id = '$user_id'");
                $row_d = mysqli_fetch_array($user_d);
                //echo "SELECT COUNT(*) AS player FROM `players` WHERE user_id=$user_id AND player_status=1";
                $players_details = $conn->query("SELECT COUNT(*) AS player FROM `players` WHERE user_id=$user_id AND player_status=1");
                $num_p_row = $players_details->num_rows;
                
        ?>
        <div class="profile0img row">
           <!--  <div class="image img-cir img-120 col-md-4">
                <img src="images/icon/avatar-big-01.jpg" alt="John Doe" />
            </div> -->
           
            <div class="col-md-12">
                <ul>
                    <li>Hi, <?php echo $row_d['username'];?></li>
                    <!-- <li>Reg. no</li> -->
                    <?php 
                        if ($row_d['user_reg_id'] != '') { ?>
                            <li>Reg. no <?php echo $row_d['user_reg_id']?></li>
                    <?php } ?>
                    <?php if ($row_d['profile_status'] == 0 ){ ?>
                        <a href="add-player.php"><li>My profile</li></a>
                    <?php }else if($row_d['payment_status'] == 0){ ?>
                        <a href="payment.php"><li>My profile</li></a>
                    <?php } ?>
                    
                </ul>
            </div>
        </div>
        <?php } ?>
        <nav class="navbar-sidebar2">
            <ul class="list-unstyled navbar__list">
                <div class="top0-border">
                    
                    <?php if(empty($_SESSION['user_id'])){ ?>

                        <li class="">
                            <a  href="login.php">Log In </a>
                        </li>
                        <li>
                            <a href="register.php">Register As A New Member</a>
                        </li>
                    <?php } ?>
                    <?php 
                        if(!empty($_SESSION['user_id'])) { ?>
                            <li class="has-sub">
                                <a href="logout.php?logout=true">Log Out</a>
                            </li>
                    <?php } ?>
                </div>
                <!--  -->
                <div class="top0-border">
                        <li>
                            <a href="view_ground.php">View Ground <span class="badge badge-primary blink_me">ON</span></a>
                        </li>
                    <?php if (empty($_SESSION['user_id'])) { ?>
                        <li>
                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#access" data-backdrop="false" >My Tournament</a>
                        </li>
                    <?php }else { ?>
                        <li>
                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#tournament" data-backdrop="false">My Tournament</a>
                        </li>
                    <?php } ?>                    
                       
                    <?php if (empty($_SESSION['user_id'])) { ?>
                        <li>
                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#access" data-backdrop="false" >My Match</a>
                        </li>
                    <?php }else { ?>
                        <li>
                           <a href="#"  class="pop-modal"  data-toggle="modal" data-target="#match" data-backdrop="false">My Match</a>
                        </li>
                    <?php } ?>
                    
                    <?php if (empty($_SESSION['user_id'])) { ?>
                        <li>
                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#access" data-backdrop="false" >My Team</a>
                        </li>
                    <?php }else { ?>
                        <li class="has-sub">
                            <a href="my-team.php">My Team</a>
                        </li>
                    <?php } ?>

                   
                    <?php if (empty($_SESSION['user_id'])) { ?>
                        <li>
                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#access" data-backdrop="false" >My Stats</a>
                        </li>
                    <?php }else { ?>
                        <li>
                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#stats" data-backdrop="false">My Stats</a>
                        </li>
                    <?php } ?>

                    <?php  if(empty($_SESSION['user_id'])) { ?>
                        <li>
                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#access" data-backdrop="false" >My Performance</a>
                        </li>
                    <?php }else { ?>
                        <li>
                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#performance" data-backdrop="false">My Performance</a>
                        </li>
                    <?php } ?>
                    

                    <?php if(empty($_SESSION['user_id'])) { ?>
                        <li>
                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#access" data-backdrop="false" >Select My Team</a>
                        </li>
                    <?php }else { ?>
                        <li>
                            <a href="#" class="pop-modal" data-toggle="modal" data-target="#select-team" data-backdrop="false">Select My Team</a>
                        </li>
                    <?php } ?>   
                    
                </div>
                <!--  -->
                <!--  -->
                <div class="top0-border">
                    <?php if(date("Y-m-d") < "2021-07-05"){ ?>

                    <li>
                        <a href="selection.php">Selection & Promotion</a>
                        <span class="right0-img">
                            <img src="images/icon1.png">
                        </span>
                    </li>
                    <?php }else{ ?>
                    <li>
                        <a href="selection.php">Selection & Promotion</a>
                        <span class="right0-img">
                            <img src="images/icon1.png">
                        </span>
                    </li>

                    <?php } ?>
                    <?php if(date("Y-m-d") < "2021-04-15"){ ?>

                    <li>
                        <a href="prizes.php">Prizes</a>
                        <span class="right0-img">
                            <img src="images/icon2.png">
                        </span>
                    </li>
                    <?php }else{ ?>
                    <li>
                        <a href="prizes.php">Prizes</a>
                        <span class="right0-img">
                            <img src="images/icon2.png">
                        </span>
                    </li>
                    <?php } ?>
                    
                </div>
                <!--  -->
                <!--  -->
                <div class="top0-border">
                  
                    <li>
                        <a href="#" class="pop-modal" data-toggle="modal" data-target="#share">Share</a> </span>  <span class="right0-img share-icon">
                        <img src="images/icon3.png">
                        </span>
                    </li>
                
                    <?php if(date("Y-m-d") < "2021-07-05"){ ?>

                    <li class="has-sub">
                        
                        <a class="js-arrow" href="#">
                            Follow Us
                            <span class="arrow">
                                <i class="fas fa-angle-down"></i>
                            </span>  <span class="right0-img">
                            <img src="images/icon4.png">
                        </span>
                        </a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">
                            <li>
                               <a href="https://www.instagram.com/pluscircle_" target="_blank" class="w-spacing"><img src="images/icon/a.png" width="20px;" /> Instagram</a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/PLUS-Circle-Club-100431182187381" target="_blank" class="w-spacing"><img src="images/icon/b.png" width="20px;" /> Facebook</a>
                            </li>
                            <li>
                                 <a href="https://www.linkedin.com/in/plus-circle-1737b4207/" target="_blank" class=" w-spacing"><img src="images/icon/c.png" width="20px;" /> Linkedin</a>
                            </li>
                            <li>
                                 <a href="https://twitter.com/PlusCircleClub1" target="_blank" class="w-spacing"><img src="images/icon/d.png" width="20px;" /> Twitter</a>
                            </li>
                            
                        </ul>
                    </li>
                    <?php }else{ ?>
                    <li class="has-sub">
                        
                        <a class="js-arrow" href="#">
                            Follow Us
                            <span class="arrow">
                                <i class="fas fa-angle-down"></i>
                            </span>  <span class="right0-img">
                            <img src="images/icon4.png">
                        </span>
                        </a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">
                            <li>
                               <a href="https://www.instagram.com/pluscircle_" target="_blank" class="w-spacing"><img src="images/icon/a.png" width="20px;" /> Instagram</a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/PLUS-Circle-Club-100431182187381" target="_blank" class="w-spacing"><img src="images/icon/b.png" width="20px;" /> Facebook</a>
                            </li>
                            <li>
                                 <a href="https://www.linkedin.com/in/plus-circle-1737b4207/" target="_blank" class=" w-spacing"><img src="images/icon/c.png" width="20px;" /> Linkedin</a>
                            </li>
                            <li>
                                 <a href="https://twitter.com/PlusCircleClub1" target="_blank" class="w-spacing"><img src="images/icon/d.png" width="20px;" /> Twitter</a>
                            </li>
                            
                        </ul>
                    </li>

                    <?php } ?>
                    

                    <li class="has-sub">
                        <a  href="contact-us.php">
                            Contact Us
                            <span class="arrow">
                               
                            </span>
                            <span class="right0-img">
                                <img src="images/icon5.png">
                            </span>
                        </a>
                     
                    </li>
                    
                </div>
                <!--  -->
                <!--  -->
                <div class="top0-border">
                    <?php if(date("Y-m-d") < "2021-07-05"){ ?>

                    <li>
                        <a href="#" class="pop-modal" data-toggle="modal" data-target="#exampleModalCenter">News</a>
                    </li>
                    <?php }else{ ?>
                    <li>
                        <a href="#">News</a>
                    </li>
                    <?php } ?>
                    <li>
                        <a href="protocols.php">Protocol & Guidelines</a>
                    </li>
                    
                </div>
                <!--  -->
            </ul>
        </nav>
    </div>
</aside>
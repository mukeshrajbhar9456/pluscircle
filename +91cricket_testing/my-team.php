<?php include "sidebar.php"; 
$user_id = $_SESSION['user_id'];
if ($user_id == '') {
    session_destroy();
    echo "<script>";
    echo "window.location.href='login.php';";
    echo "</script>";
}
?>

<!-- END MENU SIDEBAR-->
<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- HEADER DESKTOP-->
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                 <!--  -->
                <div class="row">
                <div class="col-md-3">
                  <div class="logo91"><img src="images/91-logo.png"></div>
                    
                </div>
              </div>
               <!--  -->
               <div class="row p60">
                 <div class="col-md-12">
                    <div class="prize-title">
                      <h1>My Team </h1>
                    </div>
                 </div>
              </div>
                <div class="row">
                    <!--  -->
                  
                    <!--  -->
                    <div class="col-md-12">
                        <div class="pc-card">
                            <div class="card-body">
                                <div class="pc1-btn">      
                                    <a href="#" class="pop-modal" data-toggle="modal" data-target="#edit-teampopup"><i class="fa fa-edit"></i></a>
                                </div>
                                <div id="team-form"  class="team-padd">
                                    <?php 
                                        $user_id = $_SESSION['user_id'];
                                        $user_details = mysqli_query($conn, "SELECT * FROM users_reg WHERE id = $user_id");
                                        $row_u = mysqli_fetch_array($user_details);
                                        $players_details = "SELECT * FROM `players` WHERE user_id=$user_id AND player_status=1";
                                        $players_q = mysqli_query($conn, $players_details);
                                        
                                        
                                        
                                        
                                        
                                        
                                    ?>
                                    <?php if ($row_u['user_type'] == 1) { ?>
                                        <table class="table table-bordered" style="width: 100%;">
                                            <tr>
                                                <th>S.No</th>
                                                <th>Name</th>
                                                <th>City</th>
                                                <th>DOB</th>
                                                <th>Mobile</th>
                                                <th>Email</th>
                                                
                                            </tr>
                                           
                                            <tbody>
                                               <?php

                                                $i=1;  
                                                    while($row = mysqli_fetch_array($players_q)) { 
                                                ?>
                                                <tr>        
                                                    <td><?php echo $i; ?></td> 
                                                    <td><?php echo $row['player_name'] ?></td>
                                                    <td>
                                                        <?php 
                                                            $player_city = $row['player_city'];
                                                            if(!empty($player_city)){
                                                               $select_c = "SELECT * FROM city WHERE id=$player_city ";
                                                            $query_c = mysqli_query($conn,$select_c);
                                                            $row_c = mysqli_fetch_array($query_c);  
                                                                echo $row_c['city_name'];
                                                            }else{
                                                                echo '';
                                                            }
                                                            ?>
                                                        
                                                    </td>
                                                    <td><?php echo $row['player_dob'] ?></td>
                                                    <td>
                                                        <?php echo $row['player_mobile'] ?>
                                                    </td>
                                                    <td><?php echo $row['player_email'] ?></td>
                                                    
                                                </tr>
                                                <?php $i++; } ?>
                                            </tbody>
                                        </table>
                                    <?php }else if ($row_u['user_type'] == 2) { ?>
                                    <!-- -->
                                    <table class="table table-bordered" style="width: 100%;">
                                        <tr>
                                            <th>S.No</th>
                                            <th>Name</th>
                                            <th>City</th>
                                            <th>DOB</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                            <th>Player Role</th>
                                            <th>Batting Style</th>
                                            <th>Bowling Style</th>
                                        </tr>
                                       
                                        <tbody>
                                           <?php

                                                $i=1;  
                                                    while($row = mysqli_fetch_array($players_q)) { 
                                                ?>
                                                <tr>        
                                                    <td><?php echo $i; ?></td> 
                                                    <td><?php echo $row['player_name'] ?></td>
                                                    <td>
                                                        <?php 
                                                            $player_city = $row['player_city'];
                                                            if(!empty($player_city)){
                                                               $select_c = "SELECT * FROM city WHERE id=$player_city ";
                                                            $query_c = mysqli_query($conn,$select_c);
                                                            $row_c = mysqli_fetch_array($query_c);  
                                                                echo $row_c['city_name'];
                                                            }else{
                                                                echo '';
                                                            }
                                                            ?>
                                                        
                                                    </td>
                                                    <td><?php echo $row['player_dob'] ?></td>
                                                    <td>
                                                        <?php echo $row['player_mobile'] ?>
                                                    </td>
                                                    <td><?php echo $row['player_email'] ?></td>
                                                    <td>
                                                        <?php 
                                                            $sql_role = mysqli_query($conn, "SELECT * FROM player_role WHERE id = ".$row["player_role"]);
                                                            $row_role = mysqli_fetch_array($sql_role);
                                                            echo $row_role['role_title'];
                                                            ?>
                                                        </td>
                                                    <td><?php
                                                            $sql_bat = mysqli_query($conn, "SELECT * FROM batting WHERE id = ".$row["player_bat"]);
                                                            $row_bat = mysqli_fetch_array($sql_bat);
                                                            echo $row_bat['batting_type']; ?>
                                                    </td>
                                                    <td><?php 
                                                            $sql_bol = mysqli_query($conn, "SELECT * FROM bowling WHERE id = ".$row["player_ball"]);
                                                            $row_bol = mysqli_fetch_array($sql_bol);
                                                            echo $row_bol['bowling_type'];
                                                        ?>
                                                    </td>
                                                      
                                                </tr>
                                                <?php $i++; } ?>
                                        </tbody>
                                    </table>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                   
                </div>
               <!--  <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">
                            <p>Copyright © 2021 Pluse Circle. All rights reserved. Powered by <a href="https://briotechwebsolutions.com/" target="_blank">Briotech Websolutions</a>.</p>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>



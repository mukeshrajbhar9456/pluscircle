$(document).ready(function () {
         // Slider 
         jQuery("#carousel").owlCarousel({
           autoplay: true,
           lazyLoad: true,
           loop: true,
           margin: 0,
           dots:false,
           responsiveClass: true,
           autoHeight: true,
           autoplayTimeout: 7000,
           smartSpeed: 800,
           nav: true,
           responsive: {
             0: {
               items: 1
             },
           }
         });
         jQuery("#upcoming-activities-slider").owlCarousel({
          autoplay: true,
          lazyLoad: true,
          loop: true,
          margin: 6,
          dots:false,
          responsiveClass: true,
          autoHeight: true,
          autoplayTimeout: 7000,
          smartSpeed: 800,
          nav: true,
          responsive: {
            0: {
              items: 1             
            },
             600:{
                   items: 2,                   
               },
             1024:{                 
                   items: 3               
               }
            }
        });
        

         jQuery("#upcoming-activities-2").owlCarousel({
           autoplay: false,
           lazyLoad: true,
           loop: true,
           margin: 15,
           dots:false,
           responsiveClass: true,
           autoplayTimeout: 7000,
           smartSpeed: 800,
           nav: true,
           responsive: {
             0: {
               items: 1
             },

                600:{
                    items: 2,
                    dots: false
                },
                1200:{
                    items: 3
                }
              }
          });


          $('.country').on('change',function(){
            $country=$('.country').val();
            console.log($country);
            $.ajax({
              type : 'get',
              url: 'getstate/{country_id}',
              data : {'country_id':$country},
              success:function(res){
                  $('#state').html(res);
              }                 
            })          
          });

          window.onclick = function(event) {
            if (!event.target.matches('.dropbtn')) {
              var dropdowns = document.getElementsByClassName("dropdown-content");
              var i;
              for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                  openDropdown.classList.remove('show');
                }
              }
            }
          }

          var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
            };

        });
/** document . ready outside */
        $.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset}});

        jQuery(document).ready(function($) {
           $(".dateRangeWrapper").append($(".date-label"));
        }); 


         // dashboard left tab tooltip js
            // $(document).ready(function(){
            //   $('[data-toggle="tooltip"]').tooltip();
            // });

        


function eventModal(eventid){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'POST',
        url:"ajaxgetevent",
        data:{
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "event_id": eventid
            },
        success:function(data){
            var pkt=JSON.parse(data);

            console.log(pkt.event_profile);
            if(pkt.event_profile != null){
                $('#myModal_upcoming').find('.event_image > img.event_profile').attr('src', pkt.event_profile);
            }
            $('#myModal_upcoming').find('.event_id').html(pkt.id);
            var name=pkt.eventtitle.toUpperCase()+"("+pkt.eventtype+")";
            $('#myModal_upcoming').find('.event_name').html(name);
            $('#myModal_upcoming').find('.organiser_email').html(pkt.organiseremail);
            $('#myModal_upcoming').find('.venue').html(pkt.address);
            $('#myModal_upcoming').find('.event_type').html(pkt.eventtype);
            var from = pkt.startdate+' '+pkt.starttime
            var to = pkt.enddate+' '+pkt.endtime
            $('#myModal_upcoming').find('.from').html(from);
            $('#myModal_upcoming').find('.to').html(to);
            $('#myModal_upcoming').find('.event_facility').html(pkt.eventfacility);
            $('#myModal_upcoming').find('.description').html(pkt.description);
            $('#myModal_upcoming').find('.available_tickets').html('Available Tickets : '+50);
            $('#myModal_upcoming').find('.ticket_price').html('Price : '+pkt.ticketprice);
            $('#myModal_upcoming').find('.organised_by').html(pkt.organisername);
            $('#myModal_upcoming').find('button.buy_ticket').attr("onclick","buy_ticket("+pkt.id+")");
            if(pkt.max <= 0){
                $('#myModal_ticketpricing .button-plus').attr( "disabled", "disabled" );
                $('#myModal_upcoming').find('button.buy_ticket').attr("disabled","disabled");
            }
            $('#myModal_upcoming').find('.max').html(pkt.max); 
            $('#myModal_upcoming').modal('show');

            
        }
    });
}
function search_event(){
   var country=$('#search_country').val(); 
   var state=$('#search_state').val(); 
   var city=$('#search_city').val(); 
//    console.log(country);
//    console.log(state);
//    console.log(city);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'POST',
        url:"event_search_ajax",
        data:{
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "country": country,
            "state": state,
            "city": city
            },
        success:function(data){
            $('#upcoming-activities-2').html(data);            
        }
    });
}
function buy_ticket(eventid){
    console.log(eventid);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'POST',
        url:"ajaxgetevent",
        data:{
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "event_id": eventid
            },
        success:function(data){
            var pkt=JSON.parse(data);            
            $('#myModal_ticketpricing').find('.event_id').val(pkt.id); 
            $('#myModal_ticketpricing').find('.ticket_price').html(pkt.ticketprice); 
            if(pkt.max <= 0){
                $('#myModal_ticketpricing').find('.total_price').html(0);  
                $('#myModal_ticketpricing').find('#event_payment_btn').attr('disabled','disabled');       
            }
            else{
                $('#myModal_ticketpricing').find('.total_price').html(pkt.ticketprice);         
            }
            $('#myModal_ticketpricing').find('input.quantity-field').attr('max', pkt.max);
            $('#myModal_ticketpricing').find('input.quantity-field').attr('min', 1);
            $('#myModal_ticketpricing').find('.max').html(pkt.max);
            $('#myModal_ticketpricing').modal('show');

            
        }
    });


}
function checkValue() {
    var valww=$('#myModal_ticketpricing').find('#No_of_tickets').val();
    
}
function checkValuePlus(){
    var valww=$('#myModal_ticketpricing').find('#No_of_tickets').val();
    $('#myModal_ticketpricing input.button-minus').removeAttr('disabled');
    var max=$('#myModal_ticketpricing').find('input.quantity-field').attr('max');
    max=parseInt(max);       
    if( valww == max){
        $('#myModal_ticketpricing .button-plus').attr( "disabled", "disabled" );
    }
    else{
        valww=parseInt(valww);
        valww=valww+1;
        console.log(max+"="+valww);
        
        var price=$('#myModal_ticketpricing').find('.ticket_price').text();
        price=parseInt(price);
        totalPrice=valww*price;
        $('#myModal_ticketpricing').find('.total_price').html(totalPrice);
    }

}
function checkValueMinus(){
    var valww=$('#myModal_ticketpricing').find('#No_of_tickets').val();
    $('#myModal_ticketpricing .button-plus').removeAttr("disabled");
    valww=parseInt(valww);
    valww=valww-1;
    if(valww < 1 ){
        console.log('yes');
        $('#myModal_ticketpricing .button-minus').attr( "disabled", "disabled" );
        console.log('done');
    }    
    else{
        var price=$('#myModal_ticketpricing').find('.ticket_price').text();
        price=parseInt(price);
        totalPrice=valww*price;
        $('#myModal_ticketpricing').find('.total_price').html(totalPrice);
    }  
}
<?php include "sidebar.php"; 
$user_id = $_SESSION['user_id'];
if ($user_id == '') {
    session_destroy();
    echo "<script>";
    echo "window.location.href='https://briotech.xyz/pluse-circle/login.php';";
    echo "</script>";
}
?>
<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 offset-2">
                        <div class="pc-card">
                            
                            <div class="card-body">
                                <div class="card-title">
                                    <h1 class="text-center title-2">+91 OPEN CRICKET TOURNAMENT</h1>
                                    <h2>APPLICATION REGISTRATION FORM FOR THE PLAYERS</h2>
                                    <h3>RESET PASSWORD</h3>
                                </div>
                                <!--  -->
                                <div id="reset-form">
                                    <form method="post">
                                        <div class="row form-group">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">Email Id:</label>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <input type="email" id="email_f" name="email_f" class="form-control" placeholder="Email" required>
                                                <p id="email-err"></p>
                                            </div>
                                        </div>                                        
                                        <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" id="forget-form" type="button" name="forget-form">Submit</button>
                                        </div>
                                    </form>
                                </div>

                                <!--  -->
                                 <div id="reset-form-one" style="display: none;">
                                    <form method="post" id="forget-otp" name="forget-otp">
                                        <p id="reset-msg"></p>
                                        <p id="resetOtp"></p>
                                        <div class="row form-group">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">Enter OTP:</label>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <input type="number" id="otp_number_r" name="otp_number_r" class="form-control" placeholder="Enter OTP" required>
                                                <p id="otp-error_r"></p>
                                                <input type="hidden" name="u_email" id="u_email">
                                            </div>
                                        </div>                                        
                                        <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="forget_otp">Submit OTP</button>
                                            <button class="au-btn au-btn--block au-btn--green m-b-20 btn2 btn3" type="button" id="resend" name="resend">Resend OTP</button>
                                           
                                           </div>
                                    </form>
                                </div>
                                <!--  -->
                                <!--  -->
                                <div id="reset-form-sec" style="display: none;">
                                    <form method="post" id="login" name="login">
                                       
                                      <div class="row form-group">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">Password:</label>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <input type="password" id="f_password" name="f_password" class="form-control" placeholder="Password" required autocomplete="off">
                                               
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">Re Enter Password:</label>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <input type="password" id="f_r_password" name="f_r_password" class="form-control" placeholder="Re Enter Password" required autocomplete="off">
                                               
                                            </div>
                                        </div>                                        
                                        <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="register">Submit</button>
                                           
                                           </div>
                                    </form>
                                </div>
                                <!--  -->
                               
                               
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">
                            <p>Copyright © 2021 Pluse Circle. All rights reserved. Powered by <a href="https://briotechwebsolutions.com/" target="_blank">Briotech Websolutions</a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>
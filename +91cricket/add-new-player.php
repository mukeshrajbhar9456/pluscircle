<?php include "sidebar.php"; 
$user_id = $_SESSION['user_id'];
if ($user_id == '') {
    session_destroy();
    echo "<script>";
    echo "window.location.href='login.php';";
    echo "</script>";
}
?>
<!-- END MENU SIDEBAR-->
<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- HEADER DESKTOP-->
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <!--  -->
                    <!-- <div class="col-md-8 offset-2">
                        <div class="pc-card">
                            <div class="card-body">
                                <div class="card-title">
                                    <h1 class="text-center title-2">+91 OPEN CRICKET TOURNAMENT</h1>
                                    <h2>APPLICATION REGISTRATION FORM FOR THE PLAYERS</h2>
                                    <h3>REGISTER</h3>
                                </div>
                               
                                <div id="first-form" >
                                    <form method="post" id="register" name="register">
                                        <div class="row form-group">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">Age Group:</label>
                                            </div>
                                            <div class="col-12 col-md-6">
                                            <?php 
                                                $select_age = "SELECT * FROM age_group WHERE status=1";
                                                $query_age = mysqli_query($conn, $select_age);
                                            ?>
                                            	<select class="form-control" required name="age_group" id="age_group">
                                                    <option value="">SELECT ANYONE</option>
                                                    <?php 
                                                        while ($row_a = mysqli_fetch_array($query_age)) { ?>
                                                        <option value="<?php echo $row_a['id']?>"><?php echo $row_a['age_title']?></option>  
                                                    <?php } ?>
                                            		
                                            	</select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">TEAM/GROUP/INDIVIDUAL:</label>
                                            </div>
                                            <div class="col-12 col-md-6">
                                            	<select class="form-control" name="team_individual" id="team_individual" required>
                                            		<option value="">SELECT ANYONE</option>
                                            		<option value="1">REGISTER AS A TEAM OF 11 MEMBERS.</option>
                                            		<option value="2">REGISTER AS AN INDIVIDUAL OR GROUP</option>
                                            	</select>
                                            </div>
                                        </div>
                                        <div class="row form-group" id="state_sec" style="display: none">
                                            <div class="col col-md-6">
                                                <label for="text-input" class=" form-control-label">SELECT YOUR STATE:</label>
                                            </div>
                                           	<?php 

                                           		$select_state = "SELECT * FROM state";
                                           		$query_state = mysqli_query($conn, $select_state);
                                                
                                           	?>
                                            <div class="col-12 col-md-6" >
	                                            <select class="form-control" name="state" id="state">
	                                            	<option value="">SELECT STATE</option>
	                                            	<?php 
	                                            		while ($row_s = mysqli_fetch_array($query_state)) { ?>
	                                            		<option value="<?php echo $row_s['id']?>"><?php echo $row_s['name']?></option>	
	                                            	<?php } ?>
	                                            </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!--  -->
                    <!-- <div class="col-md-12">
                        <div class="pc-card">
                            <div class="card-body">
                                <div id="sec-form" style="display: none;">
                                    <form method="post" id="team-form" name="team-form">
                                        <p id="otp-msg"></p>
                                        <p id="otp"></p>
                                        <table class="table table-bordered" style="width: 100%;">
                                            <thead>
                                                <th>P.No</th> 
                                                <th>Name</th>
                                                <th>City</th>
                                                <th>D.O.B</th>
                                                <th>Mobile</th>
                                                <th>Email</th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $i=1;  
                                                    for ($x = 1; $x <= 11; $x++) { ?>
                                                <tr>        
                                                    <td><?php echo $x; ?></td> 
                                                    <td><input type='text' name="name[]" id="name" <?php echo ($i ==1) ? 'required' : '' ?> placeholder="Name"></td>
                                                    <td><select  name="city" class="city" id="city" name="city[]" <?php echo ($i ==1) ? 'required' : '' ?>>
                                                        </select>
                                                    </td>
                                                    <td><input type='date' name="dob[]" id="dob" <?php echo ($i ==1) ? 'required' : '' ?> ></td>
                                                    <td><input type='text' name="mobile[]" id="mobile" <?php echo ($i ==1) ? 'required' : '' ?> placeholder="Mobile" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength="10" min="1"></td>
                                                    <td><input type='text' name="email[]" id="email" <?php echo ($i ==1) ? 'required' : '' ?> placeholder="Email"></td>
                                                    <input type="hidden" name="res[]" id="res" value="<?php echo $user_id?>" >
                                                    <input type="hidden" name="state" id="state1" name="state1">
                                                </tr>
                                                <?php $i++; } ?>  
                                            </tbody>
                                        </table>
                                        
                                        
                                        <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="team-register">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!--  -->
                    <?php 
                    $user_details = mysqli_query($conn, "SELECT * FROM users_reg WHERE id = $user_id");
                    $row_u = mysqli_fetch_array($user_details);

                    ?>
                    <div class="col-md-12">
                        <div class="pc-card">
                            <div class="card-body">
                                <div id="t-form" >
                                    <form method="post" id="group-form-new" class="group-form" name="group-form-new">
                                        <div id="tablefields">
                                            <span id="error"></span>
                                            <input type="hidden" name="state" id="state" value="<?php echo $row_u['user_state']; ?>">
                                            <input type="hidden" id="age_group" value="<?php echo $row_u['age_group']; ?>">
                                            <input type="hidden" id="team_individual" value="<?php echo $row_u['user_type']; ?>">
                                           
                                            <input type="hidden" name="res[]" id="res" value="<?php echo $user_id?>" >
                                            
                                            <input type="hidden" name="state" id="state1" name="state1">
                                            <?php 
                                                $players_details2 = $conn->query("SELECT COUNT(*) AS player FROM `players` WHERE user_id=$user_id AND player_status=1");
                                                $row_p2 = mysqli_fetch_array($players_details2);
                                                ?>
                                            <input type="hidden" id="table_id" value="<?php echo $row_p2['player']?>">
                                        </div>
                                        <?php if ($row_p2['player'] > 10) { ?>
                                            <div class="pc-btn">
                                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="button" id="add_more" name="team-register">Add more</button>
                                            </div> 
                                        <?php }?>
                                        <h3 id="total_h" style="display: none">Total players <span id="total"></span></h2> 
                                        <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="add_group_register2">Submit</button>
                                        </div>    
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">
                            <p>Copyright © 2021 Pluse Circle. All rights reserved. Powered by <a href="https://briotechwebsolutions.com/" target="_blank">Briotech Websolutions</a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>



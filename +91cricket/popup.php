<?php include ('sidebar.php'); ?>

 
 <style>
.modal-window {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 999;
  visibility: hidden;
  opacity: 0;
  pointer-events: none;
  transition: all 0.3s;
}
.modal-window:target {
  visibility: visible;
  opacity: 1;
  pointer-events: auto;
}
.modal-window > div {
  width: 400px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  padding: 2em;
  background: #C5AB7A;
}
.modal-window header {
  font-weight: bold;
}
.modal-window h1 {
  font-size: 150%;
  margin: 0 0 15px;
}

.modal-close {
    color: #aaa;
    line-height: 29px;
    /* font-size: 80%; */
    position: absolute;
    right: 0;
    text-align: center;
    top: 0;
    width: 32px;
    text-decoration: none;
    font-size: 20px;
    background: #a78c5b;
    margin: 20px;
    padding: 0px 10px 3px 10px;
    color: #000;
    font-weight: 900;
}
.modal-close:hover {
  color: black;
}


.container {
  display: grid;
  justify-content: center;
  align-items: center;
  height: 100vh;
}

.modal-window > div {
  border-radius: 1rem;
}

.modal-window div:not(:last-of-type) {
  margin-bottom: 15px;
}

small {
  color: lightgray;
}

.btn {
  background-color: white;
  padding: 1em 1.5em;
  border-radius: 1rem;
  text-decoration: none;
}
.btn i {
  padding-right: 0.3em;
}
 </style>
 <div class="page-container back-blure">
  <!-- HEADER DESKTOP-->
  
  <!-- HEADER DESKTOP-->
  <!-- MAIN CONTENT-->
  <div class="main-content home-bg">

<div class="container">
  <div class="interior">
    <a class="btn" href="#open-modal">👋 Basic CSS-Only Modal</a>
  </div>
</div>
<div id="open-modal" class="modal-window">
  <div>
    <a href="#" title="Close" class="modal-close">x</a>
    <h1>Voilà!</h1>
    <div>A CSS-only modal based on the :target pseudo-class. Hope you find it helpful.</div>
    <div><small>Check out</small></div>
    <a href="https://aminoeditor.com" target="_blank">👉 Amino: Live CSS Editor for Chrome</div>
    </div>
</div>


</div>
</div>

<?php 
include ('footer.php');
?>

  



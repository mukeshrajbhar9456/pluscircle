<?php include "sidebar.php"; 
//print_r($_SESSION);
?>
<!-- END MENU SIDEBAR-->
<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- HEADER DESKTOP-->
    <!-- MAIN CONTENT-->
    <div class="main-content multiple-bg">
         <div class="login-bg"><img src="images/background/login-bg.png"> </div>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
               <!--  -->
                <div class="row">
                <div class="col-md-3">
                  <div class="logo91"><img src="images/91-logo.png"></div>
                    
                </div>
              </div>
               <!--  -->
                <div class="row">
                    <!--  -->
                    <div class="col-md-8 offset-2">
                        <div class="pc-card">
                            
                            <div class="card-body">
                                <div class="card-title">
                                    <h3>Log In</h3>
                                </div>
                                <!--  -->
                                <div id="login-form" class="pc-form" >
                                    <form method="post" id="login" name="login">
                                        <div id="log-error"></div>
                                        <div class="row form-group">
                                           
                                            <div class="col-12 col-md-12">
                                                <input type="email" id="email_id_l" name="email_id" class="form-control" placeholder="Enter Your Email Id " required>
                                               
                                            </div>
                                        </div>
                                        <div class="row form-group">                                           
                                            <div class="col-12 col-md-12">
                                                <input class="form-control" id="password_l" type="password" name="password" placeholder="Enter Your Password" required autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="register">SUBMIT</button>
                                            <a href="forget-password.php"><button class="au-btn au-btn--block au-btn--green m-b-20 btn2" type="button" name="login">Forgot Password</button></a>
                                           </div>
                                    </form>
                                </div>
                               
                               
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
               <!--  <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">
                            <p>Copyright © 2021 Pluse Circle. All rights reserved. Powered by <a href="https://briotechwebsolutions.com/" target="_blank">Briotech Websolutions</a>.</p>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>
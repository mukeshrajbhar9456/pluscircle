<?php include "sidebar.php"; ?>
<!-- END MENU SIDEBAR-->
<!-- PAGE CONTAINER-->
<div class="page-container">
  <!-- HEADER DESKTOP-->
  
  <!-- HEADER DESKTOP-->
  <!-- MAIN CONTENT-->
  <div class="main-content home-bg">
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        
        <!--  section 1 -->
        
        <!-- section 1 end -->
        <!-- section 2  -->
        <div class="row">
          <div class="col-md-4">
            
            <div class="content1">
              <div class="row align0-center">
                <div class="col-md-3 col-sm-3 col-3"> <p class="float0-right"> <span class="NormalCharacterStyle40"><sup>+</sup>91</span> </p></div>
                <div class="col-md-9 col-sm-9 col-9"> <p class="NormalCharacterStyle4">National Open</p>
                <p> <span class="NormalCharacterStyle39">Cricket</span> </p>
                <p><span class="NormalCharacterStyle38">Tournament</span> <span class="NormalCharacterStyle8">2021</span></p>
              </div>
            </div>
          </div>
          <!--  -->
           <div class="col-md-4 show-mobile">
            <div class="content5">
              <img src="images/prize.png">
              <span class="NormalCharacterStyle8"><i class="fa fa-inr"></i> 1,50,00,000</span>
            </div>
          </div>
          <!--  -->
          <div class="col-md-4 show-mobile">
            
            <div class="content2 text-center">
              <img src="images/border-img.png">
              <div class="content2-box ">
                <p class="NormalCharacterStyle44">Registration starts<br> from <br>05<sup>th</sup> July 2021</p>
              </div>
            </div>
          </div>
          <!--  -->
          <!--  -->
         
          <!--  -->
          <!--  -->
          <div class="content4">
            <p class="NormalCharacterStyle42">Matches in</p>
            <p class="NormalCharacterStyle41"> 107+ Cities in India</p>
          </div>
          <!--  -->
          <!--  -->
          <div class="content3">
            <p class="NormalCharacterStyle42">Prizes for winners:</p>
            <p class="NormalCharacterStyle41">State, Zonal & National</p>
          </div>
          <!--  -->
          
        </div>
        <div class="col-md-4 show-desktop">
          <div class="content5">
            <img src="images/prize.png">
            <span class="NormalCharacterStyle8"><i class="fa fa-inr"></i> 1,50,00,000</span>
          </div>
        </div>
        <div class="col-md-4">
          
          <div class="content2 text-center show-desktop">
            <img src="images/border-img.png">
            <div class="content2-box ">
              <p class="NormalCharacterStyle44">Registration starts<br> from <br>05<sup>th</sup> July 2021</p>
            </div>
          </div>
          <!--  -->
          <div class="content8 text-center">
            <ul>
              <li><span class="NormalCharacterStyle12">U 15</span>
              <img src="images/icon02.png">
            </li>
            <li><span class="NormalCharacterStyle12">U 19</span>
          <img src="images/icon02.png"></li>
          <li><span class="NormalCharacterStyle12">19+</span>
        <img src="images/icon02.png"></li>
        <li><span class="NormalCharacterStyle12">30+</span>
      <img src="images/icon02.png"></li>
    </ul>
  </div>
  <!--  -->
</div>
</div>
<!-- section 2 end -->
<!-- section 3 -->
<div class="row">
<!--  -->
<div class="col-md-4 show-desktop col-sm-5">
  <div class="content-new ">
    <p class="NormalCharacterStyle2">Play for a</p>
    <p class="NormalCharacterStyle5">Cause!</p>
    <ul>
      <li>
        <img src="images/img.png">
        <p class="NormalCharacterStyle">Orphan</p>
      </li>
      <li>
        <img src="images/img1.png">
        <p class="NormalCharacterStyle">Old Age</p>
      </li>
    </ul>
    <p class="NormalCharacterStyle21">Your participation can support an orphan or a homeless elderly</p>
  </div>
</div>
<!--  -->
<div class="col-md-6 col-sm-7">
  <div class="content6">
    <p class="NormalCharacterStyle2">Registration Fee <span class="NormalCharacterStyle105 text-center"></span></p>
    <table>
      <tr>
        <td><p class="NormalCharacterStyle17"><i> &check;</i> Individual</p></td>
        <td><p class="NormalCharacterStyle14 text-right"><span class="NormalCharacterStyle15"><i class="fa fa-inr"></i></span> 600/- &nbsp;&nbsp; + GST</p></td>
      </tr>
      <tr>
        <td><p class="NormalCharacterStyle17"><i> &check;</i> Group <span class="NormalCharacterStyle15">(Per Head)</span></p></td>
        <td><p class="NormalCharacterStyle14 text-right"><span class="NormalCharacterStyle15"><i class="fa fa-inr"></i></span> 600/- &nbsp;&nbsp; + GST</p></td>
      </tr>
      <tr>
        <td><p class="NormalCharacterStyle17"><i> &check;</i> Team  <span class="NormalCharacterStyle15"></span></p></td>
        <td><p class="NormalCharacterStyle14 text-right"><span class="NormalCharacterStyle15"><i class="fa fa-inr"></i></span> 6,325/- &nbsp; + GST</p></td>
      </tr>
    </table>
    
  </div>
  <!--  -->
  <div class="content7">
    
    
    <div class="content7-img">
      <p class="NormalCharacterStyle6">Hurry!</p>
      <!--   <img src="images/btn.png"> -->
      <!-- <img src="images/start-icon.png" class="start-icon"> -->
      <!--  <span class="">Limited Registration</span> -->
      <span class="pc0-btn NormalCharacterStyle9">Limited Registration</span>
      <a href="register.php" class="NormalCharacterStyle38">
        Click Here to Register
      </a>
    </div>
    
  </div>
  <!--  -->
</div>
<!--  -->
<div class="col-md-4 show-mobile">
  <div class="content-new ">
    <p class="NormalCharacterStyle2">Play for a</p>
    <p class="NormalCharacterStyle5">Cause!</p>
    <ul>
      <li>
        <img src="images/img.png">
        <p class="NormalCharacterStyle">Orphan</p>
      </li>
      <li>
        <img src="images/img1.png">
        <p class="NormalCharacterStyle">Old Age</p>
      </li>
    </ul>
    <p class="NormalCharacterStyle21">Your participation can support an orphan or a homeless elderly</p>
  </div>
</div>
<!--  -->
<div class="content9">
  <img src="images/bg-img.png">
</div>
</div>
<!-- section 3 end -->
<!-- section 4  -->

<!-- section 4 end -->

</div>
</div>
</div></div>
<!-- END MAIN CONTENT-->
<!-- END PAGE CONTAINER-->
</div>

<?php include "footer.php"; ?>
<?php include ('sidebar.php'); ?>
 <div class="page-container back-blure">
  <!-- HEADER DESKTOP-->
  
  <!-- HEADER DESKTOP-->
  <!-- MAIN CONTENT-->
  <div class="main-content multiple-bg pr060">
    <div class="pri-bg0"><img src="images/background/bg01.png"> </div>
    <div class="pri-bg01"><img src="images/background/bg02.png"> </div>
    <div class="pri-b20"><img src="images/background/bg03.png"> </div>
    <div class="section__content section__content--p30">
      <div class="container">
              <div class="row">
                <div class="col-md-3">
                  <div class="logo91"><img src="images/91-logo.png"></div>
                    
                </div>
              </div>
              <!-- section1 -->
              <div class="row p60">
                 <div class="col-md-12">
                    <div class="prize-title">
                      <h1>Selection & Promotion </h1>
                    </div>
                 </div>
              </div>
              <!-- section1 end -->
              <!-- section2 -->
              <div class="row">
                <div class="col-md-10 offset-1">
                  <div class="prize-table-container">
                  <!-- table 1 -->
                  <table class="prize-table">
                    <tr>
                      
                       <th colspan="3" class="text-center"><span class="font4">Zonal Selections & Promotions</span></th>
                       
                        
                    </tr>
                    <tr class="prizes-bg3" >
                      
                        <td class="text-center"></td>
                        <td class="text-center"><span class="font5">Promotional Basis</span></td>
                        <td class="text-center"><span class="font5">Overs</span></td>
                    </tr>
                     <tr class="prizes-bg1">
                      
                        <td class="text-start"><span class="font3">Entry Level</span></td>
                        <td class="text-center"><span class="font3">Knockout</span></td>
                        <td class="text-center"><span class="font3">15</span></td>
                    </tr>
                      <tr>
                      
                        <td class="text-start"><span class="font3">Preliminary round</span></td>
                        <td class="text-center"><span class="font3">Knockout</span></td>
                        <td class="text-center"><span class="font3">15</span></td>
                    </tr>
                     <tr class="prizes-bg3" >
                      
                       <td class="text-start"><span class="font3">Top 10</span></td>
                        <td class="text-center"><span class="font3">Run Rate</span></td>
                        <td class="text-center"><span class="font3">15</span></td>
                    </tr>
                     <tr class="prizes-bg1">
                      
                         <td class="text-start"><span class="font3">Quarter Finals(QF)</span></td>
                        <td class="text-center"><span class="font3">Run Rate</span></td>
                        <td class="text-center"><span class="font3">20</span></td>
                    </tr>
                      <tr>
                      
                        <td class="text-start"><span class="font3">Semi Finals(SF)</span></td>
                        <td class="text-center"><span class="font3">Knockout</span></td>
                        <td class="text-center"><span class="font3">20</span></td>
                    </tr>
                     <tr class="prizes-bg3" >
                      
                       <td class="text-start"><span class="font3">Finals(F)</span></td>
                        <td class="text-center"><span class="font3">Knockout</span></td>
                        <td class="text-center"><span class="font3">20</span></td>
                    </tr>
                    
                  
                  </table>
                </div>
                  <!-- table1 end  -->
                   <!-- table 2 -->
                   <div class="prize-table-container">
                  <table class="prize-table">
                    <tr>
                      
                       <th colspan="7" class="text-center"><span class="font4">National Semi Finals</span></th>
                       
                        
                    </tr>
                    <tr class="prizes-bg1" >
                      
                       
                        <td class="text-start"><span class="font5">Promotional <br> Basis</span></td>
                        <td class="text-center" colspan="6"><span class="font5">Run Rate</span></td>
                    </tr>
                     <tr class="prizes-bg3">
                      
                        <td class="text-start"><span class="font5">Overs</span></td>
                       
                        <td class="text-center" colspan="6"><span class="font6">20</span></td>
                    </tr>
                      <tr>
                      
                        <td class="text-start"><span class="font5">Match</span></td>
                        
                        <td class="text-center"><span class="font3">North Vs. <br>South</span></td>
                         <td class="text-center"><span class="font3">North Vs. <br>East</span></td>
                          <td class="text-center"><span class="font3">North Vs. <br>West</span></td>
                           <td class="text-center"><span class="font3">South Vs. <br>East</span></td>
                            <td class="text-center"><span class="font3">South Vs. <br>West</span></td>
                             <td class="text-center"><span class="font3">East Vs. <br>West</span></td>
                    </tr>
                    
                    
                    
                    
                  
                  </table>
                </div>
                  <!-- table2 end  -->
                    <!-- table 3 -->
                    <div class="prize-table-container">
                  <table class="prize-table">
                    <tr>
                      
                       <th colspan="2" class="text-center"><span class="font4">National Finals</span></th>
                       
                        
                    </tr>
                    <tr class="prizes-bg1" >
                      
                       
                        <td class="text-start width20"><span class="font5">Promotional <br> Basis</span></td>
                        <td class="text-center" ><span class="font5">Knockout</span></td>
                    </tr>
                     <tr class="prizes-bg3">
                      
                        <td class="text-start width20"><span class="font5">Overs</span></td>
                       
                        <td class="text-center" ><span class="font6">20</span></td>
                    </tr>
                      <tr>
                      
                        <td class="text-start width20"><span class="font5">Match</span></td>
                        
                       <td class="text-center"><span class="font3">Team - 1 Vs. Team -2</span></td>
                    </tr>
                    
                    
                    
                    
                  
                  </table>
                  <!-- table3 end  -->
                  
                  </div>
                </div>
              </div>
              <!-- section2 start -->
      </div>
    </div>
</div>
</div>

<?php 
include ('footer.php');
?>

  



<?php include "sidebar.php"; ?>
<!-- END MENU SIDEBAR-->
<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- HEADER DESKTOP-->
    <!-- MAIN CONTENT-->
    <div class="main-content multiple-bg">
         <div class="login-bg"><img src="images/background/login-bg.png"> </div>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
               <!--  -->
                <div class="row">
                <div class="col-md-3">
                  <div class="logo91"><img src="images/91-logo.png"></div>
                    
                </div>
              </div>
               <!--  -->
                <div class="row">
                    <!--  -->
                    <div class="col-md-8 offset-2">
                        <div class="pc-card">
                            
                            <div class="card-body">
                                <div class="card-title">
                                    <h3>Contact Us</h3>
                                </div>
                                <!--  -->
                                <!--  -->
                                <div class="row">
                                    <div class="col-md-6">
                                         <div class="contact-box">
                                             <div class="contact-icon"><i class="fab fa-whatsapp"></i></div>
                                             <p><a href="tel:919513072088">+91-95130 72088</a></p>
                                         </div>
                                    </div>
                                    <div class="col-md-6">
                                        
                                         <div class="contact-box">
                                             <div class="contact-icon"><i class="fa fa-envelope"></i></div>
                                             <p><a href="mailto:bliss@pluscircle.org">bliss@pluscircleclub.org</a></p>
                                         </div>
                                    </div>
                                </div>
                                <!--  -->
                              
                               
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
              
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>
<?php include "sidebar.php"; 
$user_id = $_SESSION['user_id'];
if ($user_id == '') {
    session_destroy();
    echo "<script>";
    echo "window.location.href='login.php';";
    echo "</script>";
}
?>
<div class="page-container">
    <!-- HEADER DESKTOP-->
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
<div class="row">
                <div class="col-md-3">
                  <div class="logo91"><img src="images/91-logo.png"></div>
                    
                </div>
              </div>
               <!--  -->
               <div class="row p60">
                 <div class="col-md-12">
                    <div class="prize-title">
                      <h1>Update Group </h1>
                       <?php
                            $plyrid=$_GET['id'];
                            $sql="Select * from players where id=$plyrid";
                            $result = mysqli_query($conn,$sql);

                            if(mysqli_num_rows($result)>0){
                              while($row = mysqli_fetch_assoc($result)){

                                ?>
                            
                    </div>
                 </div>
              </div>
           <div class="col-md-12">
                        <div class="pc-card">
                            <div class="card-body">
                                <div id="t-form" >
                                    <form method="post" id="group-form" name="group-form" action="updateteam.php">
                                        <div id="tablefields">
                                            <span id="error"></span>
                                            <table class="table table-bordered" style="width: 100%;margin-bottom:10px;">                                              
                                                <tr>
                                                    <td>
                                                        <input type="hidden" name="pid" value="<?php echo $row['id'];?>">
                                                        <input type='text' name="g_name" id="name" value="<?php echo $row['player_name'];?>"  required placeholder="Player Name"></td>
                                                    <td>
                                                        <?php 
                                                            $sql1="Select * from city";
                                                            $result1 = mysqli_query($conn,$sql1);
                                                           if(mysqli_num_rows($result1)>0){
                                                            echo '<select class="city g_city" id="g_city" name="g_city" required>';
                                                             while($row1 = mysqli_fetch_assoc($result1)){
                                                                if($row['player_city']==$row1['id']){
                                                                    $select="selected";
                                                                }else{
                                                                    $select="";
                                                                }
                                                             echo "<option {$select} value='{$row1['id']}'>{$row1['city_name']}</option>" ;

                                                         }
                                                           echo "</select>";
                                                       }

                                                        ?>
                                                        
                                                        
                                                    </td>
                                                    <td><input type='date' max="2009-12-31" name="g_dob" id="dob"value="<?php echo $row['player_dob'];?>" required></td>
                                                    <td><input type='number' name="g_mobile" value="<?php echo $row['player_mobile'];?>" id="mobile" required placeholder="Mobile" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength="10" min="1"></td>
                                                    <td><input type='email' name="g_email" id="email" value="<?php echo $row['player_email'];?>" placeholder="Email (optional)"></td>
                                                   
                                                    <input type="hidden" name="res[]" id="res" value="<?php echo $user_id?>">
                                                    <input type="hidden" name="state" id="state1" name="state1">
                                               <!--  <tr>
                                                    <th>Player bat</th>
                                                    <th>Batting Style</th>
                                                    <th>Bowlling Style</th>
                                                </tr> -->
                                                <tr>

                                                    
                                                    <td class="nodata"></td>
                                                    <td class="nodata"></td>

                                                     <td>


                                                        <?php 
                                                            $sql2="SELECT * FROM player_role";
                                                            $result2 = mysqli_query($conn,$sql2);
                                                            if(mysqli_num_rows($result2)>0){
                                                            echo '<select class="g_role" id="g_role" name="g_role" required>';
                                                             while($row2 = mysqli_fetch_assoc($result2)){
                                                                if($row['player_role']==$row2['id']){
                                                                    $select="selected";
                                                                }else{
                                                                    $select="";
                                                                }
                                                             echo "<option {$select} value='{$row2['id']}'>{$row2['role_title']}</option>" ;

                                                            }
                                                            echo "</select>";
                                                            }

                                                            ?>
                                                    </td>
                                                    <td>
                                                         <?php 
                                                        $sql3="SELECT * FROM batting";
                                                        $result3 = mysqli_query($conn,$sql3);
                                                        if(mysqli_num_rows($result3)>0){
                                                        echo '<select class="batting" id="g_batting" name="g_batting" required>';
                                                         while($row3 = mysqli_fetch_assoc($result3)){
                                                            if($row['player_bat']==$row3['id']){
                                                                $select="selected";
                                                            }else{
                                                                $select="";
                                                            }
                                                         echo "<option {$select} value='{$row3['id']}'>{$row3['batting_type']}</option>" ;

                                                        }
                                                        echo "</select>";
                                                        }

                                                        ?>                                                         
                                                    </td>
                                                    <td>
                                                        <?php 
                                                        $sql4="SELECT * FROM bowling";
                                                        $result4 = mysqli_query($conn,$sql4);
                                                        if(mysqli_num_rows($result4)>0){
                                                        echo '<select class="bowling" id="g_bowling" name="g_bowling" required>';
                                                         while($row4 = mysqli_fetch_assoc($result4)){
                                                            if($row['player_ball']==$row4['id']){
                                                                $select="selected";
                                                            }else{
                                                                $select="";
                                                            }
                                                         echo "<option {$select} value='{$row4['id']}'>{$row4['bowling_type']}</option>" ;

                                                        }
                                                        echo "</select>";
                                                        }

                                                        ?>     
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                            <input type="hidden" name="res[]" id="res" value="<?php echo $user_id?>" >
                                            <input type="hidden" name="state" id="state1" name="state1">
                                            <input type="hidden" id="table_id" value="1">
                                        </div>
                                        <!-- <div class="pc-btn">
                                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="button" id="add_more" name="team-register">Add more</button>
                                        </div> --> 
                                        <h3 id="total_h" style="display: none">Total players <span id="total"></span></h2> 
                                        <div class="pc-btn">
                                             
                                              <button class="au-btn au-btn--block au-btn--green m-b-20 " type="submit"><a class="glyphicon glyphicon-arrow-left"style="color:#c5ab7a;" href="https://pluscircleclub.org/+91cricket/my-team.php">Back</a></button>
                                            &nbsp;&nbsp;&nbsp;<button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="update">Update</button>
                                        </div>    
                                    </form>
                                <?php }}?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></div></div></div>
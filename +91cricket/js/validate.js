$(document).ready( function(){
jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z ' ']+$/i.test(value);
}, "Use alphabets only"); 



   //
	$('#register').validate({ // initialize the plugin
        rules: {
        	userName : {
        		required : true,
            lettersonly : true
        	},
            
            password : {
            	required : true,
                minlength : 6
            },
           
            number :{
            	required: true,
            	minlength:10
            }, 
            
            c_password:{
              required : true,
              equalTo: '#password'
            }
        },
        
        messages: { 
          
        },
        //submit handler
        submitHandler: function(form){  
          
            var number = $("#number").val();
            var email = $("#email_id").val();
            var username = $("#userName").val();
            var password = $("#password").val();
            var register ="register";
              

              $.ajax({
                  url: "ajax.php",
                  type: "POST",
                 dataType : "json",
                  data: {new_register:register, number:number, username:username, email_id:email, password:password},
                  success: function(result){
                    console.log(result);
                    if (result.messages == 'done') {
                      $("#first-form").hide();
                      $(".con-form").css("display", "block");
                      $("#con-num").html("CONFIRM MOBILE NO "+result.number);
                      $("#con-email").html("CONFIRM EMAIL ID "+result.email);
                      $("#otp-msg").html("OTP Sent on your email id "+result.email);
                      $("#otp").html(result.otp);                    
                      $("#u_email").val(result.email);
                      $("#r").val(result.r);
                    //   $("#register")[0].reset();
                    }
                  }
              });
           
            //console.log(number);
            return false;
        }
    });
    /* LOging*/
    $('#login').validate({ // initialize the plugin
        rules: {
            email_id :{
                required: true,
                email: true
            },
            password : {
              required : true,
                minlength : 6
            }
        },
        
        messages: { 
          
        },
        //submit handler
        submitHandler: function(form){  
            var email = $("#email_id_l").val();
           
            var password = $("#password_l").val();
            var login ="login";

            //console.log(number);
            $.ajax({
                url: "ajax.php",
                type: "POST",
                
                data: {login:login, email_id:email, password:password},
                success: function(result){
                  //console.log(result.messages);
                  if ($.trim(result) == 'success') {
                    window.location.href="index.php";
                  }else if($.trim(result) == 0){
                   window.location.href="add-player.php";
                  }else if($.trim(result) == 1){
                    
                    window.location.href="payment.php";
                  }else if($.trim(result) == 'error'){
                    $("#log-error").html('<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Invalid Email id or password</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                  }
                }
            });
            return false;
        }
    });
    
    /* Forget password*/
    
	/* Send Otp */

	$("#email_id").change(function(){
		var email = $("#email_id").val();
		var check_email = "check_email";
		if (email !== '') {
      $("#email_error").html('');
			$.ajax({
          url: "ajax.php",
          type: "POST",
          data: {check_email:check_email, email: email},
          success: function(result){
          //console.log(result);
            // 	if($.trim(result) == "email already"){
            //   	$("#email_id").val('');
            //     	$("#email_error").html('Email Already Exists').css("color", "red");
            // 	}else{
            // 		$("#email_error").html('');
            // 	}
          }
      });
		}
	});
	/* */
	$("#number").change(function(){
		var number = $("#number").val();
		var number_exists = "number_exists";
      //alert(number)
		if (number != '') {
      $("#phone_error").html(''); 
			$.ajax({
          url: "ajax.php",
          type: "POST",
          data: {number_exists:number_exists, number: number},
          success: function(result){
          //console.log(result);
            if($.trim(result) == "number already"){
                $("#phone_error").html('Number Already Exists').css("color", "red");
                $("#number").val("");
            }else{
                $("#phone_error").html('');
            }
          }
         });
		}
	});
  /* OTP validation*/
   $('#otp-form').validate({ // initialize the plugin
        rules: {
          otp_number : {
            required : true
          }
        },
        messages: { 
        },
        //submit handler
        submitHandler: function(form){  
          //alert("enter");
            var otp_number = $("#otp_number").val();
            var u_email = $("#u_email").val();
            var reg_id = $("#r").val();
            var check_otp ="check_otp";
           // alert(check_otp);
            $.ajax({
                url: "ajax.php",
                type: "POST",
                dataType : "json",
                data: {check_otp:check_otp, otp_number:otp_number, u_email:u_email, reg_id:reg_id},
                success: function(data){
                //alert($.trim(data));
                //console.log($.trim(data));
                  if ($.trim(data) == 1) {
                    //alert("valid OTP");
                    $("#otp-error").html('varify OTP').css("color", "green");
                    window.location.href="add-player.php";
                  }else if ($.trim(data)== 0){
                    //alert("Invalid OTP");
                   $("#otp-error").html('Invalid OTP').css("color", "red");
                  }
                }
            });
            return false;
        }
   });

  /* Change */
   $("#change").click(function(){
    $("#first-form").show();
    $(".con-form").css("display", "none");
   });
  /* contni */
  $("#continue").click(function(){
    $("#first-form").hide();
    $(".con-form").css("display", "none");
    $("#sec-form").hide();
    $("#otp_number").val('');
    $("#register")[0].reset();
    

  });
  /* Resend Otp */
   $("#resend").click(function(){
    //alert();
    var u_email = $("#u_email").val();
    var h_email = $("#h_email").val();
    
    var reg_id = $("#r").val();
    var resend = "resend";
    $.ajax({
        url: "ajax.php",
        type: "POST",
        dataType : "json",
        data: {resend:resend, u_email:u_email, reg_id:reg_id},
        success: function(result){
          //console.log(result.messages);
          if (result.resend == 1) {        
            $("#otp").html(result.otp);
            $("#resetOtp").html(result.otp);

          }
        }
    });
   });
  /* State */
   $("#team_individual").change(function(){
      var team = $("#team_individual").val();
      var age_group = $("#age_group").val();
      var state = $("#state").val();
      if (age_group !='' && team != '') {
         $("#state_sec").show();
         if (state !='' && team == 1) {
             $("#sec-form").show();
             $("#t-form").hide();
          }else if (state !='' && team == 2){
             //alert("sahdha");
             $("#t-form").css("display", "block");
             $("#sec-form").hide();
          }
      }

   });
   $("#age_group").change(function(){
      var age_group = $("#age_group").val();
      var team = $("#team_individual").val();
      if (age_group !='' && team != '') {
         $("#state_sec").show();
      }
   });
   /*  */
   $("#state").change(function(){
      var state = $("#state").val();
      var team = $("#team_individual").val();
      //alert(team);
      if (team == 1) {
         $("#sec-form").show();
          $("#t-form").hide();
      }else if(team == 2){
         //alert("sahdha");
         $("#t-form").css("display", "block");
         $("#sec-form").hide();
      }
   });
   /* state wise city*/
   $("#state").change(function(){
      var state = $("#state").val();
      var state_city = "state_city";
       $.ajax({
         url: "ajax.php",
         type: "POST",
         data: {state:state, state_city:state_city},
         success: function(result){
          //console.log(result.messages);
            $(".city").html(result);      
            //$(".city").html("<option value=''></option>"+result);   
            $("#state1").val(state);  
        }
      });
   
   });
   /* Submit Team */
   $('#team-form').validate({ // initialize the plugin
        rules: {
          name : {
            required: true,
            lettersonly : true
                
          },
          city : {
            required : true
          },
          mobile : {
            minlength:10,
            required : true
          },
          email : {
            required : true
          }
        },
        messages: { 
        },
        //submit handler
        submitHandler: function(form){  
          //alert("enter");
           // alert(check_otp);
            var name = [];
            $('input[name="name[]"]').each( function() {
                 name.push(this.value);
            });
            var city = [];

            $('.city').each( function() {
                 city.push(this.value);
            });

            var dob = [];
            $('input[name="dob[]"]').each( function() {
                 dob.push(this.value);
            });
            var email = [];
            $('input[name="email[]"]').each( function() {
                 email.push(this.value);
            });
            var mobile = [];
            $('input[name="mobile[]"]').each( function() {
                 mobile.push(this.value);
            });
            var state1 = $("#state1").val();
            var res = $("#res").val();
            var team_individual = $("#team_individual").val();
            var team_register = "team_register";
            var age_group = $("#age_group").val();
            $.ajax({
                url: "insert-team.php",
                type: "POST",
                data: {name:name, city:city, dob:dob, email:email, mobile:mobile, state1:state1, res:res, team_register:team_register, team_individual:team_individual, age_group:age_group},
                success: function(data){
                  // alert($.trim(data1));
                  // console.log($.trim(data1));
                  if ($.trim(data) == 1) {
                    //alert("valid OTP");
                    $("#team-form")[0].reset();
                    window.location.href="payment.php";
                  }else if ($.trim(data)== 0){
                    //alert("Invalid OTP");
                   alert("somthing went worng");
                  }
                }
            });
            return false;
        }
    });
  /* Add Group or Indivial Player*/
    
   $('#group-form').validate({ // initialize the plugin
        rules: {
          name : {
            required : true,
            lettersonly : true
          },
          city : {
            required : true
          },
          mobile : {
            minlength:10,
            required : true
          },
          email : {
            required : true
          }
        },
        messages: { 
        },
        //submit handler
        submitHandler: function(form){  
          
          //alert("enter");
           // alert(check_otp);
          var error = '';
          var name = [];
            $('input[name="g_name[]"]').each( function() {
                var count = 2;
               if($(this).val() == '')
               {
                error += "<p>Enter Item Name at "+count+" Row</p>";
                return false;
               }
               count = count + 1;

              name.push(this.value);

            });
            var city = [];
            $('.g_city').each( function() {
                 city.push(this.value);
            });
            var dob = [];
            $('input[name="g_dob[]"]').each( function() {

                 dob.push(this.value);
            });
            var email = [];
            $('input[name="g_email[]"]').each( function() {
                 email.push(this.value);
            });
            var mobile = [];
            $('input[name="g_mobile[]"]').each( function() {
                 mobile.push(this.value);
            });
            var role = [];
            $('.g_role').each( function() {
                 role.push(this.value);
            });

            var batting = [];
            $('.batting').each( function() {
                 batting.push(this.value);
            });

            var bowling = [];
            $('.bowling').each( function() {
                 bowling.push(this.value);
            });

            if (error == '') {

            }else{

              $('#error').html('<div class="alert alert-danger">'+error+'</div>');
            }

            var state1 = $("#state").val();
            var res = $("#res").val();
            var age_group = $("#age_group").val();
            var team_individual = $("#team_individual").val();
            var group_register = "group_register";
            //alert();
            $.ajax({
                url: "insert-team.php",
                type: "POST",
                data: {name:name, city:city, dob:dob, email:email, mobile:mobile, state1:state1, res:res, role:role, batting:batting, bowling:bowling, age_group:age_group, team_individual:team_individual, group_register:group_register},
                success: function(data){
                  //alert($.trim(data));
                  //console.log($.trim(data));
                  if ($.trim(data) == 1) {
                    //alert("valid OTP");
                    $("#group-form")[0].reset();
                    window.location.href="payment.php";
                  }else if ($.trim(data)== 0){
                    //alert("Invalid OTP");
                   alert("somthing went worng");
                  }
                }
            });
            return false;
        }
    });

   /**/
   $('#group-form-new').validate({ // initialize the plugin
        rules: {
          name : {
            required : true,
            lettersonly : true
          },
          city : {
            required : true
          },
          mobile : {
            required : true,
            minlength:10
          },
          email : {
            required : true
          }
        },
        messages: { 
        },
        //submit handler
        submitHandler: function(form){  
          
          //alert("enter");
           // alert(check_otp);
           var error = '';
            var name = [];
            $('input[name="g_name[]"]').each( function() {
                var count = 2;
               if($(this).val() == '')
               {
                error += "<p>Enter Item Name at "+count+" Row</p>";
                return false;
               }
               count = count + 1;

              name.push(this.value);

            });
            var city = [];
            $('.g_city').each( function() {
                 city.push(this.value);
            });
            var dob = [];
            $('input[name="g_dob[]"]').each( function() {

                 dob.push(this.value);
            });
            var email = [];
            $('input[name="g_email[]"]').each( function() {
                 email.push(this.value);
            });
            var mobile = [];
            $('input[name="g_mobile[]"]').each( function() {
                 mobile.push(this.value);
            });
            var role = [];
            $('.g_role').each( function() {
                 role.push(this.value);
            });

            var batting = [];
            $('.batting').each( function() {
                 batting.push(this.value);
            });

            var bowling = [];
            $('.bowling').each( function() {
                 bowling.push(this.value);
            });

            if (error == '') {

            }else{

              $('#error').html('<div class="alert alert-danger">'+error+'</div>');
            }

            var state1 = $("#state").val();
            var res = $("#res").val();
            var age_group = $("#age_group").val();
            var team_individual = $("#team_individual").val();
            var add_group_register = "add_group_register";
            //alert();
            $.ajax({
                url: "insert-team.php",
                type: "POST",
                data: {name:name, city:city, dob:dob, email:email, mobile:mobile, state1:state1, res:res, role:role, batting:batting, bowling:bowling, age_group:age_group, team_individual:team_individual, add_group_register:add_group_register},
                success: function(data){
                  //alert($.trim(data));
                  //console.log($.trim(data));
                  if ($.trim(data) == 1) {
                    //alert("valid OTP");
                    $("#group-form-new")[0].reset();
                    window.location.href="edit.php";
                  }else if ($.trim(data)== 0){
                    //alert("Invalid OTP");
                   alert("Somthing went worng");
                  }
                }
            });
            return false;
        }
    });

   /**/
    $('[name*="name"]').each(function() {
      $(this).rules('add', {
          required: true,
          lettersonly :true
      });
    });
    $('[name*="mobile"]').each(function() {
      $(this).rules('add', {
          required: true,
          minlength :10
      });
    });
    $('[name*="g_mobile"]').each(function() {
      $(this).rules('add', {
          required: true,
          minlength :10
      });
    });
   /*  Add more fileds*/
  $("#add_more").click(function(){

        var table_id = $("#table_id").val();
        var player = parseInt(table_id) + 1;
        var state = $("#state").val();
        var state_city = "state_city";
        var InputsWrapper   = $(".table"); 
        var x = InputsWrapper.length; 
       
        $.ajax({
           url: "ajax.php",
           type: "POST",
           data: {state:state, state_city:state_city},
           success: function(result){
            //console.log(result.messages);
              //$(".city").append(result);      
              $(".city").append(result);   
              $("#state1").val(state);  
          }
          
        });
        // 
        var table_draw = 'table_draw'; 
        $.ajax({
           url: "ajax.php",
           type: "POST",
           data: {table_draw:table_draw,state:state},
           success: function(data_table){
            //console.log(result.messages);
              //$(".city").append(result);      
              var html = $("#tablefields").first().append(data_table);

                $("#table_id").val(player);
                $("#total_h").show();
                $("#total").html(player);
                if (player == 10) {
                 $("#add_more").hide();
                }
                $("#tablefields").last().after(html);

          }
          
        });
        //$(".playnumber").html(x);
       /* var html = $("#tablefields").first().append('<table class="table table-bordered" style="width: 100%;margin-bottom:20px;" > <tr>  <th>Name</th> <th>City</th> <th>D.O.B</th> <th>Mobile</th> <th>Email</th> </tr> <tr>  <td><input type="text" name="name[]" id="name" required></td> <td><select name="city" class="city" id="city" name="city[]" required> </select> </td> <td><input type="date" name="dob[]" id="dob" required></td> <td><input type="text" name="mobile[]" id="mobile" required ></td> <td><input type="text" name="email[]" id="email" required></td> <input type="hidden" name="res[]" id="res" value=""><tr> <th>Player Role</th> <th>Batting Style</th> <th>Bowlling Style</th><th></th> </tr> <tr> <td><select name="role" id="role" name="role[]" required> <option>1</option> <option>2</option> <option>3</option> </select> </td> <td> <select name="batting" class="batting" id="batting" name="batting[]" required> <option>1</option> <option>2</option> <option>3</option> </select> </td> <td> <select name="bowling" class="bowling" id="bowling" name="bowling[]" required> <option>1</option> <option>2</option> <option>3</option> </select> </td><td><button type="button" class="removeclass btn btn-danger">Remove</button></td> </tr> </table>');
          $("#table_id").val(player);
          $("#total_h").show();
          $("#total").html(player);
          if (player == 10) {
            $("#add_more").hide();
          }
          $("#tablefields").last().after(html);*/
    
  });
  /* Remove table*/
  
  $("body").on("click",".removeclass", function(){ //user click on remove text

    var tab = $(this).parents('table').remove(); //remove text box
    var InputsWrapper   = $(".table"); 
    var x = InputsWrapper.length; 
    var table_id = $("#table_id").val();
    var player = parseInt(table_id) - 1;
    $("#table_id").val(player);
    $("#total").html(player);
      
  }); 
  

/*//////////////////*/
/*  Forget password area*/

/* Reset Password */
  $('#forget-form').click( function(){ // initialize the plugin
    var email = $("#email_f").val();
    var forget ="forget";
    if (email == '') {
      $("#email-err").html("please enter registerd email id").css("color", "red");
    }else{
       $("#email-err").html("");
          $.ajax({
              url: "ajax.php",
              type: "POST",
              dataType : "json",
              data: {forget:forget, email_id:email},
              success: function(result){
                //console.log(result.messages);
                if (result.messages == 'done') {
                  $("#reset-form").hide();
                  $("#reset-form-one").show();
                  $("#u_email").val(result.email);
                  $("#reset-msg").html("OTP Sent on your email id "+result.email);
                  $("#resetOtp").html(result.otp);    
                 
                }else if(result.messages == 'error'){
                   $("#email-err").html("Invalid Email id").css("color", "red");
                   $("#email_f").val('');
                }
              }
          });
          return false;
    }
  });

  /* Forget otp */
  $('#forget-otp').validate({ // initialize the plugin
        rules: {
          otp_number : {
            required : true
          }
        },
        messages: { 
        },
        //submit handler
        submitHandler: function(form){  
          //alert("enter");
            var otp_number = $("#otp_number_r").val();
            var h_email = $("#u_email").val();
            //var reg_id = $("#r").val();
            var check_otp_forget ="check_otp_forget";
           // alert(check_otp);
            $.ajax({
                url: "ajax.php",
                type: "POST",
               
                data: {check_otp_forget:check_otp_forget, otp_number:otp_number, h_email:h_email},
                success: function(data){
                //alert($.trim(data));
                //console.log($.trim(data));
                  if ($.trim(data) == 1) {
                   
                    $("#otp-error").html('varify OTP').css("color", "green");
                    $("#reset-form").hide();
                    $("#reset-form-one").hide();
                    $("#reset-form-sec").show();
                    //window.location.href="login.php";
                  }else if ($.trim(data)== 0){
                    //alert("Invalid OTP");
                   $("#otp-otp-error_r").html('Invalid OTP').css("color", "red");
                  }
                }
            });
            return false;
        }
  });
  /* update password */
   $('#forget-pass').validate({ // initialize the plugin
        rules: {
          f_password : {
            required : true,
            minlength : 6
          },
          f_r_password : { 
            equalTo : "#f_password",
            minlength : 6
          }
        },
        messages: { 
        },
        //submit handler
        submitHandler: function(form){  
          //alert("enter");
           
            var h_email = $("#u_email").val();
            //var reg_id = $("#r").val();
            var password = $("#f_password").val();
            var update_pass ="update_pass";
           // alert(check_otp);
            $.ajax({
                url: "ajax.php",
                type: "POST",
               
                data: {update_pass:update_pass, password:password, h_email:h_email},
                success: function(data){
                //alert($.trim(data));
                //console.log($.trim(data));
                  if ($.trim(data) == 1) {
                    
                    $("#reset-form-sec").hide();
                    $("#reset-thank-you").show();
                   
                  }else if ($.trim(data)== 0){
                    //alert("Invalid OTP");
                   $("#otp-otp-error_r").html('Invalid OTP').css("color", "red");
                  }
                }
            });
            return false;
        }
  });
/*  Forget password area End*/
 });
  function edit_player(id) {
    var age_group = $("#age_group").val();
    var team_individual = $("#team_individual").val();
    var p_name = $("#name"+id).val();
    var p_city = $("#city"+id).val();
    var p_dob = $("#dob"+id).val();
    var p_mobile = $("#mobile"+id).val();
    var p_email = $("#email"+id).val();
    var p_res = $("#res").val();
    var p_state1 = $("#state").val();
    var player = id;
    var edit_team = "edit_team";
    var p_role = $("#role"+id).val();
    var p_bat = $("#batting"+id).val();
    var p_bowl = $("#bowling"+id).val();
    //alert(id);
    $.ajax({
      url : "ajax.php",
      type : "POST",
      data : {team_individual:team_individual, age_group:age_group, p_name:p_name,p_city:p_city,p_dob:p_dob, p_mobile:p_mobile, p_email:p_email, p_res:p_res, p_state1:p_state1, player:player, edit_team:edit_team, p_bat:p_bat, p_bowl:p_bowl, p_role:p_role}, 
      success : function(result) {
        //console.log(result);
        if ($.trim(result) == 1) {
          window.location.href="edit-player.php";
        }else{
          alert("Somthing went wrong");
        }
      }
    });
  }
/*
function edit($id){
    var 
}*/
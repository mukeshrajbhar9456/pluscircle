<?php include "sidebar.php"; 
//print_r($_SESSION);
// if(!empty($_SESSION['user_id'])){
//     echo "<script>window.location.href='index.php'</script>";
// }
?>
<link rel="stylesheet" type="text/css" href="css/theme1.css">
<!-- END MENU SIDEBAR-->
<!-- PAGE CONTAINER-->
<div class="page-container view_ground">
    <!-- HEADER DESKTOP-->
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                 <!--  -->
                <div class="row">
                    <div class="col-md-3">
                      <div class="logo91"><img src="images/91-logo.png"></div>
                    </div>
                    <div class="col-md-6 text-center">
                        <!-- <h3 for="states">Select State</h3> -->
                        <?php 
                        $select_state = "SELECT * FROM state";
                        $query_state = mysqli_query($conn, $select_state);
                       ?>
                     <?php
                           $sql = "SELECT * FROM state";
                          $result = $conn->query($sql);
                          // if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) { 
                             $id=$row["id"];
                              }
                           // }
                      ?>
                        <select name="state" id="state" class="mt-3" onchange="showUser(this.value)">
                           <option value="">Select State</option>
                           <?php 
                              while ($row_s = mysqli_fetch_array($query_state)) {    
                                ?>
                              <option value="<?php echo $row_s['id']?>"><?php echo $row_s['name']?></option>  
                          <?php } ?>
                        </select>
                    </div>
                     <div class="col-md-3">
                      <div class="logo91"><img src="images/ApprovedbyGov.png"></div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="zonal_state_com text-center mt-3">Sports Committee Members</h3>
                        <div class="row mt-3" id="2" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/APTelangana/Edit/sridhar1.jpg">
                                <h3 class="member_name text-center">G.Sreedhar Kumar</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                         <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/APTelangana/Edit/RaghavMathood.jpg">
                                <h3 class="member_name text-center">Raghav Mathood</h3>
                                <p class="member_desgn text-center">Chairman</p>
                            </div>
                           <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/APTelangana/Edit/ShriRam.jpg">
                                <h3 class="member_name text-center">R Shri Ram</h3>
                                <p class="member_desgn text-center">C.E.O</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/APTelangana/Edit/Guhan.jpg">
                                <h3 class="member_name text-center">Guhan</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                               <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/APTelangana/Edit/AtherMujer2.jpg">
                                <h3 class="member_name text-center">RM ATHER MUJEER</h3>
                                <p class="member_desgn text-center">Treasurer</p>                                
                            </div>
                             <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/APTelangana/Edit/BRamaKrishna5.jpg">
                                <h3 class="member_name text-center">B RamaKrishna</h3>
                                <p class="member_desgn text-center">Jt. Secretary</p>
                            </div>
                        </div>
                        <!-- id=4 -->
                         <div class="row mt-3" id="4" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Asam/Edit/Imtaz.jpg">
                                <h3 class="member_name text-center">Imtiaz Ahmed</h3>
                                <p class="member_desgn text-center">Secretary</p>
                            </div>
                            
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Asam/Edit/Bidyut3.jpg">
                                <h3 class="member_name text-center">Dr.Bidyut Mukherjee</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Asam/Edit/Mahtab-Alam.jpg">
                                <h3 class="member_name text-center">CA.Mahtab Alam</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Karnataka/Edit/Sridhar1.jpg">
                                <h3 class="member_name text-center">G.Sreedhar Kumar</h3>
                                <p class="member_desgn text-center">E.D. </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Asam/Edit/SKarmakr.jpg">
                                <h3 class="member_name text-center">Shuvrashankha Karmakar</h3>
                                <p class="member_desgn text-center">C.E.O.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Asam/Edit/rohan.jpg">
                                <h3 class="member_name text-center">Rohan Datta</h3>
                                <p class="member_desgn text-center">Treasurer </p>
                            </div>
                            
                        </div>
                        <!-- id=5 -->
                         <div class="row mt-3" id="5" style="display: none;">
                            
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Bihar-Jarkhand/Edit/Mahtab-Alam.jpg">
                                <h3 class="member_name text-center">CA.Mahtab Alam</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Bihar-Jarkhand/Edit/Bisajeet.jpg">
                                <h3 class="member_name text-center">Biswajit Mukherjee</h3>
                                <p class="member_desgn text-center">Secretary</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Bihar-Jarkhand/Edit/Santoshtivari.jpg">
                                <h3 class="member_name text-center">Santosh Tiwari</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Bihar-Jarkhand/Edit/Bidyut3.jpg">
                                <h3 class="member_name text-center">Dr.Bidyut Mukherjee</h3>
                                <p class="member_desgn text-center">E.D.</p>                                
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Bihar-Jarkhand/Edit/SKarmakr5.jpg">
                                <h3 class="member_name text-center">Shuvrashankha Karmakar</h3>
                                <p class="member_desgn text-center">C.E.O</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Bihar-Jarkhand/Edit/rohan.jpg">
                                <h3 class="member_name text-center">Rohan Datta</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            
                            
                        </div>
                        <!-- id=6 -->
                         <div class="row mt-3" id="6" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Harpreet.jpg">
                                <h3 class="member_name text-center">Harpreet Singh</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/PawanSingh1.jpg">
                                <h3 class="member_name text-center">Pawan Singh</h3>
                                <p class="member_desgn text-center">Chairman</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Jitendra1.jpg">
                                <h3 class="member_name text-center">Jitendra Kaushik</h3>
                                <p class="member_desgn text-center">C.E.O </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Bidyut3.jpg">
                                <h3 class="member_name text-center">Dr.Bidyut Mukherjee</h3>
                                <p class="member_desgn text-center">E.D.</p>                                
                            </div>
                            
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/MahtabAlam.jpg">
                                <h3 class="member_name text-center">CA.Mahtab Alam</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Lucky.jpg">
                                <h3 class="member_name text-center">Lucky Chauhan</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            
                            
                        </div>
                        <!-- id=7-->
                         <div class="row mt-3" id="7" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Odisa-Chatisagad/Edit/SSoni2.jpg">
                                <h3 class="member_name text-center">Saurabh Soni</h3>
                                <p class="member_desgn text-center">Jt. Secretary</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Odisa-Chatisagad/Edit/Bidyut3.jpg">
                                <h3 class="member_name text-center">Dr.Bidyut Mukherjee</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Odisa-Chatisagad/Edit/MahtabAlam.jpg">
                                <h3 class="member_name text-center">CA.Mahtab Alam</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Odisa-Chatisagad/Edit/SKarmakr5.jpg">
                                <h3 class="member_name text-center">Shuvrashankha Karmakar</h3>
                                <p class="member_desgn text-center">C.E.O.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/Odisa-Chatisagad/Edit/rohan.jpg">
                                <h3 class="member_name text-center">Rohan Datta</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Karnataka/Edit/Sridhar1.jpg">
                                <h3 class="member_name text-center">G.Sreedhar Kumar</h3>
                                <p class="member_desgn text-center">E.D. </p>
                            </div>
                        </div>
                        <!-- id=10 -->
                         <div class="row mt-3" id="10" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Deli/Edit/Gauravsukla2.jpg">
                                <h3 class="member_name text-center">Gaurav Shukla</h3>
                                <p class="member_desgn text-center">Secretary </p>
                            </div>

                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Deli/Edit/AjitJhar.jpg">
                                <h3 class="member_name text-center">Ajit Jha</h3>
                                <p class="member_desgn text-center">Jt. Secretary</p>                                
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Deli/Edit/Lucky.jpg">
                                <h3 class="member_name text-center">Lucky Chauhan</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Deli/Edit/PawanSingh1.jpg">
                                <h3 class="member_name text-center">Pawan Singh</h3>
                                <p class="member_desgn text-center">Chairman</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Deli/Edit/Jitendra1.jpg">
                                <h3 class="member_name text-center">Jitendra Kaushik</h3>
                                <p class="member_desgn text-center">C.E.O </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Deli/Edit/Bidyut3.jpg">
                                <h3 class="member_name text-center">Dr.Bidyut Mukherjee</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                           
                        </div>
                        <!-- id=12 -->
                         <div class="row mt-3" id="12" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Gujrat/Edit/DrLakshank4.jpg">
                                <h3 class="member_name text-center">Dr.Lakshank Mehta</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Gujrat/Edit/ShellyTibak3.jpg">
                                <h3 class="member_name text-center">Shelly Tibak</h3>
                                <p class="member_desgn text-center">C.E.O</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Gujrat/Edit/Miland1.jpg">
                                <h3 class="member_name text-center">Milan Chavda</h3>
                                <p class="member_desgn text-center">Treasurer </p>
                            </div>
                             <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Gujrat/Edit/NeravMehta1.jpg">
                                <h3 class="member_name text-center">Nerav Mehta</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Gujrat/Edit/RishabGoyal3.jpg">
                                <h3 class="member_name text-center">Rishab Goel</h3>
                                <p class="member_desgn text-center">Jt. Secretary</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/MahtabAlam.jpg">
                                <h3 class="member_name text-center">CA.Mahtab Alam</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                        </div>
                        <!-- id=13 -->
                         <!-- <div class="row mt-3" id="13" style="display: none;">
                               <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Bidyut3.jpg">
                                <h3 class="member_name text-center">Dr.Bidyut Mukherjee</h3>
                                <p class="member_desgn text-center">Chairman</p>                                
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Harpreet.jpg">
                                <h3 class="member_name text-center">Harpreet Singh</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Jitendra1.jpg">
                                <h3 class="member_name text-center">Jitendra Kaushik</h3>
                                <p class="member_desgn text-center">Treasurer </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Lucky.jpg">
                                <h3 class="member_name text-center">Lucky Chauhan</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/MahtabAlam.jpg">
                                <h3 class="member_name text-center">CA.Mahtab Alam</h3>
                                <p class="member_desgn text-center">Jt.Secretary</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/PawanSingh1.jpg">
                                <h3 class="member_name text-center">Pawan Singh</h3>
                                <p class="member_desgn text-center">Chairman</p>
                            </div>
                        </div> -->
                        
                        <!-- id=17 -->
                         <div class="row mt-3" id="17" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Karnataka/Edit/RaghavMathood.jpg">
                                <h3 class="member_name text-center">Raghav Mathood</h3>
                                <p class="member_desgn text-center">Chairman</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Karnataka/Edit/ShriRam.jpg">
                                <h3 class="member_name text-center">R Shri Ram</h3>
                                <p class="member_desgn text-center">C.E.O</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Karnataka/Edit/Sridhar1.jpg">
                                <h3 class="member_name text-center">G.Sreedhar Kumar</h3>
                                <p class="member_desgn text-center">E.D. </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Karnataka/Edit/Guhan.jpg">
                                <h3 class="member_name text-center">Guhan</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Karnataka/Edit/Lakshmikanat.jpg">
                                <h3 class="member_name text-center">Lakshmi Kanth</h3>
                                <p class="member_desgn text-center">Secretary</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Karnataka/Edit/Gowda1.jpg">
                                <h3 class="member_name text-center">Avinash Gowda A C</h3>
                                <p class="member_desgn text-center">Treasurer</p>                                
                            </div> 
                        </div>
                        <!-- id=18 -->
                         <div class="row mt-3" id="18" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Kerala/Edit/Sridhar1.jpg">
                                <h3 class="member_name text-center">G.Sreedhar Kumar</h3>
                                <p class="member_desgn text-center">E.D. </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Kerala/Edit/RaghavMathood.jpg">
                                <h3 class="member_name text-center">Raghav Mathood</h3>
                                <p class="member_desgn text-center">Chairman</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Kerala/Edit/ShriRam.jpg">
                                <h3 class="member_name text-center">R Shri Ram</h3>
                                <p class="member_desgn text-center">C.E.O.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Kerala/Edit/Guhan.jpg">
                                <h3 class="member_name text-center">Guhan</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Kerala/Edit/Bidyut3.jpg">
                                <h3 class="member_name text-center">Dr.Bidyut Mukherjee</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/MahtabAlam.jpg">
                                <h3 class="member_name text-center">CA.Mahtab Alam</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            
                        </div>
                        <!-- id=20 -->
                         <div class="row mt-3" id="20" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Madhyapadesh/Edit/ShubhamTiwari.jpg">
                                <h3 class="member_name text-center">Shubham Tiwari</h3>
                                <p class="member_desgn text-center">Secretary </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Madhyapadesh/Edit/AmritSingh.jpg">
                                <h3 class="member_name text-center">Amrit Singh Chawla</h3>
                                <p class="member_desgn text-center">Jt. Secretary</p>                                
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Madhyapadesh/Edit/PrashantGupta1.jpg">
                                <h3 class="member_name text-center">Prashant Gupta</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Bidyut3.jpg">
                                <h3 class="member_name text-center">Dr.Bidyut Mukherjee</h3>
                                <p class="member_desgn text-center">E.D.</p>                                
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Madhyapadesh/Edit/DrLakshank4.jpg">
                                <h3 class="member_name text-center">Dr.Lakshank</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Madhyapadesh/Edit/Miland1.jpg">
                                <h3 class="member_name text-center">Milan Chavda</h3>
                                <p class="member_desgn text-center">Treasurer </p>
                            </div>
                        </div>
                        <!-- id=21 -->
                         <div class="row mt-3" id="21" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/MaharashtraGoa/Edit/Satam1.jpg">
                                <h3 class="member_name text-center">Ramakant T.Satam</h3>
                                <p class="member_desgn text-center">Secretary </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Karnataka/Edit/Sridhar1.jpg">
                                <h3 class="member_name text-center">G.Sreedhar Kumar</h3>
                                <p class="member_desgn text-center">E.D. </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/MaharashtraGoa/Edit/DrLakshank4.jpg">
                                <h3 class="member_name text-center">Dr.Lakshank</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/MaharashtraGoa/Edit/ShellyTibak3.jpg">
                                <h3 class="member_name text-center">Shelly Tibak</h3>
                                <p class="member_desgn text-center">C.E.O</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/MaharashtraGoa/Edit/Miland1.jpg">
                                <h3 class="member_name text-center">Milan Chavda</h3>
                                <p class="member_desgn text-center">Treasurer </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/MaharashtraGoa/Edit/NandedBharat.jpg">
                                <h3 class="member_name text-center">Nanded Bharat</h3>
                                <p class="member_desgn text-center">Jt. Secretary</p>
                            </div>
                        </div>
                       
                        <!-- id=35 -->
                         <div class="row mt-3" id="35" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Harpreet.jpg">
                                <h3 class="member_name text-center">Harpreet Singh</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/PawanSingh1.jpg">
                                <h3 class="member_name text-center">Pawan Singh</h3>
                                <p class="member_desgn text-center">Chairman</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Jitendra1.jpg">
                                <h3 class="member_name text-center">Jitendra Kaushik</h3>
                                <p class="member_desgn text-center">C.E.O </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Lucky.jpg">
                                <h3 class="member_name text-center">Lucky Chauhan</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                               <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/Bidyut3.jpg">
                                <h3 class="member_name text-center">Dr.Bidyut Mukherjee</h3>
                                <p class="member_desgn text-center">E.D.</p>                                
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/MahtabAlam.jpg">
                                <h3 class="member_name text-center">CA.Mahtab Alam</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                        </div>
                        <!-- id=29 -->
                         <div class="row mt-3" id="29" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Rajastan/Edit/Nagar.jpg">
                                <h3 class="member_name text-center">Kinchit Nagar</h3>
                                <p class="member_desgn text-center">Secretary </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Rajastan/Edit/DrLakshank4.jpg">
                                <h3 class="member_name text-center">Dr.Lakshank</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Rajastan/Edit/ShellyTibak3.jpg">
                                <h3 class="member_name text-center">Shelly Tibak</h3>
                                <p class="member_desgn text-center">C.E.O</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Rajastan/Edit/Miland1.jpg">
                                <h3 class="member_name text-center">Milan Chavda</h3>
                                <p class="member_desgn text-center">Treasurer </p>
                            </div>
                           <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Chandigad_Panjab_Haryan/Edit/MahtabAlam.jpg">
                                <h3 class="member_name text-center">CA.Mahtab Alam</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/WestInd/Rajastan/Edit/ShriRam.jpg">
                                <h3 class="member_name text-center">R Shri Ram</h3>
                                <p class="member_desgn text-center">C.E.O.</p>
                            </div>
                        </div>
                        <!-- id=31 -->
                         <div class="row mt-3" id="31" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Tamilnadu/Edit/ShriRam.jpg">
                                <h3 class="member_name text-center">R Shri Ram</h3>
                                <p class="member_desgn text-center">C.E.O.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Tamilnadu/Edit/Guhan.jpg">
                                <h3 class="member_name text-center">Guhan</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            <!--<div class="col-md-2 col-6">-->
                            <!--    <img class="member_avatar" src="images/icon/SouthInd/Tamilnadu/Edit/RaghavMathood.jpg">-->
                            <!--    <h3 class="member_name text-center">Raghav Mathood</h3>-->
                            <!--    <p class="member_desgn text-center">Chairman</p>-->
                            <!--</div>-->
                             <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Kerala/Edit/Sridhar1.jpg">
                                <h3 class="member_name text-center">G.Sreedhar Kumar</h3>
                                <p class="member_desgn text-center">Executive Director </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Tamilnadu/Edit/Appadurai.jpg">
                                <h3 class="member_name text-center">Appadurai.P</h3>
                                <p class="member_desgn text-center">Treasurer</p>                                
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Tamilnadu/Edit/KKumar.jpg">
                                <h3 class="member_name text-center">K Kumar</h3>
                                <p class="member_desgn text-center">Jt. Secretary</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Tamilnadu/Edit/Kannan.jpg">
                                <h3 class="member_name text-center">KANNAN_A </h3>
                                <p class="member_desgn text-center">Secretary </p>
                            </div>
                        </div>
                        
                        <!-- id=34 -->
                         <div class="row mt-3" id="34" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Upi/Edit/Vireshkumar.jpg">
                                <h3 class="member_name text-center">Viresh kumar</h3>
                                <p class="member_desgn text-center">Secretary </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Upi/Edit/AjitJhar.jpg">
                                <h3 class="member_name text-center">Ajit Jha</h3>
                                <p class="member_desgn text-center">Jt. Secretary</p>                                
                            </div>
                             <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Upi/Edit/Hemant.jpg">
                                <h3 class="member_name text-center">Hemant Tiwari</h3>
                                <p class="member_desgn text-center">Jt. Secretary</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Upi/Edit/PawanSingh1.jpg">
                                <h3 class="member_name text-center">Pawan Singh</h3>
                                <p class="member_desgn text-center">Chairman</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Upi/Edit/Jitendra1.jpg">
                                <h3 class="member_name text-center">Jitendra Kaushik</h3>
                                <p class="member_desgn text-center">C.E.O.</p>
                            </div>
                            
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Upi/Edit/Bidyut3.jpg">
                                <h3 class="member_name text-center">Dr.Bidyut Mukherjee</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                        </div>
                        
                        <!-- id=36 -->
                         <div class="row mt-3" id="36" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/West-Bengol/Edit/Subir3.jpg">
                                <h3 class="member_name text-center">Subir Banerjee</h3>
                                <p class="member_desgn text-center">Secretary </p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/West-Bengol/Edit/SSur.jpg">
                                <h3 class="member_name text-center">Shishir Sur</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/West-Bengol/Edit/Pulak2.jpg">
                                <h3 class="member_name text-center">Pulak Chatterjee</h3>
                                <p class="member_desgn text-center">Jt.Secretary</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/West-Bengol/Edit/Bidyut3.jpg">
                                <h3 class="member_name text-center">Dr.Bidyut Mukherjee</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/EastInd/West-Bengol/Edit/rohan.jpg">
                                <h3 class="member_name text-center">Rohan Datta</h3>
                                <p class="member_desgn text-center">Treasurer</p>
                            </div>
                             <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Karnataka/Edit/Sridhar1.jpg">
                                <h3 class="member_name text-center">G.Sreedhar Kumar</h3>
                                <p class="member_desgn text-center">E.D. </p>
                            </div>
                        </div>
                        <!-- id=37 -->
                       <div class="row mt-3" id="37" style="display: none;">
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Himachal_JamuKashmir/Edit/Bidyut3.jpg">
                                <h3 class="member_name text-center">Dr.Bidyut Mukherjee</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                             <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Himachal_JamuKashmir/Edit/MahtabAlam.jpg">
                                <h3 class="member_name text-center">CA.Mahtab Alam</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Himachal_JamuKashmir/Edit/DrLakshank4.jpg">
                                <h3 class="member_name text-center">Dr.Lakshank Mehta</h3>
                                <p class="member_desgn text-center">E.D.</p>
                            </div>
                            <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Himachal_JamuKashmir/Edit/PawanSingh1.jpg">
                                <h3 class="member_name text-center">Pawan Singh</h3>
                                <p class="member_desgn text-center">Chairman</p>
                            </div>
                             <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/NorthInd/Himachal_JamuKashmir/Edit/Jitendra1.jpg">
                                <h3 class="member_name text-center">Jitendra Kaushik</h3>
                                <p class="member_desgn text-center">C.E.O. </p>
                            </div>
                             <div class="col-md-2 col-6">
                                <img class="member_avatar" src="images/icon/SouthInd/Karnataka/Edit/Sridhar1.jpg">
                                <h3 class="member_name text-center">G.Sreedhar Kumar</h3>
                                <p class="member_desgn text-center">E.D. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div id="txtHint"><!-- <b>Person info will be listed here...</b> --></div>

                <div class="row mt-4" style="display: none;">
                    <div class="col-md-4">
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>Ground</th>
                              <th>City</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>AA-Ground</td>
                              <td>Chennai</td>
                            </tr>
                             <tr>
                              <td>AA-Ground</td>
                              <td>Chennai</td>
                            </tr>
                             <tr>
                              <td>AA-Ground</td>
                              <td>Chennai</td>
                            </tr>
                             <tr>
                              <td>AA-Ground</td>
                              <td>Chennai</td>
                            </tr>
                             <tr>
                              <td>AA-Ground</td>
                              <td>Chennai</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col-md-8">
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>Date</th>
                              <th>City</th>
                              <th>6:30AM</th>
                              <th>9:00AM</th>
                              <th>11:30AM</th>
                              <th>2:30PM</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>13<sup>th</sup> Sep</td>
                              <td>Chennai</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td>13<sup>th</sup> Sep</td>
                              <td>Chennai</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td>13<sup>th</sup> Sep</td>
                              <td>Chennai</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td>13<sup>th</sup> Sep</td>
                              <td>Chennai</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td>13<sup>th</sup> Sep</td>
                              <td>Chennai</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                </div>
              
                
               
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  // alert('hi');
  $("#state").change(function(){
 // alert('hi');
      var state = $("#state").val();
       // alert(state);
      var images = $(".member_avatar").val();
      var memname = $(".member_name").val();
      var memdesgn = $(".member_desgn").val();
      if (state =='2' ) {
         $("#2").show();
      }
      else{
        $("#2").hide();
      }
      if (state =='4' ) {
         $("#4").show();
      }
      else{
        $("#4").hide();
      }
      if (state =='5' ) {
         $("#5").show();
      }
      else{
        $("#5").hide();
      }
      if (state =='6' ) {
         $("#6").show();
      }
      else{
        $("#6").hide();
      }
      if (state =='7' ) {
         $("#7").show();
      }
      else{
        $("#7").hide();
      }
      if (state =='10' ) {
         $("#10").show();
      }
      else{
        $("#10").hide();
      }
      if (state =='12' ) {
         $("#12").show();
      }
      else{
        $("#12").hide();
      }
      if (state =='13' ) {
         $("#13").show();
      }
      else{
        $("#13").hide();
      }
      if (state =='16' ) {
         $("#16").show();
      }
      else{
        $("#16").hide();
      }
      if (state =='17' ) {
         $("#17").show();
      }
      else{
        $("#17").hide();
      }
      if (state =='18' ) {
         $("#18").show();
      }
      else{
        $("#18").hide();
      }
      if (state =='20' ) {
         $("#20").show();
      }
      else{
        $("#20").hide();
      }
      if (state =='21' ) {
         $("#21").show();
      }
      else{
        $("#21").hide();
      }
      if (state =='26' ) {
         $("#26").show();
      }
      else{
        $("#26").hide();
      }
      if (state =='28' ) {
         $("#28").show();
      }
      else{
        $("#28").hide();
      }
      if (state =='29' ) {
         $("#29").show();
      }
      else{
        $("#29").hide();
      }
      if (state =='31' ) {
         $("#31").show();
      }
      else{
        $("#31").hide();
      }
      if (state =='32' ) {
         $("#32").show();
      }
      else{
        $("#32").hide();
      }
      if (state =='34' ) {
         $("#34").show();
      }
      else{
        $("#34").hide();
      }
      if (state =='35' ) {
         $("#35").show();
      }
      else{
        $("#35").hide();
      }
      if (state =='36' ) {
         $("#36").show();
      }
      else{
        $("#36").hide();
      }
      if (state =='37' ) {
         $("#37").show();
      }
      else{
        $("#37").hide();
      }
    });
  });
       
</script>
<script>
function showUser(str) {
  if (str == "") {
    document.getElementById("txtHint").innerHTML = "";
    return;
  } else {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("txtHint").innerHTML = this.responseText;
      }
    };
    xmlhttp.open("GET","getuser3.php?q="+str,true);
    xmlhttp.send();
  }
}
</script>
<?php include "footer.php" ?>
<?php include "db_conn.php"; ?>

<?php 

function send_otp_mail($email, $otp){

	$to = $email;
	$subject = "Verify Email";
	$message = "
	<p>Your One Time Password (OTP) is <strong>".$otp."</strong>. Please do not share with anyone.</p>
	";
	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	// More headers
	$headers .= 'From: Plus Cricle <bliss@pluscircleclub.org>' . "\r\n";
	mail($to,$subject,$message,$headers);
    
}

function send_otp_reset($email, $otp){

	$to = $email;
	$subject = "Reset Password";
	$message = "
	<p>Your One Time Password (OTP) is <strong>".$otp."</strong>. Please do not share with anyone.</p>
	";
	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	// More headers
	$headers .= 'From: Plus Cricle <bliss@pluscircleclub.org>' . "\r\n";
	mail($to,$subject,$message,$headers);
    
}
?>
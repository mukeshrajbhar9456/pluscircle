<!-- video model -->


<div id="myModalvideo1" class="modal">
		<div class="modal-content questionmark1 center" >
		 	<div class="modal-header center">
		 	
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
					<video id="video-1"  class="video1" controls  >
						<source src="https://www.pluscircleclub.org/public/images/videos/How_We_Do_Video.mp4"  type="video/mp4">
						</video>
	    		
	    	</div>
		</div>
	</div>

	<div id="myModalvideo2" class="modal">
		<div class="modal-content questionmark1 center" >
		 	<div class="modal-header center">
		 	
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
					<video id="video-2"  class="video1" controls  >
						<source src="https://www.pluscircleclub.org/public/images/videos/Old_Age_Video2.mp4"  type="video/mp4">
						</video>
	    		
	    	</div>
		</div>
	</div>

<div id="myModalvideo3" class="modal">
		<div class="modal-content questionmark1 center" >
		 	<div class="modal-header center">
		 	
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
					<video id="video-3"  class="video1" controls >
						<source src="https://www.pluscircleclub.org/public/images/videos/Why_Video2.mp4"  type="video/mp4">
						</video>
	    		
	    	</div>
		</div>
	</div>



<div id="myModalvideo4" class="modal">
		<div class="modal-content questionmark1 center" >
		 	<div class="modal-header center">
		 	
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
					<video id="video-4"  class="video1"  controls >
						<source src="https://www.pluscircleclub.org/public/images/videos/Orphans_Video2.mp4"  type="video/mp4">
						</video>
	    		
	    	</div>
		</div>
	</div>

	

	<div id="myModalvideo55" class="modal">
		<div class="modal-content questionmark1 center" >
		 	<div class="modal-header center">
		 	
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
					<video id="video-55"  class="video1"  controls >
						<source src="https://www.pluscircleclub.org/public/images/videos/Your_Benefits.mp4"  type="video/mp4">
						</video>
	    		
	    	</div>
		</div>
	</div>
	
	
	<div id="milestone" class="modal">
		<div class="modal-content questionmark1 center" >
		 	<div class="modal-header center">
		 	
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<a data-target="#myModalRoadAhead" id="road"data-toggle="modal" href="#myModalRoadAhead" >
					<img src="https://www.pluscircleclub.org/public/images/milestone1.jpg">
				</a>
	    		
	    	</div>
		</div>
	</div>
<!-- //video model -->
<!-- Modal -->
<div class="modal fade" id="edit-teampopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content back-color">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="close-span-btn">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-padd">
        <div class="text-center">
            <h2 class="modal-text">You can edit these details after 72 hours of registration</h2>
        </div>
      </div>
      
    </div>
  </div>
</div>
<!----modal end---->

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content back-color">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="close-span-btn">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-padd">
        <div class="text-center">
            <h2 class="modal-text">Registration starts <br>from<br> 15th April 2021</h2>
        </div>
      </div>
      
    </div>
  </div>
</div>
<!----modal end---->
<!-- access -->
<div class="modal fade" id="access" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content back-color">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="close-span-btn">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-padd">
        <div class="text-center">
            <h2 class="modal-text">Please register/login <br>to access this section.</h2>
        </div>
      </div>
      
    </div>
  </div>
</div>
<!----access end---->
<!-- Tournament -->
<div class="modal fade" id="tournament" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content back-color">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="close-span-btn">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-padd">
        <div class="text-center">
            <h2 class="modal-text">Tournament details<br> will be updated soon</h2>
        </div>
      </div>
      
    </div>
  </div>
</div>
<!----Tournament end---->
<!-- Match -->
<div class="modal fade" id="match" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content back-color">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="close-span-btn">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-padd">
        <div class="text-center">
            <h2 class="modal-text">Match details<br> will be updated soon</h2>
        </div>
      </div>
      
    </div>
  </div>
</div>
<!----Match end---->
<!-- Team -->
<div class="modal fade" id="team" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content back-color">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="close-span-btn">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-padd">
        <div class="text-center">
            <h2 class="modal-text">You can modify <br> your team member’s details</h2>
            <p>This section <br> will be will be updated soon</p>
        </div>
      </div>
      
    </div>
  </div>
</div>
<!----Team end---->
<!-- Stats -->
<div class="modal fade" id="stats" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content back-color">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="close-span-btn">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-padd">
        <div class="text-center">
            <h2 class="modal-text">You do not have <br> any Status now! </h2>
          
        </div>
      </div>
      
    </div>
  </div>
</div>
<!----Stats end---->
<!-- Performance -->
<div class="modal fade" id="performance" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content back-color">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="close-span-btn">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-padd">
        <div class="text-center">
            <h2 class="modal-text">You do not have <br> any Performance data now! </h2>
          
        </div>
      </div>
      
    </div>
  </div>
</div>
<!----Performance end---->
<!-- Select Team -->
<div class="modal fade" id="select-team" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content back-color">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="close-span-btn">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-padd">
        <div class="text-center">
            <h2 class="modal-text">If you have registered as an individual<br> or a Group, You can select players<br> from your vicinity based on<br> following skills.</h2>
            <div class="st-table">
               <table>
                  <tr>
                    <th>Player Role</th>
                    <th>Batting Style</th>
                    <th>Bowling Style</th>
                  </tr>
                  <tr>
                      <td>
                        <ul class="st-col">
                            <li>Opening batsman</li>
                            <li>Top-Order Batsman</li>
                            <li>Middle-Order Batsman</li>
                            <li>Wicket-Kepper Batsman</li>
                            <li>Wicket-Kepper</li>
                            <li>Bowler</li>
                            <li>All-Rounder</li>
                            <li>Lower-Order Batsman</li>
                            <li>Others</li>
                        </ul>
                      </td>
                      <td>
                        <ul class="st-col2">
                          <li> Left-hand Batsman</li>
                          <li>Right-hand Batsman</li>
                        </ul>
                      </td>
                      <td>
                         <ul class="st-col3">
                            <li>Right-arm fast</li>
                            <li>Right-arm medium</li>
                            <li>Left-arm fast</li>
                            <li>Left-arm medium</li>
                            <li>Slow left-arm orthodox</li>
                            <li>Slow left-arm chinaman</li>
                            <li>Right-arm Off Break</li>
                            <li>Right-arm Leg Break</li>
                            <li>Left-arm Off Break</li>
                            <li>Left-arm Leg Break</li>
                         </ul>
                      </td>
                  </tr>
               </table>
            </div>
          <h2 class="modal-text p020">This section will be updated soon</h2>
        </div>
      </div>
      
    </div>
  </div>
</div>
<!----Select Team end---->
<!-- Modal -->
<div class="modal fade" id="share" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content back-color">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="close-span-btn">&times;</span>
        </button>

      </div>
      <div class="modal-body modal-padd">
        <div class="text-center">
            <h2 class="modal-text">Share Link</h2>
             
        </div>
        <p class="text-center"><a href="https://wa.me/?text= Hi, Signup for +91 Cricket Tournament. https://www.pluscircleclub.org/+91cricket/" target="_blank">
    <img src="images/what.png"  width="50px">
    </a></p>
      </div>
      
    </div>
  </div>
</div>


<!---whats aap icon---->
<div>
    <a href="https://api.whatsapp.com/send?phone=+919513072088&amp;text=.&amp;source=&amp;data=">
    <img src="images/what.png" class="what-img-2" width="80px">
    </a>
</div>
 <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
     <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <!-- Vendor JS       -->
    <!--  <script src="vendor/slick/slick.min.js"></script> -->
   <!-- <script src="vendor/wow/wow.min.js"></script> 
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
   <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script> -->
    <script src="vendor/select2/select2.min.js">
    </script>
    <!-- Main JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/validate.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){
        $(".pop-modal").click( function(){
          $(".main-content").css("filter", "blur(5px)");
        });
        $(".close").click( function(){
          $(".main-content").css("filter", "blur(0px)");
        });
      });
    </script>
   
    <script type="text/javascript">
        $(document).ready(function(){
        var url = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
            $('.list-unstyled navbar__list li').removeClass('active');
            $('[href$="'+url+'"]').parent().addClass("active");
            $('.menu-item-has-children').addClass('open',);
            $('[href$="'+url+'"]').closest('.menu-item-has-children').addClass('show');
            $('[href$="'+url+'"]').parent().addClass("active");

        });
    </script>
   <!--  <script type="text/javascript">
      $('.digit-group').find('input').each(function() {
      $(this).attr('maxlength', 1);
      $(this).on('keyup', function(e) {
        var parent = $($(this).parent());
        
        if(e.keyCode === 8 || e.keyCode === 37) {
          var prev = parent.find('input#' + $(this).data('previous'));
          
          if(prev.length) {
            $(prev).select();
          }
        } else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
          var next = parent.find('input#' + $(this).data('next'));
          
          if(next.length) {
            $(next).select();
          } else {
            if(parent.data('autosubmit')) {
              parent.submit();
            }
          }
        }
      });
    });
  </script> -->
  <script type="text/javascript">
    $("input").keypress(function(){
   if ( $(this).val().length > 1){
    $(this).addClass('active');
    }
  });
  </script>
  
</body>

</html>
<!-- end document-->
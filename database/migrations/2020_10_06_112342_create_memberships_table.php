<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberships', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid');
            $table->string('year')->nullable();
            $table->string('type')->nullable();
            $table->string('alphabet')->nullable();
            $table->string('countrycode')->nullable();
            $table->string('serialnumber')->nullable();
            $table->string('cardnumber')->nullable();
            $table->string('start_date')->nullable();
            $table->string('active')->nullable();
            $table->timestamps();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberships');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('datetime'); 
            $table->string('userid'); 
            $table->string('usertype');
            $table->string('payment_id')->nullable();            
            $table->string('description')->nullable();
            $table->string('name')->nullable(); 
            $table->string('email')->nullable(); 
            $table->string('contact')->nullable(); 
            $table->string('entity')->nullable(); 
            $table->double('amount')->nullable(); 
            $table->string('currency')->nullable(); 
            $table->string('status')->nullable(); 
            $table->string('order_id')->nullable(); 
            $table->string('invoice_id')->nullable(); 
            $table->string('international')->nullable(); 
            $table->string('method')->nullable(); 
            $table->double('amount_refunded')->nullable(); 
            $table->string('refund_status')->nullable(); 
            $table->string('captured')->nullable(); 
            $table->string('card_id')->nullable(); 
            $table->string('bank')->nullable(); 
            $table->string('wallet')->nullable(); 
            $table->string('vpa')->nullable(); 
            $table->string('authcode')->nullable(); 
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

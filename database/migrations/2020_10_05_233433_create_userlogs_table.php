<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userlogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userid');
            $table->string('usertype');
            $table->ipAddress('ipaddress');
            $table->string('session_id');
            $table->dateTime('login_at')->nullable();
            $table->dateTime('last_active')->nullable();
            $table->dateTime('logout_at')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->index(['id']); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userlogs');
    }
}

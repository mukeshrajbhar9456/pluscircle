<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->integer('userid');
            $table->string('eventtype')->nullable();
            $table->string('eventtitle')->nullable();
            $table->integer('minparticipant')->nullable();
            $table->integer('maxparticipant')->nullable();
            $table->string('eventothers')->nullable();
            $table->integer('reservedticket')->nullable();
            $table->string('ticketnumberfrom')->nullable();
            $table->string('ticketnumberto')->nullable();
            $table->time('starttime')->nullable();
            $table->time('endtime')->nullable();
            $table->date('startdate')->nullable();
            $table->date('enddate')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('pincode')->nullable();
            $table->string('address')->nullable();
            $table->integer('ticketprice')->nullable();
            $table->integer('agemin')->nullable();
            $table->integer('agemax')->nullable();
            $table->string('description')->nullable();
            $table->text('eventfacility')->nullable();
            $table->integer('donatepcc')->nullable();
            $table->string('organisername')->nullable();
            $table->string('organiseremail')->nullable();
            $table->string('organiserphone')->nullable();
            $table->string('organisersphone')->nullable();
            $table->text('internalnote')->nullable();
            $table->string('event_profile')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}

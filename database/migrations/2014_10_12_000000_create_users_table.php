<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('title');
            $table->string('phonecode')->nullable();
            $table->string('phone')->unique();
            $table->string('secondaryphone')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('phone_verified_at')->nullable();            
            $table->string('password');
            $table->rememberToken();
            $table->string('role')->default('user');
            $table->date('dob')->nullable();
            $table->string('profession')->nullable();
            $table->string('gender')->nullable();	
            $table->string('profile')->nullable();
            $table->string('kyctype')->nullable();
            $table->string('kycprofile')->nullable();
            $table->string('ccode')->nullable();
            $table->text('address')->nullable();
            $table->string('ccountry')->nullable();
            $table->string('cstate')->nullable();
            $table->string('ccity')->nullable();
            $table->string('pincode')->nullable();
            $table->boolean('isactive')->default(false);
            $table->boolean('email_verified')->default(false);
            $table->boolean('phone_verified')->default(false);
            $table->string('membershiptype')->nullable();
            $table->string('paymenttype')->nullable();
            $table->string('tmembership')->nullable();
            $table->string('pmembership')->nullable();
            $table->string('refferedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

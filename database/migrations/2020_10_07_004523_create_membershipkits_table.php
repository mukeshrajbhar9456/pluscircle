<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipkitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membershipkits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid');
            $table->string('addresstype')->nullable();
            $table->string('tshirtsize')->nullable();
            $table->string('price')->nullable();
            $table->string('payment_reference')->nullable();
            $table->string('payment_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membershipkits');
    }
}

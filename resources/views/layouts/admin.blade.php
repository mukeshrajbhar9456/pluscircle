
@include('layouts.admin_head')
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            @include('layouts.admin_header')
			@include('layouts.admin_sidebar')
			@yield('content')
			<footer class="footer">
					2020 © Plus Circle Club.
			</footer>
		</div>
        @include('layouts.admin_footer')
    </body>
</html>
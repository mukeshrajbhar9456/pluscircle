
@include('layouts.lp_head')    
@include('layouts.lp_header')    
    <body>
            @yield('content')
            @include('layouts.lp_footer') 
    </body>
</html>

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Plus Circle</title>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="google-site-verification" content="q3o_Qxr5nVyi8kW9pXQI-2n_SEcjkGQyVT5N777FTIM" />
        <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=G-WHN9PLV5EK"></script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
            
              gtag('config', 'G-WHN9PLV5EK');
            </script>
        
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/bootstrap.min.css')}}">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('public/fonts/css/all.css')}}">
        <link rel='stylesheet' href="{{asset('public/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/responsive.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('public/croppie/croppie.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/gifplayer.css')}}">
        
		
	</head>

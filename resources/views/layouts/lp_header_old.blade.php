
<style>
.bigcontent{
    margin-top: -37px;
}
</style>
	<header class="header-main">
			<div class="container-fluid">


				<div class="header-inner">
					<div class="logo">
						<a class="desktop" href="{{ url('/') }}"><img src="{{asset('public/images/logo.png')}}"></a>
						<a class="mobile" href="{{ url('/') }}"><img src="{{asset('public/images/logo-mobile.png')}}"></a>
					</div>

					<!-- <a data-target="#question_mark" data-toggle="modal" href="#question_mark" title="Why Plus Circle ?"><img src="{{asset('public/images/question_mark.png')}}"class="questionmark">
							</a> -->


					<div class="nav-bar">
						<ul>
							<li ><a data-target="#question_mark" data-toggle="modal" href="#question_mark" ><img title="Why Plus Circle" src="{{asset('public/images/question_mark.png')}}"class="questionmark mr-15">
							</a></li>

							<li class="desktop"><a data-target="#milestone" data-toggle="modal" href="#milestone" title="Road Ahead"><img src="{{asset('public/images/milestone.jpg')}}" class="mr-15"></a></li>

							<li ><a data-target="#comment" data-toggle="modal" href="#comment" ><img title="Complimentory Motion" src="{{asset('public/images/comment.png')}}"class="mr-15">
							</a></li>
							

							<!-- <li class="search_con">
								
								<form action="/search" class="search_form"> <input type="hidden" name="type" value="product"> <span class="icon-search search-submit">
								
								</span> <input type="text" name="q" placeholder="Search" value="" autocapitalize="off" autocomplete="off" autocorrect="off"></form>
							</li> -->


							<!-- 
							<li>
								<a data-target="#myModalLogin" data-toggle="modal" href="#myModalLogin" >Join & Login</a>
							</li> -->
							@auth
							<li> 
								<div class="dropdown">
							<button onclick="myFunction()" class="dropbtn">My Account &nbsp; <i class="arrow down"></i></button>
							<div id="myDropdown" class="dropdown-content">
								<a href="{{ route('user.dashboard') }}">My Dashboard</a>
								<a href="{{ route('user_logout') }}">Logout</a>
								<a href="{{ route('user.dashboard') }}">Reset Password</a>
							</div>
							</div>
						
							@else
							<li><a href="{{ route('user_login') }}"  ><img src="{{asset('public/images/join_login.png')}}" title="Join & Login"></a></li>
							<!-- <li><a href="{{ route('user_login') }}">Join & Login</a></li> -->
							@endif
							<!-- <li><div id="google_translate_element"></div></li> -->
						</ul>
						<script> function googleTranslateElementInit() { new google.translate.TranslateElement( {pageLanguage: 'en'}, 'google_translate_element' ); } </script> <script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
						<div class="navigation_button_main">
							<input type="checkbox" class="navigation_checkbox" id="navi-toggle">
						    <label for="navi-toggle" class="navigation_button">
						      <span class="navigation_icon">&nbsp;</span>
						    </label>

						    <div class="navigation_background">&nbsp;</div>
						    <div class="navigation_nav">
						        <div class="navigation_menu_box">
						            <div class="container">
						          	    <div class="menu_logo">
							            	<a href="{{ url('/') }}"><img src="{{asset('public/images/logo-light.png')}}"></a>
							          	</div>
							          	<div class="menu-list-main">
											<div class="menu-list-box">
												<h5>Why Join Plus Circle</h5>
												 <ul>
			                                     	<li><a href="{{ route('why_pluscircle') }}">Your Benefits</a></li>
			                                       <!-- <li><a href="#">Community</a></li> -->
			                                       <li><a href="{{ route('Social_Networking') }}">Networking</a></li>
			                                       <li><a href="{{ route('Supporting_Hand') }}">Supporting Hand</a></li>
			                                       <li><a href="{{ route('Scholarships_Programs') }}">Scholarship</a></li>
			                                       <li><a href="{{ route('Emergency_Support') }}">Emergency Support</a></li>
			                                       <li><a href="{{ route('Business_Opportunity') }}">Business Opportunity</a></li>
			                                       <li><a href="{{ route('Opportunities_professionals') }}">Professional Opportunity</a></li>
			                                       <li><a href="{{ route('Health_Insurance') }}">Health Insurance</a></li>
	                                   			</ul>
											</div>
											<div class="menu-list-box">
												<h5>Overview</h5>
												<ul>
													<li><a href="{{ route('about_us') }}">About us</a></li>
													<li><a href="{{ route('our-vision') }}">Vision</a></li>
			                                       	<li><a href="{{ route('our-mission') }}">Mission</a></li>
			                                      	<li><a href="{{ route('what-we-do') }}">What We Do</a></li>
			                                       	<li><a href="{{ route('how-we-do') }}">How We Do</a></li>
												</ul>
											</div>
											<div class="menu-list-box">
												<h5>Our Programs</h5>
												<ul>
			                                       <li><a href="{{ route('events') }}">Events</a></li>
			                                       <li><a href="{{ route('activities') }}">Activities</a></li>
			                                       <li><a href="{{ route('volunteer_c') }}">Volunteer</a></li>
			                                    </ul>
											</div>
											<div class="menu-list-box">
												<h5>Location</h5>
												<ul>
													<li><a href="{{ route('contact_us') }}">Bangalore</a></li>
												<!-- 	<li><a href="#">Chennai</a></li>
											785		<li><a href="#">Mumbai</a></li>
													<li><a href="#">Kolkata</a></li>
													<li><a href="#">Delhi</a></li> -->
												</ul>
											</div>
							          	</div>  
						            </div>
						        </div>
						      </div>
						    </div>
						</div>
					</div>
				</div>
			</div>				
		</header>


	<div id="dashboardothermenu" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header bigcontent contactcsspopupl">
		 		<!-- <h5 class="modal-title">Menu Access</h5> -->
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5>Thank you for registering with Plus Circle</h5>
					<p class='text-left'>You will be able to use your dashboard upon the approval of your application by our management.</p>
					<p>Once your membership application is approved, you will be informed via your registered Email id.</p>
				</div>
	    	</div>
		</div>
	</div>
		

	<div id="myModal_upcoming" class="modal">

		  <div class="modal-content" style="padding-bottom: 0px;">
		 
		    <div class="modal-header">
			
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="upcoming-event-popup ">	
			      		<div class="popup-img">
			      			<img src="{{ asset('public/images/community-img.jpg') }}">
			      		</div>
			      		<div class="popup-content-title">
			      			<strong>Demo Club</strong>
			      		</div>
			      		<div class="popup-content">
			      			<ul>
			      				<li><strong>Name : </strong>Demo</li>
			      				<li><strong>Email : </strong>demo@gmail.com</li>
			      				<li><strong>Event Id : </strong>123456</li>
			      				<li><strong>Event Id : </strong>123456</li>
			      				<li><strong>Venue : </strong>Demo</li>
			      				<li><strong>Event Type : </strong>Demo</li>
			      				<li><strong>Date : </strong>27/03/2020</li>
			      				<li><strong>Starting Time : </strong>1:00 PM</li>
			      				<li><strong>Ending Time : </strong>6:00 PM</li>
			      				<li><strong>Event Facilities : </strong></li>
			      			</ul>
			      			<div class="popup-content-text">
			      				<p><strong>Description : </strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
			      			</div>
			      			<div class="popup-content-bottom">
			      				<div class="terms-link">
			      					<a href="#">Terms & Conditions of the event</a>
			      				</div>
			      				<div class="available-tickets">
			      					<p><span>Available Tickets: 10</span><span>Price : Rs. 1000</span></p>
			      					<button class="btn" data-dismiss="modal" data-toggle="modal" data-target="#myModal2">Book a Ticket</button>
			      				</div>
			      				<div class="content-bottom">
			      					<p>Organised By : Club</p>
			      				</div>
			      			</div>
			      		</div>
			      	</div>
		     	</div>
    		</div>
		  </div>

	

		<div id="myModal2" class="modal">

		  <div class="modal-content  width-50" style="padding-bottom: 0px;">
		 
		    <div class="modal-header">
			
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			 	<div class="upcoming-event-popup ">
		    		
			      			
			      		<div class="popup-content-title">
			      			<strong>Booking Confirmation</strong>
			      		</div>
			      		<div class="popup-content">
			      			<div class="ticket-quantity">
				      			<div class="ticket-quantity-box">
				      				<strong>Ticket Price : Rs. 1500</strong>
				      			</div>
				      			<div class="ticket-quantity-box">
									<div class="input-group">
										<input type="button" value="-" class="button-minus" data-field="quantity">
										<input type="number" step="1" max="" value="1" name="quantity" class="quantity-field">
										<input type="button" value="+" class="button-plus" data-field="quantity">
									</div>
				      			</div>
				      			
				      		</div>
				      		<div class="total-price">
			      				<strong>Total Price : Rs. 3000</strong>
			      				<div class="book-btn">
			      					<button type="button"  class="btn">Continue to Pay</button>
			      				</div>
			      			</div>
			      		</div>
			      
		     	</div>
    		</div>
		  </div>

		</div>


		<div id="myModalEventsFashion" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/socialize/plus-circle-fashion.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Plus Circle Fashion   </h4>
							<p>
							We organize biggest trade fair for jewelry, fashion and accessories and offers an independent appreciation of all products, from diamonds to fur coats to expensive watches and designer eyewear. The objective of BFW is to create business relationships and to facilitate a dialogue between Fashion Designers and Trade Buyers of the World.</p>
							<p><b>Fashion Shows by Leading Fashion and Accessory Designers from Across The World</b></p>
							<p><b>Lifestyle Experience Zone</b></p>
							<p><b>After Parties</b></p>
							<p><b>Multi Designer Store</b></p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>12 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

		 <div id="myModalEventsPilgrimage" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/socialize/piligrimage events.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Pilgrimage Events   </h4>
							<p>
							It is specially designed for the one who has just stepped into meditation and spirituality.</p>


							<p>This Short Term Meditation is one of the best Meditation Programs in India, We inspires you to take a moment out of your hectic schedule and connect to your inner self. It encourages you with the opportunity to explore your true self. We follow a logical and scientific approach to every meditation technique. This course guides you to unplug, decompress, and find peace and harmony.
							</p>

							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>18 Nov 2020</b> | Location: <b>Banglore</b></p>


							</p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>




		 <div id="myModalEventsRacing" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/socialize/Racing.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4> Racing   </h4>
							<p>There’s more action happening in the Indian Motorsport scenario than ever before. Youngsters are leaping forward in making India a significant and a promising new contender in international motorsports. And established performers are displaying world class performances and setting a great example for the budding talent. While each and every motorsport event is special to us, here are a few of them that made it big amongst the crowd.</p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>8 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>



		  <div id="myModalEventsTrekking" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/socialize/Trekking.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4> Trekking   </h4>
							<p>It is the 10th highest peak in Karnataka, and has been declared a natural heritage sight by the government of Karnataka. The name Kodachadri is derived from the words ‘Kodacha’ and ‘Adri’ which mean ‘jasmine hills’ in the local language. Apart from being home to plantations, the location also has a stunning range of biodiversity. Here you can see the Pied Hornbill, Rock Python, Langoor and many species.</p>

							<p>This trek will take you through lush dense forest, picturesque landscapes, beautiful waterfalls, give you a glimpse of the Arabian Sea and lead you along a challenging trail. A 2-day trip is an ideal way to explore this peak.
							</p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>8 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>


		 <div id="myModalEventsHill" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/socialize/Hill_Climb.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>	Hill Climb   </h4>
							<p>Nandi Hills is now an eco-tourism destination. Get ready for a host of activities as a build up to an eventual Nandi Hills</p>
							<p>The aim is to spread awareness on Nandi Hills as a more-than-meets the eye destination. As part of this event, it will include nature walks, rock climbing, , bird watching events, heritage trails, and cultural events too. It’s really about bringing the community together to a place we all know and love.

							</p><p><br>Organized by: <b>PCC</b> | Date: <b>5 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>


		 <div id="myModalEventsHealth" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/adoran/health-checkups.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Health Checkups </h4>
							<p>Medical Camps are conducted regularly at different areas based on the necessity involving the local smithis who organise all  the necessary requirements like doctors, nurses, seva dal and supply of medicines.</p>
							<p>Specialties offered are General Medicine, Surgery, Gynecology, Pediatrics and Radiology.  They already had in the hospital the basic laboratory, Pharmacy, Ophthalmology, ENT, and Dentistry.

							</p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>18 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>


		 <div id="myModalEventsBlood" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/adoran/Blood-donation.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Blood Donation </h4>
							<p>Blood donation is among the highest service one can render to humanity.</p>
							<p>With this thought, Plus Circle has organized Blood Donation Camp on  10th Sep 2020| 10am– 12.00 noon in association with Blood Donation Society, , Bengaluru. This small activity can save lives of many people. Hence, we request for your participation and also request you to invite your friends, relatives for this noble cause.

							</p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>2 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>


		 <div id="myModalEventsStandup" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/adoran/stand-up-comedy.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Standup Comedy </h4>
							<p>Please note: Famous Stand Up Comedian will be performing the same material which he has been doing since last year but its not on Youtube, please avoid watching this show if you've watched him live in the last one year, since the material will be the same.</p>
							<p>Please note the below mentioned terms and conditions carefully</p>
							<p>Age Limit: 16 years and above </p>
							<p>You will receive a link to join along with the ticket confirmation</p>
							<p>Please login 10 mins before the show time. </p>
							<p>Recording or uploading of this stream is strictly not permitted. </p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>9 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>


		 <div id="myModalEventsLive" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/adoran/live-music.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Live Music </h4>
							<p>The entertainment industry has gone into a major phase of crisis due to COVID 19. Livelihoods of Musicians and Singers are affected as all live performance are halted. 
							While self-isolation and social distancing become the norm, digital concerts are the new game. “ Musical Ecstasy “ is the series of live-in-sync concerts with some of the biggest names in the music fraternity!

							</p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>27 Nov 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

		<div id="myModalEventsCycling" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/socialize/Cycling.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Cycling</h4>
							<p>We propose to ride in Bangaalore City every Month 01st morning starting at 5:45 AM . The circular route will be a comfortable 20 kms and gives us a good practice opportunity. We will all assemble there at 5:40 AM and head for a ride at 5:45 AM sharp.</p>

							Mandatory Cycling items:

							Cycle in a good running condition Helmet (a must) Drinking water Spare tube / puncture repair kit Snacks, etc
							</p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>13 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>



		<div id="myModalEventsPlusCircle" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/socialize/Plus-circle-marathon.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Plus Circle Marathon  </h4>
							<p>Being a virtual event, you are free to run wherever you want, whenever you want without being bound by any time restrictions.</p>
							<p>No matter whether you walk, run, skip, or crawl, on a treadmill, inside your house, balcony just go out there, best foot forward! Whether it’s your first race as a beginner or your first one for the year, there’s something for everyone to start your journey towards an active lifestyle.
							</p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>25 Nov 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

		 <div id="myModalEventsFoodie" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/socialize/Foodie-fest.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Foodie Fest  </h4>
							<p>Get set to experience finest food-walk dotted with rich culture and gastronomic delights.
							As the markets come alive, we surprise your taste buds with the finest forms of culinary delights  hidden food places a traveler can’t find on their own. The food trail will include snacking at the old Snacks shop, sampling mouth-watering traditional Indian sweet (Jalebi), crunchiest chicken & juiciest kebabs and many more local delicacies.</p>

							<p>All food delicacies, Lunch/Dinner and Drinks</p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>29 Nov 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>



		 <div id="myModalEventsYoga" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/socialize/yoga.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Yoga </h4>
							<p>India is for seekers who wish to explore Yoga in its most authentic and purest form. Our course is a one of its kind, month-long, complete immersion course ideal for individuals aspiring to establish themselves as holistic Yoga teachers in any part of the world.</p>

							<p>It enhances your knowledge and skill of Yoga and simultaneously helps you slow down, live in the present moment and connect to your true self, inspiring self-discovery on a physical and spiritual level. Backed by millennia of experiencAll through, we ensure a conducive environment for learning and growth.</p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>17 Nov 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>






		  <div id="myModalEventsBeach" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/socialize/beach-side-camp.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Beach Side Camp </h4>
							<p>Wonderwall,  concept, with a new boutique beach festival all set to hit the stunning shores. The event will retain Goa’s natural beauty as its backdrop with very less alterations to its surroundings. Their point is to drive the crowd crazy with some amazing beats rather than LED lights and fireworks. Our DJ will be kicking the fest off with his gig on 28th.
							</p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>21 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>



		 <div id="myModalEventsAdventurous" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/socialize/Adventurous.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Adventurous   </h4>
							<p>
							Head out for this adventurous trip to Dandeli from Bangalore and indulge in an array of activities. Experience the fun and thrill as you try river rafting and raft through the currents. Enjoy kayaking and coracle boat ride in the crystal clear waters. Delight in a quick bath while exploring the natural Jacuzzi in the river Kali and spot some crocodile.
							</p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>15 Nov 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

		<div id="myModalEventsWebinar" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/events-elegance/Webinar.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Webinar  </h4>
							<p>The goal of ecommerce Powered By Plus Circle is to build and optimize online stores for small independent businesses and artists in just a matter of days. We try businesses get to expand their selling opportunities into the online market.</p>
							
							<p>In this webinar you will get the answers to the top questions we’ve received about the Ecommerce program, including:</p>

							<p>Who is Digital Main Street?</p>
							<p>What is Ecommerce?</p>
							<p>What does it mean to sell online, and why should my business?</p>
							<p>Do I qualify for this program?</p>
							<p>What are the costs involved?</p>
							<p>Do you have any examples of completed websites?</p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>12 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

		 <div id="myModalEventsBusiness" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/events-elegance/Business-Fair.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Business Fair  </h4>
							<p>The National Virtual Small Business Expo is  BIGGEST & most anticipated virtual business-to-business networking & educational event, trade show & conference for business owners, entrepreneurs, start-ups, decision-makers or anyone who works for a small business or is interested in starting a Small Business.</p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>21 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>



		<div id="myModalEventsSocial" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/events-elegance/Social-Awareness-Programs.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Social Awareness Programs  </h4>
							<p>We try to solve the problems of sanitation and waste management in India by ensuring hygiene in major cities. It is important to display high standards of hygiene and cleanliness to change the overall global perception people have about our country.</p>
							<p>Many people helps by taking surveys from the public, talking to people in your family and neighbourhood, and joining the campaign by cleaning the areas.</p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>25 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

		 <div id="myModalEventsAnnual" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/events-elegance/Annual-Fest.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Annual Fest  </h4>
							<p>Every year, We provides a platform to a International Annual event with all our members.</p>

							<p>We expect about 5000 + from across World to participate in this mega event. Sponsors will have extensive media coverage through social media, web, posters, banners, certificates, souvenir etc. We distribute Donatinto the orphanages who participate and win in different competitions like Best Manager, Business Quiz, Business Plan Presentation, HR, Finance, Operations, Marketing, CSR and an array of informal competitions like dance, singing, treasure hunt, crime scene investigation, short film making, online gaming and many more. Details are given in the attached brochure.
							</p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>22 Nov 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

		<div id="myModalEventsSeminars" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/events-elegance/Seminar-conference.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Seminars & Conferences </h4>
							<p>The aim objective of this conference is to provide a world class platform to present and discuss all the latest research and results of scientists related Industrial, Production & Systems Engineering. This conference provides opportunities for the different areas delegates to exchange new ideas and application experiences face to face, to establish business or research relations and to find global partners for future collaboration. We hope that the conference results constituted significant contribution to the knowledge in these up to date scientific field.</p>
							<p>Organized By: Plus Circle.</p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>12 Nov 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

		 <div id="myModalEventsTrade" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/events-elegance/Trade-Shows.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Trade Shows </h4>
							<p>The Consumer Goods Expo India - VTF Organised by Plus Circle held on 03-05 Sep 2020. What's New in This Expo 1. Separate Halls for Different Product Categories. 2. Sales Kits are available in Hindi & English languages (with few announcements in regional languages as well) 3. This fair is 100% open & focused for Indian/Foreign Exhibitors, Brands & Sponsors. 4. State-of-the-art Digital Booths with great features. 5. All our registered/non-registered users are applicable for Sponsorship. 6. One-to-many & group Chat features</p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>18 Nov 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

		 <div id="myModalEventsBoard1" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/events-elegance/Board-Meetings.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Board Meetings and Shareholder Meetings </h4>
							<p>A meeting of the Board of Directors (Board) of the Plus Circle was held at  Bangalore, Karnataka, India, beginning on October 15, 2019. Picture shows the Board meeting, the two Institutional Representatives, and invited guests who had attended the Coordination Group meeting held prior to the Board meeting. </p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>11 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

		 <div id="myModalEventsProduct" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/events-elegance/Product-launch-events.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Product Launch Events </h4>
							<p>One of the greatest secrets of a winning product launch is in building excitement and creating a demand for your product or service before it is readily available to the buying public.
							By staging a product launch event in which a select number of attendees gain information and access to your soon-to-be-released product or service, you help to fuel the feeding frenzy among their peers and contemporaries.</p>

							<p>A great product launch also stems from the efforts of a number of different stakeholders. Your Sales Team, Channel or Vendor Partners, Executive Staff, and Support Team should all be on-hand to help launch attendees and industry analysts understand the full scope of your new product.</p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>25 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

		 <div id="myModalEventsAppreciation" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/events-elegance/Appreciaion-Events.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Appreciation Events </h4>
							<p>Community helpers are all around us and they go the extra mile to keep our school clean, safe and running smoothly. To help teach our students to value them, the students of KG 1 made ‘Thank You’ cards during their art class and distributed them among the school support staff, gardeners, members of the Administrative Staff and the Transport Department. This was done as a token of appreciation and gratitude. This activity also instilled in children a sense of thankfulness towards everyone around them, thereby helping them in their overall personal growth and social development.</p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>8 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>


		 <div id="myModalEventsExecutive" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/events-elegance/Retreat-Incentive-Programs.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Executive Retreats and Incentive Programs </h4>
							<p>Business development and organizational planning are typically the topics of the agenda for these retreats, but equal weight is given to enjoyable activities as part of the original incentive and reward.</p>

							<p>This is where companies generally spend more money on a per-person basis to put together these events. Executive retreats and incentive trips typically last between three and five days, and the planning requires attention to the site selection, lodging, transportation, catering, business meetings, golfing and other activities.</p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>10 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>


	<div id="myModalEventsCultural" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/adoran/cultural-events.jpeg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Cultural Event</h4>
							<p>Offering an insight to the jaw-dropping traditions; warmth giving festivals; vivacious fairs; unheard rituals; and a large number of events, India is a travel destination unlike any other. Come and dive in the sea of big and small festivities ranging from cattle fair to religious festivals to the celebration of good harvest year or that of flourishing tourism. Learn about hundreds and thousands of fairs and festivals, events, rituals, and traditions in this section aspiring to offer a glimpse of the opulent and distinctive country called India.
							</p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>2 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>



		<div id="myModalEventsSports" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/adoran/Sports.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Sports </h4>
							<p>Plus Circle Badminton Tournament</p>
							<p>1. Tournament is only for corporate players.</p>
							<p>2. Yonex AS2 feather shuttles will be used for all the matches.</p>
							<p>3. Fixture will be created by the organizing team and once published will not be changed on individual request. Fixture will be based on the seeding given by the organizing team. To save time for everyone exact time for each match will be given and players are requested to reach the venue on time.Late arrival of players leads to automatic walkover for the opponent.</p>
							<p>4. Valid ID card of the company or HR letter must be produced on the day of the event.</p>
							<p>5. Player found not to be fit in the corporate category because of various reason, can be removed from the tournament at any point of time without giving any reason.

							</p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>5 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

		 <div id="myModalEventsDance" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/adoran/Dance.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Dance  </h4>
							<p>When the music is loud and you have a partner by your side, dancing to any kind of music is the best thing to do. Whether you suffer from Salsa mania or bitten by the Ballet bug, there is an event to enjoy happening at a place near you. To find all these fun-filled dance events and workshops happening in your city, check this space on Plus Circle. Lace-up your dancing shoes and book your tickets for an event or a workshop of your choice now!</p>
							<p><br>Organized by: <b>PCC</b> | Date: <b>8 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>



		 <div id="myModalEventsStreet" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/adoran/Street-entertainment.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Street Entertainment </h4>
							<p>One of the most popular street entertainment of Goa and one of the oldest. Situated in North Goa, in the Baga Beach area, which consists of a variety of clubs and restaurants. The Club is known for its large dance floor which seems to be straight out of a movie and an open-air restaurant which gives access to Baga Beach. It serves some unique cocktails and also good food which is a little expensive compared to other places in Goa.</p>

							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>17 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>






		  <div id="myModalEventsNight" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/adoran/Night-clubs.jpg')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Night </h4>
							<p>One of the best night clubs in Bangalore, We are known for vibrant, quirky ambience.  The long benches, round tables, soft lighting will surely give you fantasy feels. And if you’d like a little alfresco experience, then you can chill at the open seating space by the pool. When you’re dancing the night away at this candy-like night club, we suggest you take a pause and sip their signature cocktails, like the Candy Lolly, which we sure found really interesting.
							</p><p><br>Organized by: <b>PCC</b> | Date: <b>22 Nov 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>



		 <div id="myModalEventsGet" class="modal">
			<div class="modal-content width-50">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="list-event-activites1 brsw" >
						<div class="list-event-activites-img1">
							<img src="{{asset('public/images/adoran/get-together.png')}}">
						</div>
						<div class="list-event-activites-content1">
							<h4>Get Together   </h4>
							<p>
							There are more than 108,000 get-togethers happening across the country — from street parties and Big Lunches to BBQs, games of football and bake offs.</p>
							<p>Many of these events are run by individuals and their neighbours, so look out for stuff going on in your area (or organise your own event if you haven’t had an invite slip through your letterbox yet!). But there are also loads of exciting bigger events where all are welcome. </p>
							<p>After the Great Get Together we'll be working on building communities closer together

							</p>
							<p><br>Organized by: <b>The ShowMakers</b> | Date: <b>13 Dec 2020</b> | Location: <b>Banglore</b></p>
						</div>
					</div>
	    		</div>
		  	</div>
		 </div>

<!-- working code for backend -->
<div id="myModal" class="modal">
	<!-- Modal content -->
		<div class="modal-content">
		 
			<div class="modal-header">
				<h5 class="modal-title mt-10"  id="exampleModalLabel">New Member's Joining Kit</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" >				
					<div class="dashboard-content">
						<div class="dashboard-profile">
							<div class="row">
								<div class="col-md-8 plan pb-10">

									<div class="row kitcss1">
										<div class="col-md-1 width-10per">
											<div class="profile-name1" >
												<input type="radio"checked="checked" name="membership" class="floating-input inputc1"  value="750">
											</div>
										</div>
										<div class="col-md-11 width-90per">
											<div class="profile-name1" >
												<h2>Membership Fee 
													<font class="kitcss2"><del >Rs. 15000 </del>&nbsp;Rs. 0
													</font>
													<font class="kitcss3" >&nbsp;&nbsp;&nbsp;(*Offer valid till 31st Oct. 2020)
													</font><br>
													<font class="font-size13">I wish to refer & join 3 members to PLUS CIRCLE CLUB
													</font>
												</h2>
											</div>
										</div>
									</div>
									<div class="row p1-5">
										<div class="col-md-1 width-10per">	
											<div class="profile-name1 pt-29">
												<input type="radio" name="membership" class="floating-input inputc1"  value="15000">
											</div>
										</div>
										<div class="col-md-11 width-90per">
											<div class="profile-name1 feescs">
												<h2 >Membership Fee 
													<font class="kitcss2">Rs. 15000 </font><br>
													<font class="font-size13">I do not wish to refer any member to PLUS CIRCLE CLUB</font>
												</h2>
											</div>
										</div>
									</div>
									<div class="row p1-5">
											<div class="col-md-9 hrcss offerscs" >

												<div class="col-md-12 center2">
													
													<div class="profile-name1 p1-10" >
														<h2 class="h2css"  >MEMBER JOINING KIT</h2>
													</div>
												</div>
												<div class="row">

													<div class="col-md-4 center2">
														<p class="offerscss1 center1">T-SHIRT</p>
														<img src="{{ asset('public/images/sec-c/tshirt.jpg')}}" class="height-187" />

														<div class="col-md-12 pt-10 center2">
											
														<select name="t_shirtsize"  id="t_shirtsize"  class="shirtcs"  >
															<option value=''>Select Size</option>
															<option value="S" >S</option>
															<option value="M">M</option>
															<option value="L">L</option>
															<option value="XL">XL</option>
															<option value="XXL">XXL</option>
														</select>
														</div>

													</div>
													<div class="col-md-4 center2">	
														<p class="offerscss1">CAR STICKER</p>
														<img class="height-187" src="{{ asset('public/images/sec-c/car_sticker.jpg')}}">
													</div>
													<div class="col-md-4 center2">	
														<p class="offerscss1">IDENTITY CARD</p>
														<img class="height-187" src="{{ asset('public/images/sec-c/id_card.jpg')}}">	
													</div>
												</div>

												<!-- <div class="offerscss2"></div>
 -->

											</div>
											<div class="col-md-3 hrcss offerscs0">
												<div class="col-md-12 center1">

													
													<div class="profile-name1 p1-10" >
														<h2 class="h2css"  >&nbsp;</h2>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12 center2">	
														<p class="offerscss1">COMPLIMENTORY</p>

														<img class="height-187" src="{{ asset('public/images/sec-c/offer.jpg')}}">	
													</div>
												</div>

											<!-- 	<div class="offerscss3"></div> -->
											</div>

									</div>
								<!-- 	<div class="row p1-5">
										<div class="col-md-3 pt-10">
											<center>
												<select name="t_shirtsize"  id="t_shirtsize"  class="shirtcs"  >
													<option value=''>Select Size</option>
													<option value="S" >S</option>
													<option value="M">M</option>
													<option value="L">L</option>
													<option value="XL">XL</option>
													<option value="XXL">XXL</option>
												</select>
											</center>
										</div>
										<div class="col-md-9 pt1-5" >
											<div class="profile-name1" ><p> <span id="error_size" class="error_sizecs" >&#x2190; Please select the T-shirt size</span> </p>
											</div>
										</div>
									</div> -->
								</div>
								<div class="col-md-4 plan payment pb-10" id="Member750">	
									<div class="row pl-5" >
										<div class="col-md-8 width-65per">
											<p >Membership Fee    </p>
											<p>Membership Joining Kit  </p>
											<p class="mt-28" ><b> TOTAL </b></p>
										</div>
										<div class="col-md-4 pl-29 width-35per">
											<p > Rs. 0</p>
											<p> Rs. 785</p>
											<p class="mt-28"><b>Rs. 785</b></p>
										</div>
									</div>
									<div class="row p1-5">
										<div class="col-md-12 pt-38" >
											</div>
										<div class="col-md-12">	
											<div class="profile-name1" >
												<h2><b>Shipping Address : </b></h2>
											</div>
										</div>
									</div>
									<div class="row p1-5" >
										<div class="col-md-12">	</div>
										<div class="col-md-2 width-10per">
											<div class="profile-name1" >
											<input type="radio"  checked="checked"  name="address" value='default_address' class="floating-input inputc1" >
											</div>
										</div>
										<div class="col-md-10 width-90per">
											<div class="profile-name1" >
												<h2>Same as Registered Address</h2>
											</div>
										</div>
									</div>
									<div class="row p1-5" >
										<div class="col-md-12">	</div>
											<div class="col-md-2 width-10per">
											<div class="profile-name1" >
												<input type="radio" name="address" value='new_address' class="floating-input inputc1" >
											</div>
										</div>
										<div class="col-md-10 width-90per">	
											<div class="profile-name1" >
												<h2>
													<a  data-target="#myModelship_add" data-toggle="modal" href="#myModelship_add">Change Shipping Address</a>
												</h2>
											</div>
										</div>
									</div>
									<div class="row p1-5" >
										<div class="col-md-12 pt-30"></div>
										<div class="col-md-2 width-10per">
											<div class="profile-name1" >
											<input type="radio" name="termsConditions" class="floating-input inputc2" id="terms_condition" >
											</div>
										</div>
										<div class="col-md-10 width-90per">	
											<div class="profile-name1" >
												<h2 >I accept the 
													<a href="{{ route('terms_of_services') }}" class="accept">Terms & Conditions</a>
												</h2>
											</div>
										</div>
										<div class="col-md-12">	
											<!-- <p> 
												<span id="error_agree" class="error_agreecs" >Please accept the terms and conditions</span> 
											</p> -->
										</div>
										<div class="col-md-1">
											
										</div>
									</div>
									<div class="row paycss" >
										<div class="col-md-12">	
											<div class="submit-btn divsub rzp-button1" id='send'  >
												<input type="submit" name="send" class="next-step mt-40 divsub1"  value="PAY Rs. 785">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 plan payment pb-10" id="Member15000"  style="display:none;">	
									<div class="row pl-5">
										<div class="col-md-8 width-65per">
											<p >Membership Fee    </p>
											<p>Membership Joining Kit  </p>
											<p class="mt-28"><b>TOTAL </b></p>
										</div>
										<div class="col-md-4 pl-29 width-35per">
											<p> Rs.    15000 </p>
											<p> Rs. 0</p>
											<p class="mt-28"><b>Rs. 15000</b></p>
										</div>
									</div>
									<div class="row p1-5">
											<div class="col-md-12 pt-38">	</div>
										<div class="col-md-12">	
											<div class="profile-name1" >
												<h2><b>Shipping Address : </b></h2>
											</div>
										</div>
									</div>
									<div class="row p1-5" >
										<div class="col-md-12">	</div>
										<div class="col-md-2 width-10per">
											<div class="profile-name1" >
												<input type="radio" checked="checked" name="address1" class="floating-input inputc1" >
											</div>
										</div>
										<div class="col-md-10 width-90per">	
											<div class="profile-name1" >
												<h2>Same as Registered Address</h2>
											</div>
										</div>
									</div>
									<div class="row p1-5" >
										<div class="col-md-12">	</div>
										<div class="col-md-2 width-10per">
											<div class="profile-name1" >
												<input type="radio"   name="address1" class="floating-input inputc1" >
											</div>
										</div>
										<div class="col-md-10 width-90per">	
											<div class="profile-name1" >
												<h2>
													<a  data-target="#myModelship_add" data-toggle="modal" href="#myModelship_add">Change Shipping Address</a>
												</h2>
											</div>
										</div>
									</div>
									<div class="row p1-5" >
										<div class="col-md-12 pt-30"></div>
								
										<div class="col-md-2 width-10per">
											<div class="profile-name1" >
											<input type="radio"   name="money" class="floating-input inputc2" id="money" >
											</div>
										</div>
										<div class="col-md-9 width-90per">	
											<div class="profile-name1" >

											<h2 >I accept the <a href="{{ route('terms_of_services') }}"  class="accept">Terms & Conditions</a></h2>
											
											</div>
										</div>
										<div class="col-md-12">	
											<p> 
												<!-- <span id="error_agree" class="error_agreecs" >Please accept the terms and conditions</span>  -->
											</p>
										</div>
										<div class="col-md-1"></div>
									</div>
									<div class="row paycss">
										<div class="col-md-12">	
											<div class="submit-btn divsub rzp-button1" id='money1' >
												<input type="button"name="money1"     class="next-step mt-40 divsub1" value="PAY Rs. 15000">
											</div>											
										</div>
									</div>
								</div>
								<div class="col-md-12 p1-5 pb-0">	
									<div class="profile-name1 offercss">
										<!-- <p>	<font class="font-size13">*Offer date is subject to.......</font> </p> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>



		<div id="myModalfixedgovernor" class="modal">
	        <div class="modal-content">
	            <div class="modal-header">
	               <center>
	                  <h5 class="modal-title" style=" display: block; 
	                     display:block" id="exampleModalLabel"></h5>
	               </center>
	               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	               <span aria-hidden="true">&times;</span>
	               </button>
	            </div>
	            <div class="modal-body">
	               <div class="col-md-12"style="">
	                     <img src="{{asset('public/images/hirarchy/heirarchy_DG.jpg')}}">
	               </div>
	            </div>
	        </div>
	     </div>


	     <div id="myModalfixedchairman" class="modal">
	        <div class="modal-content">
	            <div class="modal-header">
	               <center>
	                  <h5 class="modal-title" style=" display: block; 
	                     display:block" id="exampleModalLabel"></h5>
	               </center>
	               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	               <span aria-hidden="true">&times;</span>
	               </button>
	            </div>
	            <div class="modal-body">
	               <div class="col-md-12"style="">
	                     <img src="{{asset('public/images/hirarchy/heirarchy_DC.jpg')}}">
	               </div>
	            </div>
	        </div>
	     </div>
	
		 <div id="myModalDistrictSecretary" class="modal">
	        <div class="modal-content">
	            <div class="modal-header">
	               <center>
	                  <h5 class="modal-title" style=" display: block; 
	                     display:block" id="exampleModalLabel"></h5>
	               </center>
	               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	               <span aria-hidden="true">&times;</span>
	               </button>
	            </div>
	            <div class="modal-body">
	               <div class="col-md-12"style="">
	                     <img src="{{asset('public/images/hirarchy/heirarchy_DSc.jpg')}}">
	               </div>
	            </div>
	        </div>
	     </div>

		 <div id="myModalDistrictTreasurer" class="modal">
	        <div class="modal-content">
	            <div class="modal-header">
	               <center>
	                  <h5 class="modal-title" style=" display: block; 
	                     display:block" id="exampleModalLabel"></h5>
	               </center>
	               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	               <span aria-hidden="true">&times;</span>
	               </button>
	            </div>
	            <div class="modal-body">
	               <div class="col-md-12"style="">
	                     <img src="{{asset('public/images/hirarchy/heirarchy_DT.jpg')}}">
	               </div>
	            </div>
	        </div>
	     </div>

		 <div id="myModalDistrictZonalChairman" class="modal">
	        <div class="modal-content">
	            <div class="modal-header">
	               <center>
	                  <h5 class="modal-title" style=" display: block; 
	                     display:block" id="exampleModalLabel"></h5>
	               </center>
	               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	               <span aria-hidden="true">&times;</span>
	               </button>
	            </div>
	            <div class="modal-body">
	               <div class="col-md-12"style="">
	                     <img src="{{asset('public/images/hirarchy/heirarchy_DZC.jpg')}}">
	               </div>
	            </div>
	        </div>
	     </div>

		 <div id="myModalfixedallstates" class="modal">
	        <div class="modal-content">
	            <div class="modal-header">
	               <center>
	                  <h5 class="modal-title" style=" display: block; 
	                     display:block" id="exampleModalLabel"></h5>
	               </center>
	               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	               <span aria-hidden="true">&times;</span>
	               </button>
	            </div>
	            <div class="modal-body">
	               <div class="col-md-12 text-center"style="">
	                     <img src="{{asset('public/images/comittee_popup.jpg')}}">
	               </div>
	            </div>
	        </div>
	     </div>


	<!---   Road ahead menu poup start      -->
	<div id="myModalRoadAhead" class="modal">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="col-md-12 center1">
					<img src="{{ asset('public/images/sec-e/road_ahead.jpg')}}">
					<div clsas="row">

						<div class="col-md-12 mt-28">
							<div class="my-contribution-main">
								<div class="dashboard-table members-joined-table1">
									<table id="roadtablecss">
										<thead>
											<tr>
												<th colspan="3"></th>
												<th colspan="2">Budget per Year </th>
											</tr>
											<tr>
												<th class="width-70"><b>Year</b></th>
												<th class="width-290" >We intend to support</th>
												<th class="width-75">Count </th>
												<th class="width-100">INR in Crores </th>
												<th class="width-100">US$ in Million</th>
											</tr>
										</thead>

										<tbody>
											<tr>
												<td rowspan="10"><p >2021</p></td>
												<td><p class="tdp1">Children's (Orphans)</p> </td>
												<td><p>500</p></td>
												<td ><p>2.74</p></td>
												<td ><p>0.38</p></td>
											</tr>

											<tr>
												<td><p class="tdp1" >Old Age</p></td>
												<td><p>1500</p></td>
												<td><p>6.84</p></td>
												<td><p>0.95 </p></td>
											</tr>

											<tr>
												<td colspan="2"><p><b>Budget ( for social activities )</p></b></td>
												<td><p><b>9.58</b></p></td>
												<td><p><b>1.33</b></p></td>
											</tr>

											<tr class="tr1"><td colspan="4" ></td></tr>

											<tr>
												<td colspan=""><p class="tdp1">Purchase the land and construct for old age & orphaned  </p></td>
												<td></td>
												<td><p>15.00 </p></td>
												<td><p>2.08 </p></td>
											</tr>

											<tr>
												<td colspan=""><p><b>Total Budget </b></p></td>
												<td></td>
												<td><p><b>24.58 </b></p></td>
												<td><p><b>3.41</b></p></td>
											</tr>

											<tr>
												<td colspan=""><p class="tdp1">Incorporation with leading hotels </p></td>
												<td colspan=""><p>100 </p></td>
												<td></td>
												<td></td>
											</tr>


											<tr>
												<td colspan=""><p class="tdp1">Incorporation with leading clubs </p></td>
												<td colspan=""><p>20 </p></td>
												<td></td>
												<td></td>

											</tr>

											<tr>
												<td><p class="tdp1">Incorporation with leading entertainment centers</p></td>
												<td><p>20 </p></td>
												<td></td>
												<td></td>
											</tr>

											<tr>
												<td colspan=""><p class="tdp1">Incorporation with 100 leading hyper markets </p></td>
												<td colspan=""><p>100 </p></td>
												<td></td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="col-md-12 mt-28">
							<div class="my-contribution-main">
								<div class="dashboard-table members-joined-table1">
									<table id="roadtablecss1">
										<thead>
											<tr>
												<th colspan="3"></th>
												<th colspan="2">Budget per Year </th>
											</tr>

											<tr>
												<th class="width-70"><b>Year</b></th>
												<th class="width-290" >We intend to support</th>
												<th class="width-75">Count </th>
												<th class="width-100">INR in Crores </th>
												<th class="width-100">US$ in Million</th>
											</tr>

										</thead>

										<tbody>
											<tr>
												<td rowspan="11"><p>2022</p>
												</td>
												<td><p  class="tdp1">Children's (Orphans)</p> </td>
												<td><p>2500</p></td>
												<td ><p>13.67</p></td>
												<td ><p>1.90</p></td>
											</tr>

											<tr>
												<td><p  class="tdp1">Old Age</p></td>
												<td><p>2500</p></td>
												<td><p>11.41</p></td>
												<td><p>1.90 </p></td>
											</tr>

											<tr>
												<td colspan="2"><p><b>Budget ( for social activities )</b></p></td>
												<td><p><b>25.08</b></p></td>
												<td><p><b>3.48</b></p></td>
											</tr>

											<tr class="tr1"><td colspan="4" ></td></tr>

											<tr>
												<td colspan=""><p  class="tdp1">Purchase the land and construct for old age & orphaned</p></td>
												<td></td>
												<td><p>15 </p></td>
												<td><p>2.08 </p></td>
											</tr>

											<tr>
												<td colspan=""><p  class="tdp1">Purchase the land and construct a resorts</p></td>
												<td></td>
												<td><p>10 </p></td>
												<td><p>1.39 </p></td>
											</tr>

											<tr>
												<td colspan=""><p><b>Total Budget</b> </p></td>
												<td></td>
												<td><p><b>50.08 </b></p></td>
												<td><p><b>6.96</b></p></td>
											</tr>

											<tr>
												<td colspan=""><p  class="tdp1">Incorporation with leading hotels </p></td>
												<td colspan=""><p>500 </p></td>
												<td></td>
												<td></td>
											</tr>


											<tr>
												<td><p  class="tdp1">Incorporation with leading clubs </p></td>
												<td><p>50 </p></td>
												<td></td>
												<td></td>

											</tr>
											<tr>
												<td><p  class="tdp1">Incorporation with leading entertainment centers</p></td>
												<td><p>100 </p></td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<td ><p  class="tdp1" > Incorporation with leading hyper markets </p></td>
												<td colspan=""><p>500 </p></td>
												<td></td>
												<td></td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>



						<div class="col-md-12 mt-28">
							<div class="my-contribution-main">
								<div class="dashboard-table members-joined-table1">
									<table id="roadtablecss">
										<thead>
											<tr>
											
												<th colspan="3"></th>
												
												<th colspan="2">Budget per Year </th>
											</tr>

											<tr>
												<th class="width-70"><b>Year</b></th>
												<th class="width-290" >We intend to support</th>
												<th class="width-75">Count </th>
												<th class="width-100">INR in Crores </th>
												<th class="width-100">US$ in Million</th>
											</tr>

										</thead>
										<tbody>
											<tr>
												<td rowspan="12"><p>2023</p>
												</td>
												<td><p  class="tdp1">Children's (Orphans)</p> </td>
												<td><p>20000</p></td>
												<td ><p>109</p></td>
												<td ><p>15.14</p></td>
												   


											</tr>
											<tr>
												<td><p  class="tdp1">Old Age</p></td>
												<td><p>15000</p></td>
												<td><p>68.4</p></td>
												<td><p>9.50 </p></td>
											</tr>
											<tr>
												<td colspan=""><p><b>Total Budget</b></p></td>
												<td></td>
												<td><p><b>177.4</b></p></td>
												<td><p><b>26.64</b></p></td>

											</tr>

											<tr class="tr1"><td colspan="4" ></td></tr>
											<tr>
												<td colspan=""><p  class="tdp1">Purchase the land and construct for old age & orphaned </p></td>
												<td></td>
												<td><p>50 </p></td>
												<td><p>26.94 </p></td>
											</tr>

											<tr>
												<td colspan=""><p  class="tdp1">Purchase the land and construct a resorts </p></td>
												<td></td>
												<td><p>100 </p></td>
												<td><p>13.89 </p></td>
											</tr>

											<tr>
												<td colspan=""><p><b>Total Budget </b></p></td>
												<td></td>
												<td><p><b>327.40</b></p></td>
												<td><p><b>45.47</b></p></td>
											</tr>

											<tr>
												<td colspan=""><p  class="tdp1">Incorporation with leading hotels </p></td>
												<td colspan=""><p>3000 </p></td>
												<td></td>
												<td></td>
											</tr>


											<tr>
												<td colspan=""><p  class="tdp1">Incorporation with leading clubs </p></td>
												<td colspan=""><p>500 </p></td>
												<td></td>
												<td></td>

											</tr>
											<tr>
												<td><p  class="tdp1">Incorporation with leading entertainment centers</p></td>
												<td><p>200 </p></td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<td colspan=""><p  class="tdp1">Incorporation with leading hyper markets</p></td>
												<td colspan=""><p>1000 </p></td>
												<td></td>
												<td></td>
											</tr>

												<tr>
												<td colspan=""><p  class="tdp1"> Propose for NBFC</p></td>
												<td colspan=""></td>
												<td></td>
												<td></td>
											</tr>
											
											
										</tbody>
									</table>
								</div>
							</div>
						</div>



						<div class="col-md-12 mt-28">
							<div class="my-contribution-main">
								<div class="dashboard-table members-joined-table1">
									<table id="roadtablecss1">
										<thead>
											<tr>
											
												<th colspan="3"></th>
												
												<th colspan="2">Budget per Year </th>
											</tr>

											<tr>
												<th class="width-70"><b>Year</b></th>
												<th class="width-290" >We intend to support</th>
												<th class="width-75">Count </th>
												<th class="width-100">INR in Crores </th>
												<th class="width-100">US$ in Million</th>
											</tr>

										</thead>
										<tbody>
											<tr>
												<td rowspan="3"><p>2024</p>
												</td>
												<td><p  class="tdp1">Children's (Orphans)</p> </td>
												<td><p>100000</p></td>
												<td ><p>548</p></td>
												<td ><p>76.11</p></td>
												   


											</tr>
											<tr>
												<td><p  class="tdp1">Old Age</p></td>
												<td><p>30000</p></td>
												<td><p>137</p></td>
												<td><p>19.03</p></td>
											</tr>
											<tr>
												<td colspan="2"><p ><b>Total Budget</b></p></td>
												<td><p><b>685.00</b></p></td>
												<td><p><b>95.14</b></p></td>

											</tr>
											
											
										</tbody>
									</table>
								</div>
							</div>
						</div>


						<div class="col-md-12 mt-28">
							<div class="my-contribution-main">
								<div class="dashboard-table members-joined-table1">
									<table id="roadtablecss">
										<thead>
											<tr>
											
												<th colspan="3"></th>
												
												<th colspan="2">Budget per Year </th>
											</tr>

											<tr>
												<th class="width-70"><b>Year</b></th>
												<th class="width-290" >We intend to support</th>
												<th class="width-75">Count </th>
												<th class="width-100">INR in Crores </th>
												<th class="width-100">US$ in Million</th>
											</tr>

										</thead>
										<tbody>
											<tr>
												<td rowspan="3"><p>2025</p>
												</td>
												<td><p class="tdp1">Children's (Orphans)</p> </td>
												<td><p>500000</p></td>
												<td ><p>2,738</p></td>
												<td ><p>380.28</p></td>
												   


											</tr>
											<tr>
												<td><p  class="tdp1">Old Age</p></td>
												<td><p>150000</p></td>
												<td><p>684</p></td>
												<td><p>95.00</p></td>
											</tr>
											<tr>
												<td colspan="2"><p><b>Total Budget</b></p></td>
												<td><p><b>3,422.00</b></p></td>
												<td><p><b>475.28</b></p></td>

											</tr>
											
											
										</tbody>
									</table>
								</div>
							</div>
						</div>




						<div class="col-md-12 mt-28">
							<div class="my-contribution-main">
								<div class="dashboard-table members-joined-table1">
									<table id="roadtablecss1">
										<thead>
											<tr>
											
												<th colspan="3"></th>
												
												<th colspan="2">Budget per Year </th>
											</tr>
											<tr>
												<th class="width-70"><b>Year</b></th>
												<th class="width-290" >We intend to support</th>
												<th class="width-75">Count </th>
												<th class="width-100">INR in Crores </th>
												<th class="width-100">US$ in Million</th>
											</tr>

										</thead>
										<tbody>
											<tr>
												<td rowspan="3"><p>2025</p>
												</td>
												<td><p  class="tdp1">Children's (Orphans)</p> </td>
												<td><p>1000000</p></td>
												<td ><p>5,475</p></td>
												<td ><p>760.42</p></td>
												   


											</tr>
											<tr>
												<td><p  class="tdp1">Old Age</p></td>
												<td><p>300000</p></td>
												<td><p>1,369</p></td>
												<td><p>190.14</p></td>
											</tr>
											<tr>
												<td colspan="2"><p><b>Total Budget</b></p></td>
												<td><p><b>6,844.00</b></p></td>
												<td><p><b>950.56</b></p></td>

											</tr>
											
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="myModalvideo" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">VIDEO</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5> This section will be active on </h5><h5><font style="text-decoration: underline;">25th October 2020</font> </h5>
	    		</div>
	    		
	    	</div>
		</div>
	</div>

	<div id="mydashboardModalvideo" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">VIDEO</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5> This section will be active on </h5><h5><font style="text-decoration: underline;">21th October 2020</font> </h5>
	    		</div>
	    		
	    	</div>
		</div>
	</div>

	<div id="mydashboardsharingModalvideo" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">SHARING</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5> Sharing Will be active in 4 days after registration. </h5>
	    		</div>
	    		
	    	</div>
		</div>
	</div>

		<div id="returnpolicy" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">RETURN & REFUND POLICY</h5>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5> This section will be active on </h5><h5><font style="text-decoration: underline;">25th October 2020</font> </h5>
	    		</div>
	    		
	    	</div>
		</div>
	</div>



	<div id="contactpopup" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<h4 class="modal-title">THANK YOU !</h4>
					<h5>Thank you for contacting PLUS CIRCLE CLUB.<br>&nbsp;</h5>
					<h5 class="contactcsspopup1">Your query has been successfully submitted.</h5>
	    		</div>
	    		
	    	</div>
		</div>
	</div>



	<div id="myModalGallery" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">GALLERY</h5>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5> This section will be active on </h5><h5><font style="text-decoration: underline;">25th October 2020</font> </h5>
	    		</div>
	    		
	    	</div>
		</div>
	</div>


	<div id="myModalPressRellease" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">PRESS</h5>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5> This section will be active on </h5><h5><font style="text-decoration: underline;">25th October 2020</font> </h5>
	    		</div>
	    		
	    	</div>
		</div>
	</div>







	<div id="myModalMembersSstory" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">MEMBER STORY</h5>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5> This section will be active on </h5><h5><font style="text-decoration: underline;">25th October 2020</font> </h5>
	    		</div>
	    		
	    	</div>
		</div>
	</div>



	<div id="blog" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">BLOG</h5>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5> This section will be active on </h5><h5><font style="text-decoration: underline;">25th October 2020</font> </h5>
	    		</div>
	    		
	    	</div>
		</div>
	</div>



	<div id="career" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">CAREER</h5>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5> This section will be Functional on </h5><h5 ><font style="text-decoration: underline;">26th October 2020</font> </h5><p class="mb-40"></p>
	    		</div>
	    		
	    	</div>
		</div>
	</div>


	

	


	<div id="sucess" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">Sucees</h5>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">Successfully added
				<div class="col-md-12 mt-28">
					<h5></h5><p class="mb-40"></p>
	    		</div>
	    		
	    	</div>
		</div>
	</div>



	<div id="ship_address" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">Shipping address</h5>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5>Shipping address successfully added (sender is saved success)</h5><p class="mb-40"></p>
	    		</div>
	    		
	    	</div>
		</div>
	</div>


	<div id="terms" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">Terms & Conditions</h5>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5>Please accept the terms and conditions</h5><p class="mb-40"></p>
	    		</div>
	    		
	    	</div>
		</div>
	</div>

	<div id="tshirtalertmsg" class="modal">
		<div class="modal-content contactcsspopup center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">T-shirt </h5>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<div class="col-md-12 mt-28">
					<h5>Select the size of t-shirt</h5><p class="mb-40"></p>
	    		</div>
	    		
	    	</div>
		</div>
	</div>



	<div id="question_mark" class="modal" >
		<div class="modal-content questionmark1 center">
		 	<div class="modal-header">
		 	
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<img src="{{asset('public/images/question_mark1.jpg')}}"></a>
	    		
	    	</div>
		</div>
	</div>



	<div id="comment" class="modal" >
		<div class="modal-content questionmark2 center">
		 	<div class="modal-header">
		 	
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<a href="{{ route('user_login') }}"><img src="{{asset('public/images/comment1.png')}}"></a>
	    	</div>
		</div>
	</div>

	<div id="milestone" class="modal">
		<div class="modal-content questionmark1 center" >
		 	<div class="modal-header center">
		 	
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<a data-target="#myModalRoadAhead" id="road"data-toggle="modal" href="#myModalRoadAhead" >
					<img src="{{asset('public/images/milestone1.jpg')}}">
				</a>
	    		
	    	</div>
		</div>
	</div>



	<div id="associate" class="modal">
		<div class="modal-content associate center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">ASSOCIATE</h5>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<!-- <p  class="alert-danger1">&#x26A0; Your password is incorrect or this account does not exit. Please enter valid password or mail us at info@pluscircleclub.org</p> -->
				<form id="#" action="#" >
					<div class="floating-form" >
						<div class="row">
							<div class="col-md-12">
								<div class="floating-label"> 
									<p>Enter your Login ID  <span class="alert-danger1">*</span></p>
									<input class="floating-input2" type="text" placeholder="" required>
									<span class="highlight"></span>
								</div>
							</div>

							<div class="col-md-12">
								<div class="floating-label"> 
									<p>Your Password  <span class="alert-danger1">*</span></p>
									<input class="floating-input2" type="password" placeholder="" required>
									<span class="highlight"></span>
								</div>
							</div>

							<div class="col-md-12">
								<div class="submit-btn">
									<input type="submit" value="Login" name="">
								</div>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div id="alliance" class="modal">
		<div class="modal-content associate center">
		 	<div class="modal-header contactcsspopupl">
		 		<h5 class="modal-title">ALLIANCE PARTNER</h5>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>


			</div>
			<div class="modal-body">
				<!-- <p  class="alert-danger1">&#x26A0; Your password is incorrect or this account does not exit. Please enter valid password or mail us at info@pluscircleclub.org</p> -->
				<form id="#" action="#" >
					<div class="floating-form" >
						<div class="row">
							<div class="col-md-12">
								<div class="floating-label"> 
									<p>Enter your Login ID <span class="alert-danger1">*</span></p>
									<input class="floating-input2" type="text" placeholder="" required>
									<span class="highlight"></span>
								</div>
							</div>

							<div class="col-md-12">
								<div class="floating-label"> 
									<p>Your Password <span class="alert-danger1">*</span></p>
									<input class="floating-input2" type="password" placeholder="" required>
									<span class="highlight"></span>
								</div>
							</div>

							<div class="col-md-12">
								<div class="submit-btn">
									<input type="submit" value="Login" name="">
								</div>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>



<!DOCTYPE html>
<html>
    
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="{{ asset('public/admin/assets/images/favicon.ico')}}">

        <!-- App title -->
        <!-- <title>All Members</title> -->

        <!-- DataTables -->
        <link href="{{ asset('public/admin/assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/plugins/datatables/fixedHeader.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/plugins/datatables/scroller.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
        <link href="{{ asset('public/admin/css/adminStyle.css')}}" rel="stylesheet" />

        <!-- App CSS -->
        <link href="{{ asset('public/admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/css/core.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/css/components.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/css/pages.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/css/menu.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />
        

        <script src="{{ asset('public/admin/assets/js/modernizr.min.js')}}"></script>

    </head>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Plus Circle Dashboard') }}</title>
        <!-- Favicon -->
        <link href="{{ asset('argon') }}/img/brand/favicon.png" rel="icon" type="image/png">
        <link rel="stylesheet" type="text/css" href="{{ asset('argon') }}/css/frontend/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('argon') }}/fonts/css/all.css">
        <link rel='stylesheet' type="text/css" href="{{ asset('argon') }}/css/frontend/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="{{ asset('argon') }}/css/frontend/style.css">
       
    </head>
    <body>
        @if(!empty(auth()->user()->name))
            <form id="member-logout-form" action="{{ route('member.logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @endif
        <header class="header-main">
            <div class="container-fluid">
                <div class="header-inner">
                    <div class="logo">
                        @if(!empty(auth()->user()->name))
                        <a href="{{url('member/dashboard')}}"><img src="{{ asset('argon') }}/img/brand/logo.png"></a>
                         @else 
                            <a href="{{url('/')}}"><img src="{{ asset('argon') }}/img/brand/logo.png"></a>
                        @endif
                    </div>
                    @include('layouts.navbars.frontend_navbar')            
                </div>
            </div>
        </header>
        @yield('content')
        
        @include('layouts.footers.frontend')
        <!-- <script src="{{ asset('argon') }}/js/frontend/jquery-3.2.1.slim.min.js"></script> -->
        <script src="{{ asset('argon') }}/vendor/jquery/dist/jquery.min.js"></script>
        <script src="{{ asset('argon') }}/js/frontend/popper.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="{{ asset('argon') }}/js/frontend/owl.carousel.min.js"></script>
        <script src="{{ asset('argon') }}/js/frontend/custom.js"></script>
        @stack('js')
    </body>
</html>
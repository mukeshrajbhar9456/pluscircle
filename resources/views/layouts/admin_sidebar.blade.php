<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="{{asset('public/admin/assets/images/users/avatar-1.jpg')}}" alt="user-img" title="ADMIN" class="img-circle img-thumbnail img-responsive">
                            <div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
                        </div>
                        <h5><a href="#">ADMIN</a> </h5>
                        <ul class="list-inline">
                            <li>
                                <a href="#" Title='Admin'>
                                    <i class="zmdi zmdi-account-circle"></i>
                                </a>
                            </li>
							<li>
                                <a href="#" Title='Hierarchy' >
                                    <i class="zmdi zmdi-sort-amount-desc"></i>
                                </a>
                            </li>
							<li>
                                <a href="resetpassword.html" Title='Settings' >
                                    <i class="zmdi zmdi-settings"></i>
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_logout') }}" Title='Logout' class="text-custom">
                                    <i class="zmdi zmdi-power"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>

                            <li><a href="dashboard.html" class="waves-effect active"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a>
                            </li>

                            <li><a href="{{route('admin.admin.allmembers')}}" class="waves-effect"><i class="zmdi zmdi-home"></i> <span> Home Page  </span> </a>
                            </li>

                            <li><a href="#" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span>Roles & Permissions </span> </a>
                            </li>

                            
							
							<li><a href="{{ route('admin.sa') }}" class="waves-effect"><i class="zmdi zmdi-card-membership"></i> <span>Members</span> </a></li>
                            <li><a href="{{ route('sccm.application') }}" class="waves-effect"><i class="zmdi zmdi-card-membership"></i> <span>Applications</span> </a></li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-money"></i><span> Payment </span> <span class="menu-arrow"></span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="rec_payment.html" class="waves-effect">Received</a></li>
                                    <li class="has_sub"><a href="javascript:void(0);" class="waves-effect">To Pay <span class="menu-arrow"></span> </a>
										<ul class="list-unstyled">
											<li><a href="#">Ledger / Statement of Account  </a></li>
										</ul>
									</li>
                                    <li class="has_sub"><a href="javascript:void(0);" class="waves-effect">Paid<span class="menu-arrow"></span> </a>
										<ul class="list-unstyled">
											<li><a href="#">Ledger / Statement of Account  </a></li>
										</ul>
									</li>
                                    <li><a href="#" class="waves-effect">BRS</a></li>
                                </ul>
                            </li>


                            <li class="has_sub"> <a href="#" class="waves-effect"><i class="fa fa-medkit"></i><span> PCC Kit </span></a>
                            </li>

                            <li> <a href="#" class="waves-effect"><i class="zmdi zmdi-calendar"></i><span> Events </span></a>
                            </li>

                            <li> <a href="#" class="waves-effect"><i class="zmdi zmdi-blogger"></i><span> Blogs </span></a>
                            </li>

                            <li> <a href="#" class="waves-effect"><i class="zmdi zmdi-email-open"></i><span> News Letter </span></a>
                            </li>

                            <li><a href="#" class="waves-effect"><i class="zmdi zmdi-filter-center-focus"></i><span> Master Filter </span></a>
                            </li>

                            <li><a href="#" class="waves-effect"><i class="zmdi zmdi-help"></i><span> Volunteer </span></a>
                            </li>

                            <li><a href="#" class="waves-effect"><i class="zmdi zmdi-account-box-o"></i><span> Vendors </span></a>
                            </li>

                            <li><a href="#" class="waves-effect"><i class="fa fa-birthday-cake"></i><span> Birthdays </span></a>
                            </li>

                            <li> <a href="#" class="waves-effect"><i class="zmdi zmdi-flash-auto"></i><span> Special Approvals </span></a>
                            </li>

                            <li> <a href="#" class="waves-effect"><i class="zmdi zmdi-email"></i><span> Mail Box </span></a>
                            </li>

                            <li> <a href="#" class="waves-effect"><i class="zmdi zmdi-quote"></i><span> Testimonials </span></a>
                            </li>

                            <li><a href="#" class="waves-effect"><i class="zmdi zmdi-cloud-box"></i><span> Backup & Exports </span></a>
                            </li>

                            <li><a href="#" class="waves-effect"><i class="zmdi zmdi-badge-check"></i><span> Rewards and Redeem </span></a>
                            </li>

                            <li><a href="#" class="waves-effect"><i class="zmdi zmdi-flag"></i><span> Abuse Report </span></a>
                            </li>

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->

@extends('layouts.lp')
	@section('content')
		<section class="event-activites-section">
			<div class="container">
				<div class="about-us-title">
					<p><a href="{{ url('/') }}">Home</a> / Your Benefits </p><br>
					<h4 class="ybh">Your Benefits</h4>
					
					<hr>
				</div>
				<div class="row">
					


					<div class="col-md-6">
						<div class="list-event-activites1 videocss2">
							<div class="list-event-activites-content1">
								<video controls class="videocss1">
									<source src="{{ asset('public/images/videos/Your_Benefits.mp4') }}"  type="video/mp4">
								</video>
							
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<p>&nbsp;<br>Plus Circle provides various opportunities to our Plus Circle
						members.</p><p> It could be earning, socializing, or participating in different
						 types of professional or formal events and activities, where you can 
						 showcase your talent or simply grab the knowledge from it. 
						</p>
					</div>

					<div class="col-md-12 mb-38" ></div>


					<div class="col-md-4 w1">
						<img src="{{asset('public/images/sec-c/activity1.jpg')}}">
					</div>

					<div class="col-md-4 w1">
						<img src="{{asset('public/images/sec-c/activity2.jpg')}}">
					</div>


					<div class="col-md-4 w1">
						<img src="{{asset('public/images/sec-c/activity3.jpg')}}">
					</div>
					<div class="col-md-12 mb-38" ></div>


					<div class="col-md-4 w1">
						<img src="{{asset('public/images/sec-c/activity4.jpg')}}">
					</div>


					<div class="col-md-4 w1">
						<img src="{{asset('public/images/sec-c/activity5.jpg')}}">
					</div>


					<div class="col-md-4 w1">
						<img src="{{asset('public/images/sec-c/activity6.jpg')}}">
					</div>


						
					<div class="col-md-12 mb-38" ></div>


					<div class="col-md-3 w1">
						<a href="{{ route('Social_Networking') }}">
							<img src="{{asset('public/images/sec-c/networking.jpg')}}" >
						</a>
					</div>



					<div class="col-md-3 w1">
						<a href="{{ route('Business_Opportunity') }}">
							<img src="{{asset('public/images/sec-c/business_opp.jpg')}}" >
						</a>
					</div>

					<div class="col-md-3 supportcss w1" >
						<a href="{{ route('Supporting_Hand') }}">
							<img src="{{asset('public/images/sec-c/supporting_hand.jpg')}}" class="right">
						</a>
					</div>



				


					<div class="col-md-3 w1" >
						<a href="{{ route('Scholarships_Programs') }}">
							<img src="{{asset('public/images/sec-c/scholarship.jpg')}}" class="right">
						</a>
					</div>


					<div class="col-md-12 mb-38" ></div>

					<div class="col-md-1">
						
					</div>


					<div class="col-md-3 w1">
						<a href="{{ route('Emergency_Support') }}">
							<img src="{{asset('public/images/sec-c/emergency.jpg')}}">
						</a>
					</div>


					<div class="col-md-3 ml-25 w1" >
							<a href="{{ route('Opportunities_professionals') }}">
							<img src="{{asset('public/images/sec-c/professional_op.jpg')}}">
						</a>
					</div>

				

					<div class="col-md-3 ml-40 w1">
						<a href="{{ route('Health_Insurance') }}">
							<img src="{{asset('public/images/sec-c/health_insurance.jpg')}}"class="right" >
						</a>
					</div>

						<div class="col-md-12 mb-38" ></div>


				</div>
			</div>
		</section>
		
		@endsection
@extends('layouts.lp')
	@section('content')	

		<section class="contact-us-main" style="background: url('{{asset('public/images/sec-c/contactbg.jpg')}}') no-repeat center;margin-bottom:6px; background-size: cover;background-size: 100% 100%;" >
			<div class="container">
			
				<div class="row " >
					<div class="col-md-3">
						<div class="contact-us-form">
							<div class="email-id">
								<h4>Contact Us</h4>
								<p>#31, 1st Main Road, 2nd Cross, Vidya Nagar, T Dasarhalli,
								Bangalore -560064, India</p>
								<ul>
									<li><a href="mailto:info@pluscircleclub.org">info@pluscircle.com</a><br><a href="mailto:press@pluscircleclub.org">press@pluscircleclub.org</a></li>
								
									<li><a href="mailto:ho@pluscircleclub.org">INDIA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ho@pluscircleclub.org</a></li>
									<li><a href="mailto:eu@pluscircleclub.org">EUROPE : eu@pluscircleclub.org</a></li>
									<li><a href="mailto:uae@pluscircleclub.org">UAE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: uae@pluscircleclub.org</a></li>
									
								</ul>
							</div>
						</div>
					</div>

					<!-- <div class="col-md-3"></div> -->

					<div class="col-md-4 ">
						<div class="contact-us-form ml-30">
							<h4>Get in touch with us</h4>
							<div class="login-form">
								<form id="contactsubmit" name="contactsubmit">
								{{ csrf_field() }}
									<div class="floating-form">
									    <div class="row">
									    	<div class="col-md-12">
										    	<div class="floating-label1">      
											      	<input class="floating-inputcontact @error('cname') is-invalid @enderror" type="text" name="cname" placeholder="Enter name " required>
													  <span class="highlight"></span>
													  <label id="name-error" class="error" for="name"></label>
											      
											    </div>
											</div>
											<div class="col-md-6"></div>
									    	<div class="col-md-12">
												<div class="floating-label1">      
											    	<input class="floating-inputcontact @error('cemail') is-invalid @enderror" type="text" name="cemail" placeholder="Enter email " required>
													<span class="highlight"></span>
													<label id="email-error" class="error" for="email"></label>
												
											    	
											    </div>
											</div>
											<div class="col-md-12">
												<div class="floating-label1">      
											    	<input class="floating-inputcontact @error('cmobile') is-invalid @enderror" type="text" name="cmobile" required placeholder="Enter number (Including country code) ">
													<span class="highlight"></span>
													<label id="phone-error" class="error" for="phone"></label>
											    
											    </div>
											</div>
										</div>
									    <div class="floating-label1"> 
									    	<textarea class="floating-inputcontact" name="cmessage" placeholder="Enter your message here "></textarea>     
									    	<span class="highlight"></span>
									    
									    </div>
									</div>
									<div class="submit-btn">
										<input type="reset"class="reset" value="reset">
										<input type="submit" value="Submit" name="" id="contactFormBtn" >
										<div id='loader' class='text-center' style="display:none;">
										<img class="gifplayer" src="{{ asset('public/images/sample.png')}}" style="width: 10%;" data-mode="video"
										data-mp4="{{ asset('public/images/ajax-loader.gif') }}" />
										</div>
									</div>
								</form>
							</div>
						</div>
						
					</div>
					
				</div>
			</div>
		</section>
		
@endsection
		
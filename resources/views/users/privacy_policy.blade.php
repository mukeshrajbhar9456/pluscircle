@extends('layouts.lp')
	@section('content')	

		<section class="cms-page-main">
			<div class="container mb-28">
				<div class="about-us-title">
					<h4 class="ybh">Privacy Policy</h4>
					<hr>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="cms-content">
						
							<p>Plus Circle required to check this page frequently to ensure that you are aware of the changes.Plus Circle reserves the right to make any modification or withdraw any feature of its website at any point of time.</p>
							


							<h5 class="mt-28">What information do we collect?</h5>
							<p>We collect information from you when you fill out a form. When registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address and phone number. You may, however, visit our site anonymously.</p>


							<h5 class="mt-28">What do we use your information for?</h5>
							<p>Any of the information we collect from you may be used in one of the following ways: To process transactions. Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested</p>

							<h5 class="mt-28">How do we protect your information?</h5>
							<p>We offer the use of a secure server. All supplied sensitive/credit information is transmitted via Secure Socket Layer(SSL) technology and then encrypted into our Payment gateway providers database to be accessible by those authorized with special access rights to such systems, and are required to keep the information confidential. After a transaction, your private information( Credit/Debit Cards, Identity details, financials, etc) will not be stored on our servers.</p>

							<h5 class="mt-28">Do we use Cookies?</h5>
							<p>Yes (Cookies are small files that a site or its service provider transfers to your computer’s hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information. We use cookie to help us remember and process the items in your cart.</p>

							<h5 class="mt-28">Disabling Cookies</h5>
							<p>You can prevent the setting of cookies by adjusting the settings on your browser (see your browser Help for how to do this). Be aware that disabling cookies will affect the functionality of this and many other websites that you visit. Disabling cookies will usually result in also disabling certain functionality and features of this site. Therefore it is recommended that you do not disable cookies.</p>

							




						</div>
					</div>
				</div>
			</div>
		</section>

		


		@endsection
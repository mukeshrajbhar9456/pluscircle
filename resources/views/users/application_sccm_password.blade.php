@extends('layouts.lp')
   @section('content')  
      <section>
         <div class="container mb-40" >
            <div class="about-us-title ">
               <h4 class="ybh">Application for District Governor & District Chairman
               </h4>
               <hr>
            </div>

               <div class="row mb-150" >
                  @if (session()->has('error'))
                  <div class="col-md-12 center">
                     <div class="alert1 alert-danger1 ">&#x26A0; 
                        {{ session('error') }}
                     </div>
                  </div>
                  @endif
              

               <div class="col-md-2 mw-13"></div>
               <div class="col-md-9 shadowpcc">
                  <div class="cms-content">
                     <div class="dashboard-profile dashboard-user-profilepcc" >
                        <form  id="passwordForm"  method="POST" name="passwordForm" action="{{ route('sccm_password') }}">
                        {{ csrf_field() }}
                     <!--    <div class="col-md-12 text-center">
                           @if (session()->has('error'))
                              <div class="alert alert-danger">
                                 {{ session('error') }}
                              </div>
                           @endif
                        </div> -->
                           <input type='hidden' name='application' value='sccm' />
                              <div class="row">
                                 <div class="col-md-12 sccmcountry" style="" >
                                 <div class='row appcationtitle text-center'>
                                    <div class='col-md-12 center'>
                                    This application is only against invitation
                                    </div>
                                 </div>
                                    <div class="row">
                                       <div class="col-md-3">
                                          <p><span class="sccmstate0">Enter Password <span class="alert-danger1">*</span></p>
                                       </div>
                                       <div class="col-md-7">
                                          <div class="floating-label1">
                                             <input type='password' name='password' id='password' class="floating-selectpcc1" style="font-size:47px;" required >
                                             
                                          </div>
                                       </div>
                                    
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <div class="submit-btn">
                                             <input type="submit" value="submit " name="">
                                          </div>
                                       </div>
                                    </div>


                                    <div class='row appcationtitle text-center'>
                                       <div class='col-md-12 center'>
                                       For more information please contact at <a href="mailto:info@pluscircleclub.org">info@pluscircleclub.org</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </form>
                     </div>
                  </div>
               </div>
            <div  id="dgmf6"class="col-md-1 height-105" style="display:none; " ><a  data-target="#myModalfixedgovernor" data-toggle="modal" href="#myModalfixedgovernor"class="Div1"> <p class="sscmview">View <br>Your <br>Hirearchy</p></a></div>
            <div  id="dcmf6"class="col-md-1 height-105" style="display:none; " ><a  data-target="#myModalfixedchairman" data-toggle="modal" href="#myModalfixedchairman"class="Div1"> <p class="sscmview">View<br> Your <br>Hirearchy</p></a></div>
         </div>
      </div>
   </section>
      
      @endsection
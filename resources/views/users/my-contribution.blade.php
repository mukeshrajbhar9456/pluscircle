@extends('layouts.lp')
	@section('content')

		<section class="dashboard-main">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-3">
						<div class="dashboard-menu">
							<?php include'dashboardleftmenu.php'; ?>
							
						</div>
					</div>
					<div class="col-md-9">
						<div class="dashboard-content">
							<div class="my-contribution-main">
								<div class="title">
									<h4>my contribution</h4>
								</div>
								<div class="my-contribution-box-main">
									<div class="my-contribution-box">
										<div class="date-list">
											<ul>
												<li><span>Jan 2020</span><span>1251.00</span></li>
												<li><span>Feb 2020</span><span>450.00</span></li>
												<li><span>Mar 2020</span><span>2250.00</span></li>
												<li><span>Apr 2020</span><span>1122.00</span></li>
											</ul>
										</div>
									</div>
									<div class="my-contribution-box">
										<div class="contribution-total">
											<strong>
												Total Contribution <br> Rs. 5037.00
											</strong>
										</div>
									</div>
								</div>
								<div class="dashboard-table members-joined-table">
									<table>
										<thead>
											<tr>
												<th>Date</th>
												<th width="150">Source</th>
												<th width="130">Your Purchase</th>
												<th width="130">Your Contribution</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>28/07/2020</td>
												<td>Dinner</td>
												<td>1200.00</td>
												<td>120.00</td>
											</tr>
											<tr>
												<td>29/07/2020</td>
												<td>Movie</td>
												<td>750.00</td>
												<td>15.00</td>
											</tr>
											<tr>
												<td>30/07/2020</td>
												<td>Event X</td>
												<td>2250.00</td>
												<td>114.00</td>
											</tr>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="2"><b>Total Spendin</b></td>
												<td><b>4200.00</b></td>
												<td><b>249.00</b></td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	
		
		
		@endsection


		
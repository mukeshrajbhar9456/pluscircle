@extends('layouts.lp')
	@section('content')
		<section class="about-us-head-main">

			<div class="container yb">
				<div class="about-us-title">
					<p><a href="{{ url('/') }}">Home</a> / Unlock Your Opportunity</p><br>
					<h4>Unlock Your Opportunity</h4>
					<hr>
				</div>
				<div class="about-us-head">
					<div class="about-us-img-main mb-40">
						<div class="about-us-img-inner" >
							<div class="about-us-img mb-20">
							<img src="{{asset('public/images/h5.jpg')}}">
							</div>
						
						</div>

						<p>A strong community is a place of opportunity. </p>

						<p>Big or small, our community always provides countless opportunities for growth and experience. With a community of people looking out for your best interests and working together for a common goal, there is no shortage of opportunities to strive for something that you want. Whether that is looking for your first job or starting your own business, our networking platform  can give the support you need to succeed.
						</p>

						<p>Our Networking platform will help you to develop and improve your skill set, stay on top of the latest trends in your industry, keep a pulse on the job market, meet prospective mentors, partners, and clients, and gain access to the necessary resources that will foster your career development.</p>

						<p>This  networking club gives you the profile and visibility you need to be known as the go-to person in your field—especially if you’re just getting started or going through a difficult transition, meeting people who've been there and succeeded, is a great way to boost your morale.</p>

						<p>We provide opportunities to increase your business growth through a structured, positive, and professional referral marketing program that enables you to develop long-term, meaningful relationships with quality business professionals. We have built a professional community that enables anyone to socialize their ideas and showcase their talent
						</p>
					</div>
				</div>
			</div>
		</section>
	
		
		@endsection
@extends('layouts.lp')
	@section('content')	

		<section class="cms-page-main">
			<div class="container mb-28">
				<div class="about-us-title">
					<h4 class="ybh">Terms of Use</h4>
					<hr>
				</div>

			


				<div class="row">
					<div class="col-md-12">
						<div class="cms-content">
							
							<p>
								Below is an overview of our Terms of Service for our “Platform”, which means any website, application, or service we offer. You should read the complete Terms of Service because that document (and not this overview) is our legally binding agreement. The Terms of Service includes information about your legal rights and covers areas such as automatic subscription renewals, limitations of liability, resolution of disputes by mandatory arbitration rather than a judge or jury in a court of law, and a class action waiver.
								</p>


								<h5 class="mt-28">Your Relationship with Plus Circle</h5>

								<ul class="mb-28">
								<li>By using our Platform, you are agreeing to our Terms of Service. The Terms of Service are a legally binding agreement between you and Plus Circle.</li>
								<li>If you break the rules, we may suspend or terminate your account.</li>
								<li>We charge for certain aspects of our Platform, and some of these fees are billed on a regular and recurring basis (unless you disable auto-renewal or cancel your subscription).</li>
							
							</ul>



							<h5 class="mt-28">1. Who May Use the Services</h5>
							<p>If the law in your jurisdiction allows you to enter into agreements with third parties, you agree to these ToS then you can use the Site and Services. However, if your mind is legally incapacitated (mental disability, intoxication, under the age of 18, etc.) for any reason, then you are not allowed to use the Plus Circle Site and Services. For those who are accepting these ToS and using the  Plus Circle Site and Services on behalf of a company, organization, government, or other legal entity, you represent and warrant that you are authorized to do so.</p>



							<h5 class="mt-28">2. Content on the services</h5>
							<p>You are responsible for your “Content”, which means any information, material, or other content posted to our Platform. Your Content must comply with our Terms of Service, which includes the Usage and Content Policies, Groups and Events Policies, Organizer and Leadership Standards, Member Restrictions, Payment Policies, Trademark Usage Guidelines, and API License Terms. 

							</p><p>We do not own the Content that you post. However, we do require that you provide us a license to use this Content in order for us to operate, improve, promote, and protect Plus Circle and our Platform for the benefit of you and others.
							</p><p>
							We are not responsible for Content that members post or the communications that members send using our Platform. We generally review Content before it’s posted. If you see Content that violates our Terms of Service, you may report inappropriate Content to us.
							 
							</p>


							<h5 class="mt-28">3. Our Platform</h5>
							<p>
							We try hard to make sure that our Platform is always available and working, but we cannot guarantee it will be. Occasionally things may not go exactly as planned. We apologize in advance for any inconvenience.
							</p><p>

							
							We are continually improving our Platform. This means that we may modify or discontinue portions of our Platform.

							</p><p>

							By using our Platform, you agree to the limitations of liability and release in our Terms of Service. You also agree to resolve any disputes you may have with us in the manner described in the Terms of Service. Unless you are based in the European Union, you agree to mandatory arbitration and waive your right to seek relief from a judge or jury in a court of law. Claims can only be bought individually, and not as part of a class action.</p><p>

							You may be based in a country where the laws that apply provide for different or additional rights and obligations. These Terms of Service will point out areas of difference.
							</p>


							<h5 class="mt-28">4. Your Account and Membership</h5>
							<p><b>Eligibility :</b> Our Platform is available to anyone who is at least 18 years old. You represent that you are at least 18. Additional eligibility requirements for a particular portion of our Platform may be set by any member who has the ability to moderate or manage that portion of our Platform. For example, the eligibility requirements for a Plus Circle group or Plus Circle event may be set by the organizers of that group.

							</p><p><b>Modification, Suspension, and Termination of Your Account :</b> We may modify, suspend, or terminate your account or access to the Platform if, in our sole discretion, we determine that you have violated this Agreement, including any of the policies or guidelines that are part of this Agreement, that it is in the best interest of the Plus Circle community, or to protect our brand or Platform. When this happens, we will notify you of the reasons for the modification, suspension, or termination. We also may remove accounts of members who are inactive for an extended period of time. Please email <a href="mailto:policy@pluscircleclub.org.">policy@pluscircleclub.org.</a> if you believe the modification, suspension, or termination has occurred in error.

							</p><p>A member who has the ability to moderate or manage a particular portion of our Platform also has the ability, in his or her sole discretion, to modify, suspend, or terminate your access to that portion of the Platform.

							</p><p><b>Account Information and Security : </b> When you register, you provide us with some basic information, including an email address and a password. Keep your email address and other account information current and accurate. Also, you agree to maintain the security and confidentiality of your password (or else we may need to disable your account). We strongly encourage you to choose a strong and unique password that is not shared with any other account or online service and practice other healthy password security habits to help avoid unauthorized access to your account. You alone are responsible for anything that happens from your failure to maintain that security and confidentiality, such as by sharing your account credentials with others. If someone is using your password or accessing your account without your permission, email us at <a href="mailto:info@pluscircleclub.org.">info@pluscircleclub.org.</a>

							</p><p><b>License to the Plus Circle Platform and Services : </b> Subject to your compliance with this Agreement, Plus Circle grants you a limited, non-exclusive, revocable, non-sublicensable, non-transferable right to use the Platform in order to access and use the services and features that we make available to you.
							</p>

							<h5 class="mt-28">5. Fees, Payments and Offers</h5>
							<p><b>Fees Charged by Plus Circle :</b> When you join as a member, you have an opportunity to review and accept the fees that will be charged. Prices, availability, and other purchase terms are subject to change.. If we implement a new or modified fee, we will give you notice in advance such as by posting changes on our Platform or sending you an email. You agree to pay those fees and any associated taxes for your continued use of the applicable service.</p>

							<h5 class="mt-28">6. Intellectual Property.</h5>
							<p>All content included on the Plus Circle Site and in connection with the Services such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations and software, is the property of Plus Circle or its content suppliers and is protected by international copyright laws. All software used on the Site is the property of Plus Circle Or its software suppliers and is protected by international copyright laws. All of the trademarks, service marks, logos, brand and trade names appearing on the Site are the proprietary intellectual property of the owners of such marks, logos or names and you may not use, modify, remove or otherwise infringe any of such proprietary intellectual property. Plus Circle retains full ownership rights with respect to the Site and the Services including but not limited to design, functionality, and documentation. You may not copy, edit, or reproduce any part of the Site or the Services.</p>

							<h5 class="mt-28">7. Third Party Websites</h5>
							<p>The Plus Circle Site may redirect or link to other websites on the Internet, or may otherwise include references to information, products or services made available by unaffiliated third parties. While we make every effort to work with trusted, reputable providers, from time to time such sites may contain information, material or policies that some may find inappropriate or personally objectionable. You understand that we are not responsible for the accuracy, completeness, decency or legality of content hosted by third party websites, nor are we responsible for errors or omissions in any references made on those websites. The inclusion of such a link or reference is provided merely as a convenience and does not imply endorsement of, or association with the Site or party by us, or any warranty of any kind, either express or implied. You are solely responsible for and assume all risk arising from your access to and/or use of any such linked websites.</p>

							<h5 class="mt-28">8. General Terms</h5>
							<p><b>Translation :</b> This Agreement was written in English. It was then translated into other languages. If there is any inconsistency between the English version and a translated version, the English language version controls. </p><p>

							<b>Entire Agreement :</b>This Agreement, including the Usage and Content Policies, Groups and Events Policies, Organizer and Leadership Standards, Member Restrictions, Intellectual Property Policies, Trademark Usage Guidelines, and API License Terms, constitutes the entire agreement between you and Plus Circle, superseding any prior agreements between you and Plus Circle on such subject matter.</p><p>

							<b>Time for Filing :</b> Any claim not subject to arbitration must be commenced within one year after the date the party asserting the claim first knows or should know of the act, omission or default giving rise to the claim, or the shortest time period permitted by applicable law. </p><p>

							<b>Thank you  :</b> Please accept our wholehearted thanks for reading our Terms of Service.</p><p>

							<b>Violations :</b>  Please report any violations of this Agreement by a member or third party by sending an email to <a href="mailto:info@pluscircleclub.org.">info@pluscircleclub.org.</a>
							</p>

							



						</div>
					</div>
				</div>
			</div>
		</section>

		
@endsection
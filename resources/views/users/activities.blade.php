@extends('layouts.lp')
	@section('content')	
		<section class="cms-page-main">
			<div class="container mb-28">
				<div class="about-us-title">
					<p><a href="{{ url('/') }}">Home</a> / Activities</p><br>
					<h4 class="ybh">Activities</h4>
					
					<hr>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="cms-content">
							<h5 class="">Orphans</h5>
							
							
							<p>We believe in the phrase “ most of our today's problems are because of yesterdays short term thingness ”  </p>

							<p>Today if we plan for our tomorrow it will be much brighter. Therefore we at PLUS CIRCLE provide equal opportunity for the needy orphan. </p><p>

							With pleasure, we take this privilege of supporting them with decent accommodation, quality food, required education, appropriate clothing, and other day-to-day needs along with their health care.</p><p>

							We believe if we light up their lives today. They will brighten up the Nation tomorrow.</p>

							<h5 class="mt-28">Old Aged</h5>
							<p>
							Taking care of those who once cared for us, is not our responsibility it’s our pride </p><p>

							We believe that we are blessed and privileged for being able to take care of the needy Old Aged, </p><p>

							With the best of our ability, we take care of the needs of their Accommodation, Food, Health, Recreational activities, and all other Day-to-Day needs. </p><p>

							We believe in Blessings and Positive Aura</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		@endsection

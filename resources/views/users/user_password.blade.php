@extends('layouts.lp')
	@section('content')	

		<section class="login-section">
			<div class="container">

				<div class="row">
					
					<div class="col-md-12">
						<div class="login-form">
							<div class="wizard">
				               <div class="wizard-inner">
				                  <div class="connecting-line"></div>
				                  <ul class="nav nav-tabs" role="tablist">
				                     
				                  </ul>
				               </div>
				               <form role="form" method="POST" action="{{ route('signin') }}" name="loginForm" class="login-form">
							  	 {{ csrf_field() }}
							   		<div class="tab-content" id="main_form">
				                     <div class="tab-pane active" role="tabpanel" >
				                     	<div class="form-title">
				                     		<!-- <h4>Enter your mobile number or email id to login</h4> -->
				                     		<h4>Enter your Password</h4>
										 </div>

										  <div class="col-md-12 center">
										  	@if (session()->has('message'))
												<div class="alert1 alert-info1">&#x2714;
													{{ session('message') }}
												</div>
											@endif
											@if (session()->has('error'))
											<div class="alert1 alert-danger1 ">&#x26A0;
												{{ session('error') }}
											</div>
											@endif
											   <!-- <div class="alert1 alert-danger1 ">&#x26A0; Your Password is incorrect or this account does not exist. <BR>Please enter valid password or<a href="#" style="text-decoration: underline;"class="alert-danger1">  Reset Password</a>
					                           </div> -->
					                     </div>


										 <input type='hidden' name='email' value="{{ $email?$email:'' }}">
												 
				                     	<div class="form-bg">
					                        <div class="floating-form">
											    <div class="floating-label">      
												  <input class="floating-input" type="password" name='password' placeholder="Enter Password ">
												  <span class="highlight"></span>
											      <!-- <label>Enter mobile number or email id</label> -->
												  <!-- <label></label> -->
												  	@error('password')
														<label id="password-error" class="error" for="password">{{ $message }}</label>
													@enderror
													@error('email')
														<label id="email-error" class="error" >{{ $message }}</label>
													@enderror
											    </div>
											</div>
											<div class="forgot-link mt-28">
												<a href="{{ route('showForgotPasswordForm') }}" class="accept1">Forgot Password? </a><br>
												<a href="{{ route('resendEmailVerify') }}" class="accept1">Didn't receive email to Verify? </a>
											
											</div>
					                        <div class="submit-btn">
												<input type="submit" name="next"  class="next-step" value="Login">
											</div>

											<div class="or-line">
												<span>or</span>
											</div>
											<div class="otp-btn">
												<a href="#" class="btn1">LOGIN WITH OTP</a>
											</div>



											<div class="new-user">
												<a href="{{ route('user_register') }}" class="accept1">Join as a new member</a>
											</div>
										</div>
									</div>
								</div>
							</form>
				            </div>
						</div>
					</div>
				</div>
			</div>
		</section>

		@endsection

		
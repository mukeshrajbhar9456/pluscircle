@extends('layouts.lp')
	@section('content')
		<section>
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="{{ url('/') }}">Home</a> / Adorn your Life with Fun</p><br>
					<h4 class="ybh">Adorn your Life with Fun</h4>
					
					<hr>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="event-activites-head">
							<p>We believes in developing the next generation of leaders. Our programs help younger leaders build leadership skills, expand education and learn the value of service.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="event-activites-section1">
			<div class="container ">
				<div class="row mb-40" id="event9">
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/adoran/cultural-events.jpeg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Cultural Event</h4>
								<p>Offering an insight to the jaw-dropping traditions; warmth giving festivals; vivacious fairs; unheard rituals; and a large number of events, India is a travel destination
								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsCultural" data-toggle="modal" href="#myModalEventsCultural" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>


					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/adoran/Sports.jpg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Sports</h4>
								<p>Plus Circle Badminton Tournament
								1. Tournament is only for corporate players.
								2. Yonex AS2 feather shuttles will be used for all the matches.
								</p>
								<div class="learn-link">

									<a data-target="#myModalEventsSports" data-toggle="modal" href="#myModalEventsSports" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/adoran/Dance.jpg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Dance</h4>
								<p>Being a virtual event, you are free to run wherever you want, whenever you want without being bound by any time restrictions. No matter whether you walk, run, skip</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsDance" data-toggle="modal" href="#myModalEventsDance" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row mb-40" id="event10">
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/adoran/Street-entertainment.jpg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Street Entertainment</h4>
								<p>One of the most popular street entertainment of Goa and one of the oldest. Situated in North Goa, in the Baga Beach area, which consists of a variety of clubs </p>
								<div class="learn-link1">
									<a data-target="#myModalEventsStreet" data-toggle="modal" href="#myModalEventsStreet" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/adoran/Night-clubs.jpg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Night Clubs</h4>
								<p>One of the best night clubs in Bangalore, We are known for vibrant, quirky ambience.  The long benches, round tables, soft lighting will surely give you fantasy feels. 
								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsNight" data-toggle="modal" href="#myModalEventsNight" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/adoran/get-together.png')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Get Together </h4>
								<p>There are more than 108,000 get-togethers happening across the country — from street parties and Big Lunches to BBQs, games of football and bake offs.
								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsGet" data-toggle="modal" href="#myModalEventsGet" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">

						<div class="center">
						  	<div class="pagination">
							  <a href="{{ route('event_activites_five',['event','']) }}">&laquo;</a>
							  <a class="active" href="{{ route('event_activites_five',['event','']) }}">1</a>
							  <a href="{{ route('event_activites_six',['event','']) }}" >2</a>
							  <a href="{{ route('event_activites_six',['event','']) }}">&raquo;</a>
							
						  	</div>
						</div>
						
					</div>

				</div>
						
			</div>
		</section>
		
		@endsection
							<ul>
								<li class="active"><a href="{{ route('user.dashboard') }}">My Dashboard</a></li>

								<li><a  href="{{ route('user-profile') }}">My Profile</a></li>

								<li><a data-toggle="tooltip"  data-placement="right"  title="You need to purchase membership kit to use this section " class="red-tooltip" id="tooltip-top" href="{{ route('upcoming_events') }}">Upcoming Events / My Ticket</a></li>

								<li><a data-target="#myModal" data-toggle="modal" href="#myModal">My Circle</a></li>
								
								<li><a data-toggle="tooltip"  data-placement="right"  title="You need to purchase membership kit to use this section " class="red-tooltip" id="tooltip-top"href="{{ route('volunteer') }}">I Wish to Volunteer</a></li>

								<li><a data-toggle="tooltip"  data-placement="right"  title="You need to purchase membership kit to use this section " class="red-tooltip" id="tooltip-top" href="{{ route('my_referrences') }}">My REFERENCES & Rewards</a></li>

								<li><a data-toggle="tooltip"  data-placement="right"  title="You need to purchase membership kit to use this section " class="red-tooltip" id="tooltip-top" href="{{ route('my_contribution') }}">My Contribution</a></li>

								<li><a data-toggle="tooltip"  data-placement="right"  title="You need to purchase membership kit to use this section " class="red-tooltip" id="tooltip-top" href="{{ route('circle_news') }}">I Wish to Host an Event</a></li>
								
								<li><a data-toggle="tooltip"  data-placement="right"  title="You need to purchase membership kit to use this section " class="red-tooltip" id="tooltip-top" href="{{ route('circle_heroes') }}">Circle Heroes & Champions</a></li>
							</ul>
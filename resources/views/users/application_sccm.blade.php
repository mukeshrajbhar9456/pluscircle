@extends('layouts.lp')
   @section('content') 
   <style>


.field-icon {
  float: right;
  margin-left: -25px;
  margin-top: -25px;
  position: relative;
  z-index: 2;
}

.container{
  padding-top:10px;
  margin: auto;
}
label.cabinet{
	display: block;
	cursor: pointer;
}

label.cabinet input.file{
	position: relative;
	height: 100%;
	width: auto;
	opacity: 0;
	-moz-opacity: 0;
  filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
  margin-top:-30px;
}

#upload-demo{
	width: 400px;
	height: 400px;
  padding-bottom:25px;
}
figure figcaption {
    position: absolute;
    bottom: 0;
    color: #fff;
    width: 100%;
    padding-left: 9px;
    padding-bottom: 5px;
    text-shadow: 0 0 10px #000;
}
p.allstate {
    margin-bottom: 0px;
    text-align: center;
    line-height: 17px;
    background-color: #7E5233;
    font-weight: 600;
    color: #fff;
    width: 102px;
    padding: 2px;
    border-radius: 5px;
    top: 232px;
    right: 91px;
    position: fixed;
    z-index: 999;
}
.sscmview {
    padding: 5px;
    text-align: center !important;
    font-size: 15px;
    width: 99px;
    min-height: 100px;
    background-color: #57637c;
    color: #fff1f1;
    position: fixed;
    z-index: 999;
    line-height: 20px;
    font-weight: 600;
    /* height: 5px; */
    border-radius: 5px;
    top: 315px !important;
    /* height: 13px!important; */
    /* left: 2px; */
    right: 90px;
}
label.error {
    font-size: 12px;
    color: #ff8300;
    font-weight: 700;
}
</style>
      <section>
         <div class="container mb-40" >
            <div class="about-us-title ">
               <h4 class="ybh">Application for District Governor & District Chairman
               </h4>
               <hr>
            </div>

            <div class="row mb-150" >
               <div class="col-md-2 mw-13"></div>

               <div class="col-md-9 shadowpcc">
                  <div class="cms-content">
                     <div class="dashboard-profile dashboard-user-profilepcc" >
                        <form id="sccmapplication" method='post' action="{{ route('sccm_application_submit')}}" name='sccmapplication' enctype="multipart/form-data">
                           {{ csrf_field() }}
                           <div class="row">
                              <div class="col-md-12 sccmcountry" style="" >
                                 <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-5">
                                       <p><span class="sccmstate0">Select Country</span></p>
                                    </div>
                                    <div class="col-md-3">
                                       <div class="floating-label1">
                                          <select  id="appcountry" name="appcountry" onchange = "ShowHideDiv()" class="floating-selectpcc1 sccmstate2" onclick="this.setAttribute('value', this.value);" value="">
                                             <option value=""></option>
                                             <option value="101">India</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="row mt-20" id ="state2"  style="display:none">
                                    <div class="col-md-6">
                                       <p><span class="sccmstate0">Select State</span></p>
                                    </div>
                                    <div class="col-md-3 sccmstate1" >
                                       <div class="floating-label1">
                                          <select id="state1" name='appstate' onchange = "ShowHideDiv2()" class="floating-selectpcc1 sccmstate2" onclick="this.setAttribute('value', this.value);" value="">
                                             <option value=""></option>
                                             <option value="Maharashtra">Maharashtra</option>
                                             <option value="Andhra Pradesh">Andhra Pradesh</option>
                                             <option value="Tamil Nadu">Tamil Nadu</option>
                                             <option value="Telangana">Telangana</option>
                                             <option value="Punjab">Punjab</option>

                                          </select>
                                       </div>
                                    </div>
                                    
                                 </div>

                                 <div class="row mt-40" id="city" style="display:none">
                                    <div class="col-md-6">
                                       <p><span class="sccmstate">I wish to apply for the post of </span></p>
                                    </div>
                                    <div class="col-md-3 sccmstate1">
                                       <div class="floating-label1">
                                          <select id="city1" onchange = "ShowHideDiv3()" class="floating-selectpcc1 sccmstate2" name='position' onclick="this.setAttribute('value', this.value);" value="">
                                             <option value=""></option>
                                             <option value="District Governor">District Governor</option>
                                             <option value="Club President">Club President</option>
                                             <option value="Club Vice President">Club Vice President</option>
                                             <option value="Club Secretary">Club Secretary</option>
                                             <option value="Club Treasurer">Club Treasurer</option>
                                             <option value="Club Zonal Cricket">Club Zonal Cricket</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>

                              </div>
                           </div>



                           <div class="row"id="allsection"  style="display:none">

                              <div class="col-md-12 mt-20 sccmcity">
                                 <center>
                                    <h4  id="dgmf" style="text-decoration: underline;">Application for District Governor </h4>
                                    <h4  id="dcmf" style="text-decoration: underline;">Application for Club President </h4>
                                    <h4  class="districtsecretary" style="text-decoration: underline;">Application for Club Vice President </h4>
                                    <h4  class="districttreasurer" style="text-decoration: underline;">Application for Club Secretary </h4>
                                    <h4  class="districtzonalchairman" style="text-decoration: underline;">Application for Club Treasurer </h4>
                                    <h4  class="districtzonalcricket" style="text-decoration: underline;">Application for Club Zonal Cricket </h4>
                                 </center>
                                 <p><ul class="ulclass">
                                    <li>The final date of this application: 11th November 2020.</li>
                                    <li>
                                       <p>Debut and group discussion with International Core Committee Members (ICCM) on 3rd December 2020, Venue: Google Meet. (You will receive a Google meeting invite in your registered email subject to the approval of your application)</p>
                                    </li>
                                    <li>
                                       <p>Personal Meeting with our International Core Committee Members (ICCM) on 16th & 17th December, 2020 Venue; ITC Windsor Manners, Bangalore (You will receive an invitation, subject to the resolution of the committee)</p>
                                    </li>
                                    </ul>
                                 </p>
                              </div>
                           


                              <div class="col-md-12">
                                 <div class="row ml-0 mt-20 appcm">


                                   

                                    
                                    <div class="col-md-12">
                                       <center>
                                          <div class="floating-label2">
                                             <img src="{{ asset('public/images/avatar.jpeg')}}" id='displayImage' name='displayImage' style='height:200px;width:200px'/>
                                             <input type='hidden' name='profile' id='profile' />
                                          </div>
                                       </center>
                                    </div>
                                    <div class="col-md-12"><center>
                                       <div class="floating-label2">
                                          <p><input type="file"  accept="image/*" name="image" id="file"  style="display: none;"></p>
                                          <p class="center">
                                             <label class="uploadcss" for="file" >Upload Photo</label></p>
                                           <!-- <img src="http://via.placeholder.com/150x150"  id="image" style="border-radius:50%"/>
                                          <input type="file" id="myfile" style="display:none"/> -->
                                         <!--  <p class="inputpcc_b">Upload Photo</p> #fff7e8--->
                                        <!--   <input class="floating-input" type="file" placeholder=" ">
                                          <span class="highlight"></span> -->
                                       </div></center>
                                    </div>


                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Name</p>
                                          <input class="floating-inputpcc" type="text" name='name' placeholder=" " required>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Date of Birth</p>
                                          <div class="row">
                                             <div class="col-md-6">
                                                <input type="date" class="floating-inputdate"  name="dob" id="bday" onchange="submitBday()">
                                             </div>
                                             <div class="col-md-6">
                                                <p id="resultBday line-height-35" ></p>
                                                <input class="floating-input2 line-height-35" type="text" style="display:none;"placeholder=" " >
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Spouse Name</p>
                                          <input class="floating-inputpcc" type="text" name='spousename' placeholder=" " required>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Father Name</p>
                                          <input class="floating-inputpcc" type="text"  name='fathername' placeholder=" " required>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Mother Name</p>
                                          <input class="floating-inputpcc" type="text"  name='mothername' placeholder=" " required>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Email Id</p>
                                          <input class="floating-inputpcc" type="email" name='email' placeholder=" " required>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Mobile No 1</p>
                                          <input class="floating-inputpcc" type="number" name='phone' placeholder=" " required>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Mobile No 2</p>
                                          <input class="floating-inputpcc" type="number" name='secondaryphone' placeholder=" ">
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-12">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Residence Address</p>
                                          <textarea class="floating-input4pcc" type="text" name='address' placeholder=" "  required></textarea>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>


                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Country</p>
                                          <select class="floating-selectpcc1" name='country' id='country'  onclick="this.setAttribute('value', this.value);" value="">
                                             <option value=""></option>
                                             <option value="101">India</option>
                                          </select>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>


                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">State</p>
                                          <select class="floating-selectpcc1" name='state' id='state3'onclick="this.setAttribute('value', this.value);" value="">
                                             <option value=""></option>
                                             <option value="MH">MH</option>
                                             <option value="MP">MP</option>
                                          </select>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>


                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">City</p>
                                          <input type='text' name='city' id='city' class="floating-selectpcc1" />
                                          <!-- <select class="floating-selectpcc1" onclick="this.setAttribute('value', this.value);"  value="">
                                             <option value=""></option>
                                             <option value="Mumbai">Mumbai</option>
                                          </select> -->
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Pincode</p>
                                          <input class="floating-inputpcc" type="number" name='pincode' placeholder="">
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-12">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Name & Address of the Firm</p>
                                          <textarea class="floating-input4pcc" name='firmaddress' type="text" placeholder=" "></textarea>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>


                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Country</p>
                                          <select class="floating-selectpcc1" name='firmcountry' id='firmcountry' onclick="this.setAttribute('value', this.value);" value="">
                                             <option value=""></option>
                                             <option value="101">India</option>
                                          </select>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">State</p>
                                          <select class="floating-selectpcc1" name='firmstate' id='firmstate' onclick="this.setAttribute('value', this.value);" value="">
                                             <option value=""></option>
                                             <option value="MH">MH</option>
                                             <option value="MP">MP</option>
                                          </select>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">City</p>
                                          <input type='text' name='firmcity' id='firmcity' class="floating-selectpcc1" />
                                          <span class="highlight"></span>
                                       </div>
                                    </div>


                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Pincode</p>
                                          <input class="floating-inputpcc" type="text" name='firmpincode' placeholder="">
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-12 mt-20">
                                       <p class="inputpcc_b">Are you a Member of any Clubs / Trust ? if Yes please mention any two. </p>
                                    </div>

                                    <div class="col-md-12">
                                       <div class="my-contribution-main">
                                          <div class="dashboard-table members-joined-table1">
                                             <table>
                                                <thead>
                                                   <tr>
                                                      <th>Name of the Club / Trust</th>
                                                      <th >Member Since (Year)</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <tr>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare" type="text" name='clubname1' placeholder=" ">
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare" type="number" name='clubyear1' placeholder=" ">
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare" type="text" name='clubname2' placeholder=" ">
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare" type="number" name="clubyear2" placeholder=" ">
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>


                                    <div class="col-md-12 mb-20"></div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b"> Nationality</p>
                                          <input class="floating-inputpcc" type="text" name='nationality' placeholder=" " required>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b"> Highest Qualification</p>
                                          <input class="floating-inputpcc" type="text" name='education' placeholder=" " required>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Annual Net Income (In INR)</p>
                                          <input class="floating-inputpcc" type="text" name='annualincome' placeholder=" " required>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6"></div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Type of ID Proof </p>
                                          <select class="floating-selectpcc1" name='idprooftype' onclick="this.setAttribute('value', this.value);" value="">
                                             <option value=""></option>
                                             <option value="Business Person">Aadhar Card</option>
                                             <option value="Passport">Passport</option>
                                             <option value=" Driving License"> Driving License</option>
                                          </select>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Id No </p>
                                          <input class="floating-inputpcc" type="text" name='idproofnumber' placeholder="">
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Profession </p>
                                          <input class="floating-inputpcc" type="text" name='profession' placeholder="" required>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Describe your Profession in 100 words.</p>
                                          <textarea class="floating-input4pcc" type="text" placeholder=" " name='profedescription' row='20' style="width:100%; height:150px;"></textarea>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-12">
                                      
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b"> Why do you wish to become a member of Plus Circle ? </p>
                                          <textarea class="floating-input4pcc" type="text" placeholder="Describe in 300 words " name='whypluscircle' style="width:100%; height:150px;"></textarea>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b">Have you ever participated in any of the social welfare activities? If Yes </p>
                                          <textarea class="floating-input4pcc" type="text" placeholder="Describe in 300 words " name='socialwelfaredesc' style="width:100%; height:150px;"></textarea>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-12">
                                       <p></p>
                                    </div>

                                    <div class="col-md-12 mt-20">
                                       <p class="inputpcc_b"> Please give two references. </p>
                                    </div>

                                    <div class="col-md-12">
                                       <div class="my-contribution-main">
                                          <div class="dashboard-table members-joined-table1">
                                             <table>
                                                <thead>
                                                   <tr>
                                                      <th>Reference  </th>
                                                      <th>Name </th>
                                                      <th>Mobile No </th>
                                                      <th>Email ID </th>
                                                      <th>Relationship </th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <tr>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare"  name='reference1' type="text" value='Reference 1' placeholder="Reference  1 ">
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare"  name='referencename1' type="text" placeholder=" " required>
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare"  type="text" name='referencephone1' placeholder=" " required>
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare" type="email" name='referenceemail1' placeholder=" " required>
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare" type="text" name='referencerelation1' placeholder=" " required>
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare"  name='reference2' type="text" value='Reference 2' placeholder="Reference 2">
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare"  name='referencename2' type="text" placeholder=" " required>
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare"  type="text" name='referencephone2' placeholder=" " required>
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare" type="email" name='referenceemail2' placeholder=" " required>
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                      <td>
                                                         <div class="col-md-12">
                                                            <div class="floating-label">
                                                               <input class="floating-inputdeclare" type="text" name='referencerelation2' placeholder=" " required>
                                                               <span class="highlight"></span>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>



                                    <div class="col-md-12 mt-40 sccmh41">
                                       <h4 class="inputpcc_b"  class="sccmh4" >Membership Fees</h4>
                                       <p></p>
                                       <p><ul class="plm-30">
                                          <li id="dgmf1" style="display:none">Membership Fees for District Governor Rs. 2,00,000/- (Rupees Two Hundred Thousand Only)</p></li>
                                          <li id="dgmf2" style="display:none">
                                             <p>Earnest Fees Rs. 50,000/- (Rupees Fifty Thousand Only) </p>
                                          </li>
                                          <li id="dcmf1" style="display:none">Membership Fees for Club President Rs. 1,00,000/- (Rupees One Hundred Thousand Only)</p></li>
                                          <li id="dcmf2" style="display:none">
                                             <p>Earnest Fees Rs. 25,000/- (Rupees Twenty-Five Thousand Only) </p>
                                          </li>
                                
                                        
                                          <li class='districtsecretary' style="display:none">Membership Fees for Club Vice President Rs. 2,500/-  (Rupees Two thousand and Five Hundred Only)</p></li>
                                          <li class='districtsecretary' style="display:none">
                                             <p>Earnest Fees Rs. 2,500/- (Rupees Two Thousand and Five Hundred Only) </p>
                                          </li>
                                
                                          <li class='districttreasurer' style="display:none">Membership Fees for Club Secretary Rs. 2,500/-  (Rupees Two thousand and Five Hundred Only)</p></li>
                                          <li class='districttreasurer' style="display:none">
                                             <p>Earnest Fees Rs. 2,500/- (Rupees Two Thousand and Five Hundred Only) </p>
                                          </li>
                                
                                          <li class='districtzonalchairman' style="display:none">Membership Fees for Club Treasurer Rs. 2,500/-  (Rupees Two thousand and Five Hundred Only)</p></li>
                                          <li class='districtzonalchairman' style="display:none">
                                             <p>Earnest Fees Rs. 2,500/- (Rupees Two Thousand and Five Hundred Only) </p>
                                          </li>
                                          <li>
                                             <p >Please transfer the earnest amount to Indian Bank :<br>Favoring <b>Plus Circle </b>Current Account no. <b>6930522630</b>, IFS Code : <b>IDIB000Y004</b></p>
                                          </li>
                                       </ul></p>
                                    </div>

                                    <div class="col-md-12 mt-20" >
                                       <p id="dgmf3">
                                          <span class="pl-30 pr-30">I, have transferred Rs. 50,000/- (Rupees Fifty Thousand Only) as an application for <b style="text-decoration: underline;">District Governor,</b><br><span class="pl-30"> via my bank account UTR / reference no:       </span>
                                          <spam >                                            
                                             <input class="floating-inputpccid" type="text"  name='utrrefno1' placeholder="">
                                          </spam>
                                          <spam >
                                             dated
                                          </spam>
                                          <spam >
                                             <input class="floating-inputpccid" type="date"  name='utrrefdate1' placeholder="">
                                          </spam>
                                       </p>
                                       <p id="dcmf3">
                                          <span class="pl-30 pr-30">I, have transferred Rs. 25,000/- (Rupees Twenty-Five Thousand Only) as an application for <b style="text-decoration: underline;">Club President,</b><br><span class="pr-30"> via my bank account UTR / reference no:       </span>
                                          <spam >                                            
                                             <input class="floating-inputpccid" type="text"   name='utrrefno2'  placeholder="">
                                          </spam>
                                          <spam >
                                             dated
                                          </spam>
                                          <spam >
                                             <input class="floating-inputpccid" type="date"  name='utrrefdate2'  placeholder="">
                                          </spam>
                                       </p>
                                      
                                       <p class='districtsecretary' style='display:none'>
                                          <span class="pl-30 pr-30">I, have transferred Rs. 2,500/- (Rupees Two Thousand, Five Hundred Only) as an application for <b style="text-decoration: underline;">Club Vice President,</b><br><span class="pr-30"> via my bank account UTR / reference no:       </span>
                                          <spam >                                            
                                             <input class="floating-inputpccid" type="text"   name='utrrefno2'  placeholder="">
                                          </spam>
                                          <spam >
                                             dated
                                          </spam>
                                          <spam >
                                             <input class="floating-inputpccid" type="date"  name='utrrefdate2'  placeholder="">
                                          </spam>
                                       </p>
                                       <p class='districttreasurer' style='display:none'>
                                          <span class="pl-30 pr-30">I, have transferred Rs. 2,500/- (Rupees Two Thousand, Five Hundred Only) as an application for <b style="text-decoration: underline;">Club Secretary,</b><br><span class="pr-30"> via my bank account UTR / reference no:       </span>
                                          <spam >                                            
                                             <input class="floating-inputpccid" type="text"   name='utrrefno2'  placeholder="">
                                          </spam>
                                          <spam >
                                             dated
                                          </spam>
                                          <spam >
                                             <input class="floating-inputpccid" type="date"  name='utrrefdate2'  placeholder="">
                                          </spam>
                                       </p>
                                       <p class='districtzonalchairman' style='display:none'>
                                          <span class="pl-30 pr-30">I, have transferred Rs. 2,500/- (Rupees Two Thousand, Five Hundred Only) as an application for <b style="text-decoration: underline;">Club Treasurer,</b><br><span class="pr-30"> via my bank account UTR / reference no:       </span>
                                          <spam >                                            
                                             <input class="floating-inputpccid" type="text"   name='utrrefno2'  placeholder="">
                                          </spam>
                                          <spam >
                                             dated
                                          </spam>
                                          <spam >
                                             <input class="floating-inputpccid" type="date"  name='utrrefdate2'  placeholder="">
                                          </spam>
                                       </p>
                                    </div>

                                    <div class="col-md-12 mt-28 sccmh41">
                                       <h4 class="inputpcc_b"class="sccmh4">Your Bank Details</h4>
                                       <p></p>
                                       <p class="pl-30 pr-30" id='dgmf6'>In case of non approval of your application by our committee members your earnest amount of Rs. 50,000/-  (Rupees Fifty Thousand) will be refunded  at; </p>
                                       <p class="pl-30 pr-30 " id='dcmf6'>In case of non approval of your application by our committee members your earnest amount of Rs. 25,000/-  (Rupees Twentyfive Thousand) will be refunded  at; </p>
                                       <p class="pl-30 pr-30 districtsecretary" >In case of non approval of your application by our committee members your earnest amount of Rs. 2,500/-  (Rupees Two Thousand and Five Hundred) will be refunded  at; </p>
                                       <p class="pl-30 pr-30 districttreasurer" >In case of non approval of your application by our committee members your earnest amount of Rs. 2,500/-  (Rupees Two Thousand and Five Hundred) will be refunded  at; </p>
                                       <p class="pl-30 pr-30 districtzonalchairman" >In case of non approval of your application by our committee members your earnest amount of Rs. 2,500/-  (Rupees Two Thousand and Five Hundred) will be refunded  at; </p>
                                       <p class="pl-30"><b>Please prove us your bank account details  </b></p>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b ml-30">Name of the Bank </p>
                                          <input class="floating-inputpcc mlw"  name='refundbank' type="text" placeholder="">
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2 mr-30" >
                                          <p class="inputpcc_b" >Name of the Account Holder</p>
                                          <input class="floating-inputpcc" type="text" name='refundaccname' placeholder=" ">
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b ml-30">Account No </p>
                                          <input class="floating-inputpcc mlw" type="number" name='refundaccountno' id='refundaccountno' placeholder=" ">
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2 mr-30">
                                          <p class="inputpcc_b" >Re-enter Account No </p>
                                          <input class="floating-inputpcc" type="number" name='rerefundaccountno' placeholder=" ">
                                          <span class="highlight"></span>
                                       </div>
                                    </div>


                                    <div class="col-md-6">
                                       <div class="floating-label2">
                                          <p class="inputpcc_b ml-30">IFS Code  </p>
                                          <input class="floating-inputpcc mlw"  type="text" name='refundifsccode' placeholder=" ">
                                          <span class="highlight"></span>
                                       </div>
                                    </div>

                                    <div class="col-md-6">
                                       <div class="floating-label2 mr-30">
                                          <p class="inputpcc_b">Account Type </p>
                                          <select class="floating-selectpcc1" name='refundaccounttype' onclick="this.setAttribute('value', this.value);" value="">
                                             <option value=""></option>
                                             <option value="Current">Current</option>
                                             <option value="Saving">Saving</option>
                                          </select>
                                          <span class="highlight"></span>
                                       </div>
                                    </div>


                                    <div class="col-md-12 mt-40 sccmh41">
                                       <h4 class="inputpcc_b" class="sccmh4">General Information & Self Declaration</h4>
                                       <p></p>
                                       <p class="sccmp">This application can be submitted only through our website, until the last date of submission as mentioned above in this Form. </p>
                                       <p  class="sccmp">
                                          The cost of Application is Free.  
                                       </p>
                                       <p id="dgmf4" class="sccmp">
                                          Membership Fees for District Governor is Rs. 2,00,000/- (Rupees Two Hundred Thousand Only) 
                                       </p>
                                       <p id="dgmf5"  class="sccmp">
                                          Earnest fees for the application is Rs. 50,000/- (Rupees Fifty Thousand Only) 
                                       </p>
                                       <p id="dcmf4"   class="sccmp">
                                       The Earnest amount shall be only be paid through Bank transfer in favor of “Plus Circle”. Indian Bank, Account No. 6930522630 IFS Code. IDIB000Y004. 
                                       </p>
                                       <p id="dcmf5"   class="sccmp">
                                          Application without the Earnest amount shall be rejected. The Earnest amount shall form part of Application Fees in case of a successful approval of your application and the said earnest amount shall not be refunded if your application is approved.
                                       </p>
                                       <p  class="sccmp">
                                          In case of unsuccessful Application, the Earnest amount shall be refunded without interest.
                                       </p>
                                       <p  class="sccmp">
                                          Interested applicant may apply this application online from the Plus Circle website <a href="www.pluscircleclub.org" >www.pluscircleclub.org</a></p>
                                       <p  class="sccmp">
                                          The applicant who is not found suitable shall not be eligible for Video Call meet and personally meet with our International Core Committee Members (ICCM) of Plus Circle
                                       </p>
                                       <p  class="sccmp"> 
                                          The ICCM of Plus Circle reserves the right to accept or reject any or all Applicant without assigning any reason.
                                       </p>
                                       <p  class="sccmp">
                                          The applicant who is not found suitable shall not be eligible for Video Call meet and personal meet with our Core International Committee Members (CICM) of Plus Circle  
                                       </p>
                                       <p  class="sccmp">
                                          The ICCM of Plus Circle reserves the right to accept or reject any or all Applicant without assigning any reason.
                                       </p>
                                       <p  class="sccmp">
                                          The appointment for the designated designation Period shall be “ Maximum period of 2 years" subject to renewable for every 2 years to the maximum of 3 terms based upon the results of the election of their District committee members or selection of the ICCM.
                                       </p>
                                       <p  class="sccmp">
                                          The appointed member will have the right to resign from the designated designation by giving three months advance notice to ICCM.
                                       </p>
                                       <p  class="sccmp">
                                          The successful Appoint shall execute the applicant appointment (agreement) with all the terms & conditions mentioned in the draft agreement approved by the ICCM, which shall be provided post-approval of the application.
                                       </p>
                                       <p  class="sccmp">
                                          Role and Responsibilities of the Applicant shall be informed after the approval by the ICCM of Plus Circle
                                       </p>
                                       <p  class="sccmp">
                                          The applicant should abide by all the rules and regulations and strictly follow the by-lays of Plus Circle furthermore support and participate actively in all the events and activities conducted by Plus Circle in their district / nationally / internationally.
                                       </p>
                                       <p  class="sccmp">
                                          Declaration I/We hereby declare that I/we am/are furnishing the above information to the best of my/our knowledge to be true. I /We also agree to abide by the terms & conditions of Plus Circle without any precondition.
                                       </p>
                                       <p  class="sccmp">
                                          In case the Plus Circle finds that any of the information furnished by me is false at a later date, I/we am/are liable for the action being initiated by Plus Circle, against me/us including cancellation of my/our Application / Membership without giving any notice.
                                       </p>
                                       <p>
                                          <span class="inputpcc1"><input  class="floating-inputcheck" id='termsncondition' name='termsncondition' type="checkbox" placeholder=" "></span>
                                          <span>
                                             I, agree to the 
                                             <a href="{{ route('terms_of_use') }}">
                                                <!-- <a  data-target="#myModalDC" data-toggle="modal" href="#myModalDC"> -->
                                                terms and condition 
                                             </a>
                                             of Plus Circle
                                          </span>
                                       </p>
                                    </div>

                                    <div class="col-md-3"></div>

                                    <div class="col-md-3">
                                       <div class="submit-btn">
                                          <input class="reset" type="submit" value="Reset " name="">
                                       </div>
                                    </div>

                                    <div class="col-md-3">
                                       <div class="submit-btn">
                                          <input type="submit" value="submit" id='applicationsubmit' name="">
                                       </div>
                                    </div>
                                 </div>
                              </div></span></p></span></p>

                           </div>

                        </form>
                     </div>
                  </div>
               </div>
       
  
               <div  id="DistrictGovernorModal"class="col-md-1 height-105" style="display:none; " ><a  data-target="#myModalfixedgovernor" data-toggle="modal" href="#myModalfixedgovernor"class="Div1"> <p class="sscmview">View <br>Your <br>Hierarchy</p></a></div>
               <div  id="DistrictChairmanModal"class="col-md-1 height-105" style="display:none; " ><a  data-target="#myModalfixedchairman" data-toggle="modal" href="#myModalfixedchairman"class="Div1"> <p class="sscmview">View<br> Your <br>Hierarchy</p></a></div>
               <div  id="DistrictSecretaryModal"class="col-md-1 height-105" style="display:none; " ><a  data-target="#myModalDistrictSecretary" data-toggle="modal" href="#myModalDistrictSecretary"class="Div1"> <p class="sscmview">View<br> Your <br>Hierarchy</p></a></div>
               <div  id="DistrictTreasurerModal"class="col-md-1 height-105" style="display:none; " ><a  data-target="#myModalDistrictTreasurer" data-toggle="modal" href="#myModalDistrictTreasurer"class="Div1"> <p class="sscmview">View<br> Your <br>Hierarchy</p></a></div>
               <div  id="DistrictZonalChairmanModal"class="col-md-1 height-105" style="display:none; " ><a  data-target="#myModalDistrictZonalChairman" data-toggle="modal" href="#myModalDistrictZonalChairman"class="Div1"> <p class="sscmview">View<br> Your <br>Hierarchy</p></a></div>
               <div  id="DistrictZonalCricketModal"class="col-md-1 height-105" style="display:none; " ><a  data-target="#myModalDistrictZonalCricket" data-toggle="modal" href="#myModalDistrictZonalCricket"class="Div1"> <p class="sscmview">View<br> Your <br>Hierarchy</p></a></div>
               <div  id="forallstate"class="col-md-1 height-105" style="display:none; "><a  data-target="#myModalfixedallstates" data-toggle="modal" href="#myModalfixedallstates"class="Div1"> <p class="allstate">View<br> Membership <br> Fees &<br>Allowance</p></a></div>
            
            </div>
         </div>
   </section>

   <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">
					</h4>
				</div>
				<div class="modal-body">
						<div id="upload-demo" class="center-block"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="cropImageBtn" class="btn btn-primary">Crop</button>
				</div>
			</div>
		</div>
	</div>
      
      @endsection
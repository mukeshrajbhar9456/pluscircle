@extends('layouts.lp')
	@section('content')

		<section class="dashboard-main">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-3">
						<div class="dashboard-menu">
							<?php include'dashboardleftmenu.php'; ?>
							
						</div>
					</div>
					<div class="col-md-9">
						<div class="dashboard-content">
							<div class="my-referrebces-box-main">
								<div class="my-referrebces-box">
									<div id="my-referrebces-box" class="my-referrebces-box-inner">
										<h4>Members Joined, With My Reference</h4>
										<strong>15</strong>
									</div>
								</div>
								<div id="reward-points" class="my-referrebces-box">
									<div class="my-referrebces-box-inner">
										<h4>Redeem & Reward Points</h4>
										<strong>1500</strong>
									</div>
								</div>
							</div>
							<div class="dashboard-table members-joined-table">
								<div class="close-btn">
									<button id="close"><i class="far fa-times-circle"></i></button>
								</div>
								<table>
									<thead>
										<tr>
											<th width="80">S No</th>
											<th width="200">Name</th>
											<th width="150">Membership No</th>
											<th>Joining Date</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Demo</td>
											<td>123456</td>
											<td>25/07/2020</td>
										</tr>
										<tr>
											<td>2</td>
											<td>Demo</td>
											<td>123456</td>
											<td>25/07/2020</td>
										</tr>
										<tr>
											<td>3</td>
											<td>Demo</td>
											<td>123456</td>
											<td>25/07/2020</td>
										</tr>
										<tr>
											<td>4</td>
											<td>Demo</td>
											<td>123456</td>
											<td>25/07/2020</td>
										</tr>
										<tr>
											<td>5</td>
											<td>Demo</td>
											<td>123456</td>
											<td>25/07/2020</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="reward-points-box-main">
								<div class="close-btn">
									<button id="close-2"><i class="far fa-times-circle"></i></button>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="reward-points-box">
										</div>
									</div>
									<div class="col-md-4">
										<div class="reward-points-box">
										</div>
									</div>
									<div class="col-md-4">
										<div class="reward-points-box">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		
		@endsection
@extends('layouts.lp')
	@section('content')	

		<section class="login-section">
			<div class="container">

				<div class="row">
					
					<div class="col-md-12">
						<div class="login-form">
							<div class="wizard">
				               <div class="wizard-inner">
				                  <div class="connecting-line"></div>
				                  <ul class="nav nav-tabs" role="tablist">
				                     <!-- <li role="presentation" class="active">
				                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" aria-expanded="true"><span class="round-tab">1 </span> <i>Step 1</i></a>
				                     </li>
				                     <li role="presentation" class="disabled">
				                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" aria-expanded="false"><span class="round-tab">2</span> <i>Step 2</i></a>
				                     </li> -->
				                  </ul>
				               </div>
				               <form role="form" method="POST" action="" name="loginForm" class="login-form">
							   		
							   		<div class="tab-content" id="main_form">
				                     <div class="tab-pane active" role="tabpanel" >
				                     	<div class="form-title">
				                     		<!-- <h4>Enter your mobile number or email id to login</h4> -->
				                     			<h4>Reset your Password</h4>
				                     	</div>

				                     		<div class="col-md-4 center"></div>
				                     	<div class="col-md-4 center">
					                         <p class="accept2">Enter your email and we'll send you instructions on <br>how to reset your password</p>
					                   	</div>

				                     	


										 
				                     	<div class="form-bg">
				                     		

					                        <div class="floating-form">
											    <div class="floating-label">      
												  <input class="floating-input" type="text" name='email' value="" placeholder="Enter E-mail id ">
												  <span class="highlight"></span>
											     
												 </div>
											</div>
											
											
											
					                        <div class="submit-btn">
												<input type="submit" name="reset"  value="Send me reset instructions">
											</div>
											<div class="new-user">
												<a href="">Join as a new member</a>
											</div>
										</div>
									</div>
								</div>
							</form>
				                    
				                     
				            </div>
						</div>
					</div>
				</div>
			</div>
		</section>

		@endsection

		
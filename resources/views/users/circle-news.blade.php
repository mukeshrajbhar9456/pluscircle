@extends('layouts.lp')
	@section('content')
		<section class="dashboard-main">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-3">
						<div class="dashboard-menu">
							<?php include'dashboardleftmenu.php'; ?>
							
						</div>
					</div>

					<div class="col-md-9">
						<div class="dashboard-content">
							<div class="dashboard-profile dashboard-user-profile shadowcn">
								<form id="#" action="#" >
									<div class="profile-img-main inputpcc_b">
										<h4 >I WISH TO HOST AN EVENT</h4>
									</div>
									<div class="floating-form" >
										<div class="row">
											<div class="col-md-12"><hr></div>
											<div class="col-md-6">
									    		<div class="floating-label"> 
									    			<p>Event Type</p>   
									    		 	<select id="eventother" class="floating-select2" onclick="this.setAttribute('value', this.value);" >   
									    				<option value=""></option>
									    				<option value="">Drama</option>



														<option value="">Car Rally
														</option><option value="">	Badminton (SPORTS)
														</option><option value="">	Barbeque Fest
														</option><option value="">	Business Meet Brunch 
														</option><option value="">	Social Gathering Lunch 
														</option><option value="">	Cricket (Sports)
														</option><option value="">	Conclave 
														</option><option value="">	Tech Talks 
														</option><option value="">	Tech Meet 
														</option><option value="">	Tech Conference 
														</option><option value="">	Live Music Event 
														</option><option value="">Open Mic Event
														</option><option value="">	Festival
														</option><option value="">	Kids Activity
														</option><option value="">	Football (SPORTS)
														</option><option value="">	Gazal
														</option><option value="">	Friends accost
														</option><option value="">	Hiking 
														</option><option value="">	Busines Introduction Seminar
														</option><option value="">	Cycling (SPORTS)
														</option><option value="">	Stand Up Comedy 
														</option><option value="">	Dance Party
														</option><option value="">	Business Meet- Business 
														</option><option value="">	Kitty Party 
														</option><option value="">	Camping 
														</option><option value="">	Housie (INDOOR GAME)
														</option><option value="">	Picnic
														</option><option value="">	Lawn Tennis (SPORTS)
														</option><option value="">	Dance Event
														</option><option value="">	Beer Feast 
														</option><option value="">	Movie 
														</option><option value="">	Health & Fitness 
														</option><option value="">	Marathon 
														</option><option value="">	Swimming (SPORTS)
														</option><option value="">	Volleyball (SPORTS)
														</option><option value="">	Cultural Show 
														</option><option value="">	Food Fest
														</option><option value="">	Blood Donation 
														</option><option value="">	Career and Business Accost
														</option><option value="">	Wine Feast
														</option><option value="">	Bonfire Camp 
														</option><option value="">	Fancy Dress
														</option><option value="">	Karaoke night
														</option><option value="">	Barn Dance 
														</option><option value="">	Community Cleanup 
														</option><option value="">International Women day 
														</option><option value="">	Quiz Competition </option>
														<option value="Other">Other</option>
									    			</select>	

									    				<input style="display: none;"  class="floating-input2" type="text" placeholder=" " >


											      	<span class="highlight"></span>
											     </div>
									    	</div>

									    	<div class="col-md-6">
									    		<div class="floating-label">   
											    	 <p>Limit of the guests</p> 
											    	 <div class="row">
											    	 	<div class="col-md-6">
													     <input class="floating-input2 mb-16" type="number" placeholder="Min
													     "type="number" min="1" max="10 ">
													 	</div>		

												     	<div class="col-md-6"> <input class="floating-input2"  placeholder="Max"
												     	type="number" min="1" max="10 ">
												     	</div>
												    </div>
												    <span class="highlight"></span>
											      
											    </div>
									    	</div>

									    	<div class="col-md-6">
									    		<div class="floating-label">   
											    	<p>From Date</p>   
											      	<input class="floating-input2" type="date" placeholder=" "  id="bday" onchange="submitBday()">
											      	<span class="highlight"></span>
											    </div>
									    	</div>


									    	<div class="col-md-6">
									    		<div class="floating-label">   
											    	<p>To Date</p>   
											      	<input class="floating-input2" type="date" placeholder=" "  id="bday" onchange="submitBday()">
											      	<span class="highlight"></span>
											    </div>
									    	</div>

									    	<div class="col-md-6">
									    		<div class="floating-label">
									    			<p>Starting time of the event :</p>
									    			<input class="floating-input2" type="time" placeholder="From"type="number" min="1" max="10 ">
											    </div>
									    	</div>
									    

									    	<div class="col-md-6">
									    		<div class="floating-label">
									    			<p>  Ending time of the event :</p>
									    			<input class="floating-input2" type="time" placeholder="To" type="number" min="1" max="10 ">
											    </div>
									    	</div>

									    	<div class="col-md-12">
									    		<div class="floating-label">
									    			<p>Address of the Venue <img src="{{asset('public/images/gps_icon.png')}}" height="17px;"> </p>      
											       	<input class="floating-input2" type="text" placeholder=" " >
											     	<span class="highlight"></span> 
											    </div>
									    	</div>

									    	<div class="col-md-6">
									    		<div class="floating-label">
									    			<p>Country</p>     
									    			<select class="floating-select2" onclick="this.setAttribute('value', this.value);" value="">
									    				<option value=""></option>
									    				<option value="India">India</option>
									    			</select>	    
											      	<span class="highlight"></span>
											    </div>
									    	</div>

									    	<div class="col-md-6">
									    		<div class="floating-label">
									    		 	<p>State</p>     
									    			<select class="floating-select2" onclick="this.setAttribute('value', this.value);" value="">
									    				<option value=""></option>
									    				<option value="MH">MH</option>
									    				<option value="MP">MP</option>
									    			</select>	    
											     	<span class="highlight"></span>
											    </div>
									    	</div>

									    	<div class="col-md-6">
									    		<div class="floating-label">  
									    			<p>City</p>  
									    			<select class="floating-select2" onclick="this.setAttribute('value', this.value);"  value="">
									    				<option value=""></option>
									    				<option value="Mumbai">Mumbai</option>
									    			</select>	    
											      <span class="highlight"></span>
											    </div>
									    	</div>

									    	<div class="col-md-6">
									    		<div class="floating-label"> 
									    			<p>Pincode</p>       
											      	<input class="floating-input2" type="number" placeholder="">
											      	<span class="highlight"></span>
											    </div>
									    	</div>

									    	<div class="col-md-6">
									    	 	<div class="floating-label">   
											    	<p>Limit of the Age</p> 
											    	<div class="row">
													    <div class="col-md-6">
													     	<input class="floating-input2 mb-16" type="number" placeholder="Min"type="number" min="1" max="10 "></div>		

												     	<div class="col-md-6"> 
												     		<input class="floating-input2"  placeholder="Max"type="number" min="1" max="10 ">
												     	</div>
												    </div>
											    </div>
									    	</div>

									    	<div class="col-md-6">
									    		<div class="floating-label">
											    	<p>Ticket Price</p>
											    	<input class="floating-input2" type="number" min="1" max="10 ">
											    	<span class="highlight"></span>
											    </div>
									    	</div>


									    	<div class="col-md-6">
									    		<div class="floating-label">
									    			<p>Name to be displayed in event banner</p>
											     	<input class="floating-input2" type="text"  min="1" max="10 ">
											     	<span class="highlight"></span>
											    </div>
									    	</div>

									    	<div class="col-md-6">
									    		<div class="floating-label">
									    			<p>Mobile to be displayed in event banner</p>
											     	<input class="floating-input2" type="number">
											     	<span class="highlight"></span>
											    </div>
									    	</div>


									    	<div class="col-md-6">
									    		<div class="floating-label">
									    			<p>Email to be displayed in event banner<br>&nbsp;</p>
											     	<input class="floating-input2" type="email">    
											     	<span class="highlight"></span>
											    </div>
									    	</div>

											<div class="col-md-6">
									    		<div class="floating-label">
									    			<p>Contribution towards the social cause(Min amount accepted per ticket Rs.10) </p> 
									    			<input class="floating-input2" type="text" placeholder=" " >
											      	<span class="highlight"></span>
											     </div>
									    	</div>

									    	<div class="col-md-12">
									    		<div class="floating-label">
										    		<p>Describe the event in 60 words</p>  
												    <textarea class="floating-input4" type="text" placeholder=" "></textarea>
												    <span class="highlight"></span>
											    </div>
									    	</div>

									    	<div class="col-md-12">
									    		<div class="floating-label">   
										    		<p>Describe the facilities of the event in 60 words</p>
										    		<textarea class="floating-input4" type="text" placeholder=" "></textarea>
										    		<span class="highlight"></span>
											    </div>
									    	</div>

									   		 <div class="col-md-12">
									   		 	<div class="dashboard-note1">
									   		 		<p>Organiser Contact details:</p> 
									   		 		<textarea placeholder="Please Note"></textarea>
									    		</div>
									   		 </div>


									   		 <div class="col-md-12">
									    		<div class="submit-btn">
													<input type="submit" value="Preview Our Event" name="">
												</div>
									    	</div>
									    	
									    	<div class="col-md-12">
									    		<div class="submit-btn">
													<input type="submit" value="Pay Now INR 101" name="">
												</div>
									    	</div>
									    </div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	

		
		
		@endsection


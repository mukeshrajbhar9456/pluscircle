@extends('layouts.lp')
	@section('content')	
		<section class="cms-page-main">
			<div class="container mb-28">
				<div class="about-us-title">
					<p><a href="{{ url('/') }}">Home</a> / Events</p><br>
					<h4 class="ybh">Events</h4>
					
					<hr>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="cms-content">
							<p>We at Plus Circle understand the power of networking and the need for socializing.</p><p>

							We organize various events of a small and big gathering in different venues and locations. At the same time, we also provide an equal pulpit to our respective members to grow together. </p><p>

							We conduct various events; Sports, Lunch & Dinner get-together, Dance party, Social gathering to meet and accost new people, Business meets, Cultural and other shows, family meet with fun games and many more.  </p><p>

							Live and virtually; getting societies composed to shape communities beautiful.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		@endsection
@extends('layouts.lp')
	@section('content')

		<section class="dashboard-main">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="dashboard-menu">
							<?php include'dashboardleftmenu.php'; ?>
						</div>
					</div>
					<div class="col-md-9">
						<div class="dashboard-content">
							<div class="dashboard-title">
								<h4>My Contribution</h4>
							</div>
							<div class="dashboard-table">
								<table>
									<thead>
										<tr>
											<th>S No</th>
											<th>Charity Name</th>
											<th>Contribution Amount</th>
											<th>Transaction No</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Demo</td>
											<td>123456</td>
											<td>123456</td>
											<td>22/07/2020</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		
		@endsection
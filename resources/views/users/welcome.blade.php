
@extends('layouts.lp')
	@section('content')
		<section class="about-plus-circle-main">
			<div class="about-plus-circle">

				<div class="about-plus-box about-img-1">
					<a href="{{ route('about_us_1')}}">
						<div class="about-plus-box-img">
							<img src="{{ asset('public/images/sec-c/sectionA_1.jpg')}}"  class="responsive">
							<div class="about-plus-box-content">
								<h3>Enlarge<br>Your<br>Social<br>Circle<br>Globally</h3>
							</div>
						</div>
					</a>
				</div>
				<div class="about-plus-box about-img-2">
					<a href="{{ route('about_us_2')}}">
						<div class="about-plus-box-img">
							<img src="{{ asset('public/images/sec-c/sectionA_2.jpg')}}"  class="responsive">
							<div class="about-plus-box-content">
								<h3>Unlock<br>Your<br>Opportunity</h3>
							</div>
						</div>
					</a>
				</div>
				<div class="about-plus-box about-img-3">
					<a href="{{ route('about_us_3')}}">
						<div class="about-plus-box-img">
							<img src="{{ asset('public/images/sec-c/sectionA_3.jpg')}}"  class="responsive">
							<div class="about-plus-box-content">
								<h3>Empowering<br>Communities</h3>
							</div>
						</div>
					</a>
				</div>
<div>
    <a href="https://api.whatsapp.com/send?phone=+919513072088&amp;text=.&amp;source=&amp;data=" target="_blank">
    <img src="{{ asset('public/images/what.png') }}" class="what-img-2" width="80px">
    </a>
</div>
			</div>
		</section>

		<section class="banner-slider-main">
			<div class="owl-slider">
				<div id="carousel" class="owl-carousel">
					<div class="item container1">
						<a href="{{ route('Social_Networking')}}"><img class="owl-lazy imageeff" data-src="{{ asset('public/images/sec-c/networking1.jpg')}}"  alt=""></a>
						 
					</div>

					<div class="item container1">
						<a href="{{ route('why_pluscircle')}}"><img class="owl-lazy imageeff" data-src="{{ asset('public/images/sec-c/your_benefit1.jpg')}}" alt=""></a>

						
					</div>

					<div class="item container1">
						<a href="{{ route('Business_Opportunity')}}"><img class="owl-lazy imageeff" data-src="{{ asset('public/images/sec-c/business_opp1.jpg')}}" alt=""></a>
						
					</div>

					<div class="item container1">
						<a href="{{ route('Health_Insurance')}}"><img class="owl-lazy imageeff" data-src="{{ asset('public/images/sec-c/health_insurance1.jpg')}}" alt=""></a>
						 
					</div>

					<div class="item container1">
						<a href="{{ route('Opportunities_professionals')}}"><img class="owl-lazy imageeff" data-src="{{ asset('public/images/sec-c/professional_opp1.jpg')}}" alt=""></a>
						
					</div>

					<div class="item container1">
						<a href="{{ route('Supporting_Hand')}}"><img class="owl-lazy imageeff" data-src="{{ asset('public/images/sec-c/supporting_hands1.jpg')}}" alt=""></a>
						
					</div>

					<div class="item container1">
						<a href="{{ route('Scholarships_Programs')}}"><img class="owl-lazy imageeff" data-src="{{ asset('public/images/sec-c/scholarship1.jpg')}}" alt=""></a>
						
					</div>

					<div class="item container1">
						<a href="{{ route('Emergency_Support')}}"><img class="owl-lazy imageeff" data-src="{{ asset('public/images/sec-c/emergency_support1.jpg')}}" alt=""></a>
						
					</div>
					


				</div>
			</div>
		</section>

		<section class="video-img-section">
			<div class="video-img-box-main">
				<div class="video-img-box">
					<!-- 	<div class="video-main">
						<iframe src="{{ asset('public/images/videos/Old_Age_Video.mp4') }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>		
					</div> -->
						<div class="images-main">
						<a data-target="#myModalvideo4" data-toggle="modal" href="#myModalvideo4">
							<img src="{{ asset('public/images/orphaned.jpg')}}">
					
						</a>
					</div>
				</div>
				<div class="video-img-box">
					<div class="images-main">
						<img src="{{ asset('public/images/sec-c/c1.jpg')}}">
					</div>
				</div>
			</div>
		</section>

		<section class="video-img-section">
			<div class="video-img-box-main">
				<div class="video-img-box">
					<div class="images-main ">
						<img src="{{ asset('public/images/sec-c/d1.jpg')}}">
					</div>
				</div>	
				<div class="video-img-box">
				
					<!-- <div class="video-main">
						<iframe id="video1" src="{{ asset('public/images/videos/Why_Video.mp4') }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>		
					</div> -->
					<div class="images-main">
					<!-- 	<a class="popup-youtube" href="https://pluscircleclub.org/public/images/videos/Why_Video.mp4" id="playvideo"> -->
						<a data-target="#myModalvideo2" data-toggle="modal" href="#myModalvideo2">
							<img src="{{ asset('public/images/video-img.jpg')}}" >
						</a>
					</div>
				</div>
			</div>
		</section>

		<section class="activity-img-section activity1">
			<div class="container " >
				<div class="activity-img-box-main">
					<div class="activity-img-box">
						<div class="activity-img-box-inner container1">
							<a href="{{ route('event_activites_one',['event',''])}}"><img src="{{ asset('public/images/sec-e/events1.jpg')}}" class="imageeff">
							 <div class="middle">
							    <div class="text">Events with Elegence</div>
							  </div>
							</a>
						</div>
					</div>

					<div class="activity-img-box">
						<div class="activity-img-box-inner container1">
							<a href="{{ route('event_activites_three',['event',''])}}"><img src="{{ asset('public/images/sec-e/events2.jpg')}}"class="imageeff">
							 <div class="middle">
							    <div class="text">Socialize Just What You Needed </div>
							  </div>
							</a>
						</div>
					</div>

					<div class="activity-img-box">
						<div class="activity-img-box-inner container1">
							<a href="{{ route('event_activites_five',['event',''])}}"><img src="{{ asset('public/images/sec-e/events3.jpg')}}" class="imageeff">
							 <div class="middle">
							    <div class="text">Adorn your Life with Fun</div>
							  </div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</section>


        @endsection
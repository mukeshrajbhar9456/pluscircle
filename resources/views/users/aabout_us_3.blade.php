@extends('layouts.lp')
	@section('content')	
		<section class="about-us-head-main">
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="{{ url('/') }}">Home</a> / Empowering Communities</p><br>
					<h4>Empowering Communities</h4>
					<hr>
				</div>

				<div class="about-us-head">
					<div class="about-us-img-main mb-40">
						<div class="about-us-img-inner" >
							<div class="about-us-img mb-100">
								<img src="{{asset('public/images/h8.jpg')}}">
							</div>
							
						</div>

						<p>Community empowerment enables people to play an active role in the decisions that affect our environment. 
						
						</p><p>Through empowerment, people are able to control the factors and decisions that define their lives. By increasing their assets and building their capacities, they can therefore broaden their networks.
						
						</p><p>In order to empower a community, our organisation’s members  are involved in various activities and develop campaigns to empower the sensitizing people who are in vulnerable positions or who are potentially weak in the social structure.

						</p><p>We believe when a community is empowered, people feel free to act within the society and at the same time associate a sense of belonging to it. Through community participation and capacity-building, they rediscover their own potential and gain confidence. They also feel worthy of the community, for the help they are providing – to make a change.

						</p>
					</div>
				</div>
			</div>
		</section>

		@endsection
@extends('layouts.lp')
	@section('content')	
	
		<section class="login-section">
			<div class="container">
				<div class="row">
					<div class="col-md-6 offset-md-3">
						<div class="login-form">
							 <form  id="signupForm" method="POST" name="signupForm" action="register "> 
							<!--<form  id="signupForm" name="signupForm" >-->
							{{ csrf_field() }}
							<!-- 'name','title','email','password','phone','role','gender','profile','ccode','ccountry','cstate','ccity','isactive','membershiptype','paymenttype','membership','refferedby' -->
							<!-- 	  <input type="hidden" name="_token" value="owoCYQIE17FoveSRmTFRTumCvO7Wx6hTt1E4FLMu"> -->   
								<div class="form-title">
									<h4>Sign up as new member</h4>
								</div>

								 <div class="col-md-12 center">
								 
					                 <div class="alert1 alert-danger1 ">
					                     @if($errors->any())
					                     {{ implode(' ', $errors->all(':message')) }}
					     <!--                &#x26A0; -->
									 <!--Sorry! Entered mobile or email id is already exist. -->
					     <!--            	Register with another mobile or email id. Else retrieve your password by entering same credentials.-->
					                 	@endif
					                 </div>
					             </div>
								<div class="form-bg">
									<div class="floating-form">

									<div class="gender-name">
                                        <div class="gender-box">
                                            <div class="floating-label">  
                                                <select class="floating-select" name="title" id="title" >
												@if( old("title") == 'Mr.')
                                                    <option value="Mr." selected>Mr.</option>
												@else
													<option value="Mr.">Mr.</option>
												@endif
												@if( old("title") == 'Mr.')
													<option value="Ms." selected>Ms.</option>
												@else
													<option value="Ms.">Ms.</option>
												@endif
												@if( old("title") == 'Mr.')
													<option value="Mrs." selected>Mrs.</option>
												@else
													<option value="Mrs.">Mrs.</option>
												@endif                                                   
                                                    
                                                </select>
                                                <span class="highlight"></span>
                                            <!--     <label>Gender</label> -->
                                            </div>
                                        </div>
                                        <div class="name-box">
                                            <div class="floating-label">      
                                              <input class="floating-input @error('name') is-invalid @enderror" type="text" name="name" value="{!! old('name') !!}" id="name" placeholder="Enter name">
                                              <span class="highlight"></span>
                                            <!--   <label>Enter name <span class="astrick">*</span></label> -->
												<label id="name-error" class="error" for="name"></label>
											</div>
                                        </div>
									</div>
									
									<div class="floating-label"> 									
										
										<select class="floating-select" name="ccountry" id="country_id">
											<option value="">Select Country</option>
											@foreach($country as $ctr )
												@if( old("ccountry") == $ctr->id)
													<option value="{{ $ctr->id }}" selected >{{ $ctr->name }}</option>
												@else
													@if($ctr->id == 101)
														<option value="{{ $ctr->id }}" selected>{{ $ctr->name }}</option>
													@else
														<option value="{{ $ctr->id }}" >{{ $ctr->name }}</option>
													@endif
												@endif
											@endforeach										
										</select>
										<span class="highlight"></span>
										<!-- <label>Title</label> -->
										</div>
										
										
										<div class="gender-name">
											<div class="gender-box">
												<div class="floating-label"> 												
													<select class="floating-select" name="phonecode" id="phonecode" >														
														@foreach($countrycode as $cc)
															@if( old("phonecode") == $cc->phonecode)														
																<option value="{{ $cc->phonecode }}" selected>{{ $cc->phonecode }}</option> 
															@else
																@if($cc->phonecode==91)
																	<option value="{{ $cc->phonecode }}" selected>{{ $cc->phonecode }}</option> 
																@else
																	<option value="{{ $cc->phonecode }}">{{ $cc->phonecode }}</option> 
																
																@endif
															@endif
														@endforeach
													</select>
													<span class="highlight"></span>
												<!--     <label>Gender</label> -->
												</div>
											</div>
											<div class="name-box">
												<div class="floating-label">      
												<input class="floating-input @error('phone') is-invalid @enderror" value="{!! old('phone') !!}" type="text" name="phone" maxlength="10"placeholder="Enter mobile number * " id="phone">
												<span class="highlight"></span>
											<!-- 	<label>Enter mobile number <span class="astrick">*</span></label> -->
													
														<label id="phone-error" class="error" for="phone"></label>
													
												</div>
											</div>
										</div>

										<!-- <div class="floating-label mobile-div">      
									    	<input class="floating-input @error('phone') is-invalid @enderror" value="{{ old('phone') }}" type="text" name="phone" maxlength="10"placeholder=" " id="phone">
											<span class="highlight"></span>
											<label>Enter mobile number <span class="astrick">*</span></label>
												@error('phone')
													<label id="mobile-error" class="error" for="phone">{{ $message }}</label>
												@enderror
									    </div> -->

									    <!-- <div class="floating-label email-div" >       -->
									    <div class="floating-label " >      
											<input class="floating-input @error('email') is-invalid @enderror" type="text" value="{!! old('email') !!}" name="email" id="email" placeholder="Enter email address * ">
											<span class="highlight"></span>
											<!-- <label>Enter email address <span class="astrick">*</span></label> -->
											
												<label id="email-error" class="error" for="email"></label>
											
										 </div>

										 

										<div class="floating-label">      
									    	<input class="floating-input @error('password') is-invalid @enderror"  name="password" type="password" id="password"placeholder="Enter password *">
									    	<span class="highlight"></span>									    	
										<!-- 	<label>Enter password <span class="astrick">*</span></label> -->
											
												<label id="password-error" class="error" for="email"></label>
											
									    </div>
									    <div class="floating-label">      
									    	<input class="floating-input" type="password" name="password_confirmation" id="password-confirm" placeholder="Re-enter password *">
									    	<span class="highlight"></span>
									    	<!-- <label>Re-enter password <span class="astrick">*</span></label> -->
									    	
									    </div>
									    <div class="floating-label">      
									    	<input class="floating-input" type="text" name="refferedby"  placeholder="Referred By " value="{!! old('refferedby') !!}" id="refferedby">
									    	<span class="highlight"></span>
									    	
									    	    <label id="refferedby-error" class="erroracceptu" for="for_refferedby"></label>
									       
									    	<!-- <label>Referred By</label> -->
									    </div>

									      <div class="floating-label">      
                                        <input type="checkbox" name="terms" id="terms" {!! old("terms") == 'on' ? 'checked' : '' !!} >
                                        <span for="termcondition" style="font-size:14px;">I accept the <a href="{{ route('terms_of_services') }}" class="accept1">terms & conditions</a></span>
											
											
												<label id="terms-error" class="erroracceptu" for="terms"></label>
											
                                        
                                    </div> 	
									</div>
									<div class="submit-btn mt-20">
										 <input type="submit" value="Submit" id="signupFormBtn" name="submit">
									</div>
									<div id='loader' class='text-center' style="display:none;">
										<img class="gifplayer" src="{{ asset('public/images/sample.png')}}"
											style="width: 10%;"
											data-mode="video"
											data-mp4="{{ asset('public/images/ajax-loader.gif') }}" />

									  <div class="new-user">
                                        <a href="{{ route('user_login') }}">I'm a registered user</a>
                                    </div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>

		@endsection



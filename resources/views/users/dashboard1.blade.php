
@extends('layouts.lp')
	@section('content')

		<section class="dashboard-main">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-3">
						<div class="dashboard-menu">
							<?php include'dashboardleftmenu.php'; ?>
						</div>
					</div>
					<div class="col-md-9">
						<div class="dashboard-content">
							<div class="dashboard-profile ">
								<form id="#" action="#">
									<div class="row">
										<div class="col-md-6">	
											<div class="profile-img-main">
												<div class="profile-img-inner">
													<div class="profile-img">
														<img src="{{asset('public/images/about-plus-2.jpg')}}">
													</div>
												</div>
												<div class="profile-name mb-10">
													<h2>Hi, <i>Ram Praksh Sharma</i>, <br> Membership No. Under Process</h2>
													<p ><input type="submit" style=""name="send" class="next-step mt-40"  value="I Wish to Purchase my membership kit"></p>
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<p><a href="" ><img class="dimg" src="{{asset('public/images/share1.jpg')}}"style=""></a></p>
										</div>
									</div>

									<div class="row">		
										<div class="col-md-6">	
											<div class="dashboard-note">
												<textarea placeholder="Please Note:"></textarea>
											</div>
										</div>
										<div class="col-md-6">
											<div class="dashboard-video">
												<iframe src="https://www.youtube.com/embed/3VZFpwlXKpg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		@endsection

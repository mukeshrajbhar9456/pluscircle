@extends('layouts.lp')
   @section('content')  
      <section>
         <div class="container mb-40" >
            <div class="about-us-title ">
               <h4 class="ybh">Application for District Governor & District Chairman
               </h4>
               <hr>
            </div>

            <div class="row mb-150" >
               <div class="col-md-2 mw-13"></div>

               <div class="col-md-9 shadowpcc">
                  <div class="cms-content">
                     <div class="dashboard-profile dashboard-user-profilepcc" >
                     <form  id="passwordForm"  method="POST" name="passwordForm" action="{{ route('sccm_password') }}">
							
                     </div>
                        <input type='hidden' name='application' value='sccm' />
                           <div class="row">
                              <div class="col-md-12 sccmcountry" style="" >
                                 <div class="row">
                                    <div class="col-md-12">
                                       <h2> Application Submitted Successfully, Thank you.</h2>
                                    </div>                           
                                   
                                 </div>
                                 
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
       
  
               <div  id="dgmf6"class="col-md-1 height-105" style="display:none; " ><a  data-target="#myModalfixedgovernor" data-toggle="modal" href="#myModalfixedgovernor"class="Div1"> <p class="sscmview">View <br>Your <br>Hirearchy</p></a></div>

               <div  id="dcmf6"class="col-md-1 height-105" style="display:none; " ><a  data-target="#myModalfixedchairman" data-toggle="modal" href="#myModalfixedchairman"class="Div1"> <p class="sscmview">View<br> Your <br>Hirearchy</p></a></div>
            </div>
         </div>
   </section>
      
      @endsection
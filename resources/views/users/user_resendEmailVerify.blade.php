@extends('layouts.lp')
	@section('content')	

		<section class="login-section">
			<div class="container">

				<div class="row">
				
					<div class="col-md-12">
						<div class="login-form">
							<div class="wizard">
				               
				               <form role="form" method="POST" action="{{ route('resendemailverificationlink') }}" name="loginForm" class="login-form">
							   		{{ csrf_field() }}
									   
							   		<div class="tab-content" id="main_form">
				                     <div class="tab-pane active" role="tabpanel" >
				                     	<div class="form-title">
				                     		<!-- <h4>Enter your mobile number or email id to login</h4> -->
				                     		<h4>Didn't Receive Email to verify?<br>
				                     		<!--  Enter your E-mail ID and reset. -->
				                     		</h4>
				                     	</div>
										 <div class="col-md-12 center">
					                         <p class="accept2 center">Enter your email and we'll send you Email Verification link Again.</p>
					                     </div>
										 <div class="col-md-12 center">
											@foreach ($errors->all() as $message)
												<div class="alert1 alert-danger1 ">&#x26A0;
												{{ $message }}
												</div>	
											@endforeach
										</div>
										 <div class="col-md-12 center">
											@if (session()->has('message'))
												<div class="alert1 alert-info1">&#x2714;
													{{ session('message') }}
												</div>
											@endif
											@if (session()->has('error'))
												<div class="alert1 alert-info1">&#x26A0;
													{{ session('error') }}
												</div>
											@endif
										</div>

				                     	<div class="form-bg">
					                        <div class="floating-form">
											    <div class="floating-label">      
												  <input class="floating-input" type="text" name='email' placeholder="Enter E-mail id to reset password ">
												  <span class="highlight"></span>
											      <!-- <label>Enter mobile number or email id</label> -->
												  <!-- <label></label> -->
												  	@error('email')
														<label id="email-error" class="error" for="email">{{ $message }}</label>
													@enderror
											    </div>
											</div>
					                        <div class="submit-btn">
												<input type="submit" name="next"  value="Next">
											</div>
											<div class="new-user">
												<a href="{{ route('user_register') }}">Join as a new member</a>
											</div>
										</div>
									</div>
								</div>
							</form>
				                    
				                     
				            </div>
						</div>
					</div>
				</div>
			</div>
		</section>

		@endsection

		
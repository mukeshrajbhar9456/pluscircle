@extends('layouts.lp')
	@section('content')	
		<section class="cms-page-main">
			<div class="container mb-28">
				<div class="about-us-title">
					<p><a href="{{ url('/') }}">Home</a> / Our Mission</p><br>
					<h4 class="ybh"> Our Mission</h4>
					
					<hr>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="cms-content">
							
							<p><ul><li>	Our mission is not to take any extra amount from any members but still, they can contribute towards society.</li><li>
							We provide support to each member of our community to develop and promote their responsible and inclusive business strategy.</li><li>
							We provide opportunities for every member to stay connected with other members and get benefits from their business.</li><li>
							We provide opportunities for every member to participate in various events and make valuable connections with other members. </li><li>
							We organize different kinds of social activities for children and families in our community.</li><li>
							We provide opportunities to do socialization and socializing to members by joining our volunteering program.</li><li>
							In case of any disaster or emergency, our community will give the best support to every person and also take various efforts to help them in every possible manner.</li><li>
							We provide scholarship opportunities for students to achieve their educational goals and create a better future.</li><li>
							We provide opportunities for every member to be actively involved in the development of their locality.</li></ul>
						
						</div>
					</div>
				</div>
			</div>
		</section>
		
		@endsection
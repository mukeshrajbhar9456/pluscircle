@extends('layouts.lp')
	@section('content')	
		<section class="about-us-head-main">
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="{{ url('/') }}">Home</a> / Enlarge Your Social Circle Globally</p><br>
					<h4>Enlarge Your Social Circle Globally</h4>
					
					<hr>
				</div>

				<div class="about-us-head">
					<div class="about-us-img-main">
						<div class="about-us-img-inner">
							<div class="about-us-img">
								<img src="{{asset('public/images/sec-a/A1-1.jpg')}}">
							</div>
						</div>

						<p>We, Plus Circle believes that most connected people are often the most successful.

						<p>Our Social Networking Platform  is all about establishing and nurturing long-term, mutually beneficial relationships with the people you meet, whether you're waiting to order your morning coffee, participating in an intramural sports league, or attending a work conference.</p>

						<p>People around the world have always been looking for ways to connect and network with each other. Plus Circle provides a platform for finding and building local communities, to meet new people, learn new things, find support, and pursue their passions together.</p>
						
						<p>As a part of a strong community, we are a part of a group of people who want to help each other, whether that is socially or professionally.
						</p>
					</div>
					
				</div>
			</div>
		</section>
		
		@endsection
		
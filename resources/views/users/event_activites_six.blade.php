@extends('layouts.lp')
	@section('content')
		<section>
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="{{ url('/') }}">Home</a> / Adorn your Life with Fun</p><br>
					<h4 class="ybh"> Adorn your Life with Fun</h4>
					
					<hr>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="event-activites-head">
							<p>We believes in developing the next generation of leaders. Our programs help younger leaders build leadership skills, expand education and learn the value of service.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="event-activites-section1">
			<div class="container ">
				<div class="row mb-40" id="event11">
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/adoran/health-checkups.jpg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>	Health Checkups</h4>
								<p>Medical Camps are conducted regularly at different areas based on the necessity involving the local smithis who organise all  the necessary requirements like doctors, </p>
								<div class="learn-link1">
									<a data-target="#myModalEventsHealth" data-toggle="modal" href="#myModalEventsHealth" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/adoran/Blood-donation.jpg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Blood Donation</h4>
								<p>Blood donation is among the highest service one can render to humanity.
								With this thought, Plus Circle has organized Blood Donation Camp on</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsBlood" data-toggle="modal" href="#myModalEventsBlood" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/adoran/stand-up-comedy.jpg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Standup Comedy</h4>
								<p>Please note: Famous Stand Up Comedian will be performing the same material which he has been doing since last year but its not on Youtube, please avoid watching this 
								<div class="learn-link1">
									<a data-target="#myModalEventsStandup" data-toggle="modal" href="#myModalEventsStandup" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row mb-40" id="event12">
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/adoran/live-music.jpg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Live Music </h4>
								<p>The entertainment industry has gone into a major phase of crisis due to COVID 19. Livelihoods of Musicians and Singers are affected as all live performance are halted. 

								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsLive" data-toggle="modal" href="#myModalEventsLive" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">

						<div class="center">
						  	<div class="pagination">
							  <a href="{{ route('event_activites_five',['event','']) }}">&laquo;</a>
							  <a href="{{ route('event_activites_five',['event','']) }}">1</a>
							  <a class="active" href="{{ route('event_activites_six',['event','']) }}" >2</a>
							  <a href="{{ route('event_activites_six',['event','']) }}">&raquo;</a>
							
						 	</div>
						</div>
						
					</div>

				</div>
						
			</div>
		</section>
		
		
		@endsection
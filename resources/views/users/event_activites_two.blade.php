@extends('layouts.lp')
	@section('content')

		<section>
			<div class="container yb">
				<div class="about-us-title">
					<p><a href="{{ url('/') }}">Home</a> / Events with Elegence</p><br>
					<h4 class="ybh"> Events with Elegence</h4>
					
					<hr>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="event-activites-head">
							<p>We believes in developing the next generation of leaders. Our programs help younger leaders build leadership skills, expand education and learn the value of service.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="event-activites-section1">
			<div class="container ">
				<div class="row mb-40" id="event3">
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/events-elegance/Webinar.jpg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Webinar</h4>
								<p>The goal of ecommerce Powered By Plus Circle is to build and optimize online stores for small independent businesses and artists in just a matter of days. We try businesses get to</p>
								<div class="learn-link">

									<a data-target="#myModalEventsWebinar" data-toggle="modal" href="#myModalEventsWebinar" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/events-elegance/Business-Fair.jpg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Business Fair</h4>
								<p>The National Virtual Small Business Expo is  BIGGEST & most anticipated virtual business-to-business networking & educational event, trade show & conference for business,</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsBusiness" data-toggle="modal" href="#myModalEventsBusiness" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/events-elegance/Social-Awareness-Programs.jpg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Social Awareness Programs</h4>
								<p>We try to solve the problems of sanitation and waste management in India by ensuring hygiene in major cities. It is important to display high standards of hygiene and</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsSocial" data-toggle="modal" href="#myModalEventsSocial" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="row mb-40" id="event4">
					<div class="col-md-4">
						<div class="list-event-activites1">
							<div class="list-event-activites-img1">
								<img src="{{asset('public/images/events-elegance/Annual-Fest.jpg')}}">
							</div>
							<div class="list-event-activites-content1">
								<h4>Annual Fest</h4>
								<p>Every year, We provides a platform to a International Annual event with all our members.

								We expect about 5000 + from across World to participate in this mega event.
								</p>
								<div class="learn-link1">
									<a data-target="#myModalEventsAnnual" data-toggle="modal" href="#myModalEventsAnnual" >Learn more <i class="fas fa-long-arrow-alt-right"></i></a>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="center">
						  	<div class="pagination">
							  <a href="{{ route('event_activites_one',['event','']) }}">&laquo;</a>
							  <a href="{{ route('event_activites_one',['event','']) }}">1</a>
							  <a class="active" href="{{ route('event_activites_two',['event','']) }}" >2</a>
							  <a href="{{ route('event_activites_two',['event','']) }}">&raquo;</a>
							
						  	</div>
						</div>
						
					</div>

				</div>
						
			</div>
		</section>
		
		
		@endsection
		

@extends('layouts.lp')
	@section('content')
		<section class="about-plus-circle-main">
			<div class="about-plus-circle">
				<div class="about-plus-box about-img-1">
					<a href="{{ route('about_us_1') }}">
						<div class="about-plus-box-img">
							<img src="{{asset('public/images/h1.jpg')}}"  class="responsive">
							<div class="about-plus-box-content">
								<h3>Enlarge<br>Your<br>Social<br>Circle<br>Globally</h3>
							</div>
						</div>
					</a>
				</div>
				<div class="about-plus-box about-img-2">
					<a href="{{ route('about_us_2') }}">
						<div class="about-plus-box-img">
							<img src="{{asset('public/images/h4.jpg')}}"  class="responsive">
							<div class="about-plus-box-content">
								<h3>Unlock<br>Your<br>Opportunity</h3>
							</div>
						</div>
					</a>
				</div>
				<div class="about-plus-box about-img-3">
					<a href="{{ route('about_us_3') }}">
						<div class="about-plus-box-img">
							<img src="{{asset('public/images/h7.jpg')}}"  class="responsive">
							<div class="about-plus-box-content">
								<h3>Empowering<br>Communities</h3>
							</div>
						</div>
					</a>
				</div>
			</div>
		</section>
			
		<section class="banner-slider-main">
			<div class="owl-slider">
				<div id="carousel" class="owl-carousel">
					<div class="item container1">
						<a href="{{ route('Social_Networking') }}"><img class="owl-lazy imageeff" data-src="{{asset('public/images/h2.jpg')}}"  alt=""></a>
						 <div class="middle">
							    <div class="text" style="font-size:60px;color:#fff;">Social Networking</div>
						</div>
						

					</div>

						
					<div class="item container1">
						<a href="{{ route('why_pluscircle') }}"><img class="owl-lazy imageeff" data-src="{{asset('public/images/h13.jpg')}}" alt=""></a>

						 <div class="middle">
							    <div class="text" style="font-size:60px;color:#fff;">Your Benefits</div>
						</div>
					</div>

					<div class="item container1">
						<a href="{{ route('Business_Opportunity') }}"><img class="owl-lazy imageeff" data-src="{{asset('public/images/h11.jpg')}}" alt=""></a>
						 <div class="middle">
							    <div class="text" style="font-size:60px;color:#fff;">Business Opportunity</div>
						</div>
					</div>

					<div class="item container1">
						<a href="{{ route('Health_Insurance') }}"><img class="owl-lazy imageeff" data-src="https://cdn.pixabay.com/photo/2019/09/27/20/40/houses-4509404_960_720.jpg" alt=""></a>
						 <div class="middle">
							    <div class="text" style="font-size:60px;color:#fff;">Health Insurance</div>
						</div>
					</div>

					<div class="item container1">
						<a href="{{ route('Opportunities_professionals') }}"><img class="owl-lazy imageeff" data-src="https://cdn.pixabay.com/photo/2020/07/10/13/27/aloe-5390681_960_720.jpg" alt=""></a>
						 <div class="middle">
							    <div class="text" style="font-size:60px;color:#fff;">Opportunities for the professionals</div>
						</div>
					</div>

					<div class="item container1">
						<a href="{{ route('Supporting_Hand') }}"><img class="owl-lazy imageeff" data-src="https://cdn.pixabay.com/photo/2020/07/10/03/55/fisherman-5389426_960_720.jpg" alt=""></a>
						 <div class="middle">
							    <div class="text" style="font-size:60px;color:#fff;">Supporting Hand</div>
						</div>
					</div>

					<div class="item container1">
						<a href="{{ route('Scholarships_Programs') }}"><img class="owl-lazy imageeff" data-src="https://cdn.pixabay.com/photo/2020/07/16/16/16/nature-5411408_960_720.jpg" alt=""></a>
						 <div class="middle">
							    <div class="text" style="font-size:60px;color:#fff;">Scholarships Programs</div>
						</div>
					</div>

					<div class="item container1">
						<a href="{{ route('Emergency_Support') }}"><img class="owl-lazy imageeff" data-src="https://cdn.pixabay.com/photo/2020/07/21/10/52/girl-5425872_960_720.jpg" alt=""></a>
						 <div class="middle">
							    <div class="text" style="font-size:60px;color:#fff;">Emergency Support</div>
						</div>
					</div>

					<div class="item container1"> 
						<a href="{{ route('Health_Insurance') }}"><img class="owl-lazy imageeff" data-src="https://cdn.pixabay.com/photo/2020/07/15/01/54/beach-5406139_960_720.jpg" alt=""></a>
						 <div class="middle">
							    <div class="text" style="font-size:60px;color:#fff;">Health Insurance</div>
						</div>
					</div>

					<div class="item container1">
							<a href="{{ route('Business_Opportunity') }}"><img class="owl-lazy imageeff" data-src="https://cdn.pixabay.com/photo/2020/05/31/04/36/investment-5241253_960_720.jpg" alt=""></a>
						 <div class="middle">
							    <div class="text" style="font-size:60px;color:#fff;">Business Opportunity</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="video-img-section">
			<div class="video-img-box-main">
				<div class="video-img-box">
					<div class="video-main">
						<iframe src="https://www.youtube.com/embed/3VZFpwlXKpg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	
					</div>
				</div>
				<div class="video-img-box">
					<div class="images-main">
						<img src="{{asset('public/images/h102.jpg')}}">
					</div>
				</div>
			</div>
		</section>

		<section class="video-img-section">
			<div class="video-img-box-main">
				<div class="video-img-box">
					<div class="images-main ">
						<img src="{{asset('public/images/h91.jpg')}}">

						<!--  <div class="middle1">
							    <div class="text">Caring is not our Job it’s our Pride  </div>
							  </div> -->

					</div>
				</div>	
				<div class="video-img-box">
					<div class="video-main">
						<iframe src="https://www.youtube.com/embed/3VZFpwlXKpg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	
					</div>
				</div>
			</div>
		</section>

		<section class="activity-img-section activity1">
			<div class="container " >
				<div class="activity-img-box-main">
					<div class="activity-img-box">
						<div class="activity-img-box-inner container1">
							<a href="{{ route('event_activites_one',['event','']) }}"><img src="{{asset('public/images/about-plus-2.jpg')}}" class="imageeff">
							 <div class="middle">
							    <div class="text">Events with Elegence</div>
							  </div>
							</a>
						</div>
					</div>

					<div class="activity-img-box">
						<div class="activity-img-box-inner container1">
							<a href="{{ route('event_activites_three',['event','']) }}"><img src="{{asset('public/images/about-plus-2.jpg')}}" class="imageeff">
							 <div class="middle">
							    <div class="text">Socialize Just What You Needed </div>
							  </div>
							</a>
						</div>
					</div>

					<div class="activity-img-box">
						<div class="activity-img-box-inner container1">
							<a href="{{ route('event_activites_five',['event','']) }}"><img src="{{asset('public/images/about-plus-2.jpg')}}" class="imageeff">
							 <div class="middle">
							    <div class="text">Adorn your Life with Fun</div>
							  </div>
							</a>
						</div>
					</div>

				<!-- 	<div class="activity-img-box">
						<div class="activity-img-box-inner">
							<a href="event-activites.html"><img src="{{asset('public/images/about-plus-2.jpg')}}"></a>
						</div>
					</div>
					<div class="activity-img-box">
						<div class="activity-img-box-inner">
							<a href="event-activites.html"><img src="{{asset('public/images/about-plus-2.jpg')}}"></a>
						</div>
					</div> -->
				</div>
			</div>
		</section>

		<section>
			<div class="upcoming-slider-main upcome1">
				<div id="upcoming-activities-slider" class="owl-carousel upcome1">
					<div class="item">
						<img class="owl-lazy upcome-img" data-src="{{asset('public/images/community-img.jpg')}}" alt="">
					</div>
					<div class="item">
						<img class="owl-lazy upcome-img" data-src="{{asset('public/images/community-img.jpg')}}" alt="">
					</div>
					<div class="item">
						<img class="owl-lazy upcome-img" data-src="{{asset('public/images/community-img.jpg')}}" alt="">
					</div>
					<div class="item">
						<img class="owl-lazy upcome-img" data-src="{{asset('public/images/community-img.jpg')}}" alt="">
					</div>
				</div>
			</div>
		</section>
		@endsection


<!DOCTYPE html>

<html>
<head></head>
<body>
<div style="width:100%;">



<div style="border:0px solid #000;width:100%;margin: 0 auto;background:#c5ab7a;padding-bottom: 10px;border-radius:20px;">


 <div style="border:0px solid #000;width:90%;margin: 0 auto;"> 


<img src="https://pluscircleclub.org/public/images/email/logo.png" style="float:left;height:40px;padding-top:20px;margin-bottom: 25px;text-align: justify;">
</div>

<hr style="width:100%;border:1px solid #eae9ee;">
 <div style="border:0px solid #000;width:90%;margin: 0 auto;"> 


  <table border="0" cellpadding="0" cellspacing="0" width="100%"  style="color:#1f2b45; padding:0px;border-radius:10px;font-family: 'Montserrat', sans-serif;font-size: 17px;">
   <tr>
  <td style="padding: 15px 0 0px 0;font-family: 'Montserrat', sans-serif;line-height: 25px;">
      
 <p>Hi {{ $data['name'] }}, </p>
  </td>
 </tr>

 <tr>
  <td style="padding: 0px 0 0px 0;font-family: 'Montserrat', sans-serif;line-height: 25px;">
  <p style="text-align: justify;">Greetings!
</p> </td>
 </tr>

  <tr>
  <td style="padding: 0px 0 0px 0;font-family: 'Montserrat', sans-serif;line-height: 25px;">
  <p style="text-align: justify;">We confirm the receipt of our payment of <b>Rs. 15,000/-</b> towards the Membership Fee.
</p> </td>
 </tr>


  <tr>
  <td style="padding: 0px 0 0px 0;font-family: 'Montserrat', sans-serif;line-height: 25px;">
  <p style="text-align: justify;">Your Permanent Membership Number is <b><font style="font-size:18px;">{{ $data['mcard'] }}</font></b>
</p> </td>
 </tr>

 <tr>

  <td style="padding: 0px 0 0px 0;font-family: 'Montserrat', sans-serif;line-height: 25px;"> 
    <p style="text-align: justify; ">
Please Note: You could inform your Membership Number to your friends and family to Join Plus Circle, which shall be accounted to your account. </p>
</td>
</tr>
<td style="padding: 0px 0 0px 0;font-family: 'Montserrat', sans-serif;line-height: 25px;"> 
    <p style="text-align: justify; ">

Once again, we delightfully welcome you to our Circle.
</p> </td>
 </tr>




 <tr>
  <td style="padding: 0px 0 0px 0;font-family: 'Montserrat', sans-serif;line-height: 25px;">
  <p style="text-align: justify;">Best wishes, </p>


  </td>
 </tr>


 <tr>
  <td style="padding: 0px 0 0px 0;font-family: 'Montserrat', sans-serif;line-height: 25px;">
  <p style="text-align: justify;"> Circle Club <br>Crew </p>


  </td>
 </tr>


 <tr>
  <td style="padding: 40px 0 0px 0;font-family: 'Montserrat', sans-serif;line-height: 20px;">

    <p style="text-align: center;">
      <img src="https://pluscircleclub.org/public/images/email/wel7.png"  >
      <img src="https://pluscircleclub.org/public/images/email/wel8.png" >
      <img src="https://pluscircleclub.org/public/images/email/wel9.png"  >
       </p>
 


  </td>

</tr>


</table>

<p style="text-align: center;margin-top: 30px;color:#1f2b45;font-family: 'Montserrat', sans-serif;font-size: 13px;">
<img src="https://pluscircleclub.org/public/images/email/logo-mobile2.jpg" style="height:80px;">
</p>

<p style="text-align: center;font-weight: 500;margin-top: 0px;color:#1d170b;font-family: 'Montserrat', sans-serif;font-size: 13px;">
Unique Power of Circle
</p>

<p style="text-align: center;font-weight: 500;margin-top: 0px;color:#1d170b;font-family: 'Montserrat', sans-serif;font-size: 13px;">
www.pluscircleclub.org &nbsp; &nbsp;&nbsp;&nbsp; info@pluscircleclub.org 
</p>

<p style="text-align: center;margin-top: 50px;color:#1f2b45;font-family: 'Montserrat', sans-serif;font-size: 13px;">
Follow us on
</p>
<p style="text-align: center;margin-top: 0px;">
 <img src="https://pluscircleclub.org/public/images/email/fb1.png" style="height:20px;margin-right: 10px;">
 <img src="https://pluscircleclub.org/public/images/email/insta1.png" style="height:20px;margin-right: 15px;">
<img src="https://pluscircleclub.org/public/images/email/li1.png" style="height:20px;margin-right: 10px;">
<img src="https://pluscircleclub.org/public/images/email/twit1.png" style="height:20px;"></p>

<p style="text-align: justify;margin-top: 50px;color:#1f2b45;font-family: 'Montserrat', sans-serif;font-size: 10px;">This is an auto-generated email. Please do not reply. Approval of  your membership is sole decision of our management and subject to receipt of your payment to our account. You are receiving this email because you have authorised pluscircleclub.org to send you information about our products / services. For more information please visit www.pluscircleclub.org<br>
* Terms & Condition apply. 
</p>
<p style="text-align: center;margin-top: 10px;color:#1f2b45;font-family: 'Montserrat', sans-serif;font-size: 11px;">&#xA9; 2020 Plus Circle All rights reserved
</p>


</div>

</div>
</div>

</body>
</html>

@extends('layouts.lp')
	@section('content')
	
<style>

.floating-input2, .floating-select2 {
    padding: 6px;
}



.field-icon {
  float: right;
  margin-left: -25px;
  margin-top: -25px;
  position: relative;
  z-index: 2;
}

.container{
  padding-top:10px;
  margin: auto;
}
label.cabinet{
	display: block;
	cursor: pointer;
}

label.cabinet input.file{
	position: relative;
	height: 100%;
	width: auto;
	opacity: 0;
	-moz-opacity: 0;
  filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
  margin-top:-30px;
}
#upload-demo{
	width: 400px;
	height: 400px;
  padding-bottom:25px;
}
#upload-demo1{
	width: 400px;
	height: 400px;
  padding-bottom:25px;
}
#upload-demo2{
	width: 400px;
	height: 400px;
  padding-bottom:25px;
}
figure figcaption {
    position: absolute;
    bottom: 0;
    color: #fff;
    width: 100%;
    padding-left: 9px;
    padding-bottom: 5px;
    text-shadow: 0 0 10px #000;
}
p.allstate {
    margin-bottom: 0px;
    text-align: center;
    line-height: 17px;
    background-color: #7E5233;
    font-weight: 600;
    color: #fff;
    width: 102px;
    padding: 2px;
    border-radius: 5px;
    top: 232px;
    right: 91px;
    position: fixed;
    z-index: 999;
}
.sscmview {
    padding: 5px;
    text-align: center !important;
    font-size: 15px;
    width: 99px;
    min-height: 100px;
    background-color: #57637c;
    color: #fff1f1;
    position: fixed;
    z-index: 999;
    line-height: 20px;
    font-weight: 600;
    /* height: 5px; */
    border-radius: 5px;
    top: 315px !important;
    /* height: 13px!important; */
    /* left: 2px; */
    right: 90px;
}
label.error {
    font-size: 12px;
    color: #ff8300;
    font-weight: 700;
}
a.purchasekit {
    color: #1f2b44;
    border: 1px solid #1f2b44;
    padding: -1px;
    font-size: 15px;
    padding: 5px;
    border-radius: 5px;
    font-weight: 600;
}
a.purchasekit:hover {
    color: #1f2b44;
    border: 1px solid #1f2b44;
    padding: -1px;
    font-size: 15px;
    padding: 5px;
    border-radius: 5px;
    font-weight: 600;
	background:#c5ab7a;
}
input#userprofile {
    width: 112px;
    /* border: 1px solid red; */
    height: 100px;
    /* border-radius: 50%; */
}
.cr-viewport.cr-vp-square {
    border-radius: 50%;
}
</style>
	@include('user.usermodals')			
		<section class="dashboard-main">
			<div class="container-fluid">
				<div class="row">
				<div class="col-md-12 center">
					@if (session()->has('message'))
						<div class="alert1 alert-info1">&#x2714;
							{{ session('message') }}
						</div>
					@endif
				</div>
					<div class="col-md-3">
						<div class="dashboard-menu">
						@include('user.dashboardleftmenu')
						</div>
					</div>
					<div class="col-md-9">
						<div class="dashboard-content">
							<div class="dashboard-profile dashboard-user-profile shadowcn" >
							<form id="user profile" method='post' action="{{ route('user.update_user_profile')}}" name='sccmapplication' enctype="multipart/form-data">
                           		{{ csrf_field() }}	
									<div class="row">

										<div class="col-md-5">	
											<div class="profile-img-main" style="justify-content: left;">
												<div class="profile-img-inner">
													<div class="profile-img" id='profile_img_div'>
														<img src="{{ Auth::user()->profile?Auth::user()->profile:asset('public/images/noimg.png')}} " id='profile_img'>
													</div>
													<input type='file' id='userprofile' name="userprofile" class="userprofile" />
												
													
												</div>

												<div class="profile-name">
													<h2>Membership No. : <strong>{{ Auth::user()->tmembership?Auth::user()->tmembership:'Under Process' }}</strong></h2>
												</div>
											
											</div>
										</div>

										<div class="col-md-7">
										@if(Auth::user()->tmembership =='')
											<a class='purchasekit' data-target="#myModal" data-toggle="modal" href="#myModal">I Wish to Purchase my membership kit</a>
										@endif
											<!-- <p><input type="submit" style=""name="send" class="next-step mt-40"  value="I Wish to Purchase my membership kit"></p> -->
										</div>
									</div>

									<div class="floating-form" >
										<div class="row">
									    	<div class="col-md-12"><hr></div>
									    		<div class="col-md-6">
									    			<div class="floating-label">   
												    	<p>User Name</p>   
												      	<input class="floating-input2" type="text" name='name' value="{{ Auth::user()->name }}" placeholder=" ">
												      	<span class="highlight"></span>
												    </div>
									    		</div>

										    	<div class="col-md-6">
										    		<div class="floating-label">   
												    	<p>Date of Birth</p>   
												    	<div class="row">
													    	<div class="col-md-6">
													      		<input class="floating-input2" type="date" name="bday" id="bday" value="{{ Auth::user()->dob?Auth::user()->dob:'' }}" onchange="submitBday()">
													      	</div>

												      		<div class="col-md-6">
												      			<p id="resultBday" style="  line-height: 35px;"></p>
												     			<!--  	<input class="floating-input2" type="text" placeholder=" " > -->
												      		</div>
												      	</div>
												      	<span class="highlight"></span>
												    </div>
										    	</div>

										    	<div class="col-md-6">
										    		<div class="floating-label"> 
										    			<p>Mobile No 1</p> 
												      	<input class="floating-input2" type="number" name='phone' value="{{ Auth::user()->phone?Auth::user()->phone:'' }}" readonly>
												      	<span class="highlight"></span>
												    </div>
									    		</div>
										    	<div class="col-md-6">
										    		<div class="floating-label">  
										    			<p>Mobile No 2</p>     
												      	<input class="floating-input2" type="number" name='secondaryphone' value="{{ Auth::user()->secondaryphone?Auth::user()->secondaryphone:'' }}" placeholder=" ">
												      	<span class="highlight"></span>
												      	
												    </div>
										    	</div>
									    	
										    	<div class="col-md-6">
										    		<div class="floating-label">
										    			<p>Email Address</p>       
												     	<input class="floating-input2" type="email" name='email' value="{{ Auth::user()->email?Auth::user()->email:'' }}" readonly>
												      	<span class="highlight"></span>
													</div>
										    	</div>

										    	<div class="col-md-6">
										    		<div class="floating-label"> 
										    		 	<p>Profession </p>   
														 <input class="floating-input2" type="text" name='profession' value="{{ Auth::user()->profession?Auth::user()->profession:'' }}">
										    			<!-- <select class="floating-select2" onclick="this.setAttribute('value', this.value);" value="">
										    				<option value=""></option>
										    				<option value="profession">Profession</option>
										    			</select>	     -->
												      	<span class="highlight"></span>
												    </div>
										    	</div>

										    	<div class="col-md-6">
												    <div class="floating-label"> 
												     	<p>Country </p>   
														<select class="floating-select2" id='profile_country' name='profile_country'  >
															<option value=""></option>
															@foreach($country as $ctr )
																@if($ctr->id == 101)
																	<option value="{{ $ctr->id }}" selected >{{ $ctr->name }}</option>
																@else
																	<option value="{{ $ctr->id }}" >{{ $ctr->name }}</option>
																@endif
															@endforeach	
														</select>
														<span class="highlight"></span>
														
													</div>
										    	</div>

										    	<div class="col-md-6">
										    		<div class="floating-label">  
										    			<p>State</p>
														<select class="floating-select2" id='profile_state' name='profile_state' onclick="this.setAttribute('value', this.value);" value="">
															<option value=""></option>
															@foreach($states as $s)
																@if($s->id == Auth::user()->cstate)
																	<option value="{{ $s->id }}" class='{{ $s->country_id }} ' selected >{{ $s->name }} </option>
																@else
																	<option value=" {{$s->id }}" class='{{ $s->country_id }}' >{{ $s->name }}</option>
																@endif
															@endforeach
															
														</select>
														<span class="highlight"></span>
														
													</div>
										    	</div>

										    	<div class="col-md-6">
										    		<div class="floating-label">  
										    			<p>City</p>
														<input class="floating-input2" type="text" name='city' value="{{ Auth::user()->ccity?Auth::user()->ccity:'' }}">
														<!-- <select class="floating-select2" onclick="this.setAttribute('value', this.value);" value="">
															<option value=""></option>
															<option value="c">City</option>
														</select> -->
														<span class="highlight"></span>														
													</div>
										    	</div>

										    	<div class="col-md-6">
										    		<div class="floating-label">
										    			<p>Pincode</p>      
												      <input class="floating-input2" type="number" name='pincode' value="{{ Auth::user()->pincode?Auth::user()->pincode:'' }}">
												      <span class="highlight"></span>
												   
												    </div>
										    	</div>

										    	<div class="col-md-12">
										    		<div class="floating-label"> 
										    		<p>Address</p>        
												      <textarea class="floating-input2" type="text" name='address' >{{ Auth::user()->address?Auth::user()->address:'' }}</textarea>
												      <span class="highlight"></span>
												      
												    </div>
										    	</div>

										    	<!-- <div class="col-md-6">
										    		<div class="floating-label">  
										    		<p>Change Password</p>    
												      <input class="floating-input2" type="password" placeholder=" ">
												      <span class="highlight"></span>
												      
												    </div>
										    	</div>

										    	<div class="col-md-6">
										    		<div class="floating-label">
										    		<p>Re-confirm Password</p>       
												      <input class="floating-input2" type="password" placeholder=" ">
												      <span class="highlight"></span>
												     
												    </div>
										    	</div> -->
												<br>
												<div class="col-md-6">
										    		<div class="floating-label  kyc text-center">  
										    			<p>Choose your KYC</p>
														<select class="floating-select2" id='kyctype' name='kyctype' >
															<option value=""></option>
															@if(Auth::user()->kyctype == 'adhaarcard')
																<option value="adhaarcard" selected>Adhaar Card</option>
															@else
																<option value="adhaarcard">Adhaar Card</option>
															@endif
															@if(Auth::user()->kyctype == 'drivinglicence')
																<option value="drivinglicence" selected>Driving Licence</option>
															@else
																<option value="drivinglicence">Driving Licence</option>
															@endif
															@if(Auth::user()->kyctype == 'passport')
																<option value="passport" selected>Passport</option>
															@else
																<option value="passport">Passport</option>
															@endif
															@if(Auth::user()->kyctype == 'prc')
																<option value="prc" selected>Permanent Residence Card</option>
															@else
																<option value="prc">Permanent Residence Card</option>
															@endif
														</select>
														<span class="highlight"></span>
													</div>
										    	</div>
										    	<div class="col-md-6">
										    		<div class="floating-label kyc text-center">   
										    			<p>KYC Documents</p>     
												     	 <input class="floating-input3" type="file" name='kyc' id='kyc' >
														  
														  <img src="{{ Auth::user()->kycprofile?Auth::user()->kycprofile:asset('public/images/kyc.png')}}" id='displayImage1' class='kycimage' style='height:200px;' />
														  <input type='hidden' id='userprofilekyc' name='userprofilekyc' />
														<span class="highlight"></span>
												     
												    </div>
										    	</div>
										    	<div class="col-md-12">
										    		<div class="submit-btn">
														<input type="submit" value="Update Profile" name="">
													</div>
										    	</div>
										    </div>
									    </div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		<div class="modal fade" id="cropImagePop1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">
					</h4>
				</div>
				<div class="modal-body">
						<div id="upload-demo1" class="center-block"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="cropImageBtn1" class="btn btn-primary">Crop</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="cropImagePop2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">
					</h4>
				</div>
				<div class="modal-body">
						<div id="upload-demo2" class="center-block"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="cropImageBtn2" class="btn btn-primary">Crop</button>
				</div>
			</div>
		</div>
	</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready( function() {



                    $(document).on('change', '#userprofile', function(){

                    var name = document.getElementById("userprofile").files[0].name;
                    var form_data = new FormData();
                    var ext = name.split('.').pop().toLowerCase();
                    if(jQuery.inArray(ext, ['png','jpg','jpeg']) == -1)
                    {
                    alert("Invalid File Format (Only images allowed)");
                    }
                    var oFReader = new FileReader();
                    oFReader.readAsDataURL(document.getElementById("userprofile").files[0]);
                    var f = document.getElementById("userprofile").files[0];
                    var fsize = f.size||f.fileSize;
                    if(fsize > 2000000)
                    {
                    alert("File Size is very big (Allowed size is Max 2MB)");
                    }
                    else
                    {
                        event.preventDefault();

                    form_data.append("file", document.getElementById('userprofile').files[0]);

                    $.ajax({
                        url:"/upload-image",
                        method:"POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend:function(){
                        $('#uploaded_coverpage').html("<label class='text-dark'>Cover Page Uploading...(Fill other fields)</label>");
                        },

                        success:function(data)
                        {
                        $('#uploaded_coverpage').html(data);
                        },
                        resetForm: true
                    });
                    }
                    });

                    $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                                });

                });
    </script>
		
		@endsection
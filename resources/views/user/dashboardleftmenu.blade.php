	<div id="referencesubmit" class="modal">
		<div class="modal-content orderconfirmcss center">
		 	<div class="modal-header orderconfirmcss1">
		 		<!-- <h5 class="modal-title">Sucees</h5> -->
		 		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="col-md-12 text-center">
					<!-- <p class="font-size22px">Reference</p> -->
					<p class="font-size22px">Reference saved successfully</p>
				</div>
			</div>
		</div>
	</div>

	<div id="dashboardpopup" class="modal" style="display:none">
		
		<div class="modal-content orderconfirmcss center">

		 	<div class="modal-header orderconfirmcss1 ">
		 		<h5 class="modal-title orderdashboardcss font-size14 ">CLAIM YOUR<br>
				<img class="left"src="{{asset('public/images/sec-c/Handcrafted_TANJORE_Art_text.png')}}">
				<img class="right lapellogocss" src="{{asset('public/images/sec-c/Handcrafted_TANJORE_Art.png')}}">
				<span class="whited ml-10 font-size14 center1 mt-5" >&nbsp;<br>Form a minimum of 3 members community</span></h5>
				<!-- <span class="whited left">MEMBER 1 : </span> </p></h5> -->
				<button type="button"  id="btnorder3" onclick="document.getElementById('dashboardpopup').style.display='none'" 
			    class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form name="memberreferenceForm" id="memberreferenceForm" >
			{{ csrf_field() }}	
			<input class="floating-inputpcc" name="user_id" type="hidden" value="{{ Auth::user()->id }}">
			
				<div class="modal-body">
					<p class="whited font-size14">MEMBER <span class="seqId">1</span> : </p>
					<div class="col-md-12 bluebg p11-5">
						<div class="row">
							<div class="col-md-4">
								<p class="underbottom1 font-size14"> NAME</p>
								<p class="underbottom"><input class="floating-inputpcc @error('name1') is-invalid @enderror" name="name1" id="name1" type="text"> 

								<label id="name1-error" class="error11" for="name1"></label></p>
							</div>
							<div class="col-md-4">
								<p class="underbottom1 font-size14"> MOBILE NO.</p>
								<p class="underbottom"><input class="floating-inputpcc @error('mobile1') is-invalid @enderror" name="mobile1" id="mobile1" type="number">
								<label id="mobile1-error" class="error11" for="mobile1"></label></p>
							</div>
							<div class="col-md-4">
								<p class="underbottom1 font-size14"> RELATION</p>
								<p class="underbottom">
									<select class="floating-selectpcc1" name="relation1" required>
										<option value="Friend" >Friend</option>
										<option value="Family ">Family </option>
										<option value="Other" >Other</option>
									</select>
								</p>
							</div>
						</div>
					</div>
					<p class="whited mt-15 font-size14">MEMBER <span class="seqId">2</span> : </p>
					<div class="col-md-12 bluebg p11-5">
						<div class="row">
							<div class="col-md-4">
								<p class="underbottom1 font-size14"> NAME</p>
								<p class="underbottom"><input class="floating-inputpcc @error('name2') is-invalid @enderror" name="name2" id="name2" type="text" placeholder="">
								<label id="name2-error" class="error11" for="name2"></label></p>
							</div>
							<div class="col-md-4">
								<p class="underbottom1 font-size14"> MOBILE NO.</p>
								<p class="underbottom"><input class="floating-inputpcc @error('mobile2') is-invalid @enderror" name="mobile2" id="mobile2" type="number" placeholder="">
								<label id="mobile2-error" class="error11" for="mobile2"></label>
							 	</p>
							</div>
							<div class="col-md-4">
								<p class="underbottom1 font-size14"> RELATION</p>
								<p class="underbottom">
									<select class="floating-selectpcc1"  name="relation2" required>
										<option value="Friend" >Friend</option>
										<option value="Family ">Family </option>
										<option value="Other" >Other</option>
									</select>
								</p>
							</div>
						</div>
					</div>
					<p class="whited mt-15 font-size14">MEMBER  <span class="seqId">3</span> : </p>
					<div class="col-md-12 bluebg p11-5 after-add-more">
						<div class="row">
							<div class="col-md-4">
								<p class="underbottom1 font-size14"> NAME</p>
								<p class="underbottom"><input class="floating-inputpcc @error('name3') is-invalid @enderror" name="name3" id="name3" type="text" placeholder="">
								<label id="name3-error" class="error11" for="name3"></label></p>
							</div>
							<div class="col-md-4">
								<p class="underbottom1 font-size14"> MOBILE NO.</p>
								<p class="underbottom"><input class="floating-inputpcc @error('mobile3') is-invalid @enderror" name="mobile3" id="mobile3" type="number" placeholder="">
								<label id="mobile3-error" class="error11" for="mobile3"></label>
							 	</p>
							</div>
							<div class="col-md-4"> 
								<p class="underbottom1 font-size14"> RELATION</p>
								<p class="underbottom">
									<select class="floating-selectpcc1"  name="relation3" required>
										<option value="Friend" >Friend</option>
										<option value="Family ">Family </option>
										<option value="Other" >Other</option>
									</select>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="memberrefdash">

				<div class="row mt-15 before-add-more">
					<div class="col-md-5">
						<div class="submit-btn">
						<input type="button" value="+ ADD MORE MEMBERS "  class="add-more volunteerpost" name="">
						</div>
					</div>
					
					<div class="col-md-4"> 
						<input type="submit" value="Submit" id="memberreferenceBtn" class="left volunteersubmit" name="">
						<div id='loader' class='text-center' style="display:none;">
							<img class="gifplayer" src="{{ asset('public/images/sample.png')}}"style="width: 10%;"
							data-mode="video"data-mp4="{{ asset('public/images/ajax-loader.gif') }}" />
						</div>	
					</div>
				</div>
				
				</div>
			</form>
		</div>
	</div>

							<ul>
								<li class="{{ (request()->is('dashboard')) ? 'active' : '' }}" ><a href="{{ route('user.dashboard') }}">My Dashboard</a></li>

								<li  class="{{ (request()->is('user_profile')) ? 'active' : '' }}" ><a  href="{{ route('user.user_profile') }}">My Profile</a></li>
								<li  class="{{ (request()->is('upcoming_events')) ? 'active' : '' }}" ><a  href="{{ route('user.upcoming_events') }}">Upcoming Events / My Ticket</a></li>

								<!-- <li  class="" ><a data-target="#dashboardothermenu" data-toggle="modal" href="#dashboardothermenu"  data-placement="right"  title="You need to purchase membership kit to use this section " class="red-tooltip" id="tooltip-top" href="{{ route('user.upcoming_events') }}">Upcoming Events / My Ticket</a></li> -->
								@if(Auth::user()->tmembership =='')
									<li  class="" ><a data-target="#myModal" data-toggle="modal" href="#myModal">My Circle</a></li>
								@else
									<li  class="" ><a data-target="#dashboardothermenu" data-toggle="modal" href="#dashboardothermenu">My Circle</a></li>
								
								@endif
								<li class="" ><a data-target="#dashboardothermenu" data-toggle="modal" href="#dashboardothermenu"  data-placement="right"  title="You need to purchase membership kit to use this section " class="red-tooltip" id="tooltip-top" href="">I Wish to Volunteer</a></li>
								
								<li class="" ><a data-target="#dashboardothermenu" data-toggle="modal" href="#dashboardothermenu"  data-placement="right"  title="You need to purchase membership kit to use this section " class="red-tooltip" id="tooltip-top" href="">My REFERENCES & Rewards</a></li>

								<li class="" ><a data-target="#dashboardothermenu" data-toggle="modal" href="#dashboardothermenu"  data-placement="right"  title="You need to purchase membership kit to use this section " class="red-tooltip" id="tooltip-top" href="">My Contribution</a></li>
								
								<li  class="{{ (request()->is('i_wish_to_host_an_event')) ? 'active' : '' }}" ><a  href="{{ route('user.i_wish_to_host_an_event') }}">I Wish to Host an Event</a></li> 
								
								<li class="" ><a data-target="#dashboardothermenu" data-toggle="modal" href="#dashboardothermenu"  data-placement="right"  title="You need to purchase membership kit to use this section " class="red-tooltip" id="tooltip-top" href="">Circle Heroes & Champions</a></li>
							</ul>

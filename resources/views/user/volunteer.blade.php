@extends('layouts.lp')
	@section('content')
	@include('user.usermodals')			
		<section class="dashboard-main">
			<div class="container-fluid">
				<div class="row">
				<div class="col-md-12 center">
				<!-- 	@if (session()->has('message'))
						<div class="alert1 alert-info1">&#x2714;
							{{ session('message') }}
						</div>
					@endif -->
				</div>
					<div class="col-md-3">
						<div class="dashboard-menu">
						@include('user.dashboardleftmenu')
						</div>
					</div>

					<div class="col-md-9">
						<div class="dashboard-content">
							<div class="dashboard-profile dashboard-user-profile shadowbg">

								<form  id="volunteer"  name="volunteer">
									<div class="profile-img-main volunteerhead">
										<h4 >I WISH TO VOLUNTEER</h4>
									</div>

									<div class="col-md-12 center">
										@if (session()->has('message'))
											<div class="alert1 alert-info1">&#x2714;
												{{ session('message') }}
											</div>
										@endif
									</div>



										{{ csrf_field() }}	
									<div class="row">
										<div class="col-md-12">
											<div class="floating-form">
											    <div class="row">
													<div class="col-md-9">
														<div class="row">
															<div class="col-md-6">
							                                     <div class="floating-label2">
							                                        <p class="">NAME</p>
							                                        <input class="floating-inputpcc readonly1" name="name" type="text" readonly value="{{ Auth::user()->name }}">
																	<label id="name-error" class="error" for="name"></label>
							                                     </div>
							                                </div>

							                                <div class="col-md-6">
							                                     <div class="floating-label2">
							                                        <p class="">MEMBERSHIP NO.</p>
							                                        <input class="floating-inputpcc readonly1" name="membershipno"type="text" readonly value=" {{ Auth::user()->tmembership?Auth::user()->tmembership:'Under Process' }}">
							                                        
							                                     </div>
							                                </div>

							                                <div class="col-md-6">
							                                     <div class="floating-label2">
							                                        <p class="">CONTACT NO. </p>
							                                        <input class="floating-inputpcc @error('mobile') is-invalid @enderror" name='mobile' id="mobile"type="number" placeholder="">
							                                     
																	<label id="phone-error" class="error" for="phone"></label>
							                                     </div>
							                                </div>

							                                <div class="col-md-6">
							                                     <div class="floating-label2">
							                                        <p class="">EMAIL ID </p>
							                                        <input class="floating-inputpcc @error('email') is-invalid @enderror"  id="email" type="email"  name='email' placeholder=" ">
							                                        <span class="highlight"></span>
																	<label id="email-error" class="error" for="email"></label>
							                                     </div>
							                                </div>



							                                


							                                 <div class="col-md-12">
							                                     <div class="floating-label22">
							                                        <p class="">I WISH TO VOLUNTEER FOR LOCATION </p>
							                                       
							                                     </div>
							                                </div>
													    	
													    	<div class="col-md-4">

															    <div class="floating-label2">  
																	<select class="floating-selectpcc1" id='volunteer_country' name='volunteer_country' >
																		<!-- <option value="">COUNTRY</option> -->
																		@foreach($country as $ctr)
																			@if($ctr->id==101)
																				<option value="{{ $ctr->id }}"  selected>{{ $ctr->name }}</option>
																			@else
																				<option value="{{ $ctr->id }}" >{{ $ctr->name }}</option>
																			@endif
																		
																		@endforeach	
																	</select>
																	<span class="highlight"></span>
																
																</div>
													    	</div>


													    	<div class="col-md-4">

															    <div class="floating-label2">  
																	<select class="floating-selectpcc1" id='volunteer_state' name='volunteer_state'>
																		<option value="">STATE</option>
																	
																	</select>
																	<span class="highlight"></span>
																
																</div>
													    	</div>


													    	<div class="col-md-4">

															    <div class="floating-label2">  
																<input class="floating-inputpcc" name="volunteer_city"type="text" placeholder="City">
																   
																
																	<!-- <select class="floating-selectpcc1" id='volunteer_city' name='volunteer_city'>
																		<option value="">CITY</option>
																	</select> -->
																
																</div>
													    	</div>
													    
													    	<div class="col-md-9">
													    		<div class="row">
													    			<div class="col-md-12">
													    				<div class="floating-label22">
													    					<p>I WISH TO VOLUNTEER FORM-TO</p>
													    				</div>
													    			</div>

													    		
													    			 <div class="col-md-5">
													    			 	<div class="floating-label2">
													    			 	<input class="floating-inputpcc" name="fromdate" type="date" placeholder=" ">
													    				 </div>	
													    			 </div>

													    			 <div class="col-md-1 label22">
													    				<div class="floating-label22 ">
													    					<p>TO</p>
													    				</div>
													    			</div>

							                               			  <div class="col-md-5">
							                                     		<div class="floating-label2">
							                                     			<input class="floating-inputpcc"   name="todate" type="date" placeholder=" ">
							                                     		</div>
							                               			 </div>	
							                               			</div>
															</div>


															<div class="col-md-9">
													    		<div class="row">
													    			<div class="col-md-12">
													    				<div class="floating-label22">
													    					<p>I WISH TO VOLUNTEER FORM-TO</p>
													    				</div>
													    			</div>

													    		
													    			 <div class="col-md-5">
													    			 	<div class="floating-label2">
													    			 	<input class="floating-inputpcc" name="fromtime"  type="time" placeholder=" ">
													    				 </div>	
													    			 </div>

													    			 <div class="col-md-1 label22">
													    				<div class="floating-label22 ">
													    					<p>TO</p>
													    				</div>
													    			</div>

							                               			  <div class="col-md-5">
							                                     		<div class="floating-label2">
							                                     			<input class="floating-inputpcc" name="totime"  type="time" placeholder=" ">
							                                     		</div>
							                               			 </div>	
							                               			</div>
															</div>






															<div class="col-md-12">
																<div class="floating-label22 ">
													    					<p>I WISH TO SAY...</p>
													    		</div>
																<div class="dashboard-note">      
															      <textarea  type="text" name="message" placeholder="200 WORDS"></textarea>
															    </div>
															</div>

														</div>
													</div>

													<div class="col-md-3 center1">
														<img src="{{ asset('public/images/sound1.png')}}" class="imgvol">
					                                </div>

					                                <div class="col-md-12">
					                                	<div class="row">
					                                		<div class="col-md-12">
																<div class="floating-label2">
																	<p><input class="floating-inputcheck inputc2" type="checkbox" name="terms" id="terms" {!! old("terms") == 'on' ? 'checked' : '' !!}>
												    				I accept the <a href="terms-of-services.php"  class="blue">Terms & Conditions</a></p>
																	<label id="terms-error" class="erroracceptu" for="terms"></label>
												    			</div>
												    		</div>

												    		<div class="col-md-2">
													    		<div class="submit-btn">
																	<input type="submit" value="Submit" id="volunteerFormBtn" class="volunteersubmit" name="">
																</div>

																<div id='loader' class='text-center' style="display:none;">
																<img class="gifplayer" src="{{ asset('public/images/sample.png')}}"
																	style="width: 10%;"
																	data-mode="video"
																	data-mp4="{{ asset('public/images/ajax-loader.gif') }}" />
																</div>


											    			</div>
													    	<div class="col-md-2">
													    		<div class="submit-btn">
																	<input type="reset" value="Reset" class="volunteerreset"name="">
																</div>
													    	</div>
												    	</div>
												    </div>

												</div>
											</div>
										</div>
										
									</div>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


	
		@endsection
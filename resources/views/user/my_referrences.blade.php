@extends('layouts.lp')
	@section('content')
	@include('user.usermodals')			
		<section class="dashboard-main">
			<div class="container-fluid">
				<div class="row">
				<div class="col-md-12 center">
				<!-- 	@if (session()->has('message'))
						<div class="alert1 alert-info1">&#x2714;
							{{ session('message') }}
						</div>
					@endif -->
				</div>
					<div class="col-md-3">
						<div class="dashboard-menu">
						@include('user.dashboardleftmenu')
						</div>
					</div>

					<div class="col-md-9">
							<div class="dashboard-content">
						
							<div class="dashboard-profile dashboard-user-profile dashboard-upcomingcss">
								<div class="floating-form">
									<div class="row mb-10">
										<div class="col-md-9"></div>

										<div class="col-md-3">
											<img src="{{ asset('public/images/share_btn.png')}}" class="right">
										</div>
									</div>
								</div>

								<div class="hasDatepicker-main shadowbg my-referrebces">

									<div class="profile-img-main volunteerhead">
										<h4 >MY REFERENCES & REWARDS</h4>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="my-referrebces-box-main">
												<div class="my-referrebces-box left">
													<div id="my-referrebces-box" class="my-referrebces-box-inner">
														<h4>Members Joined, With My Reference</h4>
														<strong>15</strong>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6 right">
											<div class="my-referrebces-box-main right">
												<div id="reward-points" class="my-referrebces-box1">
													<div class="my-referrebces-box-inner1">
														<h4>Redeem & Reward Points</h4>
														<strong>1500</strong>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="dashboard-table members-joined-table" >
										<div class="close-btn">
											<button id="close"><i class="far fa-times-circle"></i></button>
										</div>
										<div id="referrebcestable">
										<table >
											<thead>
												<tr>
													<th class="center1 width-70">SR. NO</th>
													<th class="width-100">NAME</th>
													<th class="width-100">MEMBERSHIP NO.</th>
													<th class="width-100">JOINING DATE</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="center1">1</td>
													<td >Demo</td>
													<td>123456</td>
													<td>25/07/2020</td>
												</tr>
												<tr>
													<td class="center1">2</td>
													<td>Demo</td>
													<td>123456</td>
													<td>25/07/2020</td>
												</tr>
												<tr>
													<td class="center1">3</td>
													<td>Demo</td>
													<td>123456</td>
													<td>25/07/2020</td>
												</tr>

												<tr>
													<td class="center1">4</td>
													<td>Demo</td>
													<td>123456</td>
													<td>25/07/2020</td>
												</tr>
												<tr>
													<td class="center1">5</td>
													<td>Demo</td>
													<td>123456</td>
													<td>25/07/2020</td>
												</tr>
												<tr>
													<td class="center1">6</td>
													<td>Demo</td>
													<td>123456</td>
													<td>25/07/2020</td>
												</tr>
											
											
											</tbody>
										</table>
									</div>
									</div>
								
						</div>
					</div>
				</div>
			</div>
		</section>

		<script type="text/javascript">$(document).ready(function() {
			    $("#my-referrebces-box").click(function(){
				  $(".members-joined-table").toggleClass("open");
				  $(".reward-points-box-main").removeClass("open");
				});
				$("#close").click(function(){
				  $(".members-joined-table").toggleClass("open");
				});
				$("#reward-points").click(function(){
				  $(".reward-points-box-main").toggleClass("open");
				  $(".members-joined-table").removeClass("open");
				});
				$("#close-2").click(function(){
				  $(".reward-points-box-main").toggleClass("open");
				});
			});
		</script>		
		
		@endsection
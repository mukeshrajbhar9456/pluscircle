@extends('layouts.lp')
	@section('content')
	@include('user.usermodals')			
		<section class="dashboard-main">
			<div class="container-fluid">
				<div class="row">
				<div class="col-md-12 center">
				<!-- 	@if (session()->has('message'))
						<div class="alert1 alert-info1">&#x2714;
							{{ session('message') }}
						</div>
					@endif -->
				</div>
					<div class="col-md-3">
						<div class="dashboard-menu">
						@include('user.dashboardleftmenu')
						</div>
					</div>
					<div class="col-md-9">
						<div class="dashboard-content">
							<div class="dashboard-profile dashboard-user-profile dashboard-upcomingcss">
								<div class="floating-form">
									<div class="row mb-10">
										<div class="col-md-7"></div>
										<div class="col-md-3 center1">
										<p class="center1"><a class="right ticketcss"><i class="fa fa-ticket-alt"></i> My ticket (12)</a></p>
										</div>
										<div class="col-md-2 center1">
											<img src="{{ asset('public/images/share_btn.png')}}" class="right">
										</div>
									</div>
								</div>

							

								<div class="hasDatepicker-main shadowbg">

								<div class="profile-img-main volunteerhead">
										<h4 >UPCOMING ACTIVITIES</h4>
								</div>


									<div class="floating-form">
										<div class="row center1">
											<!-- <div class="col-md-3">
												<div class="floating-label">    
												<input class="floating-inputpcc" type="date" placeholder=" ">
												</div>
												<div class="floating-label floating-label-date">    
											      	<input class="floating-input" type="text" id="txtDateRange" name="txtDateRange" class="inputField shortInputField dateRangeField" placeholder="Date " data-from-field="txtDateFrom" data-to-field="txtDateTo" />
											      	<input type="hidden" id="txtDateFrom" value="" />
													<input type="hidden" id="txtDateTo" value="" />  
											      	<span class="highlight"></span>
											     
											    </div> 
											</div> -->
											<div class="col-md-3">
												<div class="floating-label">      
													<select class="floating-selectpcc1" id='search_country' value="">
														@foreach($country as $c)
															<option value="{{ $c->id }}">{{ $c->country }}</option>
														@endforeach
													</select>
											      	<span class="highlight"></span>											 
											    </div>
											</div>
											<div class="col-md-3">
												<div class="floating-label">      
											      	<select class="floating-selectpcc1" id='search_state' value="">
														@foreach($state as $s)
															<option value="{{ $s->id }}">{{ $s->state }}</option>
														@endforeach														
													</select>
											     </div>
											</div>
											<div class="col-md-3">
												<div class="floating-label">      
											      	<select class="floating-selectpcc1" id='search_city' value="">
														@foreach($city as $c)
															<option value="{{ $c->city }}">{{ $c->city }}</option>
														@endforeach														
													</select>
											     </div>
											</div>
											<div class="col-md-2">
												<div class="submit-btn btnmargin eventbt">
													<input type="button" name="next" class="volunteersubmit" onclick='search_event()' value="Search">
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
								<div class="upcoming-events-slider">
									
									<div class="">
										<div class="upcoming-slider-main">
											<div id="upcoming-activities-2" class="owl-carousel">
												@foreach($events as $e)
												<div class="item">
													<div class="slider-img">
														<!-- <a href="#myModal_upcoming" data-toggle="modal" data-target="#myModal_upcoming"> -->
														<a onclick='eventModal({{"$e->id" }});' >
														@if($e->event_profile != '')
															<img class="owl-lazy" data-src="{{ asset($e->event_profile)}}" alt="">
														@else
															<img class="owl-lazy" data-src="{{ asset('public/images//community-img.jpg')}}" alt="">
														@endif
														</a>
													</div>
													<div class="upcoming-slider-content">
														<div class="events-name"> 
															<strong>{{ $e->eventtitle}} ( {{ $e->eventtype }} )</strong>
														</div>
														<p>{{ $e->description }}</p>
														<div class="row">
															<div class="col-md-12">
																<div class="text-box1">
																	<p> <strong> From </strong> {{ $e->startdate }} &nbsp; {{ $e->starttime }}</p>
																	<p> <strong> To </strong> {{ $e->enddate }} &nbsp; {{ $e->endtime }} </p>
																	<p> <strong> INR </strong> {{ $e->ticketprice }}</p>
																</div>
																
															</div>
															<div class="col-md-12">
																<div class="slider-btn">
																	<button data-toggle="modal" data-target="#myModal2" onclick="buy_ticket({{ $e->id }});" class="btn volunteersubmit">Book a Ticket</button>
																</div>
															</div>
														</div>
													</div>
												
												</div>
												@endforeach
												 
												<!--<div class="item">
													<div class="slider-img">
														<a href="#myModal_upcoming" data-toggle="modal" data-target="#myModal_upcoming"><img class="owl-lazy" data-src="{{ asset('public/images//community-img.jpg')}}" alt=""></a>
													</div>
													<div class="upcoming-slider-content">
														<div class="events-name">
															<strong>Event Name</strong>
														</div>
														<p>Lorem Ipsum is simply dummy text of the printing.</p>
														<div class="row">
															<div class="col-md-6">
																<div class="text-box1">
																	<p> 15 Nov 2020</p>
																	<p> 05:00 PM</p>
																	<p> INR 2,000.00</p>
																</div>
																
															</div>
															<div class="col-md-6">
																<div class="slider-btn">
																	<button data-toggle="modal" data-target="#myModal2" class="btn volunteersubmit">Book<br> a<br>  Ticket</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="item">
												
													<div class="slider-img">
														<a href="#myModal_upcoming" data-toggle="modal" data-target="#myModal_upcoming"><img class="owl-lazy" data-src="{{ asset('public/images//community-img.jpg')}}" alt=""></a>
													</div>
													<div class="upcoming-slider-content">
														<div class="events-name">
															<strong>Event Name</strong>
														</div>
														<p>Lorem Ipsum is simply dummy text of the printing.</p>
														<div class="row">
															<div class="col-md-6">
																<div class="text-box1">
																	<p> 15 Nov 2020</p>
																	<p> 05:00 PM</p>
																	<p> INR 2,000.00</p>
																</div>
																
															</div>
															<div class="col-md-6">
																<div class="slider-btn">
																	<button data-toggle="modal" data-target="#myModal2" class="btn volunteersubmit">Book<br> a<br>  Ticket</button>
																</div>
															</div>
														</div>
														
													</div>
												</div>
												<div class="item">
													<div class="slider-img">
														<a href="#myModal_upcoming" data-toggle="modal" data-target="#myModal_upcoming"><img class="owl-lazy" data-src="{{ asset('public/images//community-img.jpg')}}" alt=""></a>
													</div>
													<div class="upcoming-slider-content">
														<div class="events-name">
															<strong>Event Name</strong>
														</div>
														<p>Lorem Ipsum is simply dummy text of the printing.</p>
														<div class="row">
															<div class="col-md-6">
																<div class="text-box1">
																	<p> 15 Nov 2020</p>
																	<p> 05:00 PM</p>
																	<p> INR 2,000.00</p>
																</div>
																
															</div>
															<div class="col-md-6">
																<div class="slider-btn">
																	<button data-toggle="modal" data-target="#myModal2" class="btn volunteersubmit">Book<br> a<br>  Ticket</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div> -->
											<!-- <div class="view-all-btn">
												<button class="btn">View All</button>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		
		
		@endsection
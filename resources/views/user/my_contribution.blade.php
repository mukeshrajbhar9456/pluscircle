@extends('layouts.lp')
	@section('content')
	@include('user.usermodals')			
		<section class="dashboard-main">
			<div class="container-fluid">
				<div class="row">
				<div class="col-md-12 center">
				<!-- 	@if (session()->has('message'))
						<div class="alert1 alert-info1">&#x2714;
							{{ session('message') }}
						</div>
					@endif -->
				</div>
					<div class="col-md-3">
						<div class="dashboard-menu">
						@include('user.dashboardleftmenu')
						</div>
					</div>

						<div class="col-md-9">
							<div class="dashboard-content">
						
							<div class="dashboard-profile dashboard-user-profile dashboard-upcomingcss">
								<div class="floating-form">
									<div class="row mb-10">
										<div class="col-md-9"></div>

										<div class="col-md-3">
											<img src="{{ asset('public/images/share_btn.png')}}" class="right">
										</div>
									</div>
								</div>

								<div class="hasDatepicker-main shadowbg my-referrebces">

									<div class="profile-img-main volunteerhead">
										<h4 >MY CONTRIBUTION</h4>
									</div>


							<div class="my-contribution-main">

													
								<div class="my-contribution-box-main">
									<div class="my-contribution-box">
										<div class="date-list">
											<ul>
												<li><span>Jan 2020</span><span>INR 1251.00</span></li>
												<li><span>Feb 2020</span><span>INR 450.00</span></li>
												<li><span>Mar 2020</span><span>INR 2250.00</span></li>
												<li><span>Apr 2020</span><span>INR 1122.00</span></li>
											</ul>
										</div>
									</div>
									<div class="my-contribution-box">
										<div class="contribution-total">
												<h4>Total Contribution </h4>
											<strong>
										 Rs. 5037.00
											</strong>
										</div>
									</div>
								</div>
								<div class="dashboard-table members-joined-table">
									<div id="contributioncss1">
									<table class="contributioncss2">
										<thead>
											<tr>
												<th class="width-100">DATE</th>
												<th class="width-100">SOURCE</th>
												<th class="width-100">PURCHASE</th>
												<th class="width-100">CONTRIBUTION</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>28/07/2020</td>
												<td>Dinner</td>
												<td>1200.00</td>
												<td >120.00</td>
											</tr>
											<tr>
												<td>29/07/2020</td>
												<td>Movie</td>
												<td>750.00</td>
												<td>15.00</td>
											</tr>
											<tr>
												<td>30/07/2020</td>
												<td>Event X</td>
												<td>2250.00</td>
												<td>114.00</td>
											</tr>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="2"><b>TOTAL SPENDING & CONTRIBUTION</b></td>
												<td><b>4200.00</b></td>
												<td><b>249.00</b></td>
											</tr>
										</tfoot>
									</table>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	
		@endsection
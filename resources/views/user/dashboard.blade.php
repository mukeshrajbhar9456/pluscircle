@extends('layouts.lp')
	@section('content')	
	
<style>
.hidden{
	display:none;
}
.iwishtopurchase {
    border-radius: 5px !important;
    /* background: #000e2b; */
	color:#1f2b44 !important;
    margin-top: 0px;
    margin-bottom: 10px;
    border: 1px solid #1f2b44;
    font-size: 14px;
    font-weight: 700;
}
.dashboard-note {
    border: 1px solid grey;
    padding: 10px 10px;
    border-radius: 10px;
    margin: 5px 5px 5px 5px;
    overflow-y: scroll;
}
.dashboard-video img {
    height: 270px;
    width: auto;
}
a.purchasekit {
    color: #1f2b44;
    border: 1px solid #1f2b44;
    padding: -1px;
    font-size: 10px;
    padding: 5px;
    border-radius: 5px;
    font-weight: 600;
}
a.purchasekit:hover {
    color: #1f2b44;
    border: 1px solid #1f2b44;
    padding: -1px;
    font-size: 14px;
    padding: 5px;
    border-radius: 5px;
    font-weight: 600;
	background:#c5ab7a;
}
</style>

<div class="modal fade" id="modalAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
         <h3>Accept the terms and conditions</h3>
        </div>
      </div>
    </div>
  </div>
</div>

	@include('user.usermodals')

	<section class="dashboard-main">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3">
					<div class="dashboard-menu">
					@include('user.dashboardleftmenu')
					
					</div>
				</div>
				<div class="col-md-9">
					<div class="dashboard-content">
						<div class="dashboard-profile ">
							<form id="#" action="#">
								<div class="row">
									<div class="col-md-6">	
										<div class="profile-img-main">
											<div class="profile-img-inner">
												<div class="profile-img">
													<img src="{{ Auth::user()->profile?Auth::user()->profile:asset('public/images/noimg.png')}}">
												</div>
											</div>
											<div class="profile-name">
												<h2>Hi, <i>{{ Auth::user()->name}}</i>, <br> Membership No. : <strong>{{ Auth::user()->tmembership?Auth::user()->tmembership:'Under Process' }}</strong></h2>
												@if($memberkit == 0)
												@if(Auth::user()->tmembership =='')
												<a class='purchasekit' data-target="#myModal" data-toggle="modal" href="#myModal">I Wish to Purchase my membership kit</a>
												@endif
												@endif
												<!-- <p ><input type="submit" name="send" class="next-step mt-40 iwishtopurchase"  value="I Wish to Purchase my membership kit"></p> -->
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<p >
											<a href="float:right;" data-target="#mydashboardsharingModalvideo" data-toggle="modal" href="#mydashboardsharingModalvideo" >
												<img src="{{ asset('public/images/share_btn.png')}}" class="dimg" />
											</a>
										</p>
									</div>
								</div>
								<div class="row">		
									<div class="col-md-6">	
										<div class="dashboard-note">
											<h4 class='text-center'>
											ATTENTION REQUIRED		
											</h4>
											<p>Please complete the underneath steps to avail your membership:
											</p>
											<ul>
											@if(Auth::user()->tmembership =='')
											<li>
												Purchase new member’s Joining Kit.
											</li>
											@endif
											<li>
												Complete your user profile from <a href="{{ route('user.user_profile') }}"><strong>MY PROFILE</strong></a>.
											</li>
											</ul>
											<!--<p>-->
											<!--If you are not completed the above steps in 48 hrs your membership application will automatically be cancelled.-->
											<!--</p>											-->
										</div>
									</div>
									<div class="col-md-6">
										<div class="dashboard-video">
											<a data-target="#mydashboardModalvideo" data-toggle="modal" href="#mydashboardModalvideo" >
												<img src="{{ asset('public/images/dashboard.png')}}" />
											</a>
											<!-- <iframe src="https://www.youtube.com/embed/3VZFpwlXKpg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe> -->
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>	

		
	@endsection

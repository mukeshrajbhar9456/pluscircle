@extends('layouts.lp')
	@section('content')
	<style>

.floating-input2, .floating-select2 {
    padding: 6px;
}

.field-icon {
  float: right;
  margin-left: -25px;
  margin-top: -25px;
  position: relative;
  z-index: 2;
}

.container{
  padding-top:10px;
  margin: auto;
}
label.cabinet{
	display: block;
	cursor: pointer;
}

label.cabinet input.file{
	position: relative;
	height: 100%;
	width: auto;
	opacity: 0;
	-moz-opacity: 0;
  filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
  margin-top:-30px;
}

#upload-demo3{
	width: 400px;
	height: 400px;
  padding-bottom:25px;
}

figure figcaption {
    position: absolute;
    bottom: 0;
    color: #fff;
    width: 100%;
    padding-left: 9px;
    padding-bottom: 5px;
    text-shadow: 0 0 10px #000;
}
p.allstate {
    margin-bottom: 0px;
    text-align: center;
    line-height: 17px;
    background-color: #7E5233;
    font-weight: 600;
    color: #fff;
    width: 102px;
    padding: 2px;
    border-radius: 5px;
    top: 232px;
    right: 91px;
    position: fixed;
    z-index: 999;
}
.sscmview {
    padding: 5px;
    text-align: center !important;
    font-size: 15px;
    width: 99px;
    min-height: 100px;
    background-color: #57637c;
    color: #fff1f1;
    position: fixed;
    z-index: 999;
    line-height: 20px;
    font-weight: 600;
    /* height: 5px; */
    border-radius: 5px;
    top: 315px !important;
    /* height: 13px!important; */
    /* left: 2px; */
    right: 90px;
}
label.error {
    font-size: 12px;
    color: #ff8300;
    font-weight: 700;
}
a.purchasekit {
    color: #1f2b44;
    border: 1px solid #1f2b44;
    padding: -1px;
    font-size: 15px;
    padding: 5px;
    border-radius: 5px;
    font-weight: 600;
}
a.purchasekit:hover {
    color: #1f2b44;
    border: 1px solid #1f2b44;
    padding: -1px;
    font-size: 15px;
    padding: 5px;
    border-radius: 5px;
    font-weight: 600;
	background:#c5ab7a;
}
input#user_profile {
    width: 112px;
    /* border: 1px solid red; */
    height: 100px;
    /* border-radius: 50%; */
}
img#event-image {
    padding-top: -16px;
    margin-top: -48px;
    width: 60%;
    height: auto;
}
.alert-danger {
    color: #721c24;
    background-color: #f1cd91;
    border-color: #f5c6cb;
    font-size: 11px;
    font-weight: 600;
    padding-top: 1px;
}
</style>
	@include('user.usermodals')			
		<section class="dashboard-main">
			<div class="container-fluid">
				<div class="row">
				<div class="col-md-12 center">
					@if (session()->has('message'))
						<div class="alert1 alert-info1">&#x2714;
							{{ session('message') }}
						</div>
					@endif
					@if (session()->has('error'))
					<div class="alert1 alert-danger1 ">&#x26A0;
						{{ session('error') }}
					</div>
					@endif
				</div>
					<div class="col-md-3">
						<div class="dashboard-menu">
						@include('user.dashboardleftmenu')
						</div>
					</div>

					<div class="col-md-9">
						<div class="dashboard-content">
							<div class="dashboard-profile dashboard-user-profile shadowbg">

							<form id="event" method='post' action="{{ route('user.save_event')}}" name='event' enctype="multipart/form-data">
                           		{{ csrf_field() }}
									<div class="profile-img-main volunteerhead">
										<h4 >I WISH TO HOST AN EVENT</h4>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="floating-form">
												<div class="row">
													<div class="col-md-9">
														<div class="row">
																						
													    	<div class="col-md-12">
													    		<div class="floating-label head">
													    		<p class="head"> Event Id<br><b>A-1002312</b></p>
													    		</div>
													    	</div>												    	

															<div class="col-md-6">
													    		<div class="floating-label2">
													    			<p>Event Type</p>   
													    		 	<select id="eventother" class="floating-selectpcc1" value="{!! old('eventtype') !!}" name='eventtype'>   
													    				<option value="Drama">Drama</option>
																		<option value="Car Rally">Car Rally
																		</option><option value="Badminton (SPORTS)">Badminton (SPORTS)
																		</option><option value="Barbeque Fest">Barbeque Fest
																		</option><option value="Business Meet Brunch ">Business Meet Brunch 
																		</option><option value="Social Gathering Lunch">Social Gathering Lunch 
																		</option><option value="Cricket (Sports)">Cricket (Sports)
																		</option><option value="Conclave">Conclave 
																		</option><option value="Tech Talks">Tech Talks 
																		</option><option value="Tech Meet">Tech Meet 
																		</option><option value="Tech Conference">Tech Conference 
																		</option><option value="Live Music Event">Live Music Event 
																		</option><option value="Open Mic Event">Open Mic Event
																		</option><option value="Festival">Festival
																		</option><option value="Kids Activity">Kids Activity
																		</option><option value="Football (SPORTS)">Football (SPORTS)
																		</option><option value="Gazal">Gazal
																		</option><option value="Friends accost">Friends accost
																		</option><option value="Hiking">Hiking 
																		</option><option value="Busines Introduction Seminar">Busines Introduction Seminar
																		</option><option value="Cycling (SPORTS)">Cycling (SPORTS)
																		</option><option value="Stand Up Comedy">Stand Up Comedy 
																		</option><option value="Dance Party">Dance Party
																		</option><option value="Business Meet- Business">Business Meet- Business 
																		</option><option value="Kitty Party">Kitty Party 
																		</option><option value="Camping">Camping 
																		</option><option value="Housie (INDOOR GAME)">Housie (INDOOR GAME)
																		</option><option value="Picnic">Picnic
																		</option><option value="Lawn Tennis (SPORTS)">Lawn Tennis (SPORTS)
																		</option><option value="Dance Event">Dance Event
																		</option><option value="Beer Feast">Beer Feast 
																		</option><option value="Movie ">Movie 
																		</option><option value="Health & Fitness">Health & Fitness 
																		</option><option value="Marathon">Marathon 
																		</option><option value="Swimming (SPORTS)">Swimming (SPORTS)
																		</option><option value="Volleyball (SPORTS)">Volleyball (SPORTS)
																		</option><option value="Cultural Show">Cultural Show 
																		</option><option value="Food Fest">Food Fest
																		</option><option value="Blood Donation">Blood Donation 
																		</option><option value="Career and Business Accost">Career and Business Accost
																		</option><option value="Wine Feast">Wine Feast
																		</option><option value="Bonfire Camp">Bonfire Camp 
																		</option><option value="Fancy Dress">Fancy Dress
																		</option><option value="Karaoke night">Karaoke night
																		</option><option value="Barn Dance">Barn Dance 
																		</option><option value="Community Cleanup">Community Cleanup 
																		</option><option value="International Women day">International Women day 
																		</option><option value="Quiz Competition">Quiz Competition </option>
																		<option value="Other">Other</option>
													    			</select>														    			
															     </div>
													    	</div>

													    	<div class="col-md-6">
							                                     <div class="floating-label2">
							                                        <p class="">Event title</p>
							                                        <input class="floating-inputpcc" type="text" value="{!! old('eventtitle') !!}" name='eventtitle' placeholder="">
							                                        @error('eventtitle')
																		<div class="alert alert-danger">{{ $message }}</div>
																	@enderror
																	<span class="highlight"></span>
							                                     </div>
							                                </div>

													    	<div class="col-md-6">
													    		<div class="floating-label2">  
															    	 <p>Participant/Audience Capacity</p> 
															    	 <div class="row">
															    	 	<div class="col-md-6">
																	     <input class="floating-inputpcc mb-16" type="number" value="{!! old('minparticipant') !!}" name='minparticipant' placeholder="MIN" type="number" min="1" max="10000 ">
																		@error('minparticipant')
																			<div class="alert alert-danger">{{ $message }}</div>
																		@enderror																		
																	 	</div>		

																     	<div class="col-md-6"> 
																		 <input class="floating-inputpcc" value="{!! old('maxparticipant') !!}" name='maxparticipant'  placeholder="MAX" type="number" min="1" max="10000 ">
																		 @error('maxparticipant')
																			<div class="alert alert-danger">{{ $message }}</div>
																		@enderror
																     	</div>
																    </div>
																    <span class="highlight"></span>															      
															    </div>
													    	</div>
													    	<div class="col-md-6">
							                                     <div class="floating-label2">
							                                        <p class="">If Event Type 'Other'</p>
							                                        <input class="floating-inputpcc" value="{!! old('eventothers') !!}" type="text" name='eventothers' placeholder="">
																		@error('eventothers')
																			<div class="alert alert-danger">{{ $message }}</div>
																		@enderror
							                                        <span class="highlight"></span>
							                                     </div>
							                                </div>
															<div class="col-md-6">
													    		<div class="floating-label2">
															    	<p>Ticket Reserved to sell through Plus Circle</p>
															    	<input class="floating-inputpcc" value="{!! old('reservedticket') !!}" type="number" name='reservedticket'>
																	@error('reservedticket')
																		<div class="alert alert-danger">{{ $message }}</div>
																	@enderror															  
															    </div>
													    	</div>

													    	<div class="col-md-6">
													    	 	<div class="floating-label2">  
															    	<p>Ticket Numbers(if any)</p> 
															    	<div class="row">
																	    <div class="col-md-6">
																	     	<input class="floating-inputpcc mb-16" value="{!! old('ticketnumberfrom') !!}" type="number" placeholder="Ticket From" type="text"  name='ticketnumberfrom' >
																			@error('ticketnumberfrom')
																				<div class="alert alert-danger">{{ $message }}</div>
																			@enderror
																		</div>		

																     	<div class="col-md-6"> 
																     		<input class="floating-inputpcc"  value="{!! old('ticketnumberto') !!}" placeholder="Ticket To" type="text" name='ticketnumberto' >
																			@error('ticketnumberto')
																				<div class="alert alert-danger">{{ $message }}</div>
																			@enderror
																		 </div>
																    </div>
															    </div>
													    	</div>
													    	<div class="col-md-9">
													    		<div class="row">
													    			<div class="col-md-12">
													    				<div class="floating-label22">
													    					<p>Event Starting & ending time</p>
													    				</div>
													    			</div>
													    		
													    			 <div class="col-md-5">
													    			 	<div class="floating-label2">
													    			 		<input class="floating-inputpcc" value="{!! old('starttime') !!}" type="time" name='starttime' placeholder=" ">
																			 @error('starttime')
																				<div class="alert alert-danger">{{ $message }}</div>
																			@enderror
																		</div>	
													    			 </div>

													    			 <div class="col-md-1 center1 label22">
													    				<div class="floating-label22 ">
													    					<p>TO</p>
													    				</div>
													    			</div>

							                               			  <div class="col-md-5">
							                                     		<div class="floating-label2">
							                                     			<input class="floating-inputpcc" value="{!! old('endtime') !!}" type="time" name='endtime' placeholder=" ">
																			 @error('endtime')
																				<div class="alert alert-danger">{{ $message }}</div>
																			@enderror
							                                     		</div>
							                               			 </div>	
							                               		</div>
							                               	</div>


													    	<div class="col-md-9">
													    		<div class="row">
													    			<div class="col-md-12">
													    				<div class="floating-label22">
													    					<p>Event Date Range</p>
													    				</div>
													    			</div>
													    		
													    			 <div class="col-md-5">
													    			 	<div class="floating-label2">
													    			 	<input class="floating-inputpcc" value="{!! old('startdate') !!}" type="date" name='startdate' placeholder=" ">
																		 @error('startdate')
																				<div class="alert alert-danger">{{ $message }}</div>
																			@enderror
													    				 </div>	
													    			 </div>

													    			 <div class="col-md-1 center1 label22">
													    				<div class="floating-label22 ">
													    					<p>TO</p>
													    				</div>
													    			</div>

							                               			  <div class="col-md-5">
							                                     		<div class="floating-label2">
							                                     			<input class="floating-inputpcc" value="{!! old('enddate') !!}" type="date" name='enddate' placeholder=" ">
																			 @error('enddate')
																				<div class="alert alert-danger">{{ $message }}</div>
																			@enderror
							                                     		</div>
							                               			 </div>	
							                               		</div>
															</div>

															<div class="col-md-6">
															    <div class="floating-label2">  
															    	<p>Event Country</p>
																	<select class="floating-selectpcc1" value="{!! old('country') !!}" name='country' >
																	@foreach($country as $ctr)
																		@if($ctr->id == 101)
																		<option value="{{ $ctr->id }}" selected>{{ $ctr->name }}</option>
																		@else
																		<option value='{{ $ctr->id }}'>{{ $ctr->name }}</option>
																		@endif
																	@endforeach	
																	</select>
																	<span class="highlight"></span>																
																</div>
													    	</div>

													    	<div class="col-md-6">
															    <div class="floating-label2">
															    	<p>Event state</p>
																	<select class="floating-selectpcc1" value="{!! old('state') !!}" name='state'>
																	@foreach($states as $s)
																		<option value='{{ $s->id }}'>{{ $s->name }}</option>
																	@endforeach																		
																	</select>
																	<span class="highlight"></span>
																</div>
													    	</div>


													    	<div class="col-md-6">
															    <div class="floating-label2">
															    	<p>Event City</p>  
																	<input class="floating-inputpcc" value="{!! old('city') !!}" type="text" name='city' placeholder=" ">
																	@error('city')
																		<div class="alert alert-danger">{{ $message }}</div>
																	@enderror
																</div>
													    	</div>

													    	 <div class="col-md-6">
							                                     <div class="floating-label2">
							                                        <p class="">Event Pincode</p>
							                                        <input class="floating-inputpcc" value="{!! old('pincode') !!}" type="text" name='pincode' placeholder=" ">
																	@error('pincode')
																		<div class="alert alert-danger">{{ $message }}</div>
																	@enderror
							                                        <span class="highlight"></span>
							                                     </div>
							                                </div>

							                                <div class="col-md-12">
													    		<div class="floating-label2">
													    			<p>Venue <img  src="{{ asset('public/images/gps_icon.png')}}"  height="17px;"> </p>      
															       	<input class="floating-inputpcc" value="{!! old('address') !!}" type="text" placeholder="ADDRESS " name='address' >
																	   @error('address')
																			<div class="alert alert-danger">{{ $message }}</div>
																		@enderror
															    </div>
													    	</div>

													    	<div class="col-md-6">
													    		<div class="floating-label2">
															    	<p>Ticket Price</p>
															    	<input class="floating-inputpcc" value="{!! old('ticketprice') !!}" type="number" name='ticketprice'>
																	@error('ticketprice')
																		<div class="alert alert-danger">{{ $message }}</div>
																	@enderror															  
															    </div>
													    	</div>

													    	<div class="col-md-6">
													    	 	<div class="floating-label2">  
															    	<p>Age Limit of the Audience</p> 
															    	<div class="row">
																	    <div class="col-md-6">
																	     	<input class="floating-inputpcc mb-16" value="{!! old('agemin') !!}" type="number" placeholder="MIN" type="number"  name='agemin' >
																			@error('agemin')
																				<div class="alert alert-danger">{{ $message }}</div>
																			@enderror
																		</div>		

																     	<div class="col-md-6"> 
																     		<input class="floating-inputpcc"  value="{!! old('agemax') !!}" placeholder="MAX" type="number" name='agemax' >
																			@error('agemax')
																				<div class="alert alert-danger">{{ $message }}</div>
																			@enderror
																		 </div>
																    </div>
															    </div>
													    	</div>

													    	<div class="col-md-12">
													    		<div class="floating-label2">
													    			<p>Description (MAX. 60 WORDS)</p>
															     	<input class="floating-inputpcc" value="{!! old('description') !!}" type="text" placeholder="" name='description'>
																	@error('description')
																		<div class="alert alert-danger">{{ $message }}</div>
																	@enderror
															     	<span class="highlight"></span>
															    </div>
													    	</div>

													    	<div class="col-md-12">
													    		<div class="floating-label2">
													    			<p>Event Facilities (MAX. 60 WORDS)</p>
															     	<input class="floating-inputpcc" value="{!! old('eventfacility') !!}" type="text"  name='eventfacility' placeholder="Please describe about the facilities provided to the members in the event">
																	@error('eventfacility')
																		<div class="alert alert-danger">{{ $message }}</div>
																	@enderror															     	
															     	<span class="highlight"></span>
															    </div>
													    	</div>




															<div class="col-md-12">
													    		<div class="floating-label2">
													    			<p>Contribution per ticket to plus circle (for the social cause) </p> 
													    			<input class="floating-inputpcc" value="{!! old('donatepcc') !!}" type="number" name="donatepcc" placeholder=" INR - (MIN 10)" min=10 max='100000' >
																	@error('donatepcc')
																		<div class="alert alert-danger">{{ $message }}</div>
																	@enderror
															      	<span class="highlight"></span>
															     </div>
													    	</div>

													   		 <div class="col-md-12">
													    		<div class="row">
													    			<div class="col-md-12">
													    				<div class="floating-label22">
													    				<p>Organiser Contact details <span>(to be display in event banner)</span></p> 
													    				</div>
													    			</div>
													    		
													    			<div class="col-md-6">
													    			 	<div class="floating-label2">													    			 	
													    			 	<input class="floating-inputpcc" value="{!! old('organisername') !!}" type="text" name="organisername" placeholder="Name ">
																		@error('organisername')
																			<div class="alert alert-danger">{{ $message }}</div>
																		@enderror																		
													    				 </div>	
													    			</div>

													    			 <div class="col-md-6">
													    				<div class="floating-label2">													    					
													    					<input class="floating-inputpcc" value="{!! old('organiseremail') !!}" type="Email"  name="organiseremail" placeholder=" Email">
																			@error('organisername')
																				<div class="alert alert-danger">{{ $message }}</div>
																			@enderror																		
													    				</div>
													    			</div>

							                               			  <div class="col-md-6">
							                                     		<div class="floating-label2">
							                                     			<input class="floating-inputpcc" value="{!! old('organiserphone') !!}" type="number"  name="organiserphone" placeholder="Primary Phone Number">
																			 @error('organiserphone')
																				<div class="alert alert-danger">{{ $message }}</div>
																			@enderror
																		
							                                     		</div>
							                               			 </div>	

							                               			  <div class="col-md-6">
							                                     		<div class="floating-label2">
							                                     			<input class="floating-inputpcc" value="{!! old('organisersphone') !!}" type="number" name="organisersphone" placeholder="Secondary Phone Number">
																			 @error('organisersphone')
																				<div class="alert alert-danger">{{ $message }}</div>
																			@enderror
																		
							                                     		</div>
							                               			 </div>	
							                               		</div>
															</div>

															<div class="col-md-12">
																<div class="floating-label2 ">
																	<p>Internal note to the management from the organizer : </p>
																	<input class="floating-inputpcc" value="{!! old('internalnote') !!}" type="text" name="internalnote"  placeholder="MAX 150 WORDS" >
																	@error('organisername')
																		<div class="alert alert-danger">{{ $message }}</div>
																	@enderror
																		
													    		</div>
																<!-- <div class="dashboard-note">      
															      <textarea  type="text" placeholder="MAX 150 WORDS"></textarea>
															    </div> -->
															</div>
														</div>
													</div>

													<div class="col-md-3 center1">
														<div class="row">
															<div class="col-md-12 center1">
																<div class="floating-label2">
							                                     	<p class="center1"><span>Image Preview</span></p>
							                                        <center>   <p class="uploadcss center1" for="file" ><img src="{{ asset('public/images/noimg.png') }}" id='event-image' alt="Image" /></p></center>
																	<input type='hidden' name='event_profile' value="{!! old('event_profile') !!}" id='event_profile' />
							                                     </div>
							                                 </div>
															<div class="col-md-12 center1">
																<div class="floating-label2">
																		<p class="center1"><span>Photo Upload ( .jpg /.png )</span></p>
																		<p id="imgFileUpload" class="floating-inputpcc"><i class="fa fa-upload"></i></p>
																			<input type="file"  name="event_image_upload" id="event-image-upload" accept="image/x-png,image/gif,image/jpeg"/> 
																			@error('event_image_upload')
																				<div class="alert alert-danger">{{ $message }}</div>
																			@enderror
							                                       </div>
							                                 </div>
							                             </div>
														<img src="{{ asset('public/images/man1.png')}}" class="imghost ">
					                                </div>
					                                <div class="col-md-12">
					                                	<div class="row">
					                                		<div class="col-md-12">
																<div class="floating-label2">
																	<p>
																		<input class="floating-inputcheck inputc2" {!! old("termsnconditions") == 'on' ? 'checked' : '' !!} name="termsnconditions" type="checkbox" >
														    			I accept the <a href="{{ route('terms_of_services') }}"  class="blue">Terms & Conditions</a>
																		@error('termsnconditions')
																			<div class="alert alert-danger">{{ $message }}</div>
																		@enderror	
																	</p>
																</div>
															</div>

															<div class="col-md-3 ml-130 center1">
													    		<div class="submit-btn">
																	<input type="submit" value="View My Post" class="volunteerpost" name="">
																</div>

															</div>

															<div class="col-md-2 resetcss center1" >
													    		<div class="submit-btn">
																	<input type="submit" value="Reset" class="volunteerreset" name="">
																</div>
													    	</div>

													    	<div class="col-md-2 submitcss center1" >
													    		<div class="submit-btn">
																	<input type="submit" value="Submit" class="volunteersubmit" name="">
																</div> 
													    	</div>
													    	
													    </div>
												    </div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
					</div>
				</div>
			</div>
		</section>

		<div class="modal fade" id="cropImagePop3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">
					</h4>
				</div>
				<div class="modal-body">
						<div id="upload-demo3" class="center-block"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="cropImageBtn3" class="btn btn-primary">Crop</button>
				</div>
			</div>
		</div>
	</div>

	
		@endsection
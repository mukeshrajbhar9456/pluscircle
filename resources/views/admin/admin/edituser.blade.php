@extends('layouts.admin')
	@section('content')
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
						<div class="row">
                            <div class="col-sm-6">
								<span>
									<h1>Edit User</h1>
								</span>
							</div>
							<div class="col-sm-6 text-right">
								<a href="{{ route('admin.sa') }}"><button class='btn btn-success'>Go Back</button></a>
							</div>
						</div>
						<form role="form" method="POST" action="{{ route('admin.UpdateUser') }}" name="loginForm" class="login-form">

                            <input type="hidden" name="userid" value="{{$edituser->id}}">

                  		{{ csrf_field() }}
                        <div class="row form">
                            <div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> Name:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='name' value="{{ $edituser->name }}" />
								</div>
							</div>
							<div class='col-sm-6 create-user'>
								<div class='col-sm-4'>
									<label> User type:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='usertype' value="{{ $edituser->membershiptype }}" />
								</div>
							</div>
							<div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> E-mail (Primary):</label>
								</div>
								<div class='col-sm-8 '>
									<input type='email' name='email' value="{{ $edituser->email }}" />
								</div>
							</div>
							{{-- <div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> E-mail (Secondary):</label>
								</div>
								<div class='col-sm-8 '>
									<input type='email' name='email' />
								</div>
							</div> --}}
							{{-- <div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> Phonecode:</label>
								</div>
								<div class='col-sm-8 '>
								<select name="phonecode" id="phonecode" >
									@foreach($countrycode as $cc)
										@if($cc->phonecode==91)
											<option value="{{ $cc->phonecode }}" selected>{{ $cc->phonecode }}</option>
										@else
											<option value="{{ $cc->phonecode }}">{{ $cc->phonecode }}</option>
										@endif
									@endforeach
								</select>
								</div>
							</div> --}}
							<div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> Phone:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='phone' value="{{ $edituser->phone }}"/>
								</div>
							</div>
							<div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> Secondary Phonecode:</label>
								</div>
								<div class='col-sm-8 '>
                                    <div class='col-sm-8 '>
                                        <input type='text' name='sphone' value="{{ $edituser->secondaryphone}}"/>
                                    </div>
								</div>
							</div>
							{{-- <div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> Secondary Phone:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='sphone' value="{{ $edituser->secondaryphone}}"/>
								</div>
							</div> --}}
							<div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> Referred By:</label>
								</div>
								<div class='col-sm-8 '>
									<input type='text' name='referance' value="{{ $edituser->refferedby }}"/>
								</div>
							</div>
							{{-- <div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> Payment:</label>
								</div>
								<div class='col-sm-8 '>
									<select name="usertype" >
										<option value="" >select</option>
										<option value="0" >waive the fee</option>
									</select>
								</div>
							</div> --}}
							<div class='col-sm-6 create-user'>
								<div class='col-sm-8 '>
									<input type="checkbox" checked data-plugin="switchery" data-color="#00b19d" data-size="small"/> Email auto-verified<BR>&nbsp;<BR>
								</div>
							</div>
							<div class='col-sm-6 create-user'>
								<div class='col-sm-4 '>
									<label> membershipid :</label>
								</div>

									<div class='col-sm-8 '>
                                        <input type='text' name='membership' value="{{ $edituser->tmembership}}" readonly/>
                                    </div>

							</div>
							<div class='col-sm-12 create-user text-center submit-button'>
								<button class='btn btn-success'> Edit User</button>
							</div>
						</div>

                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
        @endsection

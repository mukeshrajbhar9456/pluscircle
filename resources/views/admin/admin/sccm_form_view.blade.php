@extends('layouts.admin')
	@section('content')	
				<!-- main content start-->
				<div id="page-wrapper">
					<div class="main-page">
						<div class="tables">
							<h2 class="title1">Tables</h2>
							<div class="panel-body widget-shadow">
							<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
								<div class="form-title">
									<h4>SCCM Application</h4>
								</div>
								
								<div class="form-body">
									<form>
									@foreach($application as $a)
										<div class="col-md-12">
											<center>
												<div class="floating-label2">
													<img src="{{ asset($a->profile)}}" id='displayImage' name='displayImage' style='height:200px;width:200px'/>
												</div>
											</center>
										</div>
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Name</label> 
											<input type="email" class="form-control" value="{{ $a->name }}"> 
										</div>  
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">State</label> 
											<input type="email" class="form-control"  value="{{ $a->state }}"> 
										</div>
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Position</label> 
											<input type="email" class="form-control" value="{{ $a->position }}"> 
										</div>  
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Email address</label> 
											<input type="email" class="form-control"  value="{{ $a->email }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Phone Number</label> 
											<input type="email" class="form-control"  value="{{ $a->phone }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Date of Birth</label> 
											<input type="email" class="form-control"  value="{{ $a->dob }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Spouse Name</label> 
											<input type="email" class="form-control"  value="{{ $a->spousename }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Father Name</label> 
											<input type="email" class="form-control"  value="{{ $a->fathername }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Mother Name</label> 
											<input type="email" class="form-control"  value="{{ $a->mothername }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Secondary Phone</label> 
											<input type="email" class="form-control"  value="{{ $a->secondaryphone }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Address</label> 
											<input type="email" class="form-control"  value="{{ $a->address }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Country</label> 
											<input type="email" class="form-control"  value="{{ $a->country }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">State</label> 
											<input type="email" class="form-control"  value="{{ $a->state }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">City</label> 
											<input type="email" class="form-control"  value="{{ $a->city }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Pincode</label> 
											<input type="email" class="form-control"  value="{{ $a->pincode }}"> 
										</div> 
										<hr>
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Firm Address</label> 
											<input type="email" class="form-control"  value="{{ $a->firmaddress }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Firm City</label> 
											<input type="email" class="form-control"  value="{{ $a->firmcity }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Firm State</label> 
											<input type="email" class="form-control"  value="{{ $a->firmstate }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Firm Country</label> 
											<input type="email" class="form-control"  value="{{ $a->firmcountry }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Firm Pincode</label> 
											<input type="email" class="form-control"  value="{{ $a->firmpincode }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Firm Country</label> 
											<input type="email" class="form-control"  value="{{ $a->firmcountry }}"> 
										</div>
										<hr><hr> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Nationality</label> 
											<input type="email" class="form-control"  value="{{ $a->nationality }}"> 
										</div>
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Education</label> 
											<input type="email" class="form-control"  value="{{ $a->education }}"> 
										</div>
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Annual Income</label> 
											<input type="email" class="form-control"  value="{{ $a->annualincome }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">ID Proof Type</label> 
											<input type="email" class="form-control"  value="{{ $a->idprooftype }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">ID Proof Number</label> 
											<input type="email" class="form-control"  value="{{ $a->idproofnumber }}"> 
										</div> 
										<div class="form-group col-md-6"> 
											<label for="exampleInputEmail1">Profession</label> 
											<input type="email" class="form-control"  value="{{ $a->profession }}"> 
										</div> 
										
									@endforeach	
										
										
										
										
										


                
										 
									</form> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
				
		@endsection
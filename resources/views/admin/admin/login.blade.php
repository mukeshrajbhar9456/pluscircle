<!DOCTYPE html>
<html>
    
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="pluscircle club circle plus ">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
		<link rel="shortcut icon" href="{{ asset('public/admin/assets/images/favicon.ico')}}">


        <!-- App title -->
        <title>Plus Circle Club : Admin Panel</title>

        <!-- App CSS -->
		<link href="{{ asset('public/admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/css/core.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/css/components.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/css/pages.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/css/menu.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/admin/assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />
        
        
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="{{ asset('public/admin/assets/js/modernizr.min.js')}}"></script>
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="text-center">
                <a href="index-2.html" class="logo"><span>Admin<span>Panel</span></span></a>
                <h5 class="text-muted m-t-0 font-600">Plus Circle Club</h5>
            </div>
			
        	<div class="m-t-40 card-box">
                <div class="text-center">
                    <h4 class="text-uppercase font-bold m-b-0">Sign In</h4>
                </div>
                <div class="panel-body">
					<div class="col-md-12 center">
						@if (session()->has('message'))
							<div class="alert1 alert-info1">&#x2714;
								{{ session('message') }}
							</div>
						@endif
						@if (session()->has('error'))
							<div class="alert1 alert-danger1 ">&#x26A0;
								{{ session('error') }}
							</div>
						@endif					                            
					</div>
					<form role="form" method="POST" action="{{ route('admin.signin') }}" name="loginForm" class="login-form">
							   
                  	{{ csrf_field() }}
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="email" required="" name='admin_email' placeholder="Email">
                            </div>
                        </div><br><br>
                
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" required="" name='admin_password' placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <div class="checkbox checkbox-custom">
                                    <input id="checkbox-signup" type="checkbox">
                                    <label for="checkbox-signup">
                                        Remember me
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="form-group text-center m-t-30">
                            <div class="col-xs-12">
                                <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>

                        <div class="form-group m-t-30 m-b-0">
                            <div class="col-sm-12">
                                <a href="{{ route('admin.showForgotPasswordForm') }}" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- end card-box-->

           
            
        </div>
        <!-- end wrapper page -->
        

        
    	<script>
            var resizefunc = [];
        </script>

		<script src="{{ asset('public/admin/assets/js/jquery.min.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/detect.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/fastclick.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/jquery.slimscroll.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/jquery.blockUI.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/waves.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/wow.min.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/jquery.nicescroll.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/jquery.scrollTo.min.js')}}"></script>

        <script src="{{ asset('public/admin/assets/js/jquery.core.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/jquery.app.js')}}"></script>
		
	
	</body>

</html>
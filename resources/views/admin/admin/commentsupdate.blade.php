


@extends('layouts.admin')
@section('content')
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <span>
                                <h1>Comment Added</h1>
                            </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="card-box table-responsive">
                                <!-- <h4>Download</h4> -->
                                <a href="{{ route('admin.sa') }}"><button class='btn btn-success'>Go Back</button></a>
                                <!-- <button class="btn btn-success waves-effect waves-light m-b-5"> <i class="fa fa-file-excel-o m-r-5"></i> <span>Excel</span> </button>
                                <button class="btn btn-warning waves-effect waves-light m-b-5"> <i class="fa fa-file-pdf-o m-r-5"></i> <span>PDF</span> </button><BR>&nbsp; -->
                                <div class='text-center'>
                                    <h3 class="text-center text-success">PlusCircleClub</h3>
                                        <br/>
                                        <h2>{{ 'Super Admin' }}          {{ Carbon\Carbon::now() }} </h2>

                                        <hr />
                                        <h4><span>{{ $user->name }}</span>'s Comments</h4>

                                        @if(count($comments) > 0)
                                        @foreach($comments as $comment)
                                        <li>{{ $comment->comment }}             {{ $comment->date }}</li>
                                        @endforeach
                                        @else
                                        {{'no comments'}}
                                        @endif
                                </div>
                            <div>
                        </div>
                        <div class='row'>
                            <hr />
            <h4>Add comment</h4>
            <form method="post" action="{{route('comments.update')}}">
                @csrf
               <div class="form-group">
                    <textarea class="form-control" id="comment" name="comment"></textarea>
                    <input type="hidden" name="user_id" value="{{ $user->id }}" />
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success comment" value="Send Comment" />
                </div>
            </form>
                        </div>
                    </div>

                            </div>
                        </div><!-- end col -->
                    </div>
                    <!-- end row -->


                </div> <!-- container -->

            </div> <!-- content -->
    @push('scripts')


    @endpush



    @endsection


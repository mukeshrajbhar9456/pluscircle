@extends('layouts.admin')
	@section('content')	
				<!-- main content start-->
				<div id="page-wrapper">
					<div class="main-page">
						<div class="tables">
							<h2 class="title1">Tables</h2>
							<div class="panel-body widget-shadow">
								<h4>SCCM Applications</h4>
								<table id="sccm_data" class="table table-hover table-bordere" width="100%">
								<thead>
									<tr>
										<th class="th-sm">Name</th>
										<th class="th-sm">Position</th>
										<th class="th-sm">State</th>
										<th class="th-sm">email</th>
										<th class="th-sm">Phone</th>
										<th class="th-sm">Action</th>
									</tr>
								</thead>
								<tbody>
								@foreach($applications as $app)
									<tr>
									<td>{{ $app->name }}</td>
									<td>{{ $app->position }}</td>
									<td>{{ $app->state }}</td>
									<td>{{ $app->email }}</td>
									<td>{{ $app->phone }}</td>
									<td><a href="{{ route('admin.view_application_data',['id' =>\Crypt::encrypt($app->id)]) }}">View<a></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
				
		@endsection
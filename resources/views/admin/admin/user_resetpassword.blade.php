@extends('layouts.lp')
	@section('content')	

		<section class="login-section">
			<div class="container">

				<div class="row">
					<div class="col-md-12 text-center">
						@if (session()->has('message'))
							<div class="alert1 alert-info1">&#x2714;
								{{ session('message') }}
							</div>
						@endif
						@if (session()->has('error'))
							<div class="alert1 alert-danger1">&#x26A0;
								{{ session('error') }}
							</div>
						@endif
					</div>
					<div class="col-md-12">
						<div class="login-form">
							<div class="wizard">
				               <div class="wizard-inner">
				                  <div class="connecting-line"></div>
				                  <ul class="nav nav-tabs" role="tablist">
				                     <!-- <li role="presentation" class="active">
				                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" aria-expanded="true"><span class="round-tab">1 </span> <i>Step 1</i></a>
				                     </li>
				                     <li role="presentation" class="disabled">
				                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" aria-expanded="false"><span class="round-tab">2</span> <i>Step 2</i></a>
				                     </li> -->
				                  </ul>
				               </div>
				               <form role="form" method="POST" action="{{ route('admin.reset') }}" name="loginForm" class="login-form">
							   		{{ csrf_field() }}
							   		<div class="tab-content" id="main_form">
				                     <div class="tab-pane active" role="tabpanel" >
				                     	<div class="form-title">				                     	
				                     		<h4>Reset your Password</h4>
										 </div>


										 <!--<div class="col-md-12 center">-->
					      <!--                   <p class="accept1">Reset you password here </p>-->
					      <!--               </div>-->


										 
				                     	<div class="form-bg">
					                        <div class="floating-form">
											    <div class="floating-label">      
												  <input class="floating-input" type="text" name='email' value="{{ $userpkt->email }}" placeholder=" Enter mobile number or email id" readonly>
												  <span class="highlight"></span>
											   
												 </div>
											</div>
											<div class="floating-label">      
												<input class="floating-input @error('password') is-invalid @enderror"  name="password" type="password" id="password" placeholder="Enter password *">
												<span class="highlight"></span>									    	
												
												@error('password')
													<label id="password-error" class="error" for="email">{{ $message }}</label>
												@enderror
											</div>
											<div class="floating-label">      
												<input class="floating-input" type="password" name="password_confirmation" id="password-confirm" placeholder="Re-enter password * ">
												<span class="highlight"></span>
											
												
											</div>
											<div class="forgot-link">
												<a href="{{ route('admin.admin.login') }}">Admin Login</a>
											</div>
					                        <div class="submit-btn">
												<input type="submit" name="reset"  value="Reset Password">
											</div>
											<!--<div class="new-user">-->
											<!--	<a href="{{ route('user_register') }}">Join as a new member</a>-->
											<!--</div>-->
										</div>
									</div>
								</div>
							</form>
				                    
				                     
				            </div>
						</div>
					</div>
				</div>
			</div>
		</section>

		@endsection

		
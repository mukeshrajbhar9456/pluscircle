<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function()
// {
//     exec('composer dump-autoload -o');
//     echo 'composer dump-autoload complete';
// });
Route::get('/', function () {
    return view('users.welcome');
});

//  Route::get('/', ['uses' => 'App\Http\Controllers\LandingPageController@index']);
Route::get('About-us', ['as' => 'about_us', 'uses' => 'App\Http\Controllers\LandingPageController@about_us']);
Route::get('Enlarge-Your-Social-Circle-Globally', ['as' => 'about_us_1', 'uses' => 'App\Http\Controllers\LandingPageController@about_us_1']);
Route::get('Unlock-Your-Opportunity', ['as' => 'about_us_2', 'uses' => 'App\Http\Controllers\LandingPageController@about_us_2']);
Route::get('Empowering-Communities', ['as' => 'about_us_3', 'uses' => 'App\Http\Controllers\LandingPageController@about_us_3']);
Route::get('Social_Networking', ['as' => 'Social_Networking', 'uses' => 'App\Http\Controllers\LandingPageController@Social_Networking']);
Route::get('Business_Opportunity', ['as' => 'Business_Opportunity', 'uses' => 'App\Http\Controllers\LandingPageController@Business_Opportunity']);
Route::get('Emergency_Support', ['as' => 'Emergency_Support', 'uses' => 'App\Http\Controllers\LandingPageController@Emergency_Support']);
Route::get('Health_Insurance', ['as' => 'Health_Insurance', 'uses' => 'App\Http\Controllers\LandingPageController@Health_Insurance']);
Route::get('Opportunities_professionals', ['as' => 'Opportunities_professionals', 'uses' => 'App\Http\Controllers\LandingPageController@Opportunities_professionals']);
Route::get('Scholarships_Programs', ['as' => 'Scholarships_Programs', 'uses' => 'App\Http\Controllers\LandingPageController@Scholarships_Programs']);
Route::get('Supporting_Hand', ['as' => 'Supporting_Hand', 'uses' => 'App\Http\Controllers\LandingPageController@Supporting_Hand']);
Route::get('Why_pluscircle', ['as' => 'why_pluscircle', 'uses' => 'App\Http\Controllers\LandingPageController@why_pluscircle']);
Route::get('privacy_policy', ['as' => 'privacy_policy', 'uses' => 'App\Http\Controllers\LandingPageController@privacy_policy']);
Route::get('terms_of_use', ['as' => 'terms_of_use', 'uses' => 'App\Http\Controllers\LandingPageController@terms_of_use']);
Route::get('event_activites', ['as' => 'event_activites', 'uses' => 'App\Http\Controllers\LandingPageController@event_activites']);
Route::get('terms_of_services', ['as' => 'terms_of_services', 'uses' => 'App\Http\Controllers\LandingPageController@terms_of_services']);
// Route::get('event_activites_1', ['as' => 'event_activites_1', 'uses' => 'App\Http\Controllers\LandingPageController@event_activites_1']);
Route::get('our-vision', ['as' => 'our-vision', 'uses' => 'App\Http\Controllers\LandingPageController@our_vision']);
Route::get('our-mission', ['as' => 'our-mission', 'uses' => 'App\Http\Controllers\LandingPageController@our_mission']);
Route::get('what-we-do', ['as' => 'what-we-do', 'uses' => 'App\Http\Controllers\LandingPageController@what_we_do']);
Route::get('how-we-do', ['as' => 'how-we-do', 'uses' => 'App\Http\Controllers\LandingPageController@how_we_do']);
Route::get('events', ['as' => 'events', 'uses' => 'App\Http\Controllers\LandingPageController@events']);
Route::get('activities', ['as' => 'activities', 'uses' => 'App\Http\Controllers\LandingPageController@activities']);
Route::get('volunteer_c', ['as' => 'volunteer_c', 'uses' => 'App\Http\Controllers\LandingPageController@volunteer_c']);
Route::get('contact_us', ['as' => 'contact_us', 'uses' => 'App\Http\Controllers\LandingPageController@contact_us']);
Route::get('blog', ['as' => 'blog', 'uses' => 'App\Http\Controllers\LandingPageController@blog']);
Route::get('return_refund_policy', ['as' => 'return_refund_policy', 'uses' => 'App\Http\Controllers\LandingPageController@return_refund_policy']);
Route::get('press', ['as' => 'press', 'uses' => 'App\Http\Controllers\LandingPageController@press']);
Route::get('video', ['as' => 'video', 'uses' => 'App\Http\Controllers\LandingPageController@video']);
Route::get('gallery', ['as' => 'gallery', 'uses' => 'App\Http\Controllers\LandingPageController@gallery']);

Route::get('event_activites_two/{event}', ['as' => 'event_activites_two', 'uses' => 'App\Http\Controllers\LandingPageController@event_activites_two']);
Route::get('event_activites_one/{event}', ['as' => 'event_activites_one', 'uses' => 'App\Http\Controllers\LandingPageController@event_activites_one']);
Route::get('event_activites_four/{event}', ['as' => 'event_activites_four', 'uses' => 'App\Http\Controllers\LandingPageController@event_activites_four']);
Route::get('event_activites_three/{event}', ['as' => 'event_activites_three', 'uses' => 'App\Http\Controllers\LandingPageController@event_activites_three']);
Route::get('event_activites_five/{event}', ['as' => 'event_activites_five', 'uses' => 'App\Http\Controllers\LandingPageController@event_activites_five']);
Route::get('event_activites_six/{event}', ['as' => 'event_activites_six', 'uses' => 'App\Http\Controllers\LandingPageController@event_activites_six']);
Route::post('savecontactus', ['as' => 'save.contactus', 'uses' => 'App\Http\Controllers\LandingPageController@saveContactus']);

Route::get('application-password', ['as' => 'application_sccm_password', 'uses' => 'App\Http\Controllers\LandingPageController@application_sccm_password']);
Route::get('application_sccm', ['as' => 'application_sccm', 'uses' => 'App\Http\Controllers\LandingPageController@application_sccm']);
Route::post('sccm_password', ['as' => 'sccm_password', 'uses' => 'App\Http\Controllers\LandingPageController@sccm_password']);
Route::get('appgetstate/{country_id}', ['as' => 'appgetstate', 'uses' => 'App\Http\Controllers\LandingPageController@getstate']);
Route::post('sccm_application_submit', ['as' => 'sccm_application_submit', 'uses' => 'App\Http\Controllers\LandingPageController@sccm_application_submit']);
Route::get('application_sccm_success', ['as' => 'application_sccm_success', 'uses' => 'App\Http\Controllers\LandingPageController@application_sccm_success']);
    

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return '<h1>Cache facade value cleared</h1>';
    Artisan::call('cache:clear');
   Artisan::call('config:clear');
   Artisan::call('config:cache');
   Artisan::call('view:clear');

  return "Cleared!";
});


Route::post('login_user_check', ['as' => 'login_user_check', 'uses' => 'App\Http\Controllers\UserController@login_user_check']);
Route::get('user_login', ['as' => 'user_login', 'uses' => 'App\Http\Controllers\UserController@user_login']);
Route::get('user_logout', ['as' => 'user_logout', 'uses' => 'App\Http\Controllers\UserController@user_logout']);
Route::post('signin', ['as' => 'signin', 'uses' => 'App\Http\Controllers\UserController@signin']);
Route::get('user_password', ['as' => 'user_password', 'uses' => 'App\Http\Controllers\UserController@user_password']);
Route::get('user_register', ['as' => 'user_register', 'uses' => 'App\Http\Controllers\UserController@user_register']);
Route::post('register', ['as' => 'register', 'uses' => 'App\Http\Controllers\UserController@register']);
// Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('status', 'verification-link-sent');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');


Route::get('verify', ['as' => 'verification.verify', 'uses' => 'App\Http\Controllers\UserController@verify']);
Route::get('forgotpassword', ['as' => 'showForgotPasswordForm', 'uses' => 'App\Http\Controllers\UserController@showForgotPasswordForm']);
Route::post('sendResetLink', ['as' => 'sendResetLink', 'uses' => 'App\Http\Controllers\UserController@sendResetLink']);
Route::get('password_reset', ['as' => 'password.reset', 'uses' => 'App\Http\Controllers\UserController@password_reset']);
Route::post('reset', ['as' => 'reset', 'uses' => 'App\Http\Controllers\UserController@reset']);
Route::get('resendEmailVerify', ['as' => 'resendEmailVerify', 'uses' => 'App\Http\Controllers\UserController@resendEmailVerify']);
Route::post('resendemailverificationlink', ['as' => 'resendemailverificationlink', 'uses' => 'App\Http\Controllers\UserController@resendemailverificationlink']);

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
       
    // Auth::routes();
    Route::group(['middleware' => 'auth:web'], function () {
        Route::get('dashboard', ['as' => 'user.dashboard', 'uses' => 'App\Http\Controllers\GuestController@dashboard']);
        Route::post('memberkit/ajax', ['as' => 'user.memberkit', 'uses' => 'App\Http\Controllers\GuestController@memberkit']);
        Route::get('getstate/{country_id}', ['as' => 'user.getstate', 'uses' => 'App\Http\Controllers\GuestController@getstate']);
        Route::post('saveaddress', ['as' => 'user.saveaddress', 'uses' => 'App\Http\Controllers\GuestController@saveaddress']);
        Route::get('user_profile', ['as' => 'user.user_profile', 'uses' => 'App\Http\Controllers\GuestController@user_profile']);
        Route::post('user/update_user_profile', ['as' => 'user.update_user_profile', 'uses' => 'App\Http\Controllers\GuestController@update_user_profile']);
        

        Route::get('user/upcoming_events', ['as' => 'user.upcoming_events', 'uses' => 'App\Http\Controllers\GuestController@upcoming_events']);
        Route::get('user/volunteer', ['as' => 'user.volunteer', 'uses' => 'App\Http\Controllers\GuestController@volunteer']);
        Route::post('user/update_volunteer', ['as' => 'user.update_volunteer', 'uses' => 'App\Http\Controllers\GuestController@update_volunteer']);
    
        Route::get('user/my_referrences', ['as' => 'user.my_referrences', 'uses' => 'App\Http\Controllers\GuestController@my_referrences']);
        Route::get('user/my_contribution', ['as' => 'user.my_contribution', 'uses' => 'App\Http\Controllers\GuestController@my_contribution']);
        Route::get('user/circle_news', ['as' => 'user.circle_news', 'uses' => 'App\Http\Controllers\GuestController@circle_news']);
        Route::get('user/circle_heroes', ['as' => 'user.circle_heroes', 'uses' => 'App\Http\Controllers\GuestController@circle_heroes']);
        Route::post('uploadprofile_ajax', ['as' => 'uploadprofile_ajax', 'uses' => 'App\Http\Controllers\GuestController@uploadprofile_ajax']);
        Route::post('user/create_member_reference', ['as' => 'user.create_member_reference', 'uses' => 'App\Http\Controllers\GuestController@create_member_reference']);
        Route::get('i-wish-to-host-an-event', ['as' => 'user.i_wish_to_host_an_event', 'uses' => 'App\Http\Controllers\GuestController@i_wish_to_host_an_event']);
        Route::post('save_event', ['as' => 'user.save_event', 'uses' => 'App\Http\Controllers\GuestController@save_event']);
        Route::get('upcoming-events', ['as' => 'user.upcoming_events', 'uses' => 'App\Http\Controllers\GuestController@upcoming_events']);
        Route::post('ajaxgetevent', ['as' => 'user.ajaxgetevent', 'uses' => 'App\Http\Controllers\GuestController@ajaxgetevent']);
        Route::post('event_search_ajax', ['as' => 'user.event_search_ajax', 'uses' => 'App\Http\Controllers\GuestController@event_search_ajax']);
        
        
        /***routes defined by suman */
        Route::post('user/create_member_reference', ['as' => 'user.create_member_reference', 'uses' => 'App\Http\Controllers\GuestController@create_member_reference']);
        /** end of routes defined by suman */
    });
        /** Admin Routes */
            
    Route::get('master', ['as' => 'admin.admin.login', 'uses' => 'App\Http\Controllers\AdminUserController@index']);
    Route::post('adminsignin', ['as' => 'admin.signin', 'uses' => 'App\Http\Controllers\AdminUserController@login']);
    // Forgotpassword admin
    // Route::any('forgot-password', ["as" => "admin.adminForgotPassword", "uses" => "App\Http\Controllers\AdminUserController@forgotPassword"]);
    // Route::any('resetlink', ["as" => "admin.adminResetLink", "uses" => "App\Http\Controllers\AdminUserController@resetlink"]);
    
    Route::get('forgotpassword1', ['as' => 'admin.showForgotPasswordForm', 'uses' => 'App\Http\Controllers\AdminUserController@showForgotPasswordForm']);
    Route::post('sendResetLink1', ['as' => 'admin.sendResetLink', 'uses' => 'App\Http\Controllers\AdminUserController@sendResetLink']);
    Route::get('password_reset1', ['as' => 'password.reset', 'uses' => 'App\Http\Controllers\AdminUserController@password_reset']);
    Route::post('reset1', ['as' => 'admin.reset', 'uses' => 'App\Http\Controllers\AdminUserController@reset']);
    // Route::get('resendEmailVerify', ['as' => 'admin.resendEmailVerify', 'uses' => 'App\Http\Controllers\AdminUserController@resendEmailVerify']);
    // Route::post('resendemailverificationlink', ['as' => 'admin.resendemailverificationlink', 'uses' => 'App\Http\Controllers\AdminUserController@resendemailverificationlink']);
    
    Route::get('admin/allmembers', ['as' => 'admin.admin.allmembers', 'uses' => 'App\Http\Controllers\AdminDashboardController@dashboard']);
    Route::get('createNewUser', ['as' => 'admin.createNewUser', 'uses' => 'App\Http\Controllers\AdminDashboardController@createNewUser']);
    Route::post('search_members', ['as' => 'search_members', 'uses' => 'App\Http\Controllers\AdminDashboardController@searchMembers']);
      Route::get('edituser', ['as' => 'admin.edituser', 'uses' => 'App\Http\Controllers\AdminDashboardController@edituser']);
      Route::any('deleteuser', ['as' => 'admin.deleteuser', 'uses' => 'App\Http\Controllers\AdminDashboardController@deleteuser']);
      Route::any('viewuser', ['as' => 'admin.viewuser', 'uses' => 'App\Http\Controllers\AdminDashboardController@viewuser']);
      Route::any('saveNewUser', ['as' => 'admin.saveNewUser', 'uses' => 'App\Http\Controllers\AdminDashboardController@saveNewUser']);
      Route::any('UpdateUser', ['as' => 'admin.UpdateUser', 'uses' => 'App\Http\Controllers\AdminDashboardController@updateuser']);
      Route::any('viewcomment', ['as' => 'admin.viewcomment', 'uses' => 'App\Http\Controllers\AdminDashboardController@viewcomment']);
      Route::any('approval', ['as' => 'admin.approval', 'uses' => 'App\Http\Controllers\AdminDashboardController@approvalmember']);

      Route::get('/admin_logout', ["as" => "admin_logout", "uses" => "App\Http\Controllers\AdminUserController@logout"]);
       Route::any('sa', ['as' => 'admin.sa', 'uses' => 'App\Http\Controllers\AdminDashboardController@application']);
       Route::any('application', ['as' => 'sccm.application', 'uses' => 'App\Http\Controllers\AdminController@sccm_applications_view']);
    
   
    
    Route::post('api/fetch-states', [App\Http\Controllers\AdminDashboardController::class, 'fetchState']);
    Route::post('api/fetch-cities', [App\Http\Controllers\AdminDashboardController::class, 'fetchCity']);
    /*** payment gateway */
    Route::any('/payment', ['as' => 'payment', 'uses' => 'App\Http\Controllers\PaymentController@payment']);
    